package com.helloc.constant;

public class ActivityConstants {

	public static final String SHARE_PREFERENCES_NAME = "Helloc";
	public static final String SHARE_PREFS_ITEM_APP_VERSION = "app_version";
	public static final String SHARE_PREFS_ITEM_LOGIN_AS_GUEST = "login_as_guest";
	public static final String SHARE_PREFS_ITEM_IS_LOGIN = "is_login";
	public static final String SHARE_PREFS_ITEM_USER_ID = "user_id";
	public static final String SHARE_PREFS_ITEM_LOGIN_ACCOUNT = "login_account";
	public static final String SHARE_PREFS_ITEM_UUID = "uuid";
	public static final String SHARE_PREFS_ITEM_PHONE = "phone";
	public static final String SHARE_PREFS_ITEM_EMAIL = "email";
	public static final String SHARE_PREFS_ITEM_NAME = "nickname";
	public static final String SHARE_PREFS_ITEM_ACCOUNT = "account";
	public static final String SHARE_PREFS_ITEM_DEVICE_ID = "registrationId";
	public static final String SHARE_PREFS_ITEM_NEW_INVITATION = "new_invitation";
	public static final String SHARE_PREFS_ITEM_NEW_FRIEND = "new_friend";
	public static final String SHARE_PREFS_ITEM_NOTIFICATIONS_SOUND = "notifications_sound";
	public static final String SHARE_PREFS_ITEM_NOTIFICATIONS_VIBRATE = "notifications_vibrate";
	public static final String SHARE_PREFS_ITEM_REFRESH_CONTACTS = "refresh_contacts";
	public static final String SHARE_PREFS_ITEM_NOT_FIRST_TO_SEND_MESSAGE = "not_first_to_send_message";
	public static final String SHARE_PREFS_ITEM_IS_REGISTER_TO_WECHAT = "is_register_to_wechat";
	public static final String SHARE_PREFS_ITEM_NOT_SHOW_REGISTER_TO_WECHAT_HINT = "not_show_register_to_wechat_hint";
	public static final String SHARE_PREFS_ITEM_NOT_SHOW_SHARE_TO_OTHER_APP_HINT = "not_show_share_to_other_app_hint";
	public static final String SHARE_PREFS_ITEM_NOT_SHOW_USER_GUIDE_1 = "not_show_user_guide_1";
	public static final String SHARE_PREFS_ITEM_NOT_SHOW_USER_GUIDE_2 = "not_show_user_guide_2";
	public static final String SHARE_PREFS_ITEM_NEW_FEATURE_ODD = "new_feature_odd";
	public static final String SHARE_PREFS_ITEM_NEW_FEATURE_EVEN = "new_feature_even";
	public static final String SHARE_PREFS_ITEM_LANGUAGE = "language";
	
	public static final String ARGS_ACTIVITY_SOURCE = "ARGS_ACTIVITY_SOURCE";
	public static final int ARGS_ACTIVITY_SEND_MESSAGE = 1;
	public static final int ARGS_ACTIVITY_SELF_GEOFENCE = 2;
	public static final int ARGS_ACTIVITY_PARTY = 3;
	public static final int ARGS_ACTIVITY_MAIN = 4;
	public static final int ARGS_ACTIVITY_RECEIVE_MESSAGE = 5;

	public static final String ARGS_MESSAGE_ID = "ARGS_MESSAGE_ID";
	public static final String ARGS_MESSAGE_DISPLAY_NAME = "ARGS_MESSAGE_DISPLAY_NAME";
	public static final String ARGS_MESSAGE_ADDRESS = "ARGS_MESSAGE_ADDRESS";
	public static final String ARGS_MESSAGE_TYPE = "ARGS_MESSAGE_TYPE";
	public static final String ARGS_MESSAGE_PRIVATE_MSG = "ARGS_MESSAGE_PRIVATE_MSG";
	public static final String ARGS_MESSAGE_PUBLIC_MSG = "ARGS_MESSAGE_PUBLIC_MSG";
	public static final String ARGS_MESSAGE_STATUS = "ARGS_MESSAGE_STATUS";
	public static final String ARGS_MESSAGE_EXPIRATION_TIME = "ARGS_MESSAGE_EXPIRATION_TIME";
	public static final String ARGS_MESSAGE_LATITUDE = "ARGS_MESSAGE_LATITUDE";
	public static final String ARGS_MESSAGE_LONGITUDE = "ARGS_MESSAGE_LONGITUDE";
	public static final String ARGS_MESSAGE_RADIUS = "ARGS_MESSAGE_RADIUS";
	public static final String ARGS_MESSAGE_IMAGE_NAME = "ARGS_MESSAGE_IMAGE_NAME";
	public static final String ARGS_MESSAGE_NEED_FEEDBACK = "ARGS_MESSAGE_NEED_FEEDBACK";

	public static final String ARGS_ACCOUNT_USER_ID = "ARGS_ACCOUNT_USER_ID";
	public static final String ARGS_ACCOUNT_UUID = "ARGS_ACCOUNT_UUID";
	public static final String ARGS_ACCOUNT_NICKNAME = "ARGS_ACCOUNT_NICKNAME";
	public static final String ARGS_ACCOUNT_PHONE = "ARGS_ACCOUNT_PHONE";
	public static final String ARGS_ACCOUNT_AVATAR = "ARGS_ACCOUNT_AVATAR";

	public static final String ARGS_INVITATION_MSG = "ARGS_INVITATION_MSG";
	public static final String ARGS_INVITATION_STATUS = "ARGS_INVITATION_STATUS";
	public static final String ARGS_INVITATION_STATUS_MSG = "ARGS_INVITATION_STATUS_MSG";

	public static final String ARGS_PARTY_ID = "ARGS_PARTY_ID";
	public static final String ARGS_PARTY_THEME = "ARGS_PARTY_THEME";
	public static final String ARGS_PARTY_DESCRIPTION = "ARGS_PARTY_DESCRIPTION";
	public static final String ARGS_PARTY_START_DATE = "ARGS_PARTY_START_DATE";
	public static final String ARGS_PARTY_START_TIME = "ARGS_PARTY_START_TIME";
	public static final String ARGS_PARTY_END_DATE = "ARGS_PARTY_END_DATE";
	public static final String ARGS_PARTY_END_TIME = "ARGS_PARTY_END_TIME";
	public static final String ARGS_PARTY_ADDRESS = "ARGS_PARTY_ADDRESS";
	public static final String ARGS_PARTY_LATITUDE = "ARGS_PARTY_LATITUDE";
	public static final String ARGS_PARTY_LONGITUDE = "ARGS_PARTY_LONGITUDE";
	public static final String ARGS_PARTY_ORIGINATOR = "ARGS_PARTY_ORIGINATOR";
	public static final String ARGS_PARTY_STATUS = "ARGS_PARTY_STATUS";

	public static final int REQUEST_GO_TO_LOGIN_ACTIVITY = 100;
	public static final int REQUEST_GO_TO_LOGOUT_ACTIVITY = 101;
	public static final int REQUEST_GO_T0_SETTING_ACTIVITY = 102;
	public static final int REQUEST_GO_TO_SELECT_FRIENDS_ACTIVITY = 103;
	public static final int REQUEST_GO_TO_DISPLAY_IMAGE_ACTIVITY = 104;
	public static final int REQUSET_GO_TO_GOOLEMAP_ACTIVITY = 105;
	public static final int REQUEST_GO_TO_RECORD_AUDIO_ACTIVITY = 106;
	public static final int REQUEST_GO_TO_REGISTER_ACTIVITY = 107;
	public static final int REQUEST_GO_TO_RECEIVE_PARTY_ACTIVITY = 108;
	public static final int REQUEST_GO_TO_PROFILE_NAME_ACTIVITY = 109;
	public static final int REQUEST_GO_TO_REPLY_MESSAGE_ACTIVITY = 110;

	public static final int RESULT_RESULT_OK = 200;
	public static final int RESULT_REGISTER_CANCLE = 201;
	public static final int RESULT_REGISTER_SUCCEED = 202;
	public static final int RESULT_LOGOUT = 203;
	public static final int RESULT_LOGIN_CANCLE = 204;
	public static final int RESULT_UPDATE_UI = 205;
}
