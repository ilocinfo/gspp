package com.helloc.constant;

public class StatusConstants {
	public static final int PARTY_STATUS_ONGOING = 0;
	public static final int PARTY_STATUS_OVER = 1;
	
	public static final String MESSAGE_STATUS = "MESSAGE_STATUS";
	public static final int MESSAGE_STATUS_SELF = 0;
	public static final int MESSAGE_STATUS_SENT = 1;
	public static final int MESSAGE_STATUS_RECEIVED_NOT_ACTIVED = 2;
	public static final int MESSAGE_STATUS_RECEIVED_ACTIVIED = 3;
	public static final int MESSAGE_STATUS_SELF_ACTIVED = 4;
	
	public static final int INVITATION_STATUS_RECOMMEND = -1;
	public static final int INVITATION_STATUS_SENT = 0;
	public static final int INVITATION_STATUS_RECEIVED_NOT_HANDLE = 1;
	public static final int INVITATION_STATUS_RECEIVED_ACCEPTED = 2;
	
	public static final int AUTO_REPLY_STATUS_NOT_ALLOWED = 0;
	public static final int AUTO_REPLY_STATUS_PENDING = 1;
	public static final int AUTO_REPLY_STATUS_ALLOWED = 2;
}
