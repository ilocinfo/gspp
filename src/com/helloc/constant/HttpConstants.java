package com.helloc.constant;

public class HttpConstants {

	public static final int CONNECT_TIMEOUT = 5000;
	//public static final String SERVER_URL = "http://158.182.111.133:8080/HellocServerSSH/";
	public static final String SERVER_URL = "http://158.182.9.118:8080/HellocServerSSH/";

	public static final String HTTP_POST_RESPONSE_URL_NULL = "Url Null";
	public static final String HTTP_POST_RESPONSE_EXCEPTION = "���ӷ�����ʧ��";
	
	public static final String LOGIN_SUCCESS = "LOGIN SUCCEED";
	public static final String LOGIN_FAILED_NO_SUCH_USER = "LOGIN FAILED, NO SUCH USER";

	public static final String REGISTER_SECOND_SUCCEED = "SECOND REGISTER SUCCEED";
	public static final String REGISTER_SUCCEED = "REGISTER SUCCEED";
	public static final String REGISTER_EMAIL_EXIST = "REGISTER EMAIL EXIST";
	public static final String REGISTER_PHONE_EXIST = "REGISTER EMAIL EXIST";
	public static final String REGISTER_UUID_EXIST = "REGISTER UUID EXIST";

	public static final String UPDATE_ILLEGAL_COLUMN = "UPDATE ILLEGAL COLUMN";
	public static final String UPDATE_SUCCESS = "UPDATE SUCCEED";

	public static final String SAVE_MESSAGE_ON_SERVER_SUCCESS = "SAVE MESSAGE ON SERVER SUCCEED";
	public static final String SAVE_MESSAGE_ON_SERVER_FAIL = "SAVE MESSAGE ON SERVER FAIL";

	public static final String SEND_MESSAGE_SUCCESS = "SEND MESSAGE SUCCEED";
	public static final String SEND_MESSAGE_FAIL = "SEND MESSAGE FAIL";

	public static final String DELETE_MESSAGE_SUCCESS = "DELETE MESSAGE SUCCEED";
	public static final String DELETE_MESSAGE_FAIL = "DELETE MESSAGE FAIL";

	public static final String SEND_INVITATION_SUCCESS = "SEND INVITATION SUCCEED";
	public static final String SEND_INVITATION_FAIL = "SEND INVITATION FAIL";

	public static final String SEND_REPLY_INVITATION_SUCCESS = "SEND REPLY INVITATION SUCCEED";
	public static final String SEND_REPLY_INVITATION_FAIL = "SEND REPLY INVITATION FAIL";

	public static final String SAVE_PARTY_ON_SERVER_SUCCESS = "SAVE PARTY ON SERVER SUCCEED";
	public static final String SAVE_PARTY_ON_SERVER_FAIL = "SAVE PARTY ON SERVER FAIL";

	public static final String SEND_PARTY_SUCCESS = "SEND PARTY SUCCEED";
	public static final String SEND_PARTY_FAIL = "SEND PARTY FAIL";

	public static final String UPDATE_PARTYING_SUCCESS = "UPDATE_PARTYING SUCCEED";
	public static final String UPDATE_PARTYING_FAIL = "UPDATE_PARTYING FAIL";

	public static final String UPLOAD_AVATAR_SUCCESS = "UPLOAD AVATAR SUCCEED";
	public static final String UPLOAD_AVATAR_FAIL = "UPLOAD AVATAR FAIL";

	public static final String GET_AVATAR_SUCCESS = "GET AVATAR SUCCEED";
	public static final String GET_AVATAR_FAIL = "GET AVATAR FAIL";

	public static final String GET_FRIEND_AVATAR_SUCCESS = "GET FRIEND AVATAR SUCCEED";
	public static final String GET_FRIEND_AVATAR_FAIL = "GET FRIEND AVATAR FAIL";

	public static final String SEARCH_FRIENDS_SUCCESS = "SEARCH FRIENDS SUCCEED";
	public static final String SEARCH_FRIENDS_FAIL = "SEARCH FRIENDS FAIL";

	public static final String UPLOAD_USERS_RELATIONSHIP_SUCCESS = "UPLOAD USERS RELATIONSHIP SUCCEED";
	public static final String UPLOAD_USERS_RELATIONSHIP_FAIL = "UPLOAD USERS RELATIONSHIP FAIL";

	public static final String GET_FRIENDS_LIST_SUCCESS = "GET FRIENDS LIST SUCCEED";
	public static final String GET_FRIENDS_LIST_FAIL = "GET FRIENDS LIST FAIL";

	public static final String UPLOAD_AUDIO_FILE_SUCCESS = "UPLOAD AUDIO FILE SUCCEED";
	public static final String UPLOAD_AUDIO_FILE_FAIL = "UPLOAD AUDIO FILE FAIL";

	public static final String GET_AUDIO_FILE_SUCCESS = "GET AUDIO FILE SUCCEED";
	public static final String GET_AUDIO_FILE_FAIL = "GET AUDIO FILE FAIL";

	public static final String QUIT_PARTY_SUCCESS = "QUIT PARTY SUCCEED";
	public static final String QUIT_PARTY_FAIL = "QUIT PARTY FAIL";

	public static final String UPLOAD_SNAPSHOT_SUCCESS = "UPLOAD SNAPSHOT SUCCEED";
	public static final String UPLOAD_SNAPSHOT_FAIL = "UPLOAD SNAPSHOT FAIL";

	public static final String DOWNLOAD_SNAPSHOT_SUCCESS = "DOWNLOAD SNAPSHOT SUCCEED";
	public static final String DOWNLOAD_SNAPSHOT_FAIL = "DOWNLOAD SNAPSHOT FAIL";

	public static final String UPLOAD_IMAGE_SUCCESS = "UPLOAD IMAGE SUCCEED";
	public static final String UPLOAD_IMAGE_FAIL = "UPLOAD IMAGE FAIL";

	public static final String DOWNLOAD_THUMBNAIL_SUCCESS = "DOWNLOAD THUMBNAIL SUCCEED";
	public static final String DOWNLOAD_THUMBNAIL_FAIL = "DOWNLOAD THUMBNAIL FAIL";
	
	public static final String DOWNLOAD_IMAGE_SUCCESS = "DOWNLOAD IMAGE SUCCEED";
	public static final String DOWNLOAD_IMAGE_FAIL = "DOWNLOAD IMAGE FAIL";
	
	public static final String UPLOAD_CONTACT_LIST_SUCCESS = "UPLOAD CONTACT LIST SUCCESS";
	public static final String UPLOAD_CONTACT_LIST_FAIL = "UPLOAD CONTACT LIST FAIL";
	
	public static final String UPDATE_PASSWORD_OLD_PASSWORD_INCORRECT = "OLD PASSWORD INCORRECT";
	public static final String UPDATE_PASSWORD_SUCCESS = "UPDATE PASSWORD SUCCESS";
	
	public static final String SEND_GLOBAL_MESSAGE_SUCCESS = "SEND GLOBAL MESSAGE SUCCESS";
	public static final String SEND_GLOBAL_MESSAGE_FAIL = "SEND GLOBAL MESSAGE FAIL";
	
	public static final String SEND_AUTOREPLY_REQUEST_SUCCESS = "SEND AUTOREPLY REQUEST SUCCESS";
	public static final String SEND_AUTOREPLY_REQUEST_FAIL = "SEND AUTOREPLY REQUEST FAIL";

	public static final String SEND_AUTOREPLY_PERMISSION_SUCCESS = "SEND AUTOREPLY PERMISSION SUCCESS";
	public static final String SEND_AUTOREPLY_PERMISSION_FAIL = "SEND AUTOREPLY PERMISSION FAIL";
	
	public static final String SEND_AUTOREPLY_MESSAGE_SUCCESS = "SEND AUTOREPLY MESSAGE SUCCESS";
	public static final String SEND_AUTOREPLY_MESSAGE_FAIL = "SEND AUTOREPLY MESSAGE FAIL";

	public static final String REMOVE_AUTOREPLY_PERMISSION_SUCCESS = "REMOVE AUTOREPLY PERMISSION SUCCESS";
	public static final String REMOVE_AUTOREPLY_PERMISSION_FAIL = "REMOVE AUTOREPLY PERMISSION FAIL";

	public static final String DELETE_FRIEND_SUCCESS = "DELETE FRIEND SUCCESS";
	public static final String DELETE_FRIEND_FAIL = "DELETE FRIEND FAIL";
	
	public static final String UPDATE_REPLIES_SUCCESS = "UPDATE REPLIES SUCCESS";
	public static final String UPDATE_REPLIES_EMPTY = "UPDATE REPLIES EMPTY";

	public static final String SEND_REPLIES_SUCCESS = "SEND REPLIES SUCCESS";
	public static final String SEND_REPLIES_EMPTY = "SEND REPLIES EMPTY";

	public static final String GET_FRIEND_NICKNAME_SUCCESS = "GET FRIEND NICKNAME SUCCESS";
	public static final String GET_FRIEND_NICKNAME_FAIL = "GET FRIEND NICKNAME FAIL";
}
