package com.helloc.constant;

public class GCMConstants {

	public static final String GCM_HTTPURL = "https://android.googleapis.com/gcm/send";
	public static final String GCM_APIKEY = "AIzaSyBJ3Ch-Pf0kmjdeAiyIFbDt0PYWdKhrmbY";
	
	public static final String GCM_COMMAND = "GCM_COMMAND";
	public static final String GCM_COMMAND_RECEIVE_MESSAGE = "GCM_COMMAND_SEND_MESSAGE";
	public static final String GCM_COMMAND_DELETE_MESSAGE = "GCM_COMMAND_DELETE_MESSAGE";
	public static final String GCM_COMMAND_RECEIVE_INVITATION = "GCM_COMMAND_SEND_INVITATION";
	public static final String GCM_COMMAND_RECEIVE_REPLY_INVITATION = "GCM_COMMAND_SEND_REPLY_INVITATION";
	public static final String GCM_COMMAND_RECEIVE_PARTY = "GCM_COMMAND_SEND_PARTY";
	public static final String GCM_COMMAND_RECEIVE_AUTOREPLY_REQUEST = "GCM_COMMAND_SEND_AUTOREPLY_REQUEST";
	public static final String GCM_COMMAND_RECEIVE_AUTOREPLY_PERMISSION = "GCM_COMMAND_SEND_AUTOREPLY_PERMISSION";
	public static final String GCM_COMMAND_REMOVE_AUTOREPLY_PERMISSION = "GCM_COMMAND_REMOVE_AUTOREPLY_PERMISSION";
	public static final String GCM_COMMAND_RECEIVE_AUTOREPLY_MESSAGE = "GCM_COMMAND_SEND_AUTOREPLY_MESSAGE";
	public static final String GCM_COMMAND_DELETE_FRIEND = "GCM_COMMAND_DELETE_FRIEND";
}
