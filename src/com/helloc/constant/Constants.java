package com.helloc.constant;

import android.os.Environment;


public class Constants {
	public static final String SENDER_ID = "862564212582";
	
	public static final String WX_APP_ID = "wx9514d2efe67b6aa3";
	//public static final String WX_APP_ID = "wx100e3dc30f7eb920";
	
	public static final String NEW_VERSION_NEED_LOGIN = "20,";
	
	public static final String DEBUG_TAG = "DEBUG_TAG";
	
	public static class ReceiveMsgActivity {
		public static final String Title = "receivemsg_title";
		public static final String ExtInfo = "receivemsg_extinfo";
	}
	
	public static final String REGULAR = "f0[0-9]{2}|f10[0-7]";// 正则表达式，用来判断消息内是否有表情
	
	public static final float MAP_ZOOM = 14;
	
	public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	public static final String DB_NAME = "helloc.db";
    public static final String DB_PASSWORD = "peg2013";
    public static final int DB_VERSION = 13;
	
	public static final String HELLOC_FOLDER = Environment.getExternalStorageDirectory() + "/helloc/";
	public static final String AUDIO_FLODER = HELLOC_FOLDER + "audio/";
	public static final String THUMBNAIL_FLODER = HELLOC_FOLDER + "thumbnail/";
	public static final String IMAGE_FLODER = HELLOC_FOLDER + "image/";
	public static final String SNAPSHOT_FLODER = HELLOC_FOLDER + "snapshot/";
	
	public static final String INVOKE_APP = "invoke_app";
	
	public static final int MESSAGE_TYPE_TEXT = 0;
	public static final int MESSAGE_TYPE_AUDIO = 1;
	
	public static final String ADD_IMAGE = "ADD_IMAGE";
	public static final String CURRENT_ADDRESS = "MAP_LOCATION";
	public static final String MAP_SEARCH = "MAP_SEARCH";
	public static final String RADIUS_CIRCLE = "RADIUS_CIRCLE";
	
	public static final int SEND_TO_WECHAT_NULL = 0; //不发送到微信  
	public static final int SEND_TO_WECHAT_FRIEND = -1; // 发送到微信好友
	public static final int SEND_TO_WECHAT_MOMENT = -2; //发送到微信朋友圈
	public static final int SEND_TO_OTHER_APP = -3;
	
	public static final int RADIUS_BASE = 300;
    public static final int RANGE_BIG_GAP = 1000;
    public static final int RANGE_MEDIUM_GAP = 500;
    public static final int RANGE_SMALL_GAP = 50;
    
    public static final int LANGUAGE_AUTO = 0;
    public static final int LANGUAGE_SIMPLIFIED = 1;
    public static final int LANGUAGE_TRADITIONAL = 2;
    public static final int LANGUAGE_ENGLISH = 3;
    
    public static final int MULTICAST_SIZE = 1000;
    
}
