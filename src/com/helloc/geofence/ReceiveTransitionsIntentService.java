package com.helloc.geofence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.helloc.R;
import com.helloc.activity.ShowMessageActivity;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.database.DBMessage;
import com.helloc.database.DBMessageReply;
import com.helloc.database.DBParty;
import com.helloc.database.DBPartying;
import com.helloc.model.Message;
import com.helloc.model.MessageReply;
import com.helloc.model.Party;
import com.helloc.model.Partying;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.NotificationUtils;
import com.helloc.utils.SharePrefsUtils;

/**
 * This class receives geofence transition events from Location Services, in the
 * form of an Intent containing the transition type and geofence id(s) that
 * triggered the event.
 */
public class ReceiveTransitionsIntentService extends IntentService {

	/**
	 * Sets an identifier for this class' background thread
	 */
	public ReceiveTransitionsIntentService() {
		super("ReceiveTransitionsIntentService");
	}

	/**
	 * Handles incoming intents
	 * 
	 * @param intent
	 *            The Intent sent by Location Services. This Intent is provided
	 *            to Location Services (inside a PendingIntent) when you call
	 *            addGeofences()
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		// Create a local broadcast Intent
		Intent broadcastIntent = new Intent();

		// Give it the category for all intents sent by the Intent Service
		broadcastIntent
				.addCategory(GeofenceConstants.CATEGORY_LOCATION_SERVICES);

		// First check for errors
		if (LocationClient.hasError(intent)) {

			// Get the error code
			int errorCode = LocationClient.getErrorCode(intent);

			// Get the error message
			String errorMessage = LocationServiceErrorMessages.getErrorString(
					this, errorCode);

			// Log the error
			Log.e(GeofenceConstants.APPTAG,
					getString(R.string.geofence_transition_error_detail,
							errorMessage));

			// Set the action and error message for the broadcast intent
			broadcastIntent.setAction(GeofenceConstants.ACTION_GEOFENCE_ERROR)
					.putExtra(GeofenceConstants.EXTRA_GEOFENCE_STATUS,
							errorMessage);

			// Broadcast the error *locally* to other components in this app
			LocalBroadcastManager.getInstance(this).sendBroadcast(
					broadcastIntent);

			// If there's no error, get the transition type and create a
			// notification
		} else {
			// check is login or not
			if(SharePrefsUtils.isLogin(this) == false) {
				return;
			}
					
			// Get the type of transition (entry or exit)
			int transition = LocationClient.getGeofenceTransition(intent);

			// Test that a valid transition was reported
			if ((transition == Geofence.GEOFENCE_TRANSITION_ENTER)
					|| (transition == Geofence.GEOFENCE_TRANSITION_EXIT)) {
				List<Geofence> geofences = LocationClient
						.getTriggeringGeofences(intent);
				// get the message geofence and party geofence number
				int messageGeofenceCounter = 0;
				int partyGeofenceCounter = 0;
				for (int index = 0; index < geofences.size(); index++) {
					// the id contains the string '$' is party geofence
					if (geofences.get(index).getRequestId().contains("$")) {
						partyGeofenceCounter++;
					} else {
						messageGeofenceCounter++;
					}
				}

				// get the message geofence and party geofence id
				String[] messageGeofenceIds = null;
				String[] partyGeofenceIds = null;
				if (messageGeofenceCounter > 0) {
					messageGeofenceIds = new String[messageGeofenceCounter];
				}
				if (partyGeofenceCounter > 0) {
					partyGeofenceIds = new String[partyGeofenceCounter];
				}
				messageGeofenceCounter = 0;
				partyGeofenceCounter = 0;
				for (int index = 0; index < geofences.size(); index++) {
					if (geofences.get(index).getRequestId().contains("$")) {
						partyGeofenceIds[partyGeofenceCounter] = geofences.get(
								index).getRequestId();
						partyGeofenceCounter++;
					} else {
						messageGeofenceIds[messageGeofenceCounter] = geofences
								.get(index).getRequestId();
						messageGeofenceCounter++;
					}
				}

				handleMessageGeofence(messageGeofenceIds);
				handlePartyGeofece(transition, partyGeofenceIds);
			} else {
				// Always log as an error
				Log.e(GeofenceConstants.APPTAG,
						getString(R.string.geofence_transition_invalid_type,
								transition));
			}
		}
	}

	private void handleMessageGeofence(String[] geofenceIds) {
		if (geofenceIds != null) {
			// Post a notification
			setNotification(geofenceIds);
			// send auto reply if need
			sendAutoReplyIfAllowed(geofenceIds);
			// change the geofence status
			activateMessageStatus(geofenceIds);
			// remove activated geofence
			GeofenceUtils.removeGeofences(this, geofenceIds);

			Log.d(GeofenceConstants.APPTAG,
					getString(R.string.geofence_transition_notification_text));

			// send a broadcast to update MenuActivity's UI
			Log.d(GeofenceConstants.APPTAG, "broadcast to update Menu UI");
			Intent broadcast = new Intent();
			broadcast.setAction("updateUI");
			getApplication().sendBroadcast(broadcast);
		}

	}

	private void handlePartyGeofece(int transition, String[] geofenceIds) {
		if (geofenceIds != null) {
			// get the smallest interval for each party
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			for (int i = 0; i < geofenceIds.length; i++) {
				String geofenceId = geofenceIds[i];
				int interval = Integer.parseInt(geofenceId.substring(geofenceId
						.length() - 1));
				if (transition == Geofence.GEOFENCE_TRANSITION_EXIT) {
					interval++;
				}

				String partyId = geofenceId.substring(0,
						geofenceId.length() - 2);
				if (map.get(partyId) != null) {
					interval = interval < map.get(partyId) ? interval : map
							.get(partyId);
				}
				map.put(partyId, interval);
			}

			String[] keys = new String[map.keySet().size()];
			map.keySet().toArray(keys);

			for (int i = 0; i < keys.length; i++) {
				final String partyId = keys[i];
				final int interval = map.get(keys[i]);
				// System.out.println("partyId = " + partyId);
				// System.out.println("interval = " + interval);
				Party party = DBParty.getPartyById(this, partyId);
				if (party != null) {
					long currentTime = System.currentTimeMillis();
					// long startTime = Long.parseLong(party.getStartTime());
					long endTime = Long.parseLong(party.getEndTime());
					// update partying
					Partying partying = new Partying();
					partying.setPartyId(partyId);
					partying.setUserId(SharePrefsUtils.getUserId(this));
					partying.setNickName(SharePrefsUtils.getNickname(this));
					partying.setInterval(interval);
					DBPartying.insert(this, partying);

					if (currentTime <= endTime) {
						new UploadIntervalTask(partyId, interval).run();
					} else {
						// change the party status to over
						DBParty.updateStatus(this, partyId);
						// remove the geofences
						String[] ids = new String[4];
						ids[0] = partyId + "$" + 1;
						ids[1] = partyId + "$" + 2;
						ids[2] = partyId + "$" + 3;
						ids[3] = partyId + "$" + 4;
						GeofenceUtils.removeGeofences(this, ids);
					}
				} else {
					String[] ids = new String[4];
					ids[0] = partyId + "$" + 1;
					ids[1] = partyId + "$" + 2;
					ids[2] = partyId + "$" + 3;
					ids[3] = partyId + "$" + 4;
					GeofenceUtils.removeGeofences(this, ids);
				}

			}
		}
	}

	private class UploadIntervalTask implements Runnable {

		String partyId;
		int interval;

		public UploadIntervalTask(String partyId, int interval) {
			this.partyId = partyId;
			this.interval = interval;
		}

		@Override
		public void run() {
			// Moves the current Thread into the background
			android.os.Process
					.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
			Map<String, String> map = new HashMap<String, String>();
			map.put("deviceGeneratedPartyId", partyId);
			map.put("userId",
					SharePrefsUtils.getUserId(getApplicationContext()));
			map.put("interval", String.valueOf(interval));
			String result = HttpRequestUtils.post("updateSelfPartying", map);

			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage.equals(HttpConstants.UPDATE_PARTYING_SUCCESS)) {
				} else {
					System.out
							.println("updatePartying ---->> " + returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("updatePartying ---->> " + e.getMessage());
			}
		}

	}

	/**
	 * Posts a notification in the notification bar when a transition is
	 * detected. If the user clicks the notification, control goes to the main
	 * Activity.
	 * 
	 * @param transitionType
	 *            The type of transition that occurred.
	 * 
	 */
	private void setNotification(String[] geofenceIds) {
		// Create an explicit content Intent that starts the main Activity
		Intent notificationIntent = new Intent(getApplicationContext(),
				ShowMessageActivity.class);
		// get the not activated geofence message from database
		Message message = DBMessage.getNotActivatedById(
				getApplicationContext(), geofenceIds[0]);
		if (message == null) {
			return;
		}

		notificationIntent.putExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID,
				message.getSenderId());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_LATITUDE,
				message.getLatitude());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_LONGITUDE,
				message.getLongitude());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_RADIUS,
				message.getRadius());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_ID,
				geofenceIds[0]);
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_STATUS,
				message.getStatus());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_ADDRESS,
				message.getAddress());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_TYPE,
				message.getType());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG,
				message.getPrivateMsg());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG,
				message.getPublicMsg());
		notificationIntent.putExtra(ActivityConstants.ARGS_MESSAGE_IMAGE_NAME,
				message.getImageName());

		String title;
		String text;
		// Only one geofence triggered
		if (geofenceIds.length == 1) {
			int type = message.getType();
			String privateMsg = "";
			if (type == Constants.MESSAGE_TYPE_TEXT) {
				privateMsg = message.getPrivateMsg();
				privateMsg = privateMsg.replaceAll(Constants.REGULAR,
						getString(R.string.text_emoticon));
			} else if (type == Constants.MESSAGE_TYPE_AUDIO) {
				privateMsg = getString(R.string.text_audio);
			}

			if (message.getStatus() == StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED) {
				title = "From: "
						+ DBFriend.getNameByUserId(getApplicationContext(),
								message.getSenderId());
				text = getString(R.string.text_message) + privateMsg;
			} else {
				title = getString(R.string.action_ring_me_around);
				text = getString(R.string.text_message) + privateMsg;
			}

			NotificationUtils.sendNotification(notificationIntent,
					getApplicationContext(), ShowMessageActivity.class, title,
					text, true);

			// TODO More than one geofence
		} else {

		}
	}

	private void sendAutoReplyIfAllowed(String[] geofenceIds) {
		for (int i = 0; i < geofenceIds.length; i++) {
			final Message message = DBMessage.getNotActivatedById(
					getApplicationContext(), geofenceIds[0]);
			if (message.isNeedFeedback()) {
				if (DBFriend.getAllowAutoReply(this, message.getSenderId()) == StatusConstants.AUTO_REPLY_STATUS_ALLOWED) {
					new AsyncTask<Void, Void, String>() {

						@Override
						protected String doInBackground(Void... params) {
							Map<String, String> map = new HashMap<String, String>();
							map.put("senderId", SharePrefsUtils
									.getUserId(getApplicationContext()));
							map.put("receiverId", message.getSenderId());
							map.put("deviceGeneratedMessageId", message.getId());
							return HttpRequestUtils.post(
									"sendAutoreplyMessage", map);
						}

						@Override
						protected void onPostExecute(String result) {
							try {
								JSONObject json = new JSONObject(result);
								String returnMessage = json
										.getString("returnMessage");
								if (returnMessage
										.equals(HttpConstants.SEND_AUTOREPLY_MESSAGE_SUCCESS)) {
									MessageReply messageReply = new MessageReply();
									messageReply.setOrder(0);
									messageReply.setMessageId(message.getId());
									messageReply
											.setUserId(SharePrefsUtils
													.getUserId(getApplicationContext()));
									messageReply
											.setUserName(getString(R.string.text_myself));
									messageReply
											.setReplyContent(getString(R.string.text_received_auto_reply));
									messageReply.setDate(String.valueOf(System
											.currentTimeMillis()));
									messageReply.setMyReply(true);
									DBMessageReply.insert(
											getApplicationContext(),
											messageReply);
								} else {
									System.out
											.println("sendAutoreplyMessage ---->> "
													+ returnMessage);
								}
							} catch (JSONException e) {
								System.out
										.println("sendAutoreplyMessage ---->> "
												+ e.getMessage());
							}
						}

					}.execute();
				}
			}
		}
	}

	/**
	 * change the message status to activated
	 */
	private void activateMessageStatus(String[] ids) {
		for (int i = 0; i < ids.length; i++) {
			DBMessage.updateStatusById(getApplicationContext(), ids[i]);
		}
	}

}
