package com.helloc.view;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.NetworkUtils;

public class ImageViewItem {
	private final int status_uploading = 0;
	private final int status_fail = 1;
	private int status;

	private Context context;
	private ViewGroup imageViewItem;
	private ImageView image;
	private ProgressBar prograssBar;
	private ImageView iconFail;
	private Button delete;
	private String imagePath;
	private String imageName;
	private boolean flag; // whether the delete button is visiable
	private ImageViewItemCallback callback;
	private ImageViewItem instance;
	private int index;
	private boolean isUploaded;
	private boolean needUpload;
	private UploadImageTask uploadImageTask;

	public interface ImageViewItemCallback {
		public void deleteImage(ImageViewItem instance);
	}

	public void setCallback(ImageViewItemCallback callback) {
		this.callback = callback;
	}

	public ImageViewItem(Context context, ViewGroup mContainerView,
			String imageName, final int index, boolean needUpload) {
		this.context = context;
		this.imageName = imageName;
		this.index = index;
		this.needUpload = needUpload;
		instance = this;

		imageViewItem = (ViewGroup) LayoutInflater.from(context).inflate(
				R.layout.item_image_upload, mContainerView, false);

		image = (ImageView) imageViewItem.findViewById(R.id.image);
		prograssBar = (ProgressBar) imageViewItem
				.findViewById(R.id.progressBar);
		delete = (Button) imageViewItem.findViewById(R.id.delete);
		iconFail = (ImageView) imageViewItem.findViewById(R.id.icon_fail);

		imagePath = Constants.THUMBNAIL_FLODER + imageName + ".jpg";
		image.setImageBitmap(BitmapFactory.decodeFile(imagePath));

		imageViewItem.findViewById(R.id.rl).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (status == status_fail) {
							uploadImage();
							return;
						}
						if (flag) {
							delete.setVisibility(View.INVISIBLE);
							flag = false;
						} else {
							delete.setVisibility(View.VISIBLE);
							flag = true;
						}
					}
				});

		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				callback.deleteImage(instance);
			}
		});

		uploadImage();
	}

	private void uploadImage() {
		if (needUpload) {
			if (NetworkUtils.isConnect(context) == false) {
				iconFail.setVisibility(View.VISIBLE);
			} else {
				uploadImageTask = new UploadImageTask();
				uploadImageTask.execute();
			}
		}

	}

	private class UploadImageTask extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			status = status_uploading;
			iconFail.setVisibility(View.INVISIBLE);
			prograssBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(Void... params) {
			imagePath = Constants.IMAGE_FLODER + imageName + ".jpg";
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("imageName", imageName);
			map.put("image", ImageUtils.encodeBitmapToString(imagePath));
			return HttpRequestUtils.post("uploadImage", map);
		}

		@Override
		protected void onPostExecute(String result) {
			prograssBar.setVisibility(View.INVISIBLE);
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if(returnMessage.equals(HttpConstants.UPLOAD_IMAGE_SUCCESS)) {
					isUploaded = true;
				} else {
					System.out.println("uploadImage ---->> " + returnMessage);
					status = status_fail;
					iconFail.setVisibility(View.VISIBLE);
				}
			} catch (JSONException e) {
				System.out.println("uploadImage ---->> " + e.getMessage());
				status = status_fail;
				iconFail.setVisibility(View.VISIBLE);
			}
		}

	}

	public ViewGroup getInstance() {
		return imageViewItem;
	}

	public boolean isUploaded() {
		return isUploaded;
	}

	public int getIndex() {
		return index;
	}

}
