package com.helloc.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class OnOffScrollView extends ScrollView {
	
	private boolean on = true;

	public OnOffScrollView(Context context) {
		super(context);
	}

	public OnOffScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public OnOffScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	// turn on the scroll view
	public void enable() {
		on = true;
	}

	// turn off the scroll view
	public void disable() {
		on = false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (on) {
			return super.onInterceptTouchEvent(ev);
		} else {
			return false;
		}
	}
}
