package com.helloc.view;

import com.helloc.R;
import com.helloc.database.DBAvatar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class TableCellView {
	private Context context;
	private ViewGroup cellViewItem;
	private ImageView image;
	private TextView tvName;
	private String userId;
	private String name;

	public TableCellView(Context context, ViewGroup mContainerView,
			String userId, String name) {
		this.context = context;
		this.userId = userId;
		this.name = name;

		setUpView(mContainerView);
	}

	private void setUpView(ViewGroup mContainerView) {
		cellViewItem = (ViewGroup) LayoutInflater.from(context).inflate(
				R.layout.item_table_cell, mContainerView, false);

		image = (ImageView) cellViewItem.findViewById(R.id.image);
		tvName = (TextView) cellViewItem.findViewById(R.id.name);

		// set the avatar
		byte[] photo = DBAvatar.getAvatar(context, userId);
		if (photo != null) {
			Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
					photo.length);
			image.setImageBitmap(avatar);
		}
		tvName.setText(name);
	}

	public ViewGroup getInstance() {
		return cellViewItem;
	}
}
