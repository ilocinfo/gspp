package com.helloc.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.database.DBAvatar;
import com.helloc.database.DBFriend;

public class AvatarView {
	private Context context;
	private ViewGroup avatarViewItem;
	private ImageView image;
	private TextView name;
	private Button delete;
	private String userId;
	private boolean flag; // whether the delete button is visiable
	private AvatarViewCallback callback;
	private AvatarView instance;
	private int avatartType; // 0: user 1:wechat 2:other app
	private int wxType;

	public interface AvatarViewCallback {
		public void deleteAvatar(AvatarView instance);

		public void resetWxType(AvatarView instance);
		
		public void deleteOtherApp(AvatarView instance);
	}

	public void setCallback(AvatarViewCallback callback) {
		this.callback = callback;
	}

	// constructor for friend's avatar
	public AvatarView(Context context, ViewGroup mContainerView, String userId) {
		avatartType = 0;
		this.context = context;
		this.userId = userId;
		instance = this;

		setUpView(mContainerView);
	}

	// constructor for wechat icon
	public AvatarView(Context context, ViewGroup mContainerView, int wxType) {
		avatartType = 1;
		this.context = context;
		this.wxType = wxType;
		instance = this;

		setUpView(mContainerView);
	}

	// constructor for other app
	public AvatarView(Context context, ViewGroup mContainerView) {
		avatartType = 2;
		this.context = context;
		instance = this;

		setUpView(mContainerView);
	}

	private void setUpView(ViewGroup mContainerView) {
		avatarViewItem = (ViewGroup) LayoutInflater.from(context).inflate(
				R.layout.item_avatar, mContainerView, false);

		image = (ImageView) avatarViewItem.findViewById(R.id.image);
		name = (TextView) avatarViewItem.findViewById(R.id.name);
		delete = (Button) avatarViewItem.findViewById(R.id.delete);

		switch (avatartType) {
		case 0:
			// set the avatar
			byte[] photo = DBAvatar.getAvatar(context, userId);
			if (photo != null) {
				Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
						photo.length);
				image.setImageBitmap(avatar);
			}
			name.setText(DBFriend.getNameByUserId(context, userId));
			break;
		case 1:
			// set wechat icon
			switch (wxType) {
			case Constants.SEND_TO_WECHAT_FRIEND:
				name.setText(context.getString(R.string.text_wechat_friend));
				image.setImageResource(R.drawable.icon_wx);
				break;
			case Constants.SEND_TO_WECHAT_MOMENT:
				name.setText(context.getString(R.string.text_wechat_moment));
				image.setImageResource(R.drawable.icon_moment);
				break;
			}
			break;
		case 2:
			// set other app icon
			name.setText(context.getString(R.string.text_other_app));
			image.setImageResource(R.drawable.icon_whatsapp);
			break;
		}

		avatarViewItem.findViewById(R.id.rl).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (flag) {
							delete.setVisibility(View.INVISIBLE);
							flag = false;
						} else {
							delete.setVisibility(View.VISIBLE);
							flag = true;
						}

					}

				});

		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (avatartType) {
				case 0:
					callback.deleteAvatar(instance);
					break;
				case 1:
					callback.resetWxType(instance);
					break;
				case 2:
					callback.deleteOtherApp(instance);
					break;
				}
			}
		});
	}

	public ViewGroup getInstance() {
		return avatarViewItem;
	}

	public String getUserId() {
		return userId;
	}

}
