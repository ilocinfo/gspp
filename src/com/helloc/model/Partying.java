package com.helloc.model;

/**
 * This the ongoing party
 *  @param interval 1: 0-100m, 2: 100-200m, 3: 200m-1km 4: 1km-5km 5:>5km
 */
public class Partying {
	private String partyId;
	private String userId;
	private String nickName;
	private int interval;
	
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	
	
}
