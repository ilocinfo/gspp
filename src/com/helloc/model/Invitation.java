package com.helloc.model;

public class Invitation {
	private String userId;
	private String uuid;
	private String nickName;
	private String phone;
	private String invitationMsg;
	// 0:send 1:receive and to be handle 2: receive and accept
	private int status;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getInvitationMsg() {
		return invitationMsg;
	}

	public void setInvitationMsg(String invitationMsg) {
		this.invitationMsg = invitationMsg;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
