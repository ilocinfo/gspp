package com.helloc.model;

public class MessageReply {
	private int id;
	private int order;
	private String messageId;
	private String userId;
	private String userName;
	private String replyContent;
	private String date;
	private boolean isMyReply;
	private int messageType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isMyReply() {
		return isMyReply;
	}

	public void setMyReply(boolean isMyReply) {
		this.isMyReply = isMyReply;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	@Override
	public String toString() {
		return "MessageReply [id=" + id + ", order=" + order + ", messageId="
				+ messageId + ", userId=" + userId + ", userName=" + userName
				+ ", replyContent=" + replyContent + ", date=" + date
				+ ", isMyReply=" + isMyReply + ", messageType=" + messageType
				+ "]";
	}

}
