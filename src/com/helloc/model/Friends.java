package com.helloc.model;

import com.helloc.constant.StatusConstants;

public class Friends {
	private String userId;
	private String uuid;
	private String name;
	private String phone;
	private int allowAutoReply; // 0: not allow 1: pending 2: allowed
	private int autoReplyPermission; // 0: not allow 1: pending 2: allowed

	public Friends() {

	}

	public Friends(String userId, String name, String phone) {
		this.userId = userId;
		this.name = name;
		this.phone = phone;
	}

	public Friends(String userId, String uuid, String name, String phone) {
		this.userId = userId;
		this.uuid = uuid;
		this.name = name;
		this.phone = phone;
		this.allowAutoReply = 0;
		this.autoReplyPermission = 0;
	}

	public Friends(String userId, String uuid, String name, String phone,
			int allowAutoReply, int autoReplyPermission) {
		this.userId = userId;
		this.uuid = uuid;
		this.name = name;
		this.phone = phone;
		this.allowAutoReply = allowAutoReply;
		this.autoReplyPermission = autoReplyPermission;
	}

	public Friends(String userId, String uuid, String name, String phone,
			String allowAutoReply, String autoReplyPermission) {
		this.userId = userId;
		this.uuid = uuid;
		this.name = name;
		this.phone = phone;
		if (allowAutoReply.equals("Y")) {
			this.allowAutoReply = StatusConstants.AUTO_REPLY_STATUS_ALLOWED;
		} else {
			this.allowAutoReply = StatusConstants.AUTO_REPLY_STATUS_NOT_ALLOWED;
		}
		if (autoReplyPermission.equals("Y")) {
			this.autoReplyPermission = StatusConstants.AUTO_REPLY_STATUS_ALLOWED;
		} else {
			this.autoReplyPermission = StatusConstants.AUTO_REPLY_STATUS_NOT_ALLOWED;
		}
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getAllowAutoReply() {
		return allowAutoReply;
	}

	public void setAllowAutoReply(int allowAutoReply) {
		this.allowAutoReply = allowAutoReply;
	}

	public int getAutoReplyPermission() {
		return autoReplyPermission;
	}

	public void setAutoReplyPermission(int autoReplyPermission) {
		this.autoReplyPermission = autoReplyPermission;
	}

	@Override
	public String toString() {
		return "Friends [userId=" + userId + ", uuid=" + uuid + ", name="
				+ name + ", phone=" + phone + ", allowAutoReply="
				+ allowAutoReply + ", autoReplyPermission="
				+ autoReplyPermission + "]";
	}

}
