package com.helloc.model;

import com.google.android.gms.location.Geofence;

public class Message {
	private String id; // The id for message, also the Geofence's request ID
	private double latitude; // Latitude of the Geofence's center
	private double longitude; // Longitude of the Geofence's center
	private float radius; // Radius of the geofence circle, in meters
	private long expirationTime; // Geofence expiration time in milliseconds
	private int transitionType; // Type of Geofence transition. Include enter and exit
	private int status; // The message status. 0: set for self, 1: send out, 2: received but not activated, 3: received and activated
	private int type; // The message type. 0: text, 1: audio
	private String address; // The address for message
	private String privateMsg; // Private message,
	private String publicMsg; // Public message,
	private String senderId; // Sender's user id
	private String receiverId; // Receiver's user id
	private String imageName;
	private boolean needFeedback; // Whether the sender need feedback when receiver get the message

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public long getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(long expirationTime) {
		this.expirationTime = expirationTime;
	}

	public int getTransitionType() {
		return transitionType;
	}

	public void setTransitionType(int transitionType) {
		this.transitionType = transitionType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPrivateMsg() {
		return privateMsg;
	}

	public void setPrivateMsg(String privateMsg) {
		this.privateMsg = privateMsg;
	}

	public String getPublicMsg() {
		return publicMsg;
	}

	public void setPublicMsg(String publicMsg) {
		this.publicMsg = publicMsg;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public boolean isNeedFeedback() {
		return needFeedback;
	}

	public void setNeedFeedback(boolean needFeedback) {
		this.needFeedback = needFeedback;
	}

	/**
	 * Creates a Location Services Geofence object
	 * 
	 * @return A Geofence object
	 */
	public Geofence getGeofence() {
		if(expirationTime - System.currentTimeMillis() > 0) {
			// Build a new Geofence object
			return new Geofence.Builder()
					.setRequestId(id)
					.setTransitionTypes(transitionType)
					.setCircularRegion(latitude, longitude, radius)
					.setExpirationDuration(
							expirationTime - System.currentTimeMillis()).build();
		} else {
			return null;
		}
		
	}
}
