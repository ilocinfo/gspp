package com.helloc.activity;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.fragment.MessageDetailFragment;

public class ReplyMessageActivity extends SherlockFragmentActivity {
	private String senderId;
	private String messageId;
	private int messageStatus;
	private int type;
	private String privateMsg;
	private String publicMsg;

	private MessageDetailFragment messageDetailFrag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reply_message);
		setTitle(getString(R.string.action_reply));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		senderId = getIntent().getStringExtra(
				ActivityConstants.ARGS_ACCOUNT_USER_ID);
		messageId = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_ID);
		messageStatus = getIntent().getIntExtra(
				ActivityConstants.ARGS_MESSAGE_STATUS, 0);
		type = getIntent().getIntExtra(ActivityConstants.ARGS_MESSAGE_TYPE,
				Constants.MESSAGE_TYPE_TEXT);
		privateMsg = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG);
		publicMsg = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG);

		setUpView();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpView() {
		messageDetailFrag = new MessageDetailFragment();
		Bundle bundle = new Bundle();
		bundle.putString(ActivityConstants.ARGS_ACCOUNT_USER_ID, senderId);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		bundle.putInt(ActivityConstants.ARGS_MESSAGE_STATUS, messageStatus);
		bundle.putInt(ActivityConstants.ARGS_MESSAGE_TYPE, type);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG, privateMsg);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG, publicMsg);
		messageDetailFrag.setArguments(bundle);
		getSupportFragmentManager().beginTransaction()
				.add(R.id.messageItem, messageDetailFrag).commit();

	}
}
