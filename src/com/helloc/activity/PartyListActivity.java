package com.helloc.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.TabHost;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.adapter.TabsAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBParty;
import com.helloc.fragment.PartyListFragment;
import com.helloc.model.Party;
import com.helloc.utils.GeofenceUtils;

public class PartyListActivity extends SherlockFragmentActivity {
	TabHost mTabHost;
	ViewPager mViewPager;
	TabsAdapter mTabsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_message_list);
		setTitle(getString(R.string.action_party));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		updatePartyStatus();
		setUpTab();

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
	}

	private void updatePartyStatus() {
		ArrayList<Party> list = DBParty.getAllByStatus(this,
				StatusConstants.PARTY_STATUS_ONGOING);
		long currentTime = System.currentTimeMillis();
		for (int i = 0; i < list.size(); i++) {
			Party party = list.get(i);
			long endTime = Long.parseLong(party.getEndTime());
			if (endTime < currentTime) {
				// change the party status to over
				DBParty.updateStatus(this, party.getPartyId());
				// remove the geofences
				String[] ids = new String[4];
				ids[0] = party.getPartyId() + "$" + 1;
				ids[1] = party.getPartyId() + "$" + 2;
				ids[2] = party.getPartyId() + "$" + 3;
				ids[3] = party.getPartyId() + "$" + 4;
				GeofenceUtils.removeGeofences(this, ids);
			}
		}
	}

	private void setUpTab() {
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mTabsAdapter = new TabsAdapter(this, mTabHost, mViewPager);
		Bundle args;

		args = new Bundle();
		args.putInt(ActivityConstants.ARGS_PARTY_STATUS,
				StatusConstants.PARTY_STATUS_ONGOING);
		mTabsAdapter.addTab(
				mTabHost.newTabSpec(getString(R.string.title_party_joining))
						.setIndicator(getString(R.string.title_party_joining)),
				PartyListFragment.class, args);

		args = new Bundle();
		args.putInt(ActivityConstants.ARGS_PARTY_STATUS,
				StatusConstants.PARTY_STATUS_OVER);
		mTabsAdapter.addTab(
				mTabHost.newTabSpec(getString(R.string.title_party_over))
						.setIndicator(getString(R.string.title_party_over)),
				PartyListFragment.class, args);
	}
}
