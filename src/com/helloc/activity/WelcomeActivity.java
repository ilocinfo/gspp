package com.helloc.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.helloc.R;
import com.helloc.utils.SystemUtils;

public class WelcomeActivity extends Activity {

	private LinearLayout ll;
	private boolean flag;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);

		SystemUtils.setLanguage(this);
		
		ll = (LinearLayout) findViewById(R.id.ll);
		if (SystemUtils.isZh(this)) {
			ll.setBackgroundResource(R.drawable.app_splash_cn);
		} else {
			ll.setBackgroundResource(R.drawable.app_splash_en);
		}

		ll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				flag = true;
				Intent intent = new Intent();
				intent.setClass(WelcomeActivity.this, MainActivity.class);
				startActivity(intent);
				WelcomeActivity.this.finish();
			}
		});

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (flag == false) {
					Intent intent = new Intent();
					intent.setClass(WelcomeActivity.this, MainActivity.class);
					startActivity(intent);
					WelcomeActivity.this.finish();
				}

			}
		}, 3000);
	}
}
