package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.HttpConstants;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.NetworkUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class ProfilePasswordActivity extends SherlockActivity {
	private EditText oldPassword;
	private EditText newPassword;
	private EditText newPasswordConfirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_password);
		setTitle(getString(R.string.action_password));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		oldPassword = (EditText) findViewById(R.id.oldPassword);
		newPassword = (EditText) findViewById(R.id.newPassword);
		newPasswordConfirm = (EditText) findViewById(R.id.newPasswordConfirm);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_done)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onDoneClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	private void onDoneClicked() {
		if (!NetworkUtils.isConnect(getApplicationContext())) {
			return;
		}
		if (TextUtils.isEmpty(oldPassword.toString())
				|| TextUtils.isEmpty(newPassword.getText())
				|| TextUtils.isEmpty(newPasswordConfirm.getText())) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.toast_complete_info), Toast.LENGTH_SHORT)
					.show();
			return;
		}
		if (newPassword.getText().toString().length() < 6) {
			Toast.makeText(this, getString(R.string.toast_password_to_short),
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (newPassword.getText().toString()
				.equals(newPasswordConfirm.getText().toString()) == false) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.toast_password_not_the_same),
					Toast.LENGTH_SHORT).show();
			return;
		}

		// upload new password
		new AsyncTask<String, Long, String>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = LoadingDialog.createLoadingDialog(
						ProfilePasswordActivity.this,
						getString(R.string.toast_processing));
				dialog.show();
			}

			@Override
			protected String doInBackground(String... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("userId",
						SharePrefsUtils.getUserId(ProfilePasswordActivity.this));
				map.put("oldPassword", oldPassword.getText().toString());
				map.put("newPassword", newPassword.getText().toString());

				return HttpRequestUtils.post("updatePassword", map);
			}

			@Override
			protected void onPostExecute(String result) {
				dialog.dismiss();
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.UPDATE_PASSWORD_SUCCESS)) {
						Toast.makeText(
								getApplicationContext(),
								getString(R.string.toast_change_password_succeed),
								Toast.LENGTH_SHORT).show();
						finish();
					} else if (returnMessage
							.equals(HttpConstants.UPDATE_PASSWORD_OLD_PASSWORD_INCORRECT)) {
						Toast.makeText(getApplicationContext(),
								getString(R.string.toast_old_password_wrong),
								Toast.LENGTH_SHORT).show();
					} else {
						System.out.println("updatePassword ---->> "
								+ returnMessage);
						Toast.makeText(getApplicationContext(),
								getString(R.string.toast_old_password_wrong),
								Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					Toast.makeText(ProfilePasswordActivity.this,
							getString(R.string.toast_update_failed),
							Toast.LENGTH_SHORT).show();
					System.out.println("updatePassword ---->> "
							+ e.getMessage());
				}
			}
		}.execute();
	}
}
