package com.helloc.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.actionbarsherlock.view.Window;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helloc.R;
import com.helloc.adapter.CustomInfoWindowAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.fragment.CommonLocationFragment;
import com.helloc.utils.GoogleMapUtils;
import com.helloc.utils.SystemUtils;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnClosedListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener;

public class GoogleMapActivity extends SherlockFragmentActivity implements
		ConnectionCallbacks, LocationListener, OnConnectionFailedListener,
		OnMapClickListener, OnMapLongClickListener, LocationSource,
		CommonLocationFragment.onItemClickedListener {

	private boolean enableCurAddress = false;
	private boolean enableMapSearch = false;
	private boolean enableRadiusCircle = false;

	private GoogleMap mMap;
	private LocationClient mLocationClient;
	private OnLocationChangedListener mListener;
	private boolean isFirstLoc = true;// 是否首次定位

	private CustomInfoWindowAdapter cusInfoWinAdapter;
	private double[] latlng;
	private Marker mMarker;
	private Circle mCircle;
	private int radius;
	private String radiusStr;
	private String address;

	private SearchAddressTask searchAddrTask;
	private int counter;
	private boolean isLocationSet;

	private SlidingMenu menu;
	private CommonLocationFragment commonLocFrag;
	private boolean isMenuOpen;

	private TextView textView;
	private MenuItem search;
	private EditText etSearch;

	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(5000) // 5 seconds
			.setFastestInterval(16) // 16ms = 60fps
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_googlemap);
		setTitle(getString(R.string.text_location));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		isLocationSet = getIntent().getBooleanExtra("isLocationSet", false);
		radius = getIntent().getIntExtra("radius", Constants.RADIUS_BASE);
		enableCurAddress = getIntent().getBooleanExtra(
				Constants.CURRENT_ADDRESS, false);
		enableMapSearch = getIntent().getBooleanExtra(Constants.MAP_SEARCH,
				false);
		enableRadiusCircle = getIntent().getBooleanExtra(
				Constants.RADIUS_CIRCLE, false);

		setUpSearch();
		setUpMapIfNeeded();
		setUpSlidingMenu();
	}

	private void setUpSearch() {
		// Inflate the custom view
		View customNav = LayoutInflater.from(this).inflate(
				R.layout.actionbar_textview, null);

		textView = (TextView) customNav.findViewById(R.id.textview);
		textView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				search.expandActionView();
				etSearch.requestFocus();
				SystemUtils.switchSoftKeyboardstate(GoogleMapActivity.this);
			}
		});

		// Attach to the action bar
		getSupportActionBar().setCustomView(customNav);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 1, getString(R.string.action_done)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		search = menu.add(0, 1, 0, getString(R.string.action_search));
		search.setIcon(R.drawable.ic_search)
				.setActionView(R.layout.actionbar_search)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_ALWAYS
								| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

		etSearch = (EditText) search.getActionView().findViewById(
				R.id.search_addr);

		search.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				etSearch.requestFocus();
				SystemUtils.switchSoftKeyboardstate(GoogleMapActivity.this);
				return false;
			}
		});

		etSearch.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					if (!TextUtils.isEmpty(etSearch.getText())) {
						search.collapseActionView();
						SystemUtils.hideSoftKeyboard(etSearch,
								GoogleMapActivity.this);
						new SearchLatLngTask().execute(etSearch.getText()
								.toString());
					}
				}
				return false;
			}
		});
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		SystemUtils.hideSoftKeyboard(etSearch, GoogleMapActivity.this);
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			if (isMenuOpen) {
				menu.toggle();
				isMenuOpen = false;
				textView.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(textView.getText())) {
					setTitle(getString(R.string.text_location));
				} else {
					setTitle("");
				}
			} else {
				finish();
			}
			return true;
		} else if (item.getItemId() == 0) {
			Intent data = new Intent();
			data.putExtra("radius", radius);
			data.putExtra("address", address);
			data.putExtra("latitude", latlng[0]);
			data.putExtra("longitude", latlng[1]);
			setResult(ActivityConstants.RESULT_RESULT_OK, data);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
		mLocationClient.connect();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mLocationClient != null) {
			mLocationClient.disconnect();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (searchAddrTask != null) {
			searchAddrTask.cancel(true);
		}
	}

	private void setUpSlidingMenu() {
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.LEFT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setShadowDrawable(null);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.menu_frame);
		menu.setOnOpenedListener(new OnOpenedListener() {

			@Override
			public void onOpened() {
				isMenuOpen = true;
				textView.setVisibility(View.GONE);
				setTitle(getString(R.string.action_frequency_location));
			}
		});
		menu.setOnClosedListener(new OnClosedListener() {

			@Override
			public void onClosed() {
				isMenuOpen = false;
				textView.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(textView.getText())) {
					setTitle(getString(R.string.text_location));
				} else {
					setTitle("");
				}
			}
		});

		commonLocFrag = new CommonLocationFragment();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.menu_frame, commonLocFrag).commit();
	}

	private void setUpMapIfNeeded() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		UiSettings mUiSettings = mMap.getUiSettings();
		mUiSettings.setCompassEnabled(false);
		mUiSettings.setTiltGesturesEnabled(false);
		mUiSettings.setRotateGesturesEnabled(false);

		mMap.setLocationSource(this);
		mMap.setMyLocationEnabled(true);
		mMap.setOnMapClickListener(this);
		mMap.setOnMapLongClickListener(this);

		if (enableRadiusCircle) {
			cusInfoWinAdapter = new CustomInfoWindowAdapter(this, true);
			mMap.setInfoWindowAdapter(cusInfoWinAdapter);
		} else {
			cusInfoWinAdapter = new CustomInfoWindowAdapter(this, false);
			mMap.setInfoWindowAdapter(cusInfoWinAdapter);
		}
		setUpOverlay();
	}

	private void setUpLocationClientIfNeeded() {
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(this, this, this);
		}
	}

	private void setUpOverlay() {
		radiusStr = radius + getString(R.string.text_meter);

		latlng = new double[2];
		if (enableMapSearch) {
			latlng[0] = getIntent().getDoubleExtra("latitude", 0);
			latlng[1] = getIntent().getDoubleExtra("longitude", 0);
			address = getIntent().getStringExtra("address");
		}

		int markerIconId = 0;
		if (enableCurAddress) {
			markerIconId = R.drawable.empty_marker;
		} else {
			markerIconId = R.drawable.icon_address;
		}
		mMarker = mMap.addMarker(new MarkerOptions().position(
				new LatLng(latlng[0], latlng[1])).icon(
				BitmapDescriptorFactory.fromResource(markerIconId)));
		mMarker.setVisible(false);

		if (enableRadiusCircle) {
			mCircle = mMap.addCircle(new CircleOptions()
					.center(new LatLng(latlng[0], latlng[1])).radius(radius)
					.strokeWidth(1).strokeColor(0xFF88b5dc)
					.fillColor(1409351437));
			mCircle.setVisible(false);
		}

		if (enableMapSearch) {
			mMarker.setVisible(true);
			if (enableRadiusCircle) {
				mCircle.setVisible(true);
			}

			mMarker.setTitle(getString(R.string.text_coverage) + radiusStr);
			mMarker.setSnippet(address);
			mMarker.showInfoWindow();
		}
	}

	private void updateRadius() {
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latlng[0],
				latlng[1]), GoogleMapUtils.getZoom(radius)));
		if (radius < 10000) {
			radiusStr = radius + getString(R.string.text_meter);
		} else {
			radiusStr = radius / 1000 + getString(R.string.text_kilometer);
		}

		mMarker.setTitle(getString(R.string.text_coverage) + radiusStr);
		mMarker.hideInfoWindow();
		mMarker.showInfoWindow();

		mCircle.setRadius(radius);
	}

	private void updateOverlayPosition() {
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
				latlng[0], latlng[1]), GoogleMapUtils.getZoom(radius)));

		mMarker.setPosition(new LatLng(latlng[0], latlng[1]));
		mMarker.setSnippet(address);
		mMarker.hideInfoWindow();
		mMarker.showInfoWindow();

		if (enableRadiusCircle) {
			mCircle.setCenter(new LatLng(latlng[0], latlng[1]));
		}
	}

	public void setOverlayPosition(double latitude, double longitude,
			String address) {
		latlng[0] = latitude;
		latlng[1] = longitude;
		this.address = address;
		updateOverlayPosition();
	}

	/**
	 * use voice controller to change the range
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			if (enableRadiusCircle) {
				counter++;
				radius = GoogleMapUtils.getDecreaseRadius(radius, counter);
				updateRadius();
				return true;
			}
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			if (enableRadiusCircle) {
				counter++;
				radius = GoogleMapUtils.getIncreaseRadius(radius, counter);
				updateRadius();
				return true;
			}
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (isMenuOpen) {
				menu.toggle();
			} else {
				finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
				|| keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			counter = 0;
		}
		return super.onKeyUp(keyCode, event);
	}

	public void onMenuClicked(View view) {
		menu.toggle();
	}

	/**
	 * Implementation of {@link CommonLocationFragment.onItemClickedListener}.
	 */
	@Override
	public void onListItemClicked(double latitude, double longitude,
			String address) {
		setOverlayPosition(latitude, longitude, address);
		menu.toggle();
	}

	/**
	 * Implementation of {@link CommonLocationFragment.onItemClickedListener}.
	 */
	@Override
	public void onFragmentClose() {
		menu.toggle();
	}

	@Override
	public void onMapLongClick(final LatLng point) {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				double[] latlng = new double[] { point.latitude,
						point.longitude };
				return GoogleMapUtils
						.getAddress(latlng, GoogleMapActivity.this);
			}

			@Override
			protected void onPostExecute(String result) {
				if (TextUtils.isEmpty(result)) {
					Toast.makeText(GoogleMapActivity.this,
							getString(R.string.toast_search_address_failed),
							Toast.LENGTH_SHORT).show();
				} else {
					commonLocFrag.setAddress(result, point);
					menu.toggle();
				}
			}

		}.execute();

	}

	@Override
	public void onMapClick(LatLng point) {
		if (enableMapSearch) {
			address = "";
			latlng[0] = point.latitude;
			latlng[1] = point.longitude;
			updateOverlayPosition();

			// start a new thread to search address
			if (searchAddrTask != null) {
				searchAddrTask.cancel(true);
			}
			searchAddrTask = new SearchAddressTask(this);
			searchAddrTask.execute(latlng);
		}
		if (mMarker != null && !mMarker.isInfoWindowShown()) {
			mMarker.showInfoWindow();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// Do nothing

	}

	@Override
	public void onLocationChanged(Location location) {
		location.setAccuracy(0);
		mListener.onLocationChanged(location);
		if (isFirstLoc && enableCurAddress) {
			isFirstLoc = false;
			latlng[0] = location.getLatitude();
			latlng[1] = location.getLongitude();
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
					latlng[0], latlng[1]), GoogleMapUtils.getZoom(radius)));
		}
		if (enableCurAddress) {
			updateOverlayPosition();
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		mLocationClient.requestLocationUpdates(REQUEST, this); // LocationListener

		if (isLocationSet == false) {
			Location currentLocation = mLocationClient.getLastLocation();
			latlng[0] = currentLocation.getLatitude();
			latlng[1] = currentLocation.getLongitude();
		}

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latlng[0],
				latlng[1]), GoogleMapUtils.getZoom(radius)));

		if (isLocationSet == false) {
			if (searchAddrTask != null) {
				searchAddrTask.cancel(true);
			}
			searchAddrTask = new SearchAddressTask(this);
			searchAddrTask.execute(new double[] { latlng[0], latlng[1] });

			updateOverlayPosition();
		}

	}

	@Override
	public void onDisconnected() {
		// Do nothing

	}

	@Override
	public void activate(OnLocationChangedListener listener) {
		mListener = listener;

	}

	@Override
	public void deactivate() {
		mListener = null;
	}

	/**
	 * async task to search address by latlng
	 */
	private class SearchAddressTask extends AsyncTask<double[], Void, String> {

		// Store the context passed to the AsyncTask when the system
		// instantiates it.
		Context context;

		// Constructor called by the system to instantiate the task
		public SearchAddressTask(Context context) {

			// Required by the semantics of AsyncTask
			super();

			// Set a Context for the background task
			this.context = context;
		}

		@Override
		protected String doInBackground(double[]... params) {
			address = GoogleMapUtils.getAddress(params[0], context);
			System.out.println("searchAddr = " + address);
			return address;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// updateAll();
			updateOverlayPosition();
		}
	}

	/**
	 * async task to search latlng by address
	 */
	private class SearchLatLngTask extends AsyncTask<String, Void, String> {
		double[] searchResult = new double[2];
		String addr;

		@Override
		protected String doInBackground(String... params) {
			addr = params[0];
			searchResult = GoogleMapUtils.getLatLng(addr);
			// System.out.println("search result = " + searchResult);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (searchResult != null) {
				address = addr;
				latlng = searchResult;
				updateOverlayPosition();
				mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
						new LatLng(latlng[0], latlng[1]),
						GoogleMapUtils.getZoom(radius)));
				textView.setText(address);
				setTitle("");
			} else {
				Toast.makeText(GoogleMapActivity.this,
						getString(R.string.toast_search_address_failed),
						Toast.LENGTH_SHORT).show();
			}
		}
	}
}
