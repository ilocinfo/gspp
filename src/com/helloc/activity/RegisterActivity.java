package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.database.DBFriend;
import com.helloc.database.DBInvitation;
import com.helloc.database.DBLocations;
import com.helloc.database.DBMapSnapshot;
import com.helloc.database.DBMessage;
import com.helloc.database.DBMessageReply;
import com.helloc.database.DBParty;
import com.helloc.database.DBPartying;
import com.helloc.utils.GCMUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.NetworkUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class RegisterActivity extends SherlockActivity {
	private EditText account;
	private EditText nickname;
	private EditText password;
	private EditText pwConfirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		setTitle(getString(R.string.action_register));
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		account = (EditText) findViewById(R.id.account);
		nickname = (EditText) findViewById(R.id.nickname);
		password = (EditText) findViewById(R.id.password);
		pwConfirm = (EditText) findViewById(R.id.password_comfirm);

		// set nickname if exist
		nickname.setText(SharePrefsUtils.getNickname(this));
		if (TextUtils.isEmpty(nickname.getText().toString()) == false) {
			nickname.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onRegisterClicked(View view) {
		// ����Ƿ�����
		if (NetworkUtils.isConnect(this) == false) {
			return;
		}
		if (TextUtils.isEmpty(account.getText().toString())) {
			Toast.makeText(this, getString(R.string.toast_input_account),
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (TextUtils.isEmpty(nickname.getText().toString())) {
			Toast.makeText(this, getString(R.string.toast_input_nickname),
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (TextUtils.isEmpty(password.getText().toString())) {
			Toast.makeText(this, getString(R.string.toast_input_password),
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (account.getText().toString().length() < 4) {
			Toast.makeText(this, getString(R.string.toast_account_to_short),
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (password.getText().toString().length() < 6) {
			Toast.makeText(this, getString(R.string.toast_password_to_short),
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (password.getText().toString()
				.equals(pwConfirm.getText().toString()) == false) {
			Toast.makeText(this,
					getString(R.string.toast_password_not_the_same),
					Toast.LENGTH_SHORT).show();
			return;
		}
		new AsyncTask<Void, Void, String>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = LoadingDialog.createLoadingDialog(
						RegisterActivity.this,
						getString(R.string.toast_register));
				dialog.show();
			}

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				if (TextUtils.isEmpty(SharePrefsUtils
						.getUserId(RegisterActivity.this)) == false
						&& SharePrefsUtils.isLogin(RegisterActivity.this)) {
					map.put("receiveUserId",
							SharePrefsUtils.getUserId(RegisterActivity.this));
				}
				map.put("universalUniqueId", account.getText().toString());
				map.put("nickname", nickname.getText().toString());
				map.put("password", password.getText().toString());

				return HttpRequestUtils.post("register", map);
			}

			@Override
			protected void onPostExecute(String result) {
				dialog.dismiss();
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage.equals(HttpConstants.REGISTER_UUID_EXIST)) {
						Toast.makeText(RegisterActivity.this,
								getString(R.string.toast_account_exist),
								Toast.LENGTH_SHORT).show();
						return;
					} else if (returnMessage
							.equals(HttpConstants.REGISTER_SUCCEED)) {
						if (SharePrefsUtils
								.isLoginAsGuest(RegisterActivity.this) == false) {
							clearPreviousRecord();
						}
						if (json.isNull("generatedUserId") == false) {
							SharePrefsUtils.setUserId(RegisterActivity.this,
									json.getString("generatedUserId"));
						}
						SharePrefsUtils.setNewFeatureOdd(RegisterActivity.this, false);
						// set is login to true
						SharePrefsUtils.setLogin(RegisterActivity.this, true);
						// set loginAsGuest to true
						SharePrefsUtils.setLoginAsGuest(RegisterActivity.this,
								false);
						// save uuid
						SharePrefsUtils.setUuid(RegisterActivity.this, account
								.getText().toString());
						// save login account
						SharePrefsUtils.setLoginAccount(RegisterActivity.this,
								account.getText().toString());
						// set nickname
						SharePrefsUtils.setNickName(RegisterActivity.this,
								nickname.getText().toString());
						// register to GCM server
						new GCMUtils().GCMRegistration(RegisterActivity.this,
								true);

						setResult(ActivityConstants.RESULT_REGISTER_SUCCEED);
						finish();
					} else {
						System.out.println("register ---->> " + returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("register ---->> " + e.getMessage());
				}

			}
		}.execute();
	}

	private void clearPreviousRecord() {
		// clear SharedPreferences
		SharePrefsUtils.clear(this);
		// clear database
		DBFriend.clearDatabase(this);
		DBInvitation.clearDatabase(this);
		DBLocations.clearDatabase(this);
		DBMapSnapshot.clearDatabase(this);
		DBMessage.clearDatabase(this);
		DBMessageReply.clearDatabase(this);
		DBParty.clearDatabase(this);
		DBPartying.clearDatabase(this);
	}
}
