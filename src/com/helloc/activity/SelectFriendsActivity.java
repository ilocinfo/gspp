package com.helloc.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.adapter.SelectFriendsListviewAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.database.DBFriend;
import com.helloc.model.Friends;
import com.helloc.utils.SharePrefsUtils;

public class SelectFriendsActivity extends SherlockActivity {
	private ListView friendListView;
	private ArrayList<Friends> friendData;
	private SelectFriendsListviewAdapter friendAdapter;
	private CheckBox wxFriend;
	private CheckBox wxMoment;
	private CheckBox otherApps;
	private LinearLayout wxItem;
	private int wxType;
	private boolean isOtherappsSelected;
	private ArrayList<String> friendsIdList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_friends);
		setTitle(getString(R.string.text_select_friends));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		friendListView = (ListView) findViewById(R.id.friends_list);
		wxItem = (LinearLayout) findViewById(R.id.wxItem);
		wxFriend = (CheckBox) findViewById(R.id.wxFriend);
		wxMoment = (CheckBox) findViewById(R.id.wxMoment);
		otherApps = (CheckBox) findViewById(R.id.otherapps);

		friendsIdList = getIntent().getStringArrayListExtra("friendsIdList");
		wxType = getIntent().getIntExtra("wxType",
				Constants.SEND_TO_WECHAT_NULL);
		isOtherappsSelected = getIntent().getBooleanExtra(
				"isOtherappsSelected", false);
		setUpView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_done)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onDoneClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpView() {
		// set wechat checkbox
		if (SharePrefsUtils.isRegsiterToWechat(this) == false) {
			wxItem.setVisibility(View.GONE);
		} else {
			switch (wxType) {
			case Constants.SEND_TO_WECHAT_FRIEND:
				wxFriend.setChecked(true);
				break;
			case Constants.SEND_TO_WECHAT_MOMENT:
				wxMoment.setChecked(true);
				break;
			}
		}

		// set other app checkbox
		if (isOtherappsSelected) {
			otherApps.setChecked(true);
		}

		// set friends list
		List<Boolean> isChecked = new ArrayList<Boolean>();
		friendData = DBFriend.getAll(this);
		for (int i = 0; i < friendData.size(); i++) {
			isChecked.add(false);
			for (int j = 0; j < friendsIdList.size(); j++) {
				if (friendData.get(i).getUserId().equals(friendsIdList.get(j))) {
					isChecked.set(i, true);
					break;
				}
			}
		}

		friendAdapter = new SelectFriendsListviewAdapter(this, friendData,
				isChecked);
		friendListView.setAdapter(friendAdapter);
	}

	private void onDoneClicked() {
		List<Boolean> checkItems = friendAdapter.getCheckItem();

		friendsIdList.clear();
		for (int i = 0; i < checkItems.size(); i++) {
			if (checkItems.get(i)) {
				friendsIdList.add(friendData.get(i).getUserId());
			}
		}

		if (wxType == Constants.SEND_TO_WECHAT_NULL
				&& friendsIdList.size() == 0 && isOtherappsSelected == false) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.toast_select_one_contacts),
					Toast.LENGTH_SHORT).show();
			return;
		}

		Intent intent = new Intent();
		intent.putStringArrayListExtra("friendsIdList", friendsIdList);
		intent.putExtra("wxType", wxType);
		intent.putExtra("isOtherappsSelected", isOtherappsSelected);
		setResult(ActivityConstants.RESULT_RESULT_OK, intent);
		finish();
	}

	public void onWXFriendClicked(View view) {
		if (wxFriend.isChecked()) {
			wxType = Constants.SEND_TO_WECHAT_NULL;
			wxFriend.setChecked(false);
		} else {
			wxMoment.setChecked(false);
			wxFriend.setChecked(true);
			wxType = Constants.SEND_TO_WECHAT_FRIEND;
		}
	}

	public void onWXMomentClicked(View view) {
		if (wxMoment.isChecked()) {
			wxType = Constants.SEND_TO_WECHAT_NULL;
			wxMoment.setChecked(false);
		} else {
			wxFriend.setChecked(false);
			wxMoment.setChecked(true);
			wxType = Constants.SEND_TO_WECHAT_MOMENT;
		}
	}

	public void onOtherappsClicked(View v) {
		if (isOtherappsSelected) {
			isOtherappsSelected = false;
			otherApps.setChecked(false);
		} else {
			if (SharePrefsUtils.isNotShowShareToOtherAppHint(this)) {
				isOtherappsSelected = true;
				otherApps.setChecked(true);
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				LayoutInflater inflater = LayoutInflater.from(this);
				View view = inflater.inflate(
						R.layout.dialog_share_to_other_app_hint, null);
				builder.setTitle(getTitle());
				builder.setView(view);
				final CheckBox checkBox = (CheckBox) view
						.findViewById(R.id.checkbox);
				checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							SharePrefsUtils.setNotShowShareToOtherAppHint(
									SelectFriendsActivity.this, true);
						} else {
							SharePrefsUtils.setNotShowShareToOtherAppHint(
									SelectFriendsActivity.this, false);
						}

					}
				});
				builder.setPositiveButton(getString(R.string.btn_I_know),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (checkBox.isChecked()) {
									SharePrefsUtils
											.setNotShowShareToOtherAppHint(
													SelectFriendsActivity.this,
													true);
								}
								isOtherappsSelected = true;
								otherApps.setChecked(true);
							}
						});
				AlertDialog dialog = builder.create();
				dialog.show();
			}

		}
	}

}
