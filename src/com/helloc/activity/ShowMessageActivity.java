package com.helloc.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessageReply;
import com.helloc.fragment.MessageDetailFragment;
import com.helloc.model.MessageReply;
import com.helloc.utils.GoogleMapUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.SharePrefsUtils;

public class ShowMessageActivity extends SherlockFragmentActivity {
	private GoogleMap mMap;

	private String senderId;
	private String messageId;
	private int messageStatus;
	private double latitude;
	private double longitude;
	private float range;
	private String radius;
	private String address;
	private int type;
	private String privateMsg;
	private String publicMsg;
	private String imageNamesStr;
	private ArrayList<String> imageIndex;

	private LinearLayout imageFragment;
	private ViewGroup mContainerView;
	private MessageDetailFragment messageDetailFrag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		messageStatus = getIntent().getIntExtra(
				ActivityConstants.ARGS_MESSAGE_STATUS, 0);
		if (messageStatus == StatusConstants.MESSAGE_STATUS_SELF
				|| messageStatus == StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
			setContentView(R.layout.activity_show_self_message);
		} else {
			setContentView(R.layout.activity_show_message);
		}

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		imageFragment = (LinearLayout) findViewById(R.id.imageFragment);
		mContainerView = (ViewGroup) findViewById(R.id.container);

		senderId = getIntent().getStringExtra(
				ActivityConstants.ARGS_ACCOUNT_USER_ID);
		messageId = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_ID);
		latitude = getIntent().getDoubleExtra(
				ActivityConstants.ARGS_MESSAGE_LATITUDE, 0);
		longitude = getIntent().getDoubleExtra(
				ActivityConstants.ARGS_MESSAGE_LONGITUDE, 0);
		range = getIntent().getFloatExtra(
				ActivityConstants.ARGS_MESSAGE_RADIUS, 0);
		address = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_ADDRESS);
		type = getIntent().getIntExtra(ActivityConstants.ARGS_MESSAGE_TYPE,
				Constants.MESSAGE_TYPE_TEXT);
		privateMsg = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG);
		publicMsg = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG);
		imageNamesStr = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_IMAGE_NAME);

		setUpTitle();
		setUpView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED
				|| messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED
				|| messageStatus == StatusConstants.MESSAGE_STATUS_SENT) {
			menu.add(0, 1, 0, getString(R.string.action_reply))
					.setShowAsAction(
							MenuItem.SHOW_AS_ACTION_ALWAYS
									| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			menu.add(0, 2, 0, getString(R.string.action_refresh))
					.setShowAsAction(
							MenuItem.SHOW_AS_ACTION_ALWAYS
									| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 1) {
			Intent intent = new Intent();
			intent.setClass(this, ReplyMessageActivity.class);
			intent.putExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID, senderId);
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_ID, messageId);
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_STATUS,
					messageStatus);
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_TYPE, type);
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG,
					privateMsg);
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG,
					publicMsg);
			startActivityForResult(intent,
					ActivityConstants.REQUEST_GO_TO_REPLY_MESSAGE_ACTIVITY);
		} else if (item.getItemId() == 2) {
			updateReplies();
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpTitle() {
		if (messageStatus == StatusConstants.MESSAGE_STATUS_SELF
				|| messageStatus == StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
			setTitle(getString(R.string.action_ring_me_around));
		} else if (messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED
				|| messageStatus == StatusConstants.MESSAGE_STATUS_SENT) {
			setTitle(getString(R.string.action_shout));
		}
	}

	private void setUpView() {
		messageDetailFrag = new MessageDetailFragment();
		Bundle bundle = new Bundle();
		bundle.putString(ActivityConstants.ARGS_ACCOUNT_USER_ID, senderId);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		bundle.putInt(ActivityConstants.ARGS_MESSAGE_STATUS, messageStatus);
		bundle.putInt(ActivityConstants.ARGS_MESSAGE_TYPE, type);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG, privateMsg);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG, publicMsg);
		messageDetailFrag.setArguments(bundle);
		getSupportFragmentManager().beginTransaction()
				.add(R.id.messageItem, messageDetailFrag).commit();

		setUpImageIfNeed();
		setUpMapIfNeeded();

	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}

	private void setUpImageIfNeed() {
		if (TextUtils.isEmpty(imageNamesStr)) {
			imageFragment.setVisibility(View.GONE);
		} else {
			imageIndex = new ArrayList<String>();

			imageFragment.setVisibility(View.VISIBLE);
			String[] imageNames = imageNamesStr.split(",");
			for (int i = 0; i < imageNames.length; i++) {
				// check whether exist
				System.out.println("imagePath = " + imageNames[i]);
				String imageName = messageId + "_" + imageNames[i];
				String imagePath = Constants.THUMBNAIL_FLODER + imageName
						+ ".jpg";
				File file = new File(imagePath);
				System.out.println("imagePath = " + imagePath);
				if (file.exists()) {
					imageIndex.add(imageName);
					addImageItem(imagePath, imageName);
				} else {
					new DownloadThumbnail(messageId + "_" + imageNames[i])
							.execute();
				}
			}
		}
	}

	private void setUpMapIfNeeded() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		if (range < 10000) {
			radius = range + getString(R.string.text_meter);
		} else {
			radius = range / 1000 + getString(R.string.text_kilometer);
		}
		mMap.addMarker(
				new MarkerOptions()
						.position(new LatLng(latitude, longitude))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.icon_address))
						.title(getString(R.string.text_coverage) + radius)
						.snippet(address)).showInfoWindow();

		mMap.addCircle(new CircleOptions()
				.center(new LatLng(latitude, longitude)).radius(range)
				.strokeWidth(1).strokeColor(0xFF88b5dc).fillColor(1409351437));

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
				longitude), GoogleMapUtils.getZoom((int) range)));
	}

	private void addImageItem(String imagePath, final String imageName) {
		// Instantiate a new "row" view.
		final ViewGroup newImage = (ViewGroup) LayoutInflater.from(this)
				.inflate(R.layout.item_image_display_thumbnail, mContainerView,
						false);
		ImageView image = (ImageView) newImage.findViewById(R.id.image);
		image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
		image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(ShowMessageActivity.this,
						DisplayImageActivity.class);
				// intent.putExtra("imageName", imageName);
				intent.putStringArrayListExtra("imageIndex", imageIndex);
				intent.putExtra("index", mContainerView.indexOfChild(newImage));
				startActivity(intent);
			}
		});
		mContainerView.addView(newImage, mContainerView.getChildCount());
	}

	private class DownloadThumbnail extends AsyncTask<Void, Void, String> {
		private String imageName;

		DownloadThumbnail(String imageName) {
			this.imageName = imageName;
		}

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("thumbnailName", imageName);

			return HttpRequestUtils.post("downloadThumbnail", map);
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage
						.equals(HttpConstants.DOWNLOAD_THUMBNAIL_SUCCESS)) {
					byte[] img = Base64.decode(json.getString("thumbnail"),
							Base64.DEFAULT);
					Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0,
							img.length);
					String imagePath = Constants.THUMBNAIL_FLODER + imageName
							+ ".jpg";
					ImageUtils.saveImage(bitmap, imagePath);
					imageIndex.add(imageName);
					addImageItem(imagePath, imageName);
				} else {
					System.out.println("downloadThumbnail ---->> "
							+ returnMessage);
				}
			} catch (JSONException e) {
				System.out
						.println("downloadThumbnail ---->> " + e.getMessage());
			}
		}
	}

	private void updateReplies() {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("deviceGeneratedMessageId", messageId);
				map.put("order", String.valueOf(DBMessageReply
						.getLatestOrderByMsgId(ShowMessageActivity.this,
								messageId)));
				return HttpRequestUtils.post("updateReplies", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.UPDATE_REPLIES_SUCCESS)) {
						JSONArray list = json.getJSONArray("messageReplies");
						for (int i = 0; i < list.length(); i++) {
							JSONObject object = (JSONObject) list.get(i);
							MessageReply messageReply = new MessageReply();
							messageReply.setOrder(Integer.parseInt(object
									.getString("order")));
							messageReply.setMessageId(messageId);
							messageReply.setUserId(object.getString("userId"));
							messageReply.setUserName("");
							messageReply.setReplyContent(object
									.getString("replyContent"));
							messageReply.setDate(object.getString("date"));
							messageReply
									.setMyReply(object
											.getString("userId")
											.equals(SharePrefsUtils
													.getUserId(ShowMessageActivity.this)));
							messageReply
									.setMessageType(Constants.MESSAGE_TYPE_TEXT);
							DBMessageReply.insert(ShowMessageActivity.this,
									messageReply);
						}
						messageDetailFrag.updateListView();
					} else {
						messageDetailFrag.updateListView();
					}
				} catch (JSONException e) {
					Toast.makeText(ShowMessageActivity.this,
							getString(R.string.toast_update_failed),
							Toast.LENGTH_SHORT).show();
					System.out
							.println("updateReplies ---->> " + e.getMessage());
				}
			}
		}.execute();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent arg2) {
		if (requestCode == ActivityConstants.REQUEST_GO_TO_REPLY_MESSAGE_ACTIVITY) {
			messageDetailFrag.updateListView();
		}
	}

}
