package com.helloc.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.TabHost;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.adapter.TabsAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.fragment.MessageListFragment;

public class MessageListActivity extends SherlockFragmentActivity {
	private int activitySrc;
	TabHost mTabHost;
	ViewPager mViewPager;
	TabsAdapter mTabsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_message_list);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		activitySrc = getIntent().getIntExtra(
				ActivityConstants.ARGS_ACTIVITY_SOURCE, -1);

		setUpLabel();

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		setUpTab();

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
	}

	private void setUpLabel() {
		switch (activitySrc) {
		case ActivityConstants.ARGS_ACTIVITY_SEND_MESSAGE:
			setTitle(getString(R.string.action_shout));
			break;

		case ActivityConstants.ARGS_ACTIVITY_SELF_GEOFENCE:
			setTitle(getString(R.string.action_ring_me_around));
			break;

		}
	}

	private void setUpTab() {
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mTabsAdapter = new TabsAdapter(this, mTabHost, mViewPager);
		Bundle args;

		switch (activitySrc) {
		case ActivityConstants.ARGS_ACTIVITY_SEND_MESSAGE:
			args = new Bundle();
			args.putInt(StatusConstants.MESSAGE_STATUS,
					StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED);
			mTabsAdapter
					.addTab(mTabHost.newTabSpec(
							getString(R.string.title_message_received))
							.setIndicator(
									getString(R.string.title_message_received)),
							MessageListFragment.class, args);

			args = new Bundle();
			args.putInt(StatusConstants.MESSAGE_STATUS,
					StatusConstants.MESSAGE_STATUS_SENT);
			mTabsAdapter.addTab(
					mTabHost.newTabSpec(getString(R.string.title_message_sent))
							.setIndicator(
									getString(R.string.title_message_sent)),
					MessageListFragment.class, args);
			break;
		case ActivityConstants.ARGS_ACTIVITY_SELF_GEOFENCE:
			args = new Bundle();
			args.putInt(StatusConstants.MESSAGE_STATUS,
					StatusConstants.MESSAGE_STATUS_SELF_ACTIVED);
			mTabsAdapter
					.addTab(mTabHost
							.newTabSpec(
									getString(R.string.title_self_geofence_activated))
							.setIndicator(
									getString(R.string.title_self_geofence_activated)),
							MessageListFragment.class, args);

			args = new Bundle();
			args.putInt(StatusConstants.MESSAGE_STATUS,
					StatusConstants.MESSAGE_STATUS_SELF);
			mTabsAdapter
					.addTab(mTabHost
							.newTabSpec(
									getString(R.string.title_self_geofence_set))
							.setIndicator(
									getString(R.string.title_self_geofence_set)),
							MessageListFragment.class, args);
			break;
		}
	}

}
