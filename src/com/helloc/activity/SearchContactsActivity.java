package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class SearchContactsActivity extends SherlockActivity {
	private EditText searchKey;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_contacts);
		setTitle(getString(R.string.action_add_contacts));
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		searchKey = (EditText) findViewById(R.id.searchKey);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_search)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onSearchButtonClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	private void onSearchButtonClicked() {
		if (searchKey.getText().toString().equals("")) {
			return;
		}
		// no need to search myself
		if (searchKey.getText().toString()
				.equals(SharePrefsUtils.getUuid(this))
				|| searchKey.getText().toString()
						.equals(SharePrefsUtils.getPhone(this))
				|| searchKey.getText().toString()
						.equals(SharePrefsUtils.getEmail(this))) {
			Intent intent = new Intent();
			intent.setClass(this, ProfileActivity.class);
			startActivity(intent);
			return;
		}
		new SearchFriends().execute(searchKey.getText().toString());
	}

	private class SearchFriends extends AsyncTask<String, Void, String> {

		Dialog dialog;

		@Override
		protected void onPreExecute() {
			dialog = LoadingDialog.createLoadingDialog(
					SearchContactsActivity.this,
					getString(R.string.toast_searching));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("keyword", params[0]);

			return HttpRequestUtils.post("searchFriends", map);
		}

		@Override
		protected void onPostExecute(String result) {
			dialog.dismiss();
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage.equals(HttpConstants.SEARCH_FRIENDS_FAIL)) {
					Toast.makeText(getApplicationContext(),
							getString(R.string.toast_friends_not_exist),
							Toast.LENGTH_SHORT).show();
					return;
				} else if (returnMessage
						.equals(HttpConstants.SEARCH_FRIENDS_SUCCESS)) {
					String userId = json.getString("userId");
					String uuid = json.getString("universalUniqueId");
					String phone = json.getString("phone");
					String nickName = json.getString("nickname");
					String encodedAvatar = json.getString("encodedAvatar");
					Intent intent = new Intent(SearchContactsActivity.this,
							SearchContactsResultActivity.class);
					intent.putExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID,
							userId);
					intent.putExtra(ActivityConstants.ARGS_ACCOUNT_UUID, uuid);
					intent.putExtra(ActivityConstants.ARGS_ACCOUNT_PHONE, phone);
					intent.putExtra(ActivityConstants.ARGS_ACCOUNT_NICKNAME,
							nickName);
					intent.putExtra(ActivityConstants.ARGS_ACCOUNT_AVATAR,
							encodedAvatar);
					startActivity(intent);
				} else {
					System.out.println("searchFriends ---->> " + returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("searchFriends ---->> " + e.getMessage());
			}

		}

	}
}
