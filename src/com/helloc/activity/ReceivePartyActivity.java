package com.helloc.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.SpannableString;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBAvatar;
import com.helloc.database.DBParty;
import com.helloc.database.DBPartying;
import com.helloc.model.Party;
import com.helloc.model.Partying;
import com.helloc.utils.ExpressionUtil;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.utils.WeChatUtils;
import com.helloc.view.TableCellView;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class ReceivePartyActivity extends SherlockFragmentActivity implements
		ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

	private GoogleMap mMap;
	private LocationClient mLocationClient;

	private LinearLayout orderTable;
	private ViewGroup unorderTable;
	private LinearLayout xAxis;

	private TextView[][] tvName;
	private ImageView[][] ivAvatar;
	private TextView[] tvNum;
	private boolean[][] isCellUsed;
	private int[] num;

	private Party party;
	private String title;
	private String theme;
	private String partyId;
	private String latitude;
	private String longitude;
	private String address;

	private ArrayList<Partying> partyingList;
	private int partyStatus;
	private boolean isUpdating;

	private UpdateViewThread updateViewThread;

	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(5000) // 5 seconds
			.setFastestInterval(16) // 16ms = 60fps
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_receive_party);
		setTitle(getString(R.string.action_party));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		setUpTable();

		partyingList = new ArrayList<Partying>();
		partyId = getIntent().getStringExtra(ActivityConstants.ARGS_PARTY_ID);
		latitude = getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_LATITUDE);
		longitude = getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_LONGITUDE);
		address = getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_ADDRESS);
		isJoinedParty();
		setUpMapIfNeeded();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
		mLocationClient.connect();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mLocationClient != null) {
			mLocationClient.disconnect();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		isUpdating = false;
		if (updateViewThread != null) {
			updateViewThread.cancel(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_details)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_ALWAYS
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			Intent intent = new Intent();
			intent.setClass(ReceivePartyActivity.this,
					ShowPartyDetailsActivity.class);
			intent.putExtra(ActivityConstants.ARGS_PARTY_THEME,
					party.getTheme());
			intent.putExtra(ActivityConstants.ARGS_PARTY_ORIGINATOR,
					party.getOriginator());
			intent.putExtra(ActivityConstants.ARGS_PARTY_DESCRIPTION,
					party.getDescription());
			intent.putExtra(ActivityConstants.ARGS_PARTY_ADDRESS,
					party.getAddress());
			intent.putExtra(ActivityConstants.ARGS_PARTY_START_DATE,
					party.getStartDate());
			intent.putExtra(ActivityConstants.ARGS_PARTY_START_TIME,
					party.getStartTime());
			intent.putExtra(ActivityConstants.ARGS_PARTY_END_DATE,
					party.getEndDate());
			intent.putExtra(ActivityConstants.ARGS_PARTY_END_TIME,
					party.getEndTime());
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void isJoinedParty() {
		party = DBParty.getPartyById(this, partyId);
		if (party == null) {
			party = new Party();
			party.setPartyId(partyId);
			party.setTheme(getIntent().getStringExtra(
					ActivityConstants.ARGS_PARTY_THEME));
			party.setDescription(getIntent().getStringExtra(
					ActivityConstants.ARGS_PARTY_DESCRIPTION));
			party.setStartDate(getIntent().getStringExtra(
					ActivityConstants.ARGS_PARTY_START_DATE));
			party.setStartTime(getIntent().getStringExtra(
					ActivityConstants.ARGS_PARTY_START_TIME));
			party.setEndDate(getIntent().getStringExtra(
					ActivityConstants.ARGS_PARTY_END_DATE));
			party.setEndTime(getIntent().getStringExtra(
					ActivityConstants.ARGS_PARTY_END_TIME));
			party.setAddress(address);
			party.setLatitude(latitude);
			party.setLongitude(longitude);
			party.setOriginator(getIntent().getStringExtra(
					ActivityConstants.ARGS_PARTY_ORIGINATOR));

			AlertDialog.Builder builder = new Builder(ReceivePartyActivity.this);
			builder.setTitle(party.getOriginator() + " "
					+ getString(R.string.text_party_invitation));
			try {
				SpannableString spannableString = ExpressionUtil
						.getExpressionString(getApplicationContext(),
								party.getTheme(), Constants.REGULAR);
				builder.setMessage(spannableString);
			} catch (Exception e) {
				e.printStackTrace();
			}
			builder.setCancelable(false);
			builder.setPositiveButton(getString(R.string.btn_accept),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							DBParty.insert(ReceivePartyActivity.this, party);
							Partying partying = new Partying();
							partying.setPartyId(partyId);
							partying.setUserId(SharePrefsUtils
									.getUserId(ReceivePartyActivity.this));
							partying.setNickName(SharePrefsUtils
									.getNickname(ReceivePartyActivity.this));
							partying.setInterval(5);
							DBPartying.insert(ReceivePartyActivity.this,
									partying);

							setUpUI();

							// upload interval
							new UploadIntervalTask().execute();

							// setup the party geofence
							GeofenceUtils.setUpIntervalGeofence(
									ReceivePartyActivity.this, partyId,
									Double.parseDouble(latitude),
									Double.parseDouble(longitude));

							updateViewThread = new UpdateViewThread();
							updateViewThread.execute();
						}

					});
			builder.setNegativeButton(getString(R.string.btn_reject),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
		} else {
			setUpUI();
			GeofenceUtils
					.setUpIntervalGeofence(this, partyId,
							Double.parseDouble(latitude),
							Double.parseDouble(longitude));
			// startService();
			if (party.getStatus() == StatusConstants.PARTY_STATUS_ONGOING) {
				updateViewThread = new UpdateViewThread();
				updateViewThread.execute();
			} else {
				updateTable();
			}
		}
	}

	private class UploadIntervalTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("deviceGeneratedPartyId", partyId);
			map.put("userId",
					SharePrefsUtils.getUserId(getApplicationContext()));
			map.put("interval", String.valueOf(5));
			return HttpRequestUtils.post("updateSelfPartying", map);
		}

		@Override
		protected void onPostExecute(String result) {
			if (result == null) {
				return;
			}
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage.equals(HttpConstants.UPDATE_PARTYING_SUCCESS)) {
				} else {
					System.out
							.println("updatePartying ---->> " + returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("updatePartying ---->> " + e.getMessage());
			}
		}

	}

	private void setUpTable() {
		orderTable = (LinearLayout) findViewById(R.id.orderTable);
		unorderTable = (ViewGroup) findViewById(R.id.unorderTable);
		xAxis = (LinearLayout) findViewById(R.id.xAxis);

		isCellUsed = new boolean[5][4];
		num = new int[5];

		tvName = new TextView[5][4];
		tvName[0][0] = (TextView) findViewById(R.id.name00);
		tvName[0][1] = (TextView) findViewById(R.id.name01);
		tvName[0][2] = (TextView) findViewById(R.id.name02);
		tvName[0][3] = (TextView) findViewById(R.id.name03);
		tvName[1][0] = (TextView) findViewById(R.id.name10);
		tvName[1][1] = (TextView) findViewById(R.id.name11);
		tvName[1][2] = (TextView) findViewById(R.id.name12);
		tvName[1][3] = (TextView) findViewById(R.id.name13);
		tvName[2][0] = (TextView) findViewById(R.id.name20);
		tvName[2][1] = (TextView) findViewById(R.id.name21);
		tvName[2][2] = (TextView) findViewById(R.id.name22);
		tvName[2][3] = (TextView) findViewById(R.id.name23);
		tvName[3][0] = (TextView) findViewById(R.id.name30);
		tvName[3][1] = (TextView) findViewById(R.id.name31);
		tvName[3][2] = (TextView) findViewById(R.id.name32);
		tvName[3][3] = (TextView) findViewById(R.id.name33);
		tvName[4][0] = (TextView) findViewById(R.id.name40);
		tvName[4][1] = (TextView) findViewById(R.id.name41);
		tvName[4][2] = (TextView) findViewById(R.id.name42);
		tvName[4][3] = (TextView) findViewById(R.id.name43);

		ivAvatar = new ImageView[5][4];
		ivAvatar[0][0] = (ImageView) findViewById(R.id.avatar00);
		ivAvatar[0][1] = (ImageView) findViewById(R.id.avatar01);
		ivAvatar[0][2] = (ImageView) findViewById(R.id.avatar02);
		ivAvatar[0][3] = (ImageView) findViewById(R.id.avatar03);
		ivAvatar[1][0] = (ImageView) findViewById(R.id.avatar10);
		ivAvatar[1][1] = (ImageView) findViewById(R.id.avatar11);
		ivAvatar[1][2] = (ImageView) findViewById(R.id.avatar12);
		ivAvatar[1][3] = (ImageView) findViewById(R.id.avatar13);
		ivAvatar[2][0] = (ImageView) findViewById(R.id.avatar20);
		ivAvatar[2][1] = (ImageView) findViewById(R.id.avatar21);
		ivAvatar[2][2] = (ImageView) findViewById(R.id.avatar22);
		ivAvatar[2][3] = (ImageView) findViewById(R.id.avatar23);
		ivAvatar[3][0] = (ImageView) findViewById(R.id.avatar30);
		ivAvatar[3][1] = (ImageView) findViewById(R.id.avatar31);
		ivAvatar[3][2] = (ImageView) findViewById(R.id.avatar32);
		ivAvatar[3][3] = (ImageView) findViewById(R.id.avatar33);
		ivAvatar[4][0] = (ImageView) findViewById(R.id.avatar40);
		ivAvatar[4][1] = (ImageView) findViewById(R.id.avatar41);
		ivAvatar[4][2] = (ImageView) findViewById(R.id.avatar42);
		ivAvatar[4][3] = (ImageView) findViewById(R.id.avatar43);

		tvNum = new TextView[5];
		tvNum[0] = (TextView) findViewById(R.id.num0);
		tvNum[1] = (TextView) findViewById(R.id.num1);
		tvNum[2] = (TextView) findViewById(R.id.num2);
		tvNum[3] = (TextView) findViewById(R.id.num3);
		tvNum[4] = (TextView) findViewById(R.id.num4);
	}

	private void setUpUI() {
		theme = party.getTheme();
		title = theme.length() > 12 ? theme.substring(0, 12) + ".." : theme;
		setTitle(title + "(" + DBPartying.getNumByPartyId(this, partyId)
				+ getString(R.string.text_people) + ")");
	}

	private void setUpLocationClientIfNeeded() {
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(this, this, this);
		}
	}

	private void setUpMapIfNeeded() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		UiSettings mUiSettings = mMap.getUiSettings();
		mUiSettings.setCompassEnabled(false);
		mUiSettings.setTiltGesturesEnabled(false);
		mUiSettings.setRotateGesturesEnabled(false);
		mUiSettings.setZoomControlsEnabled(false);
		if (party != null) {
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(Double.parseDouble(latitude), Double
							.parseDouble(longitude)), 14));
			Marker marker = mMap.addMarker(new MarkerOptions()
					.position(
							new LatLng(Double.parseDouble(latitude), Double
									.parseDouble(longitude)))
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.icon_address))
					.title(address));
			marker.showInfoWindow();
		}
	}

	public void onParytAddClicked(View view) {
		if (partyStatus == StatusConstants.PARTY_STATUS_ONGOING) {
			// acquire wxapi
			IWXAPI api = WXAPIFactory.createWXAPI(this, Constants.WX_APP_ID);
			StringBuilder params = new StringBuilder();
			params.append("receiveParty.action?");
			params.append("deviceGeneratedPartyId=" + partyId);
			String title = getString(R.string.text_message_title);
			StringBuilder description = new StringBuilder();
			description.append(SharePrefsUtils.getNickname(this));
			description.append(getString(R.string.text_originate_party));
			description.append("\n");
			description.append(getString(R.string.text_theme));
			description.append(": ");
			description.append(theme);
			WeChatUtils.reqWXFriends(getApplicationContext(), api,
					params.toString(), title, description.toString());
		}
	}

	public void onPartyQuitClicked(View view) {
		if (partyStatus == StatusConstants.PARTY_STATUS_ONGOING) {
			new AlertDialog.Builder(ReceivePartyActivity.this)
					.setTitle(getString(R.string.text_quit_party))
					.setMessage(getString(R.string.text_quit_party_hint))
					.setPositiveButton(getString(R.string.btn_confirm),
							new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Thread thread = new Thread(new Runnable() {

										@Override
										public void run() {
											isUpdating = false;

											Map<String, String> map = new HashMap<String, String>();
											map.put("userId",
													SharePrefsUtils
															.getUserId(ReceivePartyActivity.this));
											map.put("deviceGeneratedPartyId",
													partyId);
											String result = HttpRequestUtils
													.post("quitParty", map);

											try {
												JSONObject json = new JSONObject(
														result);
												String returnMessage = json
														.getString("returnMessage");
												System.out.println("------>>"
														+ returnMessage);
												if (returnMessage
														.equals(HttpConstants.QUIT_PARTY_SUCCESS)) {
													// delete from database
													DBParty.deleteById(
															ReceivePartyActivity.this,
															partyId);
													DBPartying
															.deleteById(
																	ReceivePartyActivity.this,
																	partyId);
												} else {
													System.out
															.println("quitParty ---->> "
																	+ returnMessage);
												}
											} catch (JSONException e) {
												System.out.println("quitParty ---->> "
														+ e.getMessage());
											}
										}
									});
									thread.start();

									setResult(ActivityConstants.RESULT_UPDATE_UI);
									finish();
								}
							})
					.setNegativeButton(getString(R.string.btn_cancel), null)
					.create().show();
		}
	}

	private class UpdateViewThread extends AsyncTask<Void, Integer, Void> {
		final int UPDATE_ERROR = -1;
		final int UPDATE_ONGOING = 0;
		final int UPDATE_STOP = 1;

		@Override
		protected void onPreExecute() {
			isUpdating = true;
			partyingList = DBPartying.getByPartyId(ReceivePartyActivity.this,
					partyId);
			updateTable();
		}

		@Override
		protected Void doInBackground(Void... params) {
			while (isUpdating) {
				if (validateExpiration() == false) {
					DBParty.updateStatus(ReceivePartyActivity.this, partyId);
					partyStatus = StatusConstants.PARTY_STATUS_OVER;
					publishProgress(UPDATE_STOP);
					isUpdating = false;
					return null;
				}

				// get the other's interval from server
				Map<String, String> map = new HashMap<String, String>();
				map.put("userId",
						SharePrefsUtils.getUserId(ReceivePartyActivity.this));
				map.put("deviceGeneratedPartyId", partyId);
				String response = HttpRequestUtils.post("updateOthersPartying",
						map);

				try {
					JSONObject json = new JSONObject(response);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.UPDATE_PARTYING_SUCCESS)) {
						// clear the previous partying in the database
						DBPartying.deleteByPartyIdExceptSelf(
								ReceivePartyActivity.this, partyId);

						String temp = json.getString("returnPartyingList");
						// System.out.println("temp = " + temp);
						// save to database
						if (temp.length() > 0) {
							String[] partyingArray = temp.split("&");
							for (int i = 0; i < partyingArray.length; i++) {
								String[] partyingStr = partyingArray[i]
										.split(",");
								Partying partying = new Partying();
								partying.setPartyId(partyId);
								partying.setUserId(partyingStr[0]);
								partying.setNickName(partyingStr[1]);
								partying.setInterval(Integer
										.parseInt(partyingStr[2]));
								DBPartying.insert(getApplicationContext(),
										partying);
							}
						}

						// get the data from database
						partyingList = DBPartying.getByPartyId(
								ReceivePartyActivity.this, partyId);
						System.out.println("partying list size = "
								+ partyingList.size());
						// refresh the table
						publishProgress(UPDATE_ONGOING);
					}
				} catch (JSONException e1) {
					e1.printStackTrace();
				}

				SystemClock.sleep(10000);
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			if (values[0] == UPDATE_ONGOING) {
				setTitle(title + "(" + partyingList.size()
						+ getString(R.string.text_people) + ")");
				updateTable();
			} else if (values[0] == UPDATE_STOP) {
				Toast.makeText(ReceivePartyActivity.this,
						getString(R.string.text_party_over), Toast.LENGTH_SHORT)
						.show();
				setTitle(title + "(" + getString(R.string.text_party_over)
						+ ")");
				updateTable();
			} else if (values[0] == UPDATE_ERROR) {
				Toast.makeText(ReceivePartyActivity.this,
						HttpConstants.HTTP_POST_RESPONSE_EXCEPTION,
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void updateTable() {
		if (isStartToShareLocation() && isUpdating) {
			orderTable.setVisibility(View.VISIBLE);
			xAxis.setVisibility(View.VISIBLE);
			unorderTable.setVisibility(View.INVISIBLE);
			updateOrderTable();
		} else {
			orderTable.setVisibility(View.INVISIBLE);
			xAxis.setVisibility(View.INVISIBLE);
			unorderTable.setVisibility(View.VISIBLE);
			updateUnOrderTable();
		}
	}

	private void updateOrderTable() {
		// reset the table
		for (int i = 0; i < isCellUsed.length; i++) {
			for (int j = 0; j < ivAvatar[i].length; j++) {
				isCellUsed[i][j] = false;
			}
		}
		for (int i = 0; i < tvName.length; i++) {
			for (int j = 0; j < tvName[i].length; j++) {
				tvName[i][j].setText("");
			}
		}
		for (int i = 0; i < ivAvatar.length; i++) {
			for (int j = 0; j < ivAvatar[i].length; j++) {
				ivAvatar[i][j].setImageBitmap(null);
			}
		}

		for (int i = 0; i < partyingList.size(); i++) {
			switch (partyingList.get(i).getInterval()) {
			case 1:
				updateCell(0, partyingList.get(i));
				break;
			case 2:
				updateCell(1, partyingList.get(i));
				break;
			case 3:
				updateCell(2, partyingList.get(i));
				break;
			case 4:
				updateCell(3, partyingList.get(i));
				break;
			case 5:
				updateCell(4, partyingList.get(i));
				break;
			}
		}
		updateNum();
	}

	private void updateCell(int column, final Partying partying) {
		for (int row = 0; row < isCellUsed[column].length; row++) {
			if (isCellUsed[column][row]) {
				continue;
			} else {
				tvName[column][row].setText(partying.getNickName());

				byte[] photo = DBAvatar.getAvatar(this, partying.getUserId());
				if (photo != null) {
					Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
							photo.length);
					ivAvatar[column][row].setImageBitmap(avatar);
				} else {
					ivAvatar[column][row]
							.setImageResource(R.drawable.icon_avatar_dark);

					Thread thread = new Thread(new Runnable() {

						@Override
						public void run() {
							// Moves the current Thread into the background
							android.os.Process
									.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
							ImageUtils.downloadAvatar(
									ReceivePartyActivity.this,
									partying.getUserId());
						}
					});
					thread.start();
				}

				isCellUsed[column][row] = true;
				break;
			}
		}
	}

	private void updateNum() {
		// reset the value
		for (int i = 0; i < num.length; i++) {
			num[i] = 0;
		}

		for (int i = 0; i < partyingList.size(); i++) {
			num[partyingList.get(i).getInterval() - 1]++;
		}
		for (int i = 0; i < num.length; i++) {
			if (num[i] > 0) {
				tvNum[i].setText(num[i] + getString(R.string.text_people));
			} else {
				tvNum[i].setText("");
			}
		}
	}

	private void updateUnOrderTable() {
		unorderTable.removeAllViews();
		partyingList = DBPartying.getByPartyId(ReceivePartyActivity.this,
				partyId);
		for (int i = 0; i < partyingList.size(); i++) {
			TableCellView item = new TableCellView(this, unorderTable,
					partyingList.get(i).getUserId(), partyingList.get(i)
							.getNickName());
			unorderTable.addView(item.getInstance());
		}
	}

	private boolean validateExpiration() {
		if (System.currentTimeMillis() >= Long.valueOf(party.getEndTime())) {
			return false;
		} else {
			return true;
		}
	}

	private boolean isStartToShareLocation() {
		if (System.currentTimeMillis() >= Long.valueOf(party.getStartTime())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle connectionHint) {
		mLocationClient.requestLocationUpdates(REQUEST, this);
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

}
