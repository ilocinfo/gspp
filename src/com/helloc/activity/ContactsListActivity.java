package com.helloc.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.adapter.FriendsListviewAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.database.DBInvitation;
import com.helloc.model.Friends;
import com.helloc.model.Invitation;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.PinyinUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.utils.SystemUtils;
import com.helloc.view.LoadingDialog;

public class ContactsListActivity extends SherlockActivity {
	private String TAG = "FriendsListActivity";

	private Button newInvitations;
	private ListView friendListView;
	private ArrayList<Friends> listItems;
	private FriendsListviewAdapter friendAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts_list);
		setTitle(getString(R.string.action_contacts));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		newInvitations = (Button) findViewById(R.id.new_invitations);
		friendListView = (ListView) findViewById(R.id.friends_list);

		// if it is the first time enter this activity, refresh contacts list
		// and recommend friends
		if (SharePrefsUtils.isRefreshContacts(this) == false) {
			new GetInvitationList().execute();
		}

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("updateNotice");
		this.registerReceiver(this.mBroadcastReceiver, intentFilter);
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateNotice();
		updateListView();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.unregisterReceiver(this.mBroadcastReceiver);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_add)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_ALWAYS
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onAddButtonClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i(TAG, "update UI");
			updateNotice();
			updateListView();
		}
	};

	private void updateNotice() {
		// if there is a new notice , show the new notice button
		if (SharePrefsUtils.isNewFriendsNotice(this)) {
			newInvitations.setVisibility(View.VISIBLE);
			int count = DBInvitation.getNotHandleNum(this);
			if (count > 0) {
				newInvitations.setText(String.valueOf(count));
			}
		} else {
			newInvitations.setVisibility(View.INVISIBLE);
		}
	}

	public void updateListView() {
		listItems = DBFriend.getAll(this);
		Collections.sort(listItems, new ComparatorPY());

		friendAdapter = new FriendsListviewAdapter(this, listItems);
		friendListView.setAdapter(friendAdapter);
		friendListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Intent intent = new Intent();
				intent.setClass(ContactsListActivity.this,
						UserProfileActivity.class);
				intent.putExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID,
						listItems.get(position).getUserId());
				startActivity(intent);
			}
		});
	}

	private class ComparatorPY implements Comparator<Friends> {

		@Override
		public int compare(Friends lhs, Friends rhs) {
			String str1 = PinyinUtils.getPingYin(lhs.getName());
			String str2 = PinyinUtils.getPingYin(rhs.getName());
			return str1.compareToIgnoreCase(str2);
		}

	}

	private class GetInvitationList extends AsyncTask<String, Void, String> {
		StringBuilder contactsStr = new StringBuilder();
		Dialog dialog;
		Cursor cursor;

		@Override
		protected void onPreExecute() {
			dialog = LoadingDialog.createLoadingDialog(
					ContactsListActivity.this,
					getString(R.string.toast_synchronizing));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			cursor = getContentResolver().query(ContactsQuery.CONTENT_URI,
					ContactsQuery.PROJECTION, ContactsQuery.SELECTION, null,
					ContactsQuery.SORT_ORDER);
			if (cursor.moveToFirst()) {
				do {
					String phoneStr = cursor
							.getString(ContactsQuery.PHONE_NUMBER);
					// only regard phone' length bigger than 8 as a real phone
					// number
					if (phoneStr.length() < 8) {
						continue;
					}

					// if the phone user already be my friend, no need to invite
					// again
					if (DBFriend.isMyFriendByPhone(ContactsListActivity.this,
							phoneStr)) {
						continue;
					}

					contactsStr.append(cursor.getString(
							ContactsQuery.PHONE_NUMBER).replaceAll(" ", ""));
					contactsStr.append(",");
					contactsStr.append(cursor
							.getString(ContactsQuery.DISPLAY_NAME));
					contactsStr.append("$");
				} while (cursor.moveToNext());
			}
			cursor.close();
			if (TextUtils.isEmpty(contactsStr)) {
				return null;
			} else {
				Map<String, String> map = new HashMap<String, String>();
				map.put("contactList", contactsStr.toString());
				// System.out.println("contactList = " + contactsStr);
				return HttpRequestUtils.post("uploadContactList", map);
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			dialog.dismiss();

			// System.out.println("result = " + result);
			if (result == null) {
				return;
			}
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage
						.equals(HttpConstants.UPLOAD_CONTACT_LIST_SUCCESS)) {
					String returnList = json.getString("returnList");
					if (TextUtils.isEmpty(returnList) == false) {
						if (saveInvitationList(returnList)) {
							SharePrefsUtils.setNewFriendsNotice(
									ContactsListActivity.this, true);
							updateNotice();
						}
					}

					SharePrefsUtils.setRefreshContacts(
							ContactsListActivity.this, true);
				} else {
					System.out.println("uploadContactList, returnMessage = "
							+ returnMessage);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private boolean saveInvitationList(String input) {
		boolean flag = false;
		String[] item = input.split("&");
		for (int i = 0; i < item.length; i++) {
			String[] temp = item[i].split(",");
			String userId = temp[2];
			// if the user is not my friend and myself, add to invitation list
			if (DBFriend.isMyFriendById(this, userId) == false
					&& userId.equals(SharePrefsUtils.getUserId(this)) == false) {
				// String name = temp[1]; // the name store in user's phone
				flag = true;
				Invitation invitation = new Invitation();
				invitation.setPhone(temp[0]);
				invitation.setUserId(userId);
				invitation.setNickName(temp[3]);
				invitation
						.setInvitationMsg(getString(R.string.text_contacts_friend));
				invitation
						.setStatus(StatusConstants.INVITATION_STATUS_RECOMMEND);

				DBInvitation.insert(this, invitation);
			}
		}
		return flag;
	}

	public void onNewFriendsItemClicked(View view) {
		Intent intent = new Intent();
		intent.setClass(ContactsListActivity.this, NewFriendsListActivity.class);
		startActivity(intent);
	}

	private void onAddButtonClicked() {
		Intent intent = new Intent(ContactsListActivity.this,
				SearchContactsActivity.class);
		startActivity(intent);
	}

	public interface ContactsQuery {
		// A content URI for the Contacts table
		final static Uri CONTENT_URI = CommonDataKinds.Phone.CONTENT_URI;

		@SuppressLint("InlinedApi")
		final static String SELECTION = (SystemUtils.hasHoneycomb() ? CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY
				: CommonDataKinds.Phone.DISPLAY_NAME)
				+ "<>''";

		final static String SORT_ORDER = CommonDataKinds.Phone.NUMBER;

		@SuppressLint("InlinedApi")
		final static String[] PROJECTION = {
				SystemUtils.hasHoneycomb() ? CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY
						: CommonDataKinds.Phone.DISPLAY_NAME,
				CommonDataKinds.Phone.NUMBER, };

		// The query column numbers which map to each value in the projection
		final static int DISPLAY_NAME = 0;
		final static int PHONE_NUMBER = 1;
	}

}
