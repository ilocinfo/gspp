package com.helloc.activity;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.database.DBAvatar;
import com.helloc.model.Avatar;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.SharePrefsUtils;

public class ProfileActivity extends SherlockActivity {
	private LinearLayout registeredInfo;
	private TextView tvNickName;
	private TextView tvAccount;
	private TextView tvPhone;
	private TextView tvEmail;
	private ImageView avatar;

	private Uri mImageCaptureUri;
	private static final int PICK_FROM_CAMERA = 1;
	private static final int CROP_FROM_CAMERA = 2;
	private static final int PICK_FROM_FILE = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		setTitle(getString(R.string.action_my_profile));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		registerForContextMenu(findViewById(R.id.avatarItem));

		registeredInfo = (LinearLayout) findViewById(R.id.registeredInfo);
		tvNickName = (TextView) findViewById(R.id.nickname);
		tvAccount = (TextView) findViewById(R.id.account);
		tvPhone = (TextView) findViewById(R.id.phone);
		tvEmail = (TextView) findViewById(R.id.email);
		avatar = (ImageView) findViewById(R.id.avatar);

		mImageCaptureUri = Uri.fromFile(new File(Constants.HELLOC_FOLDER
				+ "temp.jpg"));
	}

	@Override
	protected void onResume() {
		super.onResume();
		supportInvalidateOptionsMenu();
		if (SharePrefsUtils.isLoginAsGuest(this)) {
			registeredInfo.setVisibility(View.INVISIBLE);
		} else {
			registeredInfo.setVisibility(View.VISIBLE);
			tvAccount.setText(SharePrefsUtils.getUuid(this));
			tvPhone.setText(SharePrefsUtils.getPhone(this));
			tvEmail.setText(SharePrefsUtils.getEmail(this));
		}
		tvNickName.setText(SharePrefsUtils.getNickname(this));

		byte[] photo = DBAvatar
				.getAvatar(this, SharePrefsUtils.getUserId(this));
		if (photo != null) {
			avatar.setImageBitmap(BitmapFactory.decodeByteArray(photo, 0,
					photo.length));
		} else {
			// TODO download my avatar
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (SharePrefsUtils.isLoginAsGuest(this)) {
			menu.add(0, 0, 0, getString(R.string.action_register))
					.setShowAsAction(
							MenuItem.SHOW_AS_ACTION_IF_ROOM
									| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onRegisterClicked();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, 0, 0, getString(R.string.action_take_photo));
		menu.add(0, 1, 1, getString(R.string.action_choose_photo));
	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			onCameraClicked();
			break;
		case 1:
			onGalleryClicked();
			break;
		}
		return super.onContextItemSelected(item);
	}

	private void onRegisterClicked() {
		Intent intent = new Intent();
		intent.setClass(ProfileActivity.this, RegisterActivity.class);
		startActivity(intent);
	}

	private void onCameraClicked() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
				mImageCaptureUri);
		try {
			intent.putExtra("return-data", true);

			startActivityForResult(intent, PICK_FROM_CAMERA);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void onGalleryClicked() {
		// Intent intent = new Intent();
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		//intent.setType("image/*");
		//intent.setAction(Intent.ACTION_GET_CONTENT);

		startActivityForResult(
				Intent.createChooser(intent, "Complete action using"),
				PICK_FROM_FILE);
	}

	private void doCrop() {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, 0);

		int size = list.size();

		if (size == 0) {
			Toast.makeText(this, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			intent.setData(mImageCaptureUri);

			intent.putExtra("outputX", 200);
			intent.putExtra("outputY", 200);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);

			Intent i = new Intent(intent);
			ResolveInfo res = list.get(0);

			i.setComponent(new ComponentName(res.activityInfo.packageName,
					res.activityInfo.name));

			startActivityForResult(i, CROP_FROM_CAMERA);
		}
	}

	private void uploadAvatar(final Bitmap photo) {
		new AsyncTask<String, Long, String>() {

			@Override
			protected String doInBackground(String... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("userId",
						SharePrefsUtils.getUserId(ProfileActivity.this));
				map.put("image", ImageUtils.encodeBitmapToString(photo));

				return HttpRequestUtils.post("uploadAvatar", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.UPLOAD_AVATAR_SUCCESS)) {

					} else {
						System.out.println("uploadAvatar ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("uploadAvatar ---->> " + e.getMessage());
				}
			}

		}.execute();
	}

	private void saveAvatar(Bitmap photo) {
		Avatar avatar = new Avatar();
		avatar.setUserId(SharePrefsUtils.getUserId(ProfileActivity.this));
		avatar.setPhoto(ImageUtils.convertBitmapToByte(photo));
		DBAvatar.insert(this, avatar);
	}

	public void onAvatarItemCliked(View view) {
		this.openContextMenu(view);
	}

	public void onNickNameItemClicked(View view) {
		Intent intent = new Intent(ProfileActivity.this,
				ProfileNameActivity.class);
		startActivity(intent);
	}

	public void onPhoneClicked(View view) {
		Intent intent = new Intent(ProfileActivity.this,
				ProfilePhoneActivity.class);
		startActivity(intent);
	}

	public void onEmailClicked(View view) {
		Intent intent = new Intent(ProfileActivity.this,
				ProfileEmailActivity.class);
		startActivity(intent);
	}

	public void onPasswordItemClicked(View view) {
		Intent intent = new Intent(ProfileActivity.this,
				ProfilePasswordActivity.class);
		startActivity(intent);
	}

	public void onLocationsClicked(View view) {
		Intent intent = new Intent(ProfileActivity.this,
				LocationsActivity.class);
		startActivity(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;
		switch (requestCode) {
		case PICK_FROM_CAMERA:
			doCrop();
			break;
		case PICK_FROM_FILE:
			mImageCaptureUri = data.getData();
			doCrop();
			break;
		case CROP_FROM_CAMERA:
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				saveAvatar(photo);
				uploadAvatar(photo);
			}
			break;
		}
	}
}
