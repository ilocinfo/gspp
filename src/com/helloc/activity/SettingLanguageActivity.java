package com.helloc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.utils.SharePrefsUtils;

public class SettingLanguageActivity extends SherlockActivity {

	private RadioButton cbAuto;
	private RadioButton cbSimplified;
	private RadioButton cbTraditional;
	private RadioButton cbEnglish;
	private int language;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_language);
		setTitle(getString(R.string.text_language));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		cbAuto = (RadioButton) findViewById(R.id.cbAuto);
		cbSimplified = (RadioButton) findViewById(R.id.cbSimplified);
		cbTraditional = (RadioButton) findViewById(R.id.cbTraditional);
		cbEnglish = (RadioButton) findViewById(R.id.cbEnglish);

		language = SharePrefsUtils.getLanguage(this);
		if (language < 0) {
			language = Constants.LANGUAGE_AUTO;
		}
		switch (language) {
		case Constants.LANGUAGE_AUTO:
			cbAuto.setChecked(true);
			break;
		case Constants.LANGUAGE_SIMPLIFIED:
			cbSimplified.setChecked(true);
			break;
		case Constants.LANGUAGE_TRADITIONAL:
			cbTraditional.setChecked(true);
			break;
		case Constants.LANGUAGE_ENGLISH:
			cbEnglish.setChecked(true);
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.btn_save)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_ALWAYS
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			if (language != SharePrefsUtils.getLanguage(this)) {
				SharePrefsUtils.setLanguage(this, language);
				Intent i = getBaseContext().getPackageManager()
						.getLaunchIntentForPackage(
								getBaseContext().getPackageName());
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onAutoItemClicked(View view) {
		language = Constants.LANGUAGE_AUTO;
		cbAuto.setChecked(true);
		cbSimplified.setChecked(false);
		cbTraditional.setChecked(false);
		cbEnglish.setChecked(false);
	}

	public void onSimplifiedItemClicked(View view) {
		language = Constants.LANGUAGE_SIMPLIFIED;
		cbAuto.setChecked(false);
		cbSimplified.setChecked(true);
		cbTraditional.setChecked(false);
		cbEnglish.setChecked(false);
	}

	public void onTraditionalItemClicked(View view) {
		language = Constants.LANGUAGE_TRADITIONAL;
		cbAuto.setChecked(false);
		cbSimplified.setChecked(false);
		cbTraditional.setChecked(true);
		cbEnglish.setChecked(false);
	}

	public void onEnglishItemClicked(View view) {
		language = Constants.LANGUAGE_ENGLISH;
		cbAuto.setChecked(false);
		cbSimplified.setChecked(false);
		cbTraditional.setChecked(false);
		cbEnglish.setChecked(true);
	}
}
