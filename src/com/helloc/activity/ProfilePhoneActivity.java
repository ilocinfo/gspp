package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.HttpConstants;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.NetworkUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class ProfilePhoneActivity extends SherlockActivity {
	private EditText editPhone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_phone);
		setTitle(getString(R.string.action_phone));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		editPhone = (EditText) findViewById(R.id.editPhone);

		editPhone.setText(SharePrefsUtils.getPhone(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_done)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onDoneClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	private void onDoneClicked() {
		if (SharePrefsUtils.getPhone(this).equals(editPhone.getText())
				|| TextUtils.isEmpty(editPhone.getText())) {
			return;
		} else if (editPhone.getText().toString().length() < 8) {
			Toast.makeText(this, getString(R.string.toast_phone_too_short),
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (NetworkUtils.isConnect(this) == false) {
			return;
		}
		// update phone
		new AsyncTask<String, Long, String>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = LoadingDialog.createLoadingDialog(
						ProfilePhoneActivity.this,
						getString(R.string.toast_processing));
				dialog.show();
			}

			@Override
			protected String doInBackground(String... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("userId",
						SharePrefsUtils.getUserId(ProfilePhoneActivity.this));
				map.put("phone", editPhone.getText().toString());

				return HttpRequestUtils.post("updatePhone", map);
			}

			@Override
			protected void onPostExecute(String result) {
				dialog.dismiss();
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage.equals(HttpConstants.UPDATE_SUCCESS)) {
						SharePrefsUtils.setPhone(ProfilePhoneActivity.this,
								editPhone.getText().toString());
						finish();
					} else {
						Toast.makeText(ProfilePhoneActivity.this,
								getString(R.string.toast_update_failed),
								Toast.LENGTH_SHORT).show();

						System.out.println("updatePhone ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					Toast.makeText(ProfilePhoneActivity.this,
							getString(R.string.toast_update_failed),
							Toast.LENGTH_SHORT).show();

					System.out.println("updatePhone ---->> " + e.getMessage());
				}
			}
		}.execute();
	}
}
