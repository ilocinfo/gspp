package com.helloc.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;

public class SettingNotificationActivity extends SherlockActivity {
	private CheckBox soundCB;
	private CheckBox vibrateCB;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_notifications);
		setTitle(getString(R.string.action_notifications));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		soundCB = (CheckBox) findViewById(R.id.sound);
		vibrateCB = (CheckBox) findViewById(R.id.vibrate);

		prefs = getSharedPreferences(ActivityConstants.SHARE_PREFERENCES_NAME,
				Context.MODE_PRIVATE);
		if (prefs.getBoolean(
				ActivityConstants.SHARE_PREFS_ITEM_NOTIFICATIONS_SOUND, true)) {
			soundCB.setChecked(true);
		}
		if (prefs.getBoolean(
				ActivityConstants.SHARE_PREFS_ITEM_NOTIFICATIONS_VIBRATE, true)) {
			vibrateCB.setChecked(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onSoundItemClicked(View view) {
		if (soundCB.isChecked()) {
			soundCB.setChecked(false);
			prefs.edit()
					.putBoolean(
							ActivityConstants.SHARE_PREFS_ITEM_NOTIFICATIONS_SOUND,
							false).commit();
		} else {
			soundCB.setChecked(true);
			prefs.edit()
					.putBoolean(
							ActivityConstants.SHARE_PREFS_ITEM_NOTIFICATIONS_SOUND,
							true).commit();
		}
	}

	public void onVibrateItemClicked(View view) {
		if (vibrateCB.isChecked()) {
			vibrateCB.setChecked(false);
			prefs.edit()
					.putBoolean(
							ActivityConstants.SHARE_PREFS_ITEM_NOTIFICATIONS_VIBRATE,
							false).commit();
		} else {
			vibrateCB.setChecked(true);
			prefs.edit()
					.putBoolean(
							ActivityConstants.SHARE_PREFS_ITEM_NOTIFICATIONS_VIBRATE,
							true).commit();
		}
	}

}
