package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.HttpConstants;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.NetworkUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class ProfileEmailActivity extends SherlockActivity {
	private EditText editEmail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_email);
		setTitle(getString(R.string.action_email));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		editEmail = (EditText) findViewById(R.id.editEmail);

		editEmail.setText(SharePrefsUtils.getEmail(this));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_done)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onDoneClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	private void onDoneClicked() {
		if (SharePrefsUtils.getEmail(this).equals(editEmail.getText())
				|| TextUtils.isEmpty(editEmail.getText())) {
			return;
		}

		if (NetworkUtils.isConnect(this) == false) {
			return;
		}
		
		// update email
		new AsyncTask<String, Long, String>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = LoadingDialog.createLoadingDialog(
						ProfileEmailActivity.this,
						getString(R.string.toast_processing));
				dialog.show();
			}

			@Override
			protected String doInBackground(String... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("userId",
						SharePrefsUtils.getUserId(ProfileEmailActivity.this));
				map.put("email", editEmail.getText().toString());

				return HttpRequestUtils.post("updateEmail", map);
			}

			@Override
			protected void onPostExecute(String result) {
				dialog.dismiss();
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage.equals(HttpConstants.UPDATE_SUCCESS)) {
						SharePrefsUtils.setEmail(ProfileEmailActivity.this,
								editEmail.getText().toString());
						finish();
					} else {
						Toast.makeText(ProfileEmailActivity.this,
								getString(R.string.toast_update_failed),
								Toast.LENGTH_SHORT).show();
						System.out.println("updateEmail ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					Toast.makeText(ProfileEmailActivity.this,
							getString(R.string.toast_update_failed),
							Toast.LENGTH_SHORT).show();
					System.out.println("updateEmail ---->> " + e.getMessage());
				}

			}
		}.execute();
	}
}
