package com.helloc.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;

public class ShowPartyDetailsActivity extends SherlockActivity {

	private TextView theme;
	private TextView originator;
	private LinearLayout descriptionItem;
	private TextView description;
	private TextView location;
	private TextView startTime;
	private TextView endTime;
	private DateFormat formatter;
	private Calendar calendar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_party_details);
		setTitle(getString(R.string.action_details));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		formatter = new SimpleDateFormat("yyy-MM-dd hh:mm a", getResources()
				.getConfiguration().locale);
		calendar = Calendar.getInstance();

		theme = (TextView) findViewById(R.id.theme);
		originator = (TextView) findViewById(R.id.originator);
		descriptionItem = (LinearLayout) findViewById(R.id.descriptionItem);
		description = (TextView) findViewById(R.id.description);
		location = (TextView) findViewById(R.id.location);
		startTime = (TextView) findViewById(R.id.start_time);
		endTime = (TextView) findViewById(R.id.end_time);

		theme.setText(getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_THEME));
		description.setText(getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_DESCRIPTION));
		if (TextUtils.isEmpty(description.getText().toString())) {
			descriptionItem.setVisibility(View.GONE);
		}
		location.setText(getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_ADDRESS));
		startTime.setText(getStartTime());
		endTime.setText(getEndTime());

		String originatorStr = getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_ORIGINATOR);
		if (TextUtils.isEmpty(originatorStr)) {
			originator.setText(getString(R.string.text_myself));
		} else {
			originator.setText(originatorStr);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private String getStartTime() {
		calendar.setTimeInMillis(Long.parseLong(getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_START_TIME)));
		return formatter.format(calendar.getTime());
	}

	private String getEndTime() {
		calendar.setTimeInMillis(Long.parseLong(getIntent().getStringExtra(
				ActivityConstants.ARGS_PARTY_END_TIME)));
		return formatter.format(calendar.getTime());
	}

}
