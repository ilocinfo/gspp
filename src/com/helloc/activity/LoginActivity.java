package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.database.DBAvatar;
import com.helloc.database.DBFriend;
import com.helloc.database.DBInvitation;
import com.helloc.database.DBLocations;
import com.helloc.database.DBMapSnapshot;
import com.helloc.database.DBMessage;
import com.helloc.database.DBMessageReply;
import com.helloc.database.DBParty;
import com.helloc.database.DBPartying;
import com.helloc.model.Friends;
import com.helloc.utils.GCMUtils;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.NetworkUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class LoginActivity extends SherlockActivity {

	private EditText account;
	private EditText password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		account = (EditText) findViewById(R.id.account);
		password = (EditText) findViewById(R.id.password);

		account.setText(SharePrefsUtils.getLoginAccount(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_guest)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == 0) {
			onGuestClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	public void onLoginClicked(View view) {
		// 检查是否连网
		if (NetworkUtils.isConnect(this) == false) {
			return;
		}
		// check the input information
		if (TextUtils.isEmpty(account.getText().toString())) {
			Toast.makeText(getApplication(),
					getString(R.string.toast_input_account), Toast.LENGTH_SHORT)
					.show();
			return;
		} else if (TextUtils.isEmpty(password.getText().toString())) {
			Toast.makeText(getApplication(),
					getString(R.string.toast_input_password),
					Toast.LENGTH_SHORT).show();
			return;
		}
		new AsyncTask<Void, Void, String>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = LoadingDialog.createLoadingDialog(LoginActivity.this,
						getString(R.string.toast_login));
				dialog.show();
			}

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("loginAccount", account.getText().toString());
				map.put("password", password.getText().toString());
				map.put("deviceId",
						SharePrefsUtils.getDeviceId(LoginActivity.this));

				return HttpRequestUtils.post("login", map);
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				dialog.dismiss();

				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.LOGIN_FAILED_NO_SUCH_USER)) {
						Toast.makeText(
								LoginActivity.this,
								getString(R.string.toast_wrong_account_or_password),
								Toast.LENGTH_SHORT).show();
						return;
					} else if (returnMessage
							.equals(HttpConstants.LOGIN_SUCCESS)) {
						// check whether it is a new account
						isNewAccount(json.getString("userId"));
						// set the new feature to false
						SharePrefsUtils.setNewFeatureOdd(LoginActivity.this, false);
						// if get the userId
						SharePrefsUtils.setUserId(LoginActivity.this,
								json.getString("userId"));
						// set is login to true
						SharePrefsUtils.setLogin(LoginActivity.this, true);
						// set loginAsGuest to true
						SharePrefsUtils.setLoginAsGuest(LoginActivity.this,
								false);
						// set login account
						SharePrefsUtils.setLoginAccount(LoginActivity.this,
								account.getText().toString());
						SharePrefsUtils.setUuid(LoginActivity.this,
								json.getString("universalUniqueId"));
						SharePrefsUtils.setPhone(LoginActivity.this,
								json.getString("phone"));
						SharePrefsUtils.setEmail(LoginActivity.this,
								json.getString("email"));
						SharePrefsUtils.setNickName(LoginActivity.this,
								json.getString("nickname"));

						// register to GCM server
						new GCMUtils().GCMRegistration(LoginActivity.this,
								false);
						// download contacts
						downloadFriendsList();
						// download avatar
						downloadAvatarIfNeed();
						// recovery geofence
						GeofenceUtils
								.setUpGeofences(LoginActivity.this, DBMessage
										.getAllNotActivated(LoginActivity.this));
						downloadAvatarIfNeed();
						finish();
					} else {
						System.out.println("login ---->> " + returnMessage);
					}

				} catch (JSONException e) {
					System.out.println("login ---->> " + e.getMessage());
				}
			}
		}.execute();
	}

	public void onRegisterClicked(View view) {
		Intent intent = new Intent();
		intent.setClass(this, RegisterActivity.class);
		startActivityForResult(intent,
				ActivityConstants.REQUEST_GO_TO_REGISTER_ACTIVITY);
	}

	private void onGuestClicked() {
		// 检查是否连网
		if (NetworkUtils.isConnect(this) == false) {
			return;
		}
		new AsyncTask<Void, Void, String>() {
			Dialog dialog;
			String uuid;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = LoadingDialog.createLoadingDialog(LoginActivity.this,
						getString(R.string.toast_register));
				dialog.show();
			}

			@Override
			protected String doInBackground(Void... params) {
				// generate uuid
				uuid = UUID.randomUUID().toString();
				// upload uuid to server and get userId
				Map<String, String> map = new HashMap<String, String>();
				map.put("universalUniqueId", uuid);
				return HttpRequestUtils.post("register", map);
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				dialog.dismiss();
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage.equals(HttpConstants.REGISTER_SUCCEED)) {
						clearPreviousRecord();
						// set the new feature to false
						SharePrefsUtils.setNewFeatureOdd(LoginActivity.this, false);
						// set the user id
						SharePrefsUtils.setUserId(LoginActivity.this,
								json.getString("generatedUserId"));
						// set is login to true
						SharePrefsUtils.setLogin(LoginActivity.this, true);
						// set loginAsGuest to true
						SharePrefsUtils.setLoginAsGuest(LoginActivity.this,
								true);
						// save uuid
						SharePrefsUtils.setUuid(LoginActivity.this, uuid);
						// register to GCM server
						new GCMUtils()
								.GCMRegistration(LoginActivity.this, true);
						finish();
					} else {
						System.out.println("register ---->> " + returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("register ---->> " + e.getMessage());
				}
			}
		}.execute();
	}

	private void clearPreviousRecord() {
		// clear SharedPreferences
		SharePrefsUtils.clear(this);
		// clear database
		DBFriend.clearDatabase(this);
		DBInvitation.clearDatabase(this);
		DBLocations.clearDatabase(this);
		DBMapSnapshot.clearDatabase(this);
		DBMessage.clearDatabase(this);
		DBMessageReply.clearDatabase(this);
		DBParty.clearDatabase(this);
		DBPartying.clearDatabase(this);
	}

	private void isNewAccount(String newUserId) {
		if (newUserId.equals(SharePrefsUtils.getUserId(this)) == false) {
			clearPreviousRecord();
		}
	}

	private void downloadFriendsList() {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("userId", SharePrefsUtils.getUserId(LoginActivity.this));
				return HttpRequestUtils.post("getFriendsList", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.GET_FRIENDS_LIST_SUCCESS)) {
						String friendsList = json.getString("friendsList");
						System.out.println("friendsList -->>" + friendsList);
						String[] users = friendsList.split("&");
						for (int i = 0; i < users.length; i++) {
							String[] user = users[i].split(",");
							Friends friend = new Friends(user[0], user[1],
									user[2], user[3], user[4], user[5]);
							System.out.println(friend);
							DBFriend.insert(LoginActivity.this, friend);
						}
					} else {
						System.out.println("getFriendsList ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("getFriendsList ---->> "
							+ e.getMessage());
				}
			}
		}.execute();
	}

	private void downloadAvatarIfNeed() {
		byte[] photo = DBAvatar
				.getAvatar(this, SharePrefsUtils.getUserId(this));
		if (photo == null) {
			new AsyncTask<Void, Void, byte[]>() {

				@Override
				protected byte[] doInBackground(Void... params) {
					return ImageUtils.downloadAvatar(LoginActivity.this,
							SharePrefsUtils.getUserId(LoginActivity.this));
				}

				@Override
				protected void onPostExecute(byte[] result) {
				}
			}.execute();
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			setResult(ActivityConstants.RESULT_LOGIN_CANCLE);
			finish();
			return super.onKeyDown(keyCode, event);
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ActivityConstants.REQUEST_GO_TO_REGISTER_ACTIVITY) {
			if (resultCode == ActivityConstants.RESULT_REGISTER_SUCCEED) {
				finish();
			}
		}
	}

}
