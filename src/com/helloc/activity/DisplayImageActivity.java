package com.helloc.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.adapter.FullScreenImageAdapter;

public class DisplayImageActivity extends SherlockActivity {
	private ArrayList<String> imageIndex;
	private int index;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Remove System bar (and retain title bar)
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_display_image);
		setTitle(getString(R.string.action_picture));

		setUpViewPager();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpViewPager() {
		imageIndex = getIntent().getStringArrayListExtra("imageIndex");
		index = getIntent().getIntExtra("index", 0);

		ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(new FullScreenImageAdapter(this, imageIndex));
		viewPager.setCurrentItem(index);
	}

}
