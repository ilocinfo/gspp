package com.helloc.activity;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;

public class AboutActivity extends SherlockActivity {
	private TextView version;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		setTitle(getString(R.string.action_about));
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		version = (TextView) findViewById(R.id.version);
		// introduction = (TextView) findViewById(R.id.introduction);
		try {
			version.setText(getString(R.string.app_name)
					+ " "
					+ getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
