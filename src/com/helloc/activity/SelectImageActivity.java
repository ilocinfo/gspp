package com.helloc.activity;

import java.io.FileNotFoundException;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.utils.ImageUtils;
import com.helloc.view.LoadingDialog;

public class SelectImageActivity extends SherlockFragmentActivity {

	private ImageView image;
	private String imageName;
	private String imagePath;

	private Bitmap bitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_image);
		setTitle(getString(R.string.action_picture));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		imageName = getIntent().getStringExtra("imageName");
		imagePath = Constants.IMAGE_FLODER + imageName + ".jpg";

		System.out.println("imagePath = " + imagePath);
		image = (ImageView) findViewById(R.id.image);
		// bitmap = ImageUtils.getDisplayBitmap(imagePath, 480, 720);
		try {
			bitmap = ImageUtils.compressImage(this, imagePath, 480, 720);
			image.setImageBitmap(bitmap);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (bitmap != null) {
			bitmap.recycle();
			bitmap = null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_done)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			new AsyncTask<Void, Void, Void>() {
				Dialog dialog;

				@Override
				protected void onPreExecute() {
					dialog = LoadingDialog.createLoadingDialog(
							SelectImageActivity.this,
							getString(R.string.toast_processing));
					dialog.show();
				}

				@Override
				protected Void doInBackground(Void... params) {
					// save big image
					ImageUtils.saveImage(bitmap, imagePath);

					bitmap.recycle();
					// save thumbnail
					bitmap = ImageUtils.getDisplayBitmap(imagePath, 160, 240);
					imagePath = Constants.THUMBNAIL_FLODER + imageName + ".jpg";
					ImageUtils.saveImage(bitmap, imagePath);

					if (bitmap != null) {
						bitmap.recycle();
						bitmap = null;
					}
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {
					super.onPostExecute(result);
					dialog.dismiss();
					setResult(ActivityConstants.RESULT_RESULT_OK);
					finish();
				}
			}.execute();
		}
		return super.onOptionsItemSelected(item);
	}

}
