package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBAvatar;
import com.helloc.database.DBFriend;
import com.helloc.model.Friends;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.NetworkUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class UserProfileActivity extends SherlockActivity {
	private String userId;
	private ImageView ivAvatar;
	private TextView tvNickname;
	private TextView tvAccount;
	private CheckBox allowAutoReply;
	private TextView autoReplyPermission;
	private Friends friend;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
		setTitle(getString(R.string.action_profile));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		userId = getIntent().getStringExtra(
				ActivityConstants.ARGS_ACCOUNT_USER_ID);

		setUpView();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("updateView");
		this.registerReceiver(this.mBroadcastReceiver, intentFilter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_delete)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_ALWAYS
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.unregisterReceiver(this.mBroadcastReceiver);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == 0) {
			onDeleteFriendClicked();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			updateView();
		}
	};

	private void setUpView() {
		ivAvatar = (ImageView) findViewById(R.id.avatar);
		tvNickname = (TextView) findViewById(R.id.nickname);
		tvAccount = (TextView) findViewById(R.id.account);
		allowAutoReply = (CheckBox) findViewById(R.id.allowAutoReply);
		autoReplyPermission = (TextView) findViewById(R.id.autoReplyPermission);

		updateView();

		// set the avatar
		byte[] photo = DBAvatar.getAvatar(this, userId);
		if (photo != null) {
			Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
					photo.length);
			ivAvatar.setImageBitmap(avatar);
		}
		updateAvatar();

		// set the name
		tvNickname.setText(friend.getName());
		// set the account
		if (TextUtils.isEmpty(friend.getUuid())) {
			tvAccount.setVisibility(View.GONE);
		} else {
			if (friend.getUuid().length() > 20) {
				tvAccount.setVisibility(View.GONE);
			} else {
				tvAccount.setText(getString(R.string.text_account) + ": "
						+ friend.getUuid());
			}
		}
	}

	private void updateAvatar() {
		if (NetworkUtils.inConntectWifi(this)) {
			new AsyncTask<Void, Void, byte[]>() {

				@Override
				protected byte[] doInBackground(Void... params) {
					return ImageUtils.downloadAvatar(UserProfileActivity.this,
							userId);
				}

				@Override
				protected void onPostExecute(byte[] result) {
					if (result != null) {
						ivAvatar.setImageBitmap(BitmapFactory.decodeByteArray(
								result, 0, result.length));
					}
				}
			}.execute();
		}
	}

	private void updateView() {
		friend = DBFriend.getFriendByUserId(this, userId);
		// System.out.println(friend);

		if (friend.getAllowAutoReply() == StatusConstants.AUTO_REPLY_STATUS_ALLOWED) {
			allowAutoReply.setChecked(true);
		} else {
			allowAutoReply.setChecked(false);
		}

		if (friend.getAutoReplyPermission() == StatusConstants.AUTO_REPLY_STATUS_ALLOWED) {
			autoReplyPermission.setText(getString(R.string.text_granted));
		} else {
			autoReplyPermission.setText(getString(R.string.text_not_granted));
		}

		if (friend.getAllowAutoReply() == StatusConstants.AUTO_REPLY_STATUS_PENDING) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(DBFriend.getNameByUserId(this, userId)
					+ getString(R.string.text_request_for_your_auto_reply_message));
			builder.setPositiveButton(getString(R.string.btn_accept),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							new SendAutoreplyPermissionTask().execute();
						}
					});
			builder.setNegativeButton(getString(R.string.btn_reject),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							DBFriend.uploadAllowAutoReply(
									UserProfileActivity.this,
									userId,
									StatusConstants.AUTO_REPLY_STATUS_NOT_ALLOWED);

						}
					});
			builder.create().show();
		}
	}

	public void onAllowAutoReply(View view) {
		if (friend.getAllowAutoReply() == StatusConstants.AUTO_REPLY_STATUS_ALLOWED) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getString(R.string.text_disable_auto_reply_message));
			builder.setPositiveButton(getString(R.string.btn_confirm),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							new SendRemoveAutoreplyPermissionTask().execute();
						}
					});
			builder.setNegativeButton(getString(R.string.btn_cancel), null);
			builder.create().show();
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getString(R.string.text_enable_auto_reply_message));
			builder.setPositiveButton(getString(R.string.btn_confirm),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							new SendAutoreplyPermissionTask().execute();
						}
					});
			builder.setNegativeButton(getString(R.string.btn_cancel), null);
			builder.create().show();
		}
	}

	public void onAutoReplyPermission(View view) {
		if (friend.getAutoReplyPermission() != StatusConstants.AUTO_REPLY_STATUS_ALLOWED) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getString(R.string.text_request_to_enable_auto_reply_message));
			builder.setPositiveButton(getString(R.string.btn_confirm),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							new SendAutoreplyRequestTask().execute();
						}
					});
			builder.setNegativeButton(getString(R.string.btn_cancel), null);
			builder.create().show();
		}
	}

	private void onDeleteFriendClicked() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.text_delete_friend));
		builder.setPositiveButton(getString(R.string.btn_confirm),
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						new DeleteFriendTask().execute();
					}
				});
		builder.setNegativeButton(getString(R.string.btn_cancel), null);
		builder.create().show();
	}

	public void onSendClicked(View view) {
		Intent intent = new Intent();
		intent.setClass(UserProfileActivity.this, SendMessageActivity.class);
		intent.putExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID, userId);
		startActivity(intent);
		finish();
	}

	private class SendAutoreplyRequestTask extends
			AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("senderId",
					SharePrefsUtils.getUserId(UserProfileActivity.this));
			map.put("receiverId", userId);

			return HttpRequestUtils.post("sendAutoreplyRequest", map);
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage
						.equals(HttpConstants.SEND_AUTOREPLY_REQUEST_SUCCESS)) {
					friend = DBFriend.getFriendByUserId(
							UserProfileActivity.this, userId);
				} else {
					System.out.println("sendAutoreplyRequest ---->> "
							+ returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("sendAutoreplyRequest ---->> "
						+ e.getMessage());
			}
		}

	}

	private class SendAutoreplyPermissionTask extends
			AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("senderId",
					SharePrefsUtils.getUserId(UserProfileActivity.this));
			map.put("receiverId", userId);

			return HttpRequestUtils.post("sendAutoreplyPermission", map);
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage
						.equals(HttpConstants.SEND_AUTOREPLY_PERMISSION_SUCCESS)) {
					DBFriend.uploadAllowAutoReply(UserProfileActivity.this,
							userId, StatusConstants.AUTO_REPLY_STATUS_ALLOWED);
					allowAutoReply.setChecked(true);
					friend = DBFriend.getFriendByUserId(
							UserProfileActivity.this, userId);
				} else {
					System.out.println("sendAutoreplyPermission ---->> "
							+ returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("sendAutoreplyPermission ---->> "
						+ e.getMessage());
			}
		}

	}

	private class SendRemoveAutoreplyPermissionTask extends
			AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("senderId",
					SharePrefsUtils.getUserId(UserProfileActivity.this));
			map.put("receiverId", userId);

			return HttpRequestUtils.post("removeAutoreplyPermission", map);
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage
						.equals(HttpConstants.REMOVE_AUTOREPLY_PERMISSION_SUCCESS)) {
					DBFriend.uploadAllowAutoReply(UserProfileActivity.this,
							userId,
							StatusConstants.AUTO_REPLY_STATUS_NOT_ALLOWED);
					allowAutoReply.setChecked(false);
					friend = DBFriend.getFriendByUserId(
							UserProfileActivity.this, userId);
				} else {
					System.out.println("removeAutoreplyPermission ---->> "
							+ returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("removeAutoreplyPermission ---->> "
						+ e.getMessage());
			}
		}

	}

	private class DeleteFriendTask extends AsyncTask<Void, Void, String> {
		Dialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = LoadingDialog.createLoadingDialog(
					UserProfileActivity.this,
					getString(R.string.toast_processing));
			dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("selfId",
					SharePrefsUtils.getUserId(UserProfileActivity.this));
			map.put("friendId", userId);

			return HttpRequestUtils.post("deleteFriend", map);
		}

		@Override
		protected void onPostExecute(String result) {
			dialog.dismiss();
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage.equals(HttpConstants.DELETE_FRIEND_SUCCESS)) {
					DBFriend.deleteFriend(UserProfileActivity.this, userId);
					finish();
				} else {
					System.out.println("deleteFriend ---->> " + returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("deleteFriend ---->> " + e.getMessage());
			}
		}
	}
}
