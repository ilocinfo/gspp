package com.helloc.activity;

import java.util.ArrayList;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.adapter.InvitationListviewAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.database.DBInvitation;
import com.helloc.fragment.SendInvitationDialog;
import com.helloc.fragment.SendInvitationDialog.SendInvitationDialogListener;
import com.helloc.fragment.SendInvitationReplyDialog;
import com.helloc.fragment.SendInvitationReplyDialog.SendInvitationReplyDialogListener;
import com.helloc.utils.SharePrefsUtils;

public class NewFriendsListActivity extends SherlockFragmentActivity implements
	SendInvitationDialogListener, SendInvitationReplyDialogListener{
	private ListView invitationListView;
	private ArrayList<Map<String, String>> invitationData;
	private InvitationListviewAdapter invitationAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_friends_list);
		setTitle(getString(R.string.action_new_friends));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		// remove the new notice once enter the activity
		SharePrefsUtils.setNewFriendsNotice(this, false);
		SharePrefsUtils.setNewInvitation(this, false);

		setUpListView();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpListView() {
		invitationListView = (ListView) findViewById(R.id.invitaion_list);
		invitationData = DBInvitation.getAll(this);
		invitationAdapter = new InvitationListviewAdapter(this, invitationData);
		invitationListView.setAdapter(invitationAdapter);

		invitationListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				final Map<String, String> item = invitationData.get(position);
				// invitation only for non-friends
				if (DBFriend.isMyFriendById(NewFriendsListActivity.this,
						item.get(ActivityConstants.ARGS_ACCOUNT_USER_ID)) == false) {
					int status = Integer.parseInt(item
							.get(ActivityConstants.ARGS_INVITATION_STATUS));
					if (status == StatusConstants.INVITATION_STATUS_RECOMMEND
							|| status == StatusConstants.INVITATION_STATUS_SENT) {
						DialogFragment newFragment = SendInvitationDialog.newInstance(
								item.get(ActivityConstants.ARGS_ACCOUNT_USER_ID),
								item.get(ActivityConstants.ARGS_ACCOUNT_UUID),
								item.get(ActivityConstants.ARGS_ACCOUNT_NICKNAME),
								item.get(ActivityConstants.ARGS_ACCOUNT_PHONE),
								getApplicationContext());
						newFragment.show(getSupportFragmentManager(), "dialog");
					} else if (status == StatusConstants.INVITATION_STATUS_RECEIVED_NOT_HANDLE) {
						DialogFragment newFragment = SendInvitationReplyDialog.newInstance(
								item.get(ActivityConstants.ARGS_ACCOUNT_USER_ID),
								item.get(ActivityConstants.ARGS_ACCOUNT_UUID),
								item.get(ActivityConstants.ARGS_INVITATION_MSG),
								item.get(ActivityConstants.ARGS_ACCOUNT_NICKNAME),
								item.get(ActivityConstants.ARGS_ACCOUNT_PHONE),
								getApplicationContext());
						newFragment.show(getSupportFragmentManager(), "dialog");
					}
				}

			}
		});
	}

	@Override
	public void updateView() {
		setUpListView();
	}

}
