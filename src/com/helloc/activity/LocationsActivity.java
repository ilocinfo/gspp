package com.helloc.activity;

import java.util.ArrayList;
import java.util.Map;

import android.os.Bundle;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.adapter.LocationsListviewAdapter;
import com.helloc.adapter.LocationsListviewAdapter.LocationsListAdapterCallback;
import com.helloc.database.DBLocations;

public class LocationsActivity extends SherlockActivity implements
		LocationsListAdapterCallback {

	private ListView listView;
	private LocationsListviewAdapter adapter;
	private ArrayList<Map<String, String>> dataList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_locations);
		setTitle(getString(R.string.action_frequency_location));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		listView = (ListView) findViewById(R.id.list);
		dataList = DBLocations.getAll(this);
		adapter = new LocationsListviewAdapter(this, dataList);
		adapter.setCallback(this);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void deleteItem(int position) {
		DBLocations.deleteById(this, position);
		dataList = DBLocations.getAll(this);
		adapter = new LocationsListviewAdapter(this, dataList);
		adapter.setCallback(this);
		listView.setAdapter(adapter);
	}

}
