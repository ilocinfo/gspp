package com.helloc.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.SharePrefsUtils;

public class SettingActivity extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		setTitle(getString(R.string.action_settings));
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onReminderClicked(View view) {
		Intent intent = new Intent(SettingActivity.this,
				SettingNotificationActivity.class);
		startActivity(intent);
	}

	public void onLanguageClicked(View view) {
		Intent intent = new Intent(SettingActivity.this,
				SettingLanguageActivity.class);
		startActivity(intent);
	}
	
	public void onOpenLocationClicked(View view) {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		startActivity(intent);
	}

	public void onAboutItemClicked(View view) {
		Intent intent = new Intent(SettingActivity.this, AboutActivity.class);
		startActivity(intent);
	}

	public void onLogoutClicked(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.btn_logout));
		builder.setMessage(getString(R.string.text_logout_hint));
		builder.setPositiveButton(getString(R.string.btn_confirm),
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						SharePrefsUtils.setNickName(SettingActivity.this, "");
						SharePrefsUtils.setLogin(SettingActivity.this, false);
						SharePrefsUtils.setLoginAsGuest(SettingActivity.this, false);
						// clear geofence
						GeofenceUtils.clearGeofences(SettingActivity.this);
						setResult(ActivityConstants.RESULT_LOGOUT);
						finish();
					}
				});
		builder.setNegativeButton(getString(R.string.btn_cancel), null);
		builder.create().show();
	}

}
