package com.helloc.activity;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import net.kapati.widgets.DatePicker;
import net.kapati.widgets.TimePicker;
import net.simonvt.numberpicker.NumberPicker;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBParty;
import com.helloc.database.DBPartying;
import com.helloc.fragment.LocationFragment;
import com.helloc.fragment.SendToFragment;
import com.helloc.model.Party;
import com.helloc.model.Partying;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.GoogleMapUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.MyTextUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.utils.SystemUtils;
import com.helloc.utils.WeChatUtils;
import com.helloc.view.LoadingDialog;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class SendPartyActivity extends SherlockFragmentActivity {

	private IWXAPI api;

	private ScrollView scrollView;
	private LocationFragment locationFrag;
	private SendToFragment sendToFrag;
	private EditText theme;
	private EditText description;
	private net.kapati.widgets.DatePicker startDate;
	private net.kapati.widgets.TimePicker startTime;
	private TextView tvDuration;

	private Party party;
	private Partying partying;
	private String partyId;
	private long startTimeLong;
	private int hour = 2;
	private int minute;
	private int minuteIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_party);
		setTitle(getString(R.string.action_party));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		api = WXAPIFactory.createWXAPI(this, Constants.WX_APP_ID);

		checkNickname();
		setUpGuide();
		setUpView();
	}

	private void checkNickname() {
		if (SharePrefsUtils.getNickname(this).equals("")) {
			Intent intent = new Intent(this, ProfileNameActivity.class);
			startActivityForResult(intent,
					ActivityConstants.REQUEST_GO_TO_PROFILE_NAME_ACTIVITY);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_send)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			discardChanges();
			return true;
		} else if (item.getItemId() == 0) {
			onSendClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	public void setUpGuide() {
		if (SharePrefsUtils.isNotShowUserGuide2(this) == false) {
			final RelativeLayout guide = (RelativeLayout) findViewById(R.id.guide);
			guide.setVisibility(View.VISIBLE);
			final LinearLayout cover = (LinearLayout) findViewById(R.id.cover);
			cover.setVisibility(View.VISIBLE);
			cover.getBackground().setAlpha(150);

			cover.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					cover.setVisibility(View.GONE);
					guide.setVisibility(View.GONE);

					SharePrefsUtils.setNotShowUserGuide2(
							SendPartyActivity.this, true);

				}
			});
		}
	}

	private void setUpView() {
		scrollView = (ScrollView) findViewById(R.id.scrollView);

		scrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				SystemUtils
						.hideSoftKeyboard(scrollView, SendPartyActivity.this);
				return false;
			}
		});

		theme = (EditText) findViewById(R.id.theme);
		description = (EditText) findViewById(R.id.description);
		startDate = (DatePicker) findViewById(R.id.start_date);
		startTime = (TimePicker) findViewById(R.id.start_time);
		tvDuration = (TextView) findViewById(R.id.duration);

		tvDuration.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createDialog();
			}
		});

		partyId = System.currentTimeMillis() + "_"
				+ SharePrefsUtils.getUserId(this);
		Bundle bundle = new Bundle();
		bundle.putInt(ActivityConstants.ARGS_ACTIVITY_SOURCE,
				ActivityConstants.ARGS_ACTIVITY_PARTY);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, partyId);
		bundle.putBoolean(Constants.RADIUS_CIRCLE, false);
		bundle.putBoolean(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG, false);

		locationFrag = new LocationFragment();
		locationFrag.setArguments(bundle);
		sendToFrag = new SendToFragment();
		sendToFrag.setArguments(bundle);

		FragmentTransaction fragTransaction = getSupportFragmentManager()
				.beginTransaction();
		fragTransaction.add(R.id.locaion_fragment, locationFrag);
		fragTransaction.add(R.id.send_to_fragment, sendToFrag);
		fragTransaction.commit();
	}

	private void createDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(
				R.layout.dialog_share_location_duration_picker, null);
		final NumberPicker hourPicker = (NumberPicker) view
				.findViewById(R.id.hour_picker);
		hourPicker.setMaxValue(24);
		hourPicker.setMinValue(0);
		hourPicker.setValue(hour);

		final NumberPicker minutePicker = (NumberPicker) view
				.findViewById(R.id.minute_picker);
		final String[] displayedValues = MyTextUtils.getDisplayedValues(0, 60,
				5);
		minutePicker.setMinValue(0);
		minutePicker.setMaxValue(displayedValues.length - 1);
		minutePicker.setDisplayedValues(displayedValues);
		minutePicker.setValue(minuteIndex);
		builder.setView(view);
		builder.setPositiveButton(getString(R.string.btn_confirm),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						hour = hourPicker.getValue();
						minuteIndex = minutePicker.getValue();
						minute = Integer.parseInt(displayedValues[minuteIndex]);
						StringBuffer temp = new StringBuffer();
						temp.append(hour == 0 ? "" : hour + " "
								+ getString(R.string.text_hour) + "  ");
						temp.append(minute == 0 ? "" : minute + " "
								+ getString(R.string.text_minute));
						if (temp.length() == 0) {
							tvDuration
									.setText(getString(R.string.text_two_hours));
						} else {
							tvDuration.setText(temp.toString());
						}
					}
				});
		builder.create().show();
	}

	private void discardChanges() {
		if (sendToFrag.getReceivers().length() > 0
				|| theme.getText().length() > 0
				|| description.getText().length() > 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getTitle()).setMessage(
					getString(R.string.text_discard_changes));
			builder.setPositiveButton(getString(R.string.btn_confirm),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			builder.setNegativeButton(getString(R.string.btn_cancel), null);
			AlertDialog dialog = builder.create();
			dialog.show();
		} else {
			finish();
		}
	}

	private void onSendClicked() {
		if (isInfoCompleted() == false) {
			return;
		}
		if (SharePrefsUtils.isNotFirstToSendMessage(this)) {
			sendParty();
		} else {
			String receivers = sendToFrag.getReceivers();
			if (receivers.contains(String
					.valueOf(Constants.SEND_TO_WECHAT_FRIEND))) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				LayoutInflater inflater = LayoutInflater.from(this);
				View view = inflater.inflate(R.layout.dialog_send_message_hint,
						null);
				builder.setTitle(getTitle());
				builder.setView(view);
				final CheckBox checkBox = (CheckBox) view
						.findViewById(R.id.checkbox);
				checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							SharePrefsUtils.setNotFirstToSendMessage(
									SendPartyActivity.this, true);
						} else {
							SharePrefsUtils.setNotFirstToSendMessage(
									SendPartyActivity.this, false);
						}

					}
				});
				builder.setPositiveButton(getString(R.string.btn_I_know),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (checkBox.isChecked()) {
									SharePrefsUtils.setNotFirstToSendMessage(
											SendPartyActivity.this, true);
								}
								sendParty();
							}
						});
				AlertDialog dialog = builder.create();
				dialog.show();
			} else {
				sendParty();
			}
		}
	}

	private void sendParty() {
		new AsyncTask<Void, Void, Boolean>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = LoadingDialog.createLoadingDialog(
						SendPartyActivity.this,
						getString(R.string.toast_sending));
				dialog.show();
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				buildInstance();
				// 1. upload message
				if (uploadParty() == false) {
					return false;
				}
				// 2. upload snapshot
				if (ImageUtils.uploadSnapshot(partyId) == false) {
					return false;
				}
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				dialog.dismiss();
				if (result) {
					sendToWXIfNeed();
					DBParty.insert(SendPartyActivity.this, party);
					DBPartying.insert(SendPartyActivity.this, partying);
					GeofenceUtils.setUpIntervalGeofence(SendPartyActivity.this,
							partyId, locationFrag.getLatitude(),
							locationFrag.getLongitude());
					Toast.makeText(SendPartyActivity.this,
							getString(R.string.toast_send_success),
							Toast.LENGTH_SHORT).show();
					finish();
				} else {

				}
			}
		}.execute();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			discardChanges();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private boolean isInfoCompleted() {
		if (locationFrag.isLocationSet() == false) {
			Toast.makeText(this, getString(R.string.toast_select_location),
					Toast.LENGTH_SHORT).show();
			return false;
		} else if (TextUtils.isEmpty(theme.getText().toString())) {
			Toast.makeText(this, getString(R.string.toast_input_theme),
					Toast.LENGTH_SHORT).show();
			return false;
		} else if (sendToFrag.isFriendsSelected() == false) {
			Toast.makeText(this, getString(R.string.toast_select_one_contacts),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	private void buildInstance() {
		party = new Party();
		party.setPartyId(partyId);
		party.setTheme(theme.getText().toString());
		party.setDescription(description.getText().toString());
		party.setStartDate("0");
		party.setStartTime(getStartTime());
		party.setEndDate("0");
		party.setEndTime(getEndTime());
		party.setAddress(locationFrag.getAddress());
		party.setLatitude(String.valueOf(locationFrag.getLatitude()));
		party.setLongitude(String.valueOf(locationFrag.getLongitude()));
		party.setStatus(StatusConstants.PARTY_STATUS_ONGOING);

		partying = new Partying();
		partying.setPartyId(partyId);
		partying.setUserId(SharePrefsUtils.getUserId(this));
		partying.setNickName(SharePrefsUtils.getNickname(this));
		int interval = GoogleMapUtils.getInterval(
				Double.parseDouble(party.getLatitude()),
				Double.parseDouble(party.getLongitude()),
				locationFrag.getMyLocation()[0],
				locationFrag.getMyLocation()[1]);
		partying.setInterval(interval);
	}

	private boolean uploadParty() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("deviceGeneratedPartyId", party.getPartyId());
		map.put("theme", party.getTheme());
		map.put("description", party.getDescription());
		map.put("startDate", party.getStartDate());
		map.put("startTime", party.getStartTime());
		map.put("endDate", party.getEndDate());
		map.put("endTime", party.getEndTime());
		map.put("location", party.getAddress());
		map.put("latitude", party.getLatitude());
		map.put("longitude", party.getLongitude());
		map.put("originator", SharePrefsUtils.getNickname(this));
		map.put("senderId", SharePrefsUtils.getUserId(this));
		map.put("receiverId", sendToFrag.getReceivers());
		map.put("nickname", SharePrefsUtils.getNickname(this));
		map.put("interval", String.valueOf(partying.getInterval()));

		String response = HttpRequestUtils.post("sendParty", map);
		try {
			JSONObject json = new JSONObject(response);
			String returnMessage = json.getString("returnMessage");
			if (returnMessage
					.equals(HttpConstants.SAVE_PARTY_ON_SERVER_SUCCESS)
					|| returnMessage.equals(HttpConstants.SEND_PARTY_SUCCESS)) {
				return true;
			} else {
				System.out.println("sendParty ---->> " + returnMessage);
				return false;
			}
		} catch (JSONException e) {
			System.out.println("sendParty ---->> " + e.getMessage());
			return false;
		}
	}

	private void sendToWXIfNeed() {
		if (sendToFrag.getWxType() != Constants.SEND_TO_WECHAT_NULL) {
			StringBuilder params = new StringBuilder();
			params.append("receiveParty.action?");
			params.append("deviceGeneratedPartyId=" + partyId);
			String title = getString(R.string.text_message_title);
			String description = SharePrefsUtils.getNickname(this)
					+ getString(R.string.text_originate_party)
					+ "\n"
					+ getString(R.string.text_theme)
					+ ": "
					+ party.getTheme().replaceAll(Constants.REGULAR,
							getString(R.string.text_emoticon));

			// send to wechat moments
			if (sendToFrag.getWxType() == Constants.SEND_TO_WECHAT_MOMENT) {
				title = description;
				WeChatUtils.reqWXMoments(getApplicationContext(), api,
						params.toString(), title, description);
			}
			// send to wechat friend
			else if (sendToFrag.getWxType() == Constants.SEND_TO_WECHAT_FRIEND) {
				WeChatUtils.reqWXFriends(getApplicationContext(), api,
						params.toString(), title, description);
			}
		}
	}

	private String getStartTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(startDate.getYear(), startDate.getMonth(),
				startDate.getDay(), startTime.getHour(), startTime.getMinute(),
				0);
		// System.out.println(startTime.getHour());
		startTimeLong = calendar.getTimeInMillis();
		String startTimeStr = String.valueOf(startTimeLong);
		// System.out.println("start time = " + startTimeStr);
		return startTimeStr;
	}

	private String getEndTime() {
		long endTimeLong = startTimeLong + hour * DateUtils.HOUR_IN_MILLIS
				+ minute * DateUtils.MINUTE_IN_MILLIS;
		return String.valueOf(endTimeLong);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent arg2) {
		super.onActivityResult(requestCode, resultCode, arg2);
		if (requestCode == ActivityConstants.REQUEST_GO_TO_PROFILE_NAME_ACTIVITY) {
			if (resultCode != ActivityConstants.RESULT_RESULT_OK) {
				finish();
			}
		}
	}

}
