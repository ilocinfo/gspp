package com.helloc.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.location.Geofence;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessage;
import com.helloc.fragment.DurationFragment;
import com.helloc.fragment.EmoticonFragment;
import com.helloc.fragment.EmoticonFragment.EmoticonFragmentListener;
import com.helloc.fragment.ImageFragment;
import com.helloc.fragment.InputFragment;
import com.helloc.fragment.InputFragment.InputFragmentListener;
import com.helloc.fragment.LocationFragment;
import com.helloc.model.Message;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.utils.SystemUtils;
import com.helloc.view.OnOffScrollView;

public class SelfGeofenceActivity extends SherlockFragmentActivity implements
		InputFragmentListener, EmoticonFragmentListener {

	private OnOffScrollView scrollView;
	private InputFragment inputFrag;
	private LocationFragment locationFrag;
	private DurationFragment durationFrag;
	private ImageFragment imageFrag;
	private EmoticonFragment emoticonFrag;

	private int activitySrc;
	private String messageId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_self_geofence);
		setTitle(getString(R.string.action_ring_me_around));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		setUpGuide();
		setUpParams();
		setUpView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_done)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_IF_ROOM
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			discardChanges();
			return true;
		} else if (item.getItemId() == 0) {
			onDoneClicked();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			discardChanges();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void setUpGuide() {
		if (SharePrefsUtils.isNotShowUserGuide2(this) == false) {
			final RelativeLayout guide = (RelativeLayout) findViewById(R.id.guide);
			guide.setVisibility(View.VISIBLE);
			final LinearLayout cover = (LinearLayout) findViewById(R.id.cover);
			cover.setVisibility(View.VISIBLE);
			cover.getBackground().setAlpha(150);

			cover.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					cover.setVisibility(View.GONE);
					guide.setVisibility(View.GONE);

					SharePrefsUtils.setNotShowUserGuide2(
							SelfGeofenceActivity.this, true);

				}
			});
		}
	}

	private void setUpParams() {
		activitySrc = getIntent().getIntExtra(
				ActivityConstants.ARGS_ACTIVITY_SOURCE,
				ActivityConstants.ARGS_ACTIVITY_SELF_GEOFENCE);
		if (activitySrc == ActivityConstants.ARGS_ACTIVITY_SELF_GEOFENCE) {
			messageId = System.currentTimeMillis() + "_"
					+ SharePrefsUtils.getUserId(this);
		} else {
			messageId = getIntent().getStringExtra(
					ActivityConstants.ARGS_MESSAGE_ID);
		}
	}

	private void setUpView() {
		scrollView = (OnOffScrollView) findViewById(R.id.scrollView);

		scrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (emoticonFrag.isVisible()) {
					getSupportFragmentManager().beginTransaction()
							.hide(emoticonFrag).commit();
					inputFrag.setEmoticonShow(false);
				}
				SystemUtils.hideSoftKeyboard(scrollView,
						SelfGeofenceActivity.this);
				return false;
			}
		});

		Bundle bundle = new Bundle();
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		bundle.putBoolean("enableAudio", false);
		inputFrag = new InputFragment();
		inputFrag.setArguments(bundle);

		bundle = new Bundle();
		bundle.putInt(ActivityConstants.ARGS_ACTIVITY_SOURCE, activitySrc);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		bundle.putBoolean(Constants.RADIUS_CIRCLE, true);
		bundle.putDouble(ActivityConstants.ARGS_MESSAGE_LATITUDE, getIntent()
				.getDoubleExtra(ActivityConstants.ARGS_MESSAGE_LATITUDE, 0));
		bundle.putDouble(ActivityConstants.ARGS_MESSAGE_LONGITUDE, getIntent()
				.getDoubleExtra(ActivityConstants.ARGS_MESSAGE_LONGITUDE, 0));
		bundle.putFloat(ActivityConstants.ARGS_MESSAGE_RADIUS, getIntent()
				.getFloatExtra(ActivityConstants.ARGS_MESSAGE_RADIUS, 0));
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ADDRESS, getIntent()
				.getStringExtra(ActivityConstants.ARGS_MESSAGE_ADDRESS));
		locationFrag = new LocationFragment();
		locationFrag.setArguments(bundle);

		bundle = new Bundle();
		bundle.putInt(ActivityConstants.ARGS_ACTIVITY_SOURCE, activitySrc);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		bundle.putString(ActivityConstants.ARGS_MESSAGE_IMAGE_NAME, getIntent()
				.getStringExtra(ActivityConstants.ARGS_MESSAGE_IMAGE_NAME));
		imageFrag = new ImageFragment();
		imageFrag.setArguments(bundle);

		durationFrag = new DurationFragment();
		emoticonFrag = new EmoticonFragment();

		FragmentTransaction fragTransaction = getSupportFragmentManager()
				.beginTransaction();
		fragTransaction.add(R.id.input_fragment, inputFrag);
		fragTransaction.add(R.id.locaion_fragment, locationFrag);
		fragTransaction.add(R.id.duration_fragment, durationFrag);
		fragTransaction.add(R.id.image_fragment, imageFrag);
		fragTransaction.add(R.id.emoticon_fragment, emoticonFrag);
		fragTransaction.hide(emoticonFrag);
		fragTransaction.commit();
	}

	private void discardChanges() {
		if (inputFrag.getMessageType() == Constants.MESSAGE_TYPE_AUDIO
				|| imageFrag.getImagesName().length() > 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getTitle()).setMessage(
					getString(R.string.text_discard_changes));
			builder.setPositiveButton(getString(R.string.btn_confirm),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			builder.setNegativeButton(getString(R.string.btn_cancel), null);
			AlertDialog dialog = builder.create();
			dialog.show();
		} else {
			finish();
		}
	}

	private void onDoneClicked() {
		if (isInfoCompleted() == false) {
			return;
		}

		Message message = new Message();
		message.setId(messageId);
		message.setLatitude(locationFrag.getLatitude());
		message.setLongitude(locationFrag.getLongitude());
		message.setRadius(locationFrag.getRadius());
		message.setExpirationTime(System.currentTimeMillis()
				+ durationFrag.getDuration());
		message.setTransitionType(Geofence.GEOFENCE_TRANSITION_ENTER
				| Geofence.GEOFENCE_TRANSITION_EXIT);
		message.setStatus(StatusConstants.MESSAGE_STATUS_SELF);
		message.setType(inputFrag.getMessageType());
		message.setAddress(locationFrag.getAddress());
		message.setPrivateMsg(getPrivateMsg());
		message.setPublicMsg("");
		message.setSenderId("");
		message.setReceiverId("");
		message.setImageName(imageFrag.getImagesName());
		message.setNeedFeedback(false);

		// save message
		DBMessage.insert(this, message);
		// set up geofence
		GeofenceUtils.setUpGeofence(this, message);
		
		Toast.makeText(SelfGeofenceActivity.this,
				getString(R.string.toast_setting_success),
				Toast.LENGTH_SHORT).show();
		finish();
	}

	private String getPrivateMsg() {
		if (inputFrag.getMessageType() == Constants.MESSAGE_TYPE_TEXT
				&& TextUtils.isEmpty(inputFrag.getPrivateMsg())) {
			return getString(R.string.text_image);
		} else {
			return inputFrag.getPrivateMsg();
		}
	}

	private boolean isInfoCompleted() {
		if (locationFrag.isLocationSet() == false) {
			Toast.makeText(this, getString(R.string.toast_select_location),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (inputFrag.isInputCompleted() == false
				&& imageFrag.isImageSelected() == false) {
			Toast.makeText(this, getString(R.string.toast_input_something),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	@Override
	public void onEmoticonSelected(SpannableString emoticon) {
		inputFrag.appendEmoticon(emoticon);
	}

	@Override
	public void onEmoticonBtnClicked() {
		if (emoticonFrag.isHidden()) {
			getSupportFragmentManager().beginTransaction().show(emoticonFrag)
					.commit();
		} else {
			getSupportFragmentManager().beginTransaction().hide(emoticonFrag)
					.commit();
		}
	}

	@Override
	public void hideEmoticon() {
		if (emoticonFrag.isVisible()) {
			getSupportFragmentManager().beginTransaction().hide(emoticonFrag)
					.commit();
			inputFrag.setEmoticonShow(false);
		}

	}

	@Override
	public void onCameraClicked() {
		imageFrag.openContextMenu();
	}

}
