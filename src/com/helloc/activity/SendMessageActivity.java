package com.helloc.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.location.Geofence;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessage;
import com.helloc.fragment.DurationFragment;
import com.helloc.fragment.EmoticonFragment;
import com.helloc.fragment.EmoticonFragment.EmoticonFragmentListener;
import com.helloc.fragment.ImageFragment;
import com.helloc.fragment.InputFragment;
import com.helloc.fragment.InputFragment.InputFragmentListener;
import com.helloc.fragment.LocationFragment;
import com.helloc.fragment.SendToFragment;
import com.helloc.model.Message;
import com.helloc.utils.AudioUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.MyTextUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.utils.SystemUtils;
import com.helloc.utils.WeChatUtils;
import com.helloc.view.LoadingDialog;
import com.helloc.view.OnOffScrollView;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class SendMessageActivity extends SherlockFragmentActivity implements
		EmoticonFragmentListener, InputFragmentListener {

	// ΢�����
	private IWXAPI api;

	private OnOffScrollView scrollView;
	private InputFragment inputFrag;
	private LocationFragment locationFrag;
	private DurationFragment durationFrag;
	private ImageFragment imageFrag;
	private SendToFragment sendToFrag;
	private EmoticonFragment emoticonFrag;

	private String messageId;
	private Message message;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_message);
		setTitle(getString(R.string.action_shout));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		// acquire wxapi
		api = WXAPIFactory.createWXAPI(this, Constants.WX_APP_ID);

		checkNickname();
		setUpGuide();
		setUpView();
	}

	private void checkNickname() {
		if (SharePrefsUtils.getNickname(this).equals("")) {
			Intent intent = new Intent(this, ProfileNameActivity.class);
			startActivityForResult(intent,
					ActivityConstants.REQUEST_GO_TO_PROFILE_NAME_ACTIVITY);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, getString(R.string.action_send)).setShowAsAction(
				MenuItem.SHOW_AS_ACTION_ALWAYS
						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			discardChanges();
			return true;
		} else if (item.getItemId() == 0) {
			onSendClicked();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			discardChanges();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void setUpGuide() {
		if (SharePrefsUtils.isNotShowUserGuide2(this) == false) {
			final RelativeLayout guide = (RelativeLayout) findViewById(R.id.guide);
			guide.setVisibility(View.VISIBLE);
			final LinearLayout cover = (LinearLayout) findViewById(R.id.cover);
			cover.setVisibility(View.VISIBLE);
			cover.getBackground().setAlpha(150);

			cover.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					cover.setVisibility(View.GONE);
					guide.setVisibility(View.GONE);

					SharePrefsUtils.setNotShowUserGuide2(
							SendMessageActivity.this, true);

				}
			});
		}
	}

	private void setUpView() {
		scrollView = (OnOffScrollView) findViewById(R.id.scrollView);

		scrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (emoticonFrag.isVisible()) {
					getSupportFragmentManager().beginTransaction()
							.hide(emoticonFrag).commit();
					inputFrag.setEmoticonShow(false);
				}
				SystemUtils.hideSoftKeyboard(scrollView,
						SendMessageActivity.this);
				return false;
			}
		});

		messageId = System.currentTimeMillis() + "_"
				+ SharePrefsUtils.getUserId(this);

		Bundle bundle = new Bundle();
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		inputFrag = new InputFragment();
		inputFrag.setArguments(bundle);

		bundle = new Bundle();
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		bundle.putBoolean(Constants.RADIUS_CIRCLE, true);
		locationFrag = new LocationFragment();
		locationFrag.setArguments(bundle);

		bundle = new Bundle();
		bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
		bundle.putInt(ActivityConstants.ARGS_ACTIVITY_SOURCE,
				ActivityConstants.ARGS_ACTIVITY_SEND_MESSAGE);
		imageFrag = new ImageFragment();
		imageFrag.setArguments(bundle);

		bundle = new Bundle();
		bundle.putInt(ActivityConstants.ARGS_ACTIVITY_SOURCE,
				ActivityConstants.ARGS_ACTIVITY_SEND_MESSAGE);
		bundle.putBoolean(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG, true);
		bundle.putString(ActivityConstants.ARGS_ACCOUNT_USER_ID, getIntent()
				.getStringExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID));
		sendToFrag = new SendToFragment();
		sendToFrag.setArguments(bundle);

		durationFrag = new DurationFragment();
		emoticonFrag = new EmoticonFragment();

		FragmentTransaction fragTransaction = getSupportFragmentManager()
				.beginTransaction();
		fragTransaction.add(R.id.input_box_fragment, inputFrag);
		fragTransaction.add(R.id.locaion_fragment, locationFrag);
		fragTransaction.add(R.id.duration_fragment, durationFrag);
		fragTransaction.add(R.id.image_fragment, imageFrag);
		fragTransaction.add(R.id.send_to_fragment, sendToFrag);
		fragTransaction.add(R.id.emoticon_fragment, emoticonFrag);
		fragTransaction.hide(emoticonFrag);
		fragTransaction.commit();
	}

	private void discardChanges() {
		if (sendToFrag.getReceivers().length() > 0
				|| inputFrag.getMessageType() == Constants.MESSAGE_TYPE_AUDIO
				|| imageFrag.getImagesName().length() > 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getTitle()).setMessage(
					getString(R.string.text_discard_changes));
			builder.setPositiveButton(getString(R.string.btn_confirm),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			builder.setNegativeButton(getString(R.string.btn_cancel), null);
			AlertDialog dialog = builder.create();
			dialog.show();
		} else {
			finish();
		}
	}

	private void onSendClicked() {
		if (isInfoCompleted() == false) {
			return;
		}
		if (SharePrefsUtils.isNotFirstToSendMessage(this)) {
			sendMessage();
		} else {
			String receivers = sendToFrag.getReceivers();
			if (receivers.contains(String
					.valueOf(Constants.SEND_TO_WECHAT_FRIEND))) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				LayoutInflater inflater = LayoutInflater.from(this);
				View view = inflater.inflate(R.layout.dialog_send_message_hint,
						null);
				builder.setTitle(getTitle());
				builder.setView(view);
				final CheckBox checkBox = (CheckBox) view
						.findViewById(R.id.checkbox);
				checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							SharePrefsUtils.setNotFirstToSendMessage(
									SendMessageActivity.this, true);
						} else {
							SharePrefsUtils.setNotFirstToSendMessage(
									SendMessageActivity.this, false);
						}

					}
				});
				builder.setPositiveButton(getString(R.string.btn_I_know),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (checkBox.isChecked()) {
									SharePrefsUtils.setNotFirstToSendMessage(
											SendMessageActivity.this, true);
								}
								sendMessage();
							}
						});
				AlertDialog dialog = builder.create();
				dialog.show();
			} else {
				sendMessage();
			}

		}
	}

	private void sendMessage() {
		new AsyncTask<Void, Void, Boolean>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = LoadingDialog.createLoadingDialog(
						SendMessageActivity.this,
						getString(R.string.toast_sending));
				dialog.show();
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				buildInstance();
				// 1. upload message
				if (uploadMessage() == false) {
					return false;
				}
				// 2. upload snapshot
				if (ImageUtils.uploadSnapshot(messageId) == false) {
					return false;
				}
				// 3. upload audio if need
				if (uploadAudioIfNeed() == false) {
					return false;
				}
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				dialog.dismiss();
				if (result) {
					DBMessage.insert(SendMessageActivity.this, message);
					sendToWXIfNeed();
					copyToClipboardIfNeed();
					Toast.makeText(SendMessageActivity.this,
							getString(R.string.toast_send_success),
							Toast.LENGTH_SHORT).show();
					finish();
				} else {
					Toast.makeText(SendMessageActivity.this,
							getString(R.string.toast_send_failed),
							Toast.LENGTH_SHORT).show();
				}
			}
		}.execute();
	}

	private boolean isInfoCompleted() {
		if (locationFrag.isLocationSet() == false) {
			Toast.makeText(this, getString(R.string.toast_select_location),
					Toast.LENGTH_SHORT).show();
			return false;
		} else if (inputFrag.isInputCompleted() == false
				&& imageFrag.getImagesName().length() == 0) {
			Toast.makeText(this, getString(R.string.toast_input_something),
					Toast.LENGTH_SHORT).show();
			return false;
		} else if (sendToFrag.isFriendsSelected() == false) {
			Toast.makeText(this, getString(R.string.toast_select_one_contacts),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	private void buildInstance() {
		message = new Message();
		message.setId(messageId);
		message.setLatitude(locationFrag.getLatitude());
		message.setLongitude(locationFrag.getLongitude());
		message.setRadius(locationFrag.getRadius());
		message.setExpirationTime(System.currentTimeMillis()
				+ durationFrag.getDuration());
		message.setTransitionType(Geofence.GEOFENCE_TRANSITION_ENTER
				| Geofence.GEOFENCE_TRANSITION_EXIT);
		message.setStatus(StatusConstants.MESSAGE_STATUS_SENT);
		message.setType(inputFrag.getMessageType());
		message.setAddress(locationFrag.getAddress());
		message.setPrivateMsg(getPrivateMsg());
		message.setPublicMsg(sendToFrag.getPublicMsg());
		message.setSenderId(SharePrefsUtils.getUserId(this));
		message.setReceiverId(sendToFrag.getReceivers());
		message.setImageName(imageFrag.getImagesName());
		message.setNeedFeedback(sendToFrag.needFeedback());
	}

	private String getPrivateMsg() {
		if (inputFrag.getMessageType() == Constants.MESSAGE_TYPE_TEXT
				&& TextUtils.isEmpty(inputFrag.getPrivateMsg())) {
			return getString(R.string.text_image);
		} else {
			return inputFrag.getPrivateMsg();
		}
	}

	private boolean uploadMessage() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("deviceGeneratedMessageId", message.getId());
		map.put("address", message.getAddress());
		map.put("privateMessage", message.getPrivateMsg());
		map.put("publicMessage", message.getPublicMsg());
		map.put("expirationTime", String.valueOf(message.getExpirationTime()));
		map.put("latitude", String.valueOf(message.getLatitude()));
		map.put("longitude", String.valueOf(message.getLongitude()));
		map.put("radius", String.valueOf(message.getRadius()));
		map.put("transition", String.valueOf(message.getTransitionType()));
		map.put("type", String.valueOf(message.getType()));
		map.put("imageName", message.getImageName());
		map.put("status", String.valueOf(message.getStatus()));
		map.put("needFeedBack", String.valueOf(message.isNeedFeedback()));
		map.put("senderId", message.getSenderId());
		map.put("receiverId", message.getReceiverId());

		String response = HttpRequestUtils.post("sendMessage", map);
		try {
			JSONObject json = new JSONObject(response);
			String returnMessage = json.getString("returnMessage");
			if (returnMessage
					.equals(HttpConstants.SAVE_MESSAGE_ON_SERVER_SUCCESS)
					|| returnMessage.equals(HttpConstants.SEND_MESSAGE_SUCCESS)
					|| returnMessage
							.equals(HttpConstants.SEND_GLOBAL_MESSAGE_SUCCESS)) {
				return true;
			} else {
				System.out.println("sendMessage ---->> " + returnMessage);
				return false;
			}
		} catch (JSONException e) {
			System.out.println("sendMessage ---->> " + e.getMessage());
			return false;
		}
	}

	private boolean uploadAudioIfNeed() {
		if (message.getType() == Constants.MESSAGE_TYPE_AUDIO) {
			if (AudioUtils.uploadAudio(SendMessageActivity.this, messageId) == false) {
				return false;
			}
		}
		return true;
	}

	private void sendToWXIfNeed() {
		if (sendToFrag.getWxType() != Constants.SEND_TO_WECHAT_NULL) {

			StringBuilder params = new StringBuilder();
			params.append("receiveMessage.action?");
			params.append("deviceGeneratedMessageId=" + message.getId());
			String title = getString(R.string.text_message_title);
			String description = SharePrefsUtils.getNickname(this)
					+ getString(R.string.text_message_description_1)
					+ message.getAddress()
					+ getString(R.string.text_message_description_2);

			// send to wechat moments
			if (sendToFrag.getWxType() == Constants.SEND_TO_WECHAT_MOMENT) {
				// title += description;
				WeChatUtils.reqWXMoments(getApplicationContext(), api,
						params.toString(), description, description);
			}
			// send to wechat friend
			else if (sendToFrag.getWxType() == Constants.SEND_TO_WECHAT_FRIEND) {
				// request wechat
				WeChatUtils.reqWXFriends(getApplicationContext(), api,
						params.toString(), title, description);
			}
		}
	}

	private void copyToClipboardIfNeed() {
		if (sendToFrag.isOtherappsSelected()) {
			StringBuilder content = new StringBuilder();
			content.append(HttpConstants.SERVER_URL);
			content.append("receiveMessage.action?");
			content.append("deviceGeneratedMessageId=" + message.getId());
			MyTextUtils.copyToClipboard(content.toString(), this);
		}
	}

	@Override
	public void onEmoticonSelected(SpannableString emoticon) {
		inputFrag.appendEmoticon(emoticon);
	}

	@Override
	public void onEmoticonBtnClicked() {
		if (emoticonFrag.isHidden()) {
			getSupportFragmentManager().beginTransaction().show(emoticonFrag)
					.commit();
		} else {
			getSupportFragmentManager().beginTransaction().hide(emoticonFrag)
					.commit();
		}
	}

	@Override
	public void hideEmoticon() {
		if (emoticonFrag.isVisible()) {
			getSupportFragmentManager().beginTransaction().hide(emoticonFrag)
					.commit();
			inputFrag.setEmoticonShow(false);
		}

	}

	@Override
	public void onCameraClicked() {
		imageFrag.openContextMenu();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent arg2) {
		super.onActivityResult(requestCode, resultCode, arg2);
		if (requestCode == ActivityConstants.REQUEST_GO_TO_PROFILE_NAME_ACTIVITY) {
			if (resultCode != ActivityConstants.RESULT_RESULT_OK) {
				finish();
			}
		}
	}

}
