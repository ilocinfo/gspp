package com.helloc.activity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.database.DBAvatar;
import com.helloc.database.DBFriend;
import com.helloc.fragment.SendInvitationDialog;
import com.helloc.fragment.SendInvitationDialog.SendInvitationDialogListener;
import com.helloc.model.Avatar;

public class SearchContactsResultActivity extends SherlockFragmentActivity
		implements SendInvitationDialogListener {
	private ImageView ivAvatar;
	private String userId;
	private String uuid;
	private String nickname;
	private String phone;
	private String encodedAvatar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_contacts_result);
		setTitle(getString(R.string.action_profile));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		userId = getIntent().getStringExtra(
				ActivityConstants.ARGS_ACCOUNT_USER_ID);
		uuid = getIntent().getStringExtra(ActivityConstants.ARGS_ACCOUNT_UUID);
		nickname = getIntent().getStringExtra(
				ActivityConstants.ARGS_ACCOUNT_NICKNAME);
		phone = getIntent()
				.getStringExtra(ActivityConstants.ARGS_ACCOUNT_PHONE);
		encodedAvatar = getIntent().getStringExtra(
				ActivityConstants.ARGS_ACCOUNT_AVATAR);

		View tv = findViewById(R.id.name);
		((TextView) tv).setText(nickname);
		// System.out.println("encodedAvatar = " + encodedAvatar);
		if (TextUtils.isEmpty(encodedAvatar) == false
				&& encodedAvatar.equals("null") == false) {
			byte[] img = Base64.decode(encodedAvatar, Base64.DEFAULT);
			Avatar avatar = new Avatar();
			avatar.setUserId(userId);
			avatar.setPhoto(img);
			DBAvatar.insert(this, avatar);

			ivAvatar = (ImageView) findViewById(R.id.avatar);
			ivAvatar.setImageBitmap(BitmapFactory.decodeByteArray(img, 0,
					img.length));
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onItemClicked(View view) {
		// invitation only for non-friends
		if (DBFriend.isMyFriendById(this, userId)) {
			Intent intent = new Intent();
			intent.setClass(SearchContactsResultActivity.this,
					UserProfileActivity.class);
			intent.putExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID, userId);
			startActivity(intent);
			finish();
		} else {
			DialogFragment newFragment = SendInvitationDialog.newInstance(
					userId, uuid, nickname, phone, this);
			newFragment.show(getSupportFragmentManager(), "dialog");
		}
	}

	@Override
	public void updateView() {
		finish();
	}

}
