package com.helloc.activity;

import java.io.File;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.model.SoundMeter;

public class RecordAudioActivity extends SherlockActivity {

	private Button btnRcdAudio;
	private Button volume;
	private RelativeLayout btnMic;
	private TextView recordHint;
	private int flag = 1;
	private Handler mHandler = new Handler();
	private SoundMeter mSensor;
	private String audioName;
	private String audioPath;
	private long startVoiceT, endVoiceT;
	private static final int POLL_INTERVAL = 300;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record_audio);

		btnRcdAudio = (Button) findViewById(R.id.btn_record);
		volume = (Button) findViewById(R.id.volume);
		btnMic = (RelativeLayout) findViewById(R.id.btn_mic);
		recordHint = (TextView) findViewById(R.id.record_hint);

		btnRcdAudio.setOnTouchListener(new BtnRcdAudioOnTouchListener());

		audioName = getIntent().getStringExtra("geofenceMsgId");
		audioName += ".amr";
		audioPath = Constants.AUDIO_FLODER + audioName;
		mSensor = new SoundMeter();
	}

	private class BtnRcdAudioOnTouchListener implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (!Environment.getExternalStorageDirectory().exists()) {
				Toast.makeText(RecordAudioActivity.this, "No SDCard",
						Toast.LENGTH_SHORT).show();
				return true;
			}

			if (event.getAction() == MotionEvent.ACTION_DOWN && flag == 1) {
				btnRcdAudio
						.setBackgroundResource(R.drawable.bg_record_voice_pressed);
				btnMic.setBackgroundResource(R.drawable.icon_record_voice_working);
				recordHint.setText(getString(R.string.btn_release_to_finish));

				startVoiceT = System.currentTimeMillis();
				startRecord();
				flag = 2;
				return true;
			} else if (event.getAction() == MotionEvent.ACTION_UP && flag == 2) {// 松开手势时执行录制完成
				btnRcdAudio
						.setBackgroundResource(R.drawable.bg_record_voice_normal);
				btnMic.setBackgroundResource(R.drawable.icon_record_voice_normal);

				// 判断手势抬起的位置是否是语音录制按钮的范围内
				if (event.getY() > 0 && event.getX() > 0
						&& event.getY() <= btnRcdAudio.getHeight()
						&& event.getX() <= btnRcdAudio.getWidth()) {
					flag = 1;
					stopRecord();
					endVoiceT = System.currentTimeMillis();
					int time = (int) ((endVoiceT - startVoiceT) / 1000);
					if (time < 1) {
						Toast.makeText(RecordAudioActivity.this,
								getString(R.string.text_voice_rcd_to_short),
								Toast.LENGTH_SHORT).show();
						File file = new File(audioPath);
						if (file.exists()) {
							file.delete();
						}
						return true;
					}
					// if record succeed, finish
					mHandler.postDelayed(new Runnable() {
						public void run() {
							setResult(ActivityConstants.RESULT_RESULT_OK);
							finish();
						}
					}, 300);
				} else {
					stopRecord();
					flag = 1;
					File file = new File(audioPath);
					if (file.exists()) {
						file.delete();
					}
				}
			}
			if (event.getY() > 0 && event.getY() <= btnRcdAudio.getHeight()
					&& event.getX() > 0
					&& event.getX() <= btnRcdAudio.getWidth()) {
				recordHint.setText(getString(R.string.btn_release_to_finish));
			} else {
				recordHint.setText(getString(R.string.btn_release_to_cancel));
			}
			return true;
		}

	}

	private void startRecord() {
		mSensor.start(audioName);
		mHandler.postDelayed(mPollTask, POLL_INTERVAL);
		volume.setVisibility(View.VISIBLE);
	}

	private void stopRecord() {
		mHandler.removeCallbacks(mSleepTask);
		mHandler.removeCallbacks(mPollTask);
		mSensor.stop();
		volume.setBackgroundResource(R.drawable.bg_record_voice_0);
		volume.setVisibility(View.INVISIBLE);
		recordHint.setText(getString(R.string.btn_hold_to_talk));
	}

	private Runnable mSleepTask = new Runnable() {
		public void run() {
			stopRecord();
		}
	};

	private Runnable mPollTask = new Runnable() {
		public void run() {
			double amp = mSensor.getAmplitude();
			System.out.println("amp = " + amp);
			updateDisplay(amp);
			mHandler.postDelayed(mPollTask, POLL_INTERVAL);

		}
	};

	private void updateDisplay(double signalEMA) {
		switch ((int) signalEMA) {
		case 0:
			volume.setBackgroundResource(R.drawable.bg_record_voice_0);
			break;
		case 1:
			volume.setBackgroundResource(R.drawable.bg_record_voice_1);
			break;
		case 2:
			volume.setBackgroundResource(R.drawable.bg_record_voice_2);
			break;
		case 3:
			volume.setBackgroundResource(R.drawable.bg_record_voice_3);
			break;
		case 4:
			volume.setBackgroundResource(R.drawable.bg_record_voice_4);
			break;
		case 5:
			volume.setBackgroundResource(R.drawable.bg_record_voice_5);
			break;
		default:
			volume.setBackgroundResource(R.drawable.bg_record_voice_6);
			break;
		}
	}
}
