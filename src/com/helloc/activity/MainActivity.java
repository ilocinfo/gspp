package com.helloc.activity;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessage;
import com.helloc.database.DBParty;
import com.helloc.model.Message;
import com.helloc.model.Party;
import com.helloc.service.ListenPortService;
import com.helloc.utils.GCMUtils;
import com.helloc.utils.MyTextUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.utils.SystemUtils;
import com.helloc.utils.WeChatUtils;

public class MainActivity extends SherlockActivity {
	private String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle(getString(R.string.app_name));

		setUpGuide();
		createFloder();
		registerBroadcastReceiver();

		// start a service to listen a unique port
		Intent mServiceIntent = new Intent(this, ListenPortService.class);
		startService(mServiceIntent);

		// register to wechat
		WeChatUtils.registerToWeChat(this);

		checkLogin();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Check device for Play Services APK.
		SystemUtils.checkPlayServices(this);
		UpdateUI();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.unregisterReceiver(this.mBroadcastReceiver);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (SharePrefsUtils.isNewFriendsNotice(this)) {
			getSupportMenuInflater().inflate(R.menu.main_with_notice, menu);
		} else {
			getSupportMenuInflater().inflate(R.menu.main, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		if (item.getItemId() == R.id.action_settings) {
			Intent intent = new Intent(this, SettingActivity.class);
			startActivityForResult(intent,
					ActivityConstants.REQUEST_GO_T0_SETTING_ACTIVITY);
			return true;
		} else if (item.getItemId() == R.id.action_profile) {
			Intent intent = new Intent(this, ProfileActivity.class);
			startActivity(intent);
		} else if (item.getItemId() == R.id.action_contacts) {
			Intent intent = new Intent(this, ContactsListActivity.class);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpGuide() {
		if (SharePrefsUtils.isNotShowUserGuide1(this) == false) {
			final LinearLayout guide = (LinearLayout) findViewById(R.id.guide);
			guide.setVisibility(View.VISIBLE);
			final LinearLayout cover = (LinearLayout) findViewById(R.id.cover);
			cover.setVisibility(View.VISIBLE);
			cover.getBackground().setAlpha(150);

			cover.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					cover.setVisibility(View.GONE);
					guide.setVisibility(View.GONE);

					SharePrefsUtils.setNotShowUserGuide1(MainActivity.this,
							true);

				}
			});
		}
	}

	private void createFloder() {
		// create a avatar folder
		File floder = new File(Constants.HELLOC_FOLDER);
		if (floder.exists() == false) {
			floder.mkdirs();
		}
		floder = new File(Constants.IMAGE_FLODER);
		if (floder.exists() == false) {
			floder.mkdirs();
		}
		floder = new File(Constants.THUMBNAIL_FLODER);
		if (floder.exists() == false) {
			floder.mkdirs();
		}
		floder = new File(Constants.SNAPSHOT_FLODER);
		if (floder.exists() == false) {
			floder.mkdirs();
		}
		floder = new File(Constants.AUDIO_FLODER);
		if (floder.exists() == false) {
			floder.mkdirs();
		}
	}

	private void registerBroadcastReceiver() {
		// register a broadcast receiver to update UI
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("updateUI");
		intentFilter.addAction("updateNotice");
		this.registerReceiver(this.mBroadcastReceiver, intentFilter);
	}

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i(TAG, "update UI");
			UpdateUI();
		}
	};

	private void checkLogin() {
		// if need login
		if (SharePrefsUtils.isLogin(this) == false
				|| SharePrefsUtils.isNewFeatureOdd(this)) {
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, LoginActivity.class);
			startActivityForResult(intent,
					ActivityConstants.REQUEST_GO_TO_LOGIN_ACTIVITY);
		}
		// no need to login
		else {
			new GCMUtils().GCMRegistration(this, false);
		}
	}

	private void UpdateUI() {
		supportInvalidateOptionsMenu();

		int sentCount = DBMessage.getNumByStatus(this,
				StatusConstants.MESSAGE_STATUS_SENT);
		int receiveActivatedCount = DBMessage.getNumByStatus(this,
				StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED);
		int selfCount = DBMessage.getNumByStatus(this,
				StatusConstants.MESSAGE_STATUS_SELF);
		int selfActivatedCount = DBMessage.getNumByStatus(this,
				StatusConstants.MESSAGE_STATUS_SELF_ACTIVED);
		int partyOngoingCount = DBParty.getNumByStatus(this,
				StatusConstants.PARTY_STATUS_ONGOING);
		int partyOverCount = DBParty.getNumByStatus(this,
				StatusConstants.PARTY_STATUS_OVER);

		// update the count
		((TextView) findViewById(R.id.geofence_num_sent)).setText(sentCount
				+ getString(R.string.text_item));
		((TextView) findViewById(R.id.geofence_num_receive))
				.setText(receiveActivatedCount + getString(R.string.text_item));
		((TextView) findViewById(R.id.self_geofence_num_set)).setText(selfCount
				+ getString(R.string.text_item));
		((TextView) findViewById(R.id.self_geofence_num_activated))
				.setText(selfActivatedCount + getString(R.string.text_item));
		((TextView) findViewById(R.id.party_ongoing_num))
				.setText(partyOngoingCount + getString(R.string.text_item));
		((TextView) findViewById(R.id.party_over_num)).setText(partyOverCount
				+ getString(R.string.text_item));

		// update the newest message
		Message message = DBMessage.getLatestByStatus(this,
				StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED);
		((TextView) findViewById(R.id.new_message)).setText(MyTextUtils
				.getSpannableString(this, getLatestMessage(message)));
		message = DBMessage.getLatestByStatus(this,
				StatusConstants.MESSAGE_STATUS_SELF);
		((TextView) findViewById(R.id.new_self_geofence)).setText(MyTextUtils
				.getSpannableString(this, getLatestMessage(message)));
		((TextView) findViewById(R.id.new_party)).setText(MyTextUtils
				.getSpannableString(this, getLatestParty()));
	}

	public void onSendGeoFenceClicked(View view) {
		Intent intent = new Intent(this, SendMessageActivity.class);
		startActivity(intent);
	}

	public void onSelfGeoFenceClicked(View view) {
		Intent intent = new Intent(this, SelfGeofenceActivity.class);
		startActivity(intent);
	}

	public void onPartyClicked(View view) {
		Intent intent = new Intent(this, SendPartyActivity.class);
		startActivity(intent);
	}

	public void onGeoFenceListClicked(View view) {
		Intent intent = new Intent(this, MessageListActivity.class);
		intent.putExtra(ActivityConstants.ARGS_ACTIVITY_SOURCE,
				ActivityConstants.ARGS_ACTIVITY_SEND_MESSAGE);
		startActivity(intent);
	}

	public void onSelfGeoFenceListClicked(View view) {
		Intent intent = new Intent(this, MessageListActivity.class);
		intent.putExtra(ActivityConstants.ARGS_ACTIVITY_SOURCE,
				ActivityConstants.ARGS_ACTIVITY_SELF_GEOFENCE);
		startActivity(intent);
	}

	public void onPartyListActivityClicked(View view) {
		Intent intent = new Intent(this, PartyListActivity.class);
		startActivity(intent);
	}

	private String getLatestMessage(Message message) {
		if (message == null) {
			return getString(R.string.text_no_new_message);
		} else {
			String receiver = MyTextUtils.getReceivers(this, message);
			if (message.getType() == Constants.MESSAGE_TYPE_AUDIO) {
				return receiver + ":" + getString(R.string.text_send_audio);
			} else {
				if (TextUtils.isEmpty(message.getPrivateMsg()) == false) {
					return receiver + ": " + message.getPrivateMsg();
				} else {
					return receiver + ":" + getString(R.string.text_send_image);
				}
			}
		}
	}

	private String getLatestParty() {
		Party party = DBParty.getLatest(this);
		if (party == null) {
			return getString(R.string.text_no_new_message);
		} else {
			return "'" + party.getTheme() + "'"
					+ getString(R.string.title_party_joining);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ActivityConstants.REQUEST_GO_TO_LOGIN_ACTIVITY) {
			if (resultCode == ActivityConstants.RESULT_LOGIN_CANCLE) {
				finish();
			}
		} else if (requestCode == ActivityConstants.REQUEST_GO_T0_SETTING_ACTIVITY) {
			if (resultCode == ActivityConstants.RESULT_LOGOUT) {
				checkLogin();
			}
		}
	}
}
