package com.helloc.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessage;
import com.helloc.database.DBMessageReply;
import com.helloc.fragment.LocationFragment.ErrorDialogFragment;
import com.helloc.fragment.MessageDetailFragment;
import com.helloc.model.Message;
import com.helloc.model.MessageReply;
import com.helloc.utils.GoogleMapUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.SharePrefsUtils;

public class ReceiveMessageActivity extends SherlockFragmentActivity implements
		ConnectionCallbacks, OnConnectionFailedListener {
	private LocationClient mLocationClient;
	private GoogleMap mMap;
	private LinearLayout imageFragment;
	private ViewGroup mContainerView;

	private Message message;
	private String messageId;
	private ArrayList<String> imageIndex;

	private boolean firstLoad = true;
	private MessageDetailFragment msgDetailFrg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_message);
		setTitle(getString(R.string.action_shout));

		// check whether the message exits
		messageId = getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_ID);
		message = DBMessage.getByIdAndStatus(this, messageId,
				StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED);

		imageFragment = (LinearLayout) findViewById(R.id.imageFragment);
		mContainerView = (ViewGroup) findViewById(R.id.container);

		message = new Message();
		message.setId(messageId);
		message.setLatitude((getIntent().getDoubleExtra(
				ActivityConstants.ARGS_MESSAGE_LATITUDE, 0)));
		message.setLongitude(getIntent().getDoubleExtra(
				ActivityConstants.ARGS_MESSAGE_LONGITUDE, 0));
		message.setRadius(getIntent().getFloatExtra(
				ActivityConstants.ARGS_MESSAGE_RADIUS, 0));
		message.setExpirationTime(getIntent().getLongExtra(
				ActivityConstants.ARGS_MESSAGE_EXPIRATION_TIME, 0));
		message.setTransitionType(Geofence.GEOFENCE_TRANSITION_ENTER
				| Geofence.GEOFENCE_TRANSITION_EXIT);
		message.setStatus(StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED);
		message.setType(getIntent().getIntExtra(
				ActivityConstants.ARGS_MESSAGE_TYPE,
				Constants.MESSAGE_TYPE_TEXT));
		message.setAddress(getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_ADDRESS));
		message.setPrivateMsg(getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG));
		message.setPublicMsg(getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG));
		message.setSenderId(getIntent().getStringExtra(
				ActivityConstants.ARGS_ACCOUNT_USER_ID));
		message.setImageName(getIntent().getStringExtra(
				ActivityConstants.ARGS_MESSAGE_IMAGE_NAME));
		message.setNeedFeedback(Boolean.parseBoolean(getIntent()
				.getStringExtra(ActivityConstants.ARGS_MESSAGE_NEED_FEEDBACK)));

		if (message.getType() == Constants.MESSAGE_TYPE_TEXT
				&& TextUtils.isEmpty(message.getPrivateMsg())) {
			message.setPrivateMsg(getString(R.string.text_image));
		}
		setUpMapIfNeeded();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mLocationClient != null) {
			mLocationClient.disconnect();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (DBMessage.getSelfById(this, messageId) == null) {
			menu.add(0, 0, 0, getString(R.string.action_add_to_self))
					.setShowAsAction(
							MenuItem.SHOW_AS_ACTION_ALWAYS
									| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		} else {
			if (message.getStatus() == StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED
					|| message.getStatus() == StatusConstants.MESSAGE_STATUS_SENT) {
				menu.add(0, 1, 0, getString(R.string.action_reply))
						.setShowAsAction(
								MenuItem.SHOW_AS_ACTION_ALWAYS
										| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
				menu.add(0, 2, 0, getString(R.string.action_refresh))
						.setShowAsAction(
								MenuItem.SHOW_AS_ACTION_ALWAYS
										| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			}
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == 0) {
			Intent intent = new Intent();
			intent.setClass(this, SelfGeofenceActivity.class);
			intent.putExtra(ActivityConstants.ARGS_ACTIVITY_SOURCE,
					ActivityConstants.ARGS_ACTIVITY_RECEIVE_MESSAGE);
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_ID, message.getId());
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_LATITUDE,
					message.getLatitude());
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_LONGITUDE,
					message.getLongitude());
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_RADIUS,
					message.getRadius());
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_ADDRESS,
					message.getAddress());
			intent.putExtra(ActivityConstants.ARGS_MESSAGE_IMAGE_NAME,
					message.getImageName());
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpMapIfNeeded() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				mMap.setMyLocationEnabled(true);
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		String radius;
		if (message.getRadius() < 10000) {
			radius = message.getRadius() + getString(R.string.text_meter);
		} else {
			radius = message.getRadius() / 1000
					+ getString(R.string.text_kilometer);
		}
		if (message != null) {
			mMap.addMarker(
					new MarkerOptions()
							.position(
									new LatLng(message.getLatitude(), message
											.getLongitude()))
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.icon_address))
							.title(getString(R.string.text_coverage) + radius)
							.snippet(message.getAddress())).showInfoWindow();

			mMap.addCircle(new CircleOptions()
					.center(new LatLng(message.getLatitude(), message
							.getLongitude())).radius(message.getRadius())
					.strokeWidth(1).strokeColor(0xFF88b5dc)
					.fillColor(1409351437));

			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
					message.getLatitude(), message.getLongitude()),
					GoogleMapUtils.getZoom((int) message.getRadius())));
		}
	}

	private void setUpLocationClientIfNeeded() {
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(this, this, this);
		}
		mLocationClient.connect();
	}

	private void setUpImageIfNeed() {
		String imageNamesStr = message.getImageName();
		if (TextUtils.isEmpty(imageNamesStr)) {
			imageFragment.setVisibility(View.INVISIBLE);
		} else {
			imageFragment.setVisibility(View.VISIBLE);
			imageIndex = new ArrayList<String>();

			imageFragment.setVisibility(View.VISIBLE);
			String[] imageNames = imageNamesStr.split(",");
			for (int i = 0; i < imageNames.length; i++) {
				// check whether exist
				String imageName = messageId + "_" + imageNames[i];
				String imagePath = Constants.THUMBNAIL_FLODER + imageName
						+ ".jpg";
				File file = new File(imagePath);
				if (file.exists()) {
					imageIndex.add(imageName);
					addImageItem(imagePath, imageName);
				} else {
					System.out.println("image name = " + messageId + "_"
							+ imageNames[i]);
					new DownloadThumbnail(messageId + "_" + imageNames[i])
							.execute();
				}
			}
		}
	}

	private void addImageItem(String imagePath, final String imageName) {
		// Instantiate a new "row" view.
		final ViewGroup newImage = (ViewGroup) LayoutInflater.from(this)
				.inflate(R.layout.item_image_display_thumbnail, mContainerView,
						false);
		ImageView image = (ImageView) newImage.findViewById(R.id.image);
		image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
		image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(ReceiveMessageActivity.this,
						DisplayImageActivity.class);
				// intent.putExtra("imageName", imageName);
				intent.putStringArrayListExtra("imageIndex", imageIndex);
				intent.putExtra("index", mContainerView.indexOfChild(newImage));
				startActivity(intent);
			}
		});
		mContainerView.addView(newImage, mContainerView.getChildCount());
	}

	private class DownloadThumbnail extends AsyncTask<Void, Void, String> {
		private String imageName;

		DownloadThumbnail(String imageName) {
			this.imageName = imageName;
		}

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("thumbnailName", imageName);

			return HttpRequestUtils.post("downloadThumbnail", map);
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage
						.equals(HttpConstants.DOWNLOAD_THUMBNAIL_SUCCESS)) {
					byte[] img = Base64.decode(json.getString("thumbnail"),
							Base64.DEFAULT);
					Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0,
							img.length);
					String imagePath = Constants.THUMBNAIL_FLODER + imageName
							+ ".jpg";
					ImageUtils.saveImage(bitmap, imagePath);
					imageIndex.add(imageName);
					addImageItem(imagePath, imageName);
				} else {
					System.out.println("downloadThumbnail ---->> "
							+ returnMessage);
				}
			} catch (JSONException e) {
				System.out
						.println("downloadThumbnail ---->> " + e.getMessage());
			}
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		System.out.println("onConnectionFailed = " + result.getErrorCode());
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if (servicesConnected() && firstLoad) {
			firstLoad = false;
			// calculate the distance
			double distance = GoogleMapUtils.getDistance(mLocationClient
					.getLastLocation().getLatitude(), mLocationClient
					.getLastLocation().getLongitude(), message.getLatitude(),
					message.getLongitude());

			msgDetailFrg = new MessageDetailFragment();
			Bundle bundle = new Bundle();
			if (distance <= message.getRadius()) {
				// save the message if in the radius
				DBMessage.insert(ReceiveMessageActivity.this, message);
				setUpMapIfNeeded();
				setUpImageIfNeed();
				bundle.putString(ActivityConstants.ARGS_ACCOUNT_USER_ID,
						message.getSenderId());
				bundle.putString(ActivityConstants.ARGS_MESSAGE_ID, messageId);
				bundle.putInt(ActivityConstants.ARGS_MESSAGE_STATUS,
						message.getStatus());
				bundle.putInt(ActivityConstants.ARGS_MESSAGE_TYPE,
						message.getType());
				bundle.putString(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG,
						message.getPrivateMsg());
				bundle.putString(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG,
						message.getPublicMsg());
				updateReplies();
			} else {
				bundle.putInt(ActivityConstants.ARGS_MESSAGE_TYPE,
						Constants.MESSAGE_TYPE_TEXT);
				bundle.putString(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG,
						message.getPublicMsg());
			}
			msgDetailFrg.setArguments(bundle);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.messageItem, msgDetailFrg).commit();
		}
	}

	@Override
	public void onDisconnected() {

	}

	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d(Constants.DEBUG_TAG,
					getString(R.string.play_services_available));

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getSupportFragmentManager(),
						Constants.DEBUG_TAG);
			}
			return false;
		}
	}

	private void updateReplies() {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("deviceGeneratedMessageId", messageId);
				map.put("order", String.valueOf(DBMessageReply
						.getLatestOrderByMsgId(ReceiveMessageActivity.this,
								messageId)));
				return HttpRequestUtils.post("updateReplies", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					// System.out.println("returnMessage = " + returnMessage);
					if (returnMessage
							.equals(HttpConstants.UPDATE_REPLIES_SUCCESS)) {
						JSONArray list = json.getJSONArray("messageReplies");
						for (int i = 0; i < list.length(); i++) {
							JSONObject object = (JSONObject) list.get(i);
							// System.out.println("object = " + object);
							MessageReply messageReply = new MessageReply();
							messageReply.setOrder(Integer.parseInt(object
									.getString("order")));
							messageReply.setMessageId(messageId);
							messageReply.setUserId(object.getString("userId"));
							messageReply.setUserName("");
							messageReply.setReplyContent(object
									.getString("replyContent"));
							messageReply.setDate(object.getString("date"));
							messageReply
									.setMyReply(object
											.getString("userId")
											.equals(SharePrefsUtils
													.getUserId(ReceiveMessageActivity.this)));
							messageReply
									.setMessageType(Constants.MESSAGE_TYPE_TEXT);
							// System.out.println("messageReply = " +
							// messageReply);
							DBMessageReply.insert(ReceiveMessageActivity.this,
									messageReply);
							msgDetailFrg.updateListView();
						}
					} else {
						msgDetailFrg.updateListView();
					}
				} catch (JSONException e) {
					Toast.makeText(ReceiveMessageActivity.this,
							getString(R.string.toast_update_failed),
							Toast.LENGTH_SHORT).show();
					System.out
							.println("updateReplies ---->> " + e.getMessage());
				}
			}
		}.execute();
	}

}
