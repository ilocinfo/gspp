package com.helloc.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockFragment;
import com.helloc.R;
import com.helloc.activity.RecordAudioActivity;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.utils.AudioUtils;
import com.helloc.utils.SystemUtils;

public class InputFragment extends SherlockFragment {

	private Button btnCamera;
	private Button btnMic;
	private EditText privateMsg;
	private Button btnEmoticon;
	private Button btnPlayAudio;
	private Button btnRemeoveAudio;
	private RelativeLayout textItem;
	private RelativeLayout audioItem;

	private String geofenceMsgId;
	private int messageType = Constants.MESSAGE_TYPE_TEXT;
	private boolean isEmoticonShow;

	private InputFragmentListener mListener;

	public interface InputFragmentListener {
		public void onCameraClicked();

		public void onEmoticonBtnClicked();

		public void hideEmoticon();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (InputFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement InputFragmentListener");
		}
		geofenceMsgId = getArguments().getString(
				ActivityConstants.ARGS_MESSAGE_ID);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_input, container, false);
		btnCamera = (Button) v.findViewById(R.id.btnCamera);
		btnMic = (Button) v.findViewById(R.id.btnMic);
		privateMsg = (EditText) v.findViewById(R.id.event);
		btnEmoticon = (Button) v.findViewById(R.id.btnEmoticon);
		textItem = (RelativeLayout) v.findViewById(R.id.textItem);
		audioItem = (RelativeLayout) v.findViewById(R.id.audioItem);
		btnPlayAudio = (Button) v.findViewById(R.id.btn_play_audio);
		btnRemeoveAudio = (Button) v.findViewById(R.id.btn_remove_audio);

		btnCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mListener.onCameraClicked();
			}
		});

		btnMic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(getSherlockActivity(),
						RecordAudioActivity.class);
				intent.putExtra("geofenceMsgId", geofenceMsgId);
				startActivityForResult(intent,
						ActivityConstants.REQUEST_GO_TO_RECORD_AUDIO_ACTIVITY);
			}
		});

		btnEmoticon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mListener.onEmoticonBtnClicked();
				if (isEmoticonShow) {
					SystemUtils.showSoftKeyboard(privateMsg, getActivity());
					isEmoticonShow = false;
				} else {
					SystemUtils.hideSoftKeyboard(privateMsg, getActivity());
					isEmoticonShow = true;
				}
			}
		});

		btnRemeoveAudio.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				audioItem.setVisibility(View.GONE);
				textItem.setVisibility(View.VISIBLE);
				messageType = Constants.MESSAGE_TYPE_TEXT;
			}
		});

		privateMsg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mListener.hideEmoticon();
			}
		});

		btnPlayAudio.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AudioUtils.playAudio(Constants.AUDIO_FLODER + geofenceMsgId
						+ ".amr");
			}
		});

		privateMsg.setOnTouchListener(new OnEditTextEventTouch());
		return v;
	}

	private class OnEditTextEventTouch implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (v.getId() == R.id.event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				switch (event.getAction() & MotionEvent.ACTION_MASK) {
				case MotionEvent.ACTION_UP:
					v.getParent().requestDisallowInterceptTouchEvent(false);
					break;
				}
			}
			return false;
		}

	}

	public boolean isInputCompleted() {
		if(TextUtils.isEmpty(privateMsg.getText()) == false) {
			return true;
		} else if (messageType == Constants.MESSAGE_TYPE_AUDIO) {
			return true;
		}
		return false;
	}

	public void setEmoticonShow(boolean isEmoticonShow) {
		this.isEmoticonShow = isEmoticonShow;
	}

	public void appendEmoticon(SpannableString emoticon) {
		privateMsg.append(emoticon);
	}

	public int getMessageType() {
		return messageType;
	}

	public String getPrivateMsg() {
		return privateMsg.getText().toString();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ActivityConstants.REQUEST_GO_TO_RECORD_AUDIO_ACTIVITY) {
			if (resultCode == ActivityConstants.RESULT_RESULT_OK) {
				audioItem.setVisibility(View.VISIBLE);
				textItem.setVisibility(View.GONE);
				messageType = Constants.MESSAGE_TYPE_AUDIO;
			}
		}
	}

}
