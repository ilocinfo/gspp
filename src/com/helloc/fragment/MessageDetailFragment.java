package com.helloc.fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.helloc.R;
import com.helloc.adapter.ChatMsgViewAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.database.DBMessageReply;
import com.helloc.model.MessageReply;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.NetworkUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.utils.SystemUtils;

public class MessageDetailFragment extends SherlockFragment {

	private String senderId;
	private String messageId;
	private int messageStatus;
	private int type;
	private String privateMsg;
	private String publicMsg;
	private MessageReply theme;

	private ListView listView;
	private List<MessageReply> listData;
	private ChatMsgViewAdapter adapter;

	private EditText replyContent;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		senderId = getArguments().getString(
				ActivityConstants.ARGS_ACCOUNT_USER_ID);
		messageId = getArguments().getString(ActivityConstants.ARGS_MESSAGE_ID);
		messageStatus = getArguments().getInt(
				ActivityConstants.ARGS_MESSAGE_STATUS);
		type = getArguments().getInt(ActivityConstants.ARGS_MESSAGE_TYPE);
		privateMsg = getArguments().getString(
				ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG);
		publicMsg = getArguments().getString(
				ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setTheme();
		
		View v;
		if(messageStatus == StatusConstants.MESSAGE_STATUS_SELF
				|| messageStatus == StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
			v = inflater.inflate(R.layout.fragment_chat_without_reply, container, false);
		} else {
			v = inflater.inflate(R.layout.fragment_chat_with_reply, container, false);
			replyContent = (EditText) v.findViewById(R.id.replyContent);
			v.findViewById(R.id.send).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					onSendBtnClicked();
				}
			});
		}
		
		listView = (ListView) v.findViewById(R.id.listview);
		updateListView();
		return v;
	}

	public void updateListView() {
		listData = DBMessageReply.getAllByMessageId(getSherlockActivity(),
				messageId);
		listData.add(0, theme);
		adapter = new ChatMsgViewAdapter(getSherlockActivity(), listData);
		listView.setAdapter(adapter);
		listView.setSelection(adapter.getCount() - 1);
	}

	private void setTheme() {
		theme = new MessageReply();
		theme.setMessageId(messageId);
		theme.setDate(messageId.substring(0, messageId.indexOf("_")));

		if (messageStatus == StatusConstants.MESSAGE_STATUS_SELF
				|| messageStatus == StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
			senderId = SharePrefsUtils.getUserId(getSherlockActivity());
		}
		theme.setUserId(senderId);

		if (senderId.equals(SharePrefsUtils.getUserId(getSherlockActivity()))) {
			theme.setUserName(getString(R.string.text_myself));
			theme.setMyReply(true);
		} else {
			theme.setUserName(DBFriend.getNameByUserId(getSherlockActivity(),
					senderId));
			theme.setMyReply(false);
		}

		if (type == Constants.MESSAGE_TYPE_AUDIO) {
			theme.setMessageType(Constants.MESSAGE_TYPE_AUDIO);
		} else if (type == Constants.MESSAGE_TYPE_TEXT) {
			theme.setMessageType(Constants.MESSAGE_TYPE_TEXT);
			if (TextUtils.isEmpty(privateMsg) == false) {
				theme.setReplyContent(privateMsg);
			} else if (TextUtils.isEmpty(publicMsg) == false) {
				theme.setReplyContent(publicMsg);
			}
		}
	}

	private void onSendBtnClicked() {
		if (TextUtils.isEmpty(replyContent.getText().toString())) {
			return;
		}

		if (NetworkUtils.isConnect(getSherlockActivity()) == false) {
			return;
		}

		SystemUtils.hideSoftKeyboard(replyContent, getSherlockActivity());
		
		new AsyncTask<String, Long, String>() {

			@Override
			protected String doInBackground(String... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("deviceGeneratedMessageId", messageId);
				map.put("order", String.valueOf(DBMessageReply
						.getLatestOrderByMsgId(getSherlockActivity(),
								messageId)));
				map.put("userId",
						SharePrefsUtils.getUserId(getSherlockActivity()));
				map.put("date", String.valueOf(System.currentTimeMillis()));
				map.put("replyContent", replyContent.getText().toString());

				return HttpRequestUtils.post("sendReplies", map);

			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.SEND_REPLIES_SUCCESS)) {
						JSONArray list = json.getJSONArray("messageReplies");
						for (int i = 0; i < list.length(); i++) {
							JSONObject object = (JSONObject) list.get(i);
							MessageReply messageReply = new MessageReply();
							messageReply.setOrder(Integer.parseInt(object
									.getString("order")));
							messageReply.setMessageId(messageId);
							messageReply.setUserId(object.getString("userId"));
							messageReply.setUserName("");
							messageReply.setReplyContent(object
									.getString("replyContent"));
							messageReply.setDate(object.getString("date"));
							messageReply
									.setMyReply(object
											.getString("userId")
											.equals(SharePrefsUtils
													.getUserId(getSherlockActivity())));
							messageReply
									.setMessageType(Constants.MESSAGE_TYPE_TEXT);
							DBMessageReply.insert(getSherlockActivity(),
									messageReply);
							updateListView();
							replyContent.setText("");
						}
					} else {
						Toast.makeText(getSherlockActivity(),
								getString(R.string.toast_send_failed),
								Toast.LENGTH_SHORT).show();
						System.out.println("sendReplies ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					Toast.makeText(getSherlockActivity(),
							getString(R.string.toast_send_failed),
							Toast.LENGTH_SHORT).show();
					System.out.println("sendReplies ---->> " + e.getMessage());
				}
			}
		}.execute();
	}
}
