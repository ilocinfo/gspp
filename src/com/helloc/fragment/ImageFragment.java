package com.helloc.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.helloc.R;
import com.helloc.activity.SelectImageActivity;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.utils.ImageUtils;
import com.helloc.view.ImageViewItem;

public class ImageFragment extends SherlockFragment implements
		ImageViewItem.ImageViewItemCallback {

	private Button addImage;
	private ViewGroup imagesContainer;
	private static final int PICK_FROM_CAMERA = 1;
	private static final int PICK_FROM_FILE = 2;
	private String messageId;
	private String imageName;
	private String imagePath;
	private Uri mImageCaptureUri;
	private int imageCounter = 0;
	private ArrayList<ImageViewItem> imgViewItemList;
	private int activitySrc;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activitySrc = getArguments().getInt(
				ActivityConstants.ARGS_ACTIVITY_SOURCE);
		messageId = getArguments().getString(ActivityConstants.ARGS_MESSAGE_ID);
		imageName = messageId + "_" + imageCounter;
		imagePath = Constants.IMAGE_FLODER + imageName + ".jpg";
		mImageCaptureUri = Uri.fromFile(new File(imagePath));
		imgViewItemList = new ArrayList<ImageViewItem>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_image, container, false);
		addImage = (Button) v.findViewById(R.id.addImage);
		imagesContainer = (ViewGroup) v.findViewById(R.id.imagesContainer);

		registerForContextMenu(addImage);
		addImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getSherlockActivity().openContextMenu(v);
			}
		});

		setUpImageViewIfNeed();
		return v;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, 0, 0, getString(R.string.action_take_photo));
		menu.add(0, 1, 1, getString(R.string.action_choose_photo));
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (imagesContainer.getChildCount() == 6) {
			Toast.makeText(getActivity(),
					getString(R.string.toast_max_image_num), Toast.LENGTH_SHORT)
					.show();
			return super.onContextItemSelected(item);
		}
		switch (item.getItemId()) {
		case 0:
			onCameraClicked();
			break;
		case 1:
			onGalleryClicked();
			break;
		}
		return super.onContextItemSelected(item);
	}

	private void setUpImageViewIfNeed() {
		if (activitySrc == ActivityConstants.ARGS_ACTIVITY_RECEIVE_MESSAGE) {
			String receivedImageName = getArguments().getString(
					ActivityConstants.ARGS_MESSAGE_IMAGE_NAME);
			if (TextUtils.isEmpty(receivedImageName) == false) {
				String[] imageNames = receivedImageName.split(",");
				for (int i = 0; i < imageNames.length; i++) {
					imageName = messageId + "_" + imageNames[i];
					imagePath = Constants.IMAGE_FLODER + imageName + ".jpg";
					addImageItem();
				}
			}
		}
	}

	private void onCameraClicked() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
				mImageCaptureUri);
		try {
			intent.putExtra("return-data", true);

			startActivityForResult(intent, PICK_FROM_CAMERA);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void onGalleryClicked() {
		Intent intent = new Intent();

		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);

		startActivityForResult(intent, PICK_FROM_FILE);
	}

	private void invokeNextActivity() {
		Intent intent = new Intent();
		intent.putExtra("imageName", imageName);
		intent.setClass(getSherlockActivity(), SelectImageActivity.class);
		startActivityForResult(intent,
				ActivityConstants.REQUEST_GO_TO_DISPLAY_IMAGE_ACTIVITY);
	}

	private void addImageItem() {
		if (imagesContainer.getChildCount() == 1) {
			imagesContainer.setVisibility(View.VISIBLE);
		}
		ImageViewItem imgViewItem;
		if (activitySrc == ActivityConstants.ARGS_ACTIVITY_SEND_MESSAGE) {
			imgViewItem = new ImageViewItem(getActivity(), imagesContainer,
					imageName, imageCounter, true);
		} else {
			imgViewItem = new ImageViewItem(getActivity(), imagesContainer,
					imageName, imageCounter, false);
		}

		imgViewItem.setCallback(this);
		imgViewItemList.add(imgViewItem);

		imagesContainer.addView(imgViewItem.getInstance(),
				imagesContainer.getChildCount() - 1);

		imageCounter++;
		imageName = messageId + "_" + imageCounter;
		imagePath = Constants.IMAGE_FLODER + imageName + ".jpg";
		mImageCaptureUri = Uri.fromFile(new File(imagePath));
	}

	public void openContextMenu() {
		getSherlockActivity().openContextMenu(addImage);
	}

	public boolean isImageSelected() {
		if (imagesContainer.getChildCount() > 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case PICK_FROM_CAMERA:
			if (resultCode != Activity.RESULT_OK) {
				return;
			}
			invokeNextActivity();
			break;
		case PICK_FROM_FILE:
			if (resultCode != Activity.RESULT_OK) {
				return;
			}
			mImageCaptureUri = data.getData();
			try {
				Bitmap image = MediaStore.Images.Media.getBitmap(
						getSherlockActivity().getContentResolver(),
						mImageCaptureUri);
				if (image != null) {

					ImageUtils.saveImage(image, imagePath);
					image.recycle();
					invokeNextActivity();
				} else {
					Toast.makeText(getSherlockActivity(),
							getString(R.string.toast_get_image_failed),
							Toast.LENGTH_SHORT).show();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case ActivityConstants.REQUEST_GO_TO_DISPLAY_IMAGE_ACTIVITY:
			if (resultCode == ActivityConstants.RESULT_RESULT_OK) {
				addImageItem();
			}
			break;
		}

	}

	@Override
	public void deleteImage(ImageViewItem instance) {
		imgViewItemList.remove(instance);
		imagesContainer.removeView(instance.getInstance());
		if (imagesContainer.getChildCount() == 1) {
			imagesContainer.setVisibility(View.GONE);
		}
	}

	public String getImagesName() {
		StringBuilder imagesName = new StringBuilder();
		for (int i = 0; i < imgViewItemList.size(); i++) {
			imagesName.append(imgViewItemList.get(i).getIndex());
			imagesName.append(",");
		}
		return imagesName.toString();
	}

}
