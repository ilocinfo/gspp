package com.helloc.fragment;

import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.helloc.R;
import com.helloc.adapter.LocationsListviewAdapter;
import com.helloc.adapter.LocationsListviewAdapter.LocationsListAdapterCallback;
import com.helloc.database.DBLocations;
import com.helloc.model.Locations;
import com.helloc.utils.SystemUtils;

public class CommonLocationFragment extends Fragment implements
		LocationsListAdapterCallback {

	private EditText etName;
	private EditText etAddress;
	private Button add;
	private LinearLayout addForm;
	private LatLng point;

	private ListView listView;
	private LocationsListviewAdapter adapter;
	// private SimpleAdapter adapter;
	private ArrayList<Map<String, String>> dataList;
	private onItemClickedListener mCallback;

	GestureDetector mGesture;

	public interface onItemClickedListener {
		public void onListItemClicked(double latitude, double longitude,
				String address);

		public void onFragmentClose();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dataList = new ArrayList<Map<String, String>>();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception.
		try {
			mCallback = (onItemClickedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement onItemClickedListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater
				.inflate(R.layout.fragment_locations, container, false);

		mGesture = new GestureDetector(getActivity(), new MyGestureListener());

		addForm = (LinearLayout) v.findViewById(R.id.add_form);
		etName = (EditText) v.findViewById(R.id.loc_name);
		etAddress = (EditText) v.findViewById(R.id.loc_address);
		add = (Button) v.findViewById(R.id.loc_add);

		addForm.setVisibility(View.GONE);

		add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (TextUtils.isEmpty(etName.getText())) {
					Toast.makeText(getActivity(),
							getString(R.string.toast_please_input_name),
							Toast.LENGTH_SHORT).show();
					return;
				} else if (TextUtils.isEmpty(etAddress.getText())) {
					Toast.makeText(getActivity(),
							getString(R.string.toast_please_input_address),
							Toast.LENGTH_SHORT).show();
					return;
				}

				SystemUtils.switchSoftKeyboardstate(getActivity());

				Locations location = new Locations();
				location.setAddress(etAddress.getText().toString());
				location.setName(etName.getText().toString());
				location.setLatitude(point.latitude);
				location.setLongitude(point.longitude);
				DBLocations.insert(getActivity(), location);

				// update list view
				updateListview();

				// hide the add form
				addForm.setVisibility(View.GONE);
				Toast.makeText(getActivity(),
						getString(R.string.toast_add_succeed),
						Toast.LENGTH_SHORT).show();
			}
		});

		listView = (ListView) v.findViewById(R.id.list);
		listView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return mGesture.onTouchEvent(event);
			}
		});
		updateListview();
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int index,
					long arg3) {
				mCallback.onListItemClicked(Double.parseDouble(dataList.get(
						index).get("latitude")), Double.parseDouble(dataList
						.get(index).get("longitude")),
						dataList.get(index).get("address"));
			}
		});
		return v;
	}

	// update list view
	private void updateListview() {
		dataList = DBLocations.getAll(getActivity());
		adapter = new LocationsListviewAdapter(getActivity(), dataList);
		adapter.setCallback(this);
		listView.setAdapter(adapter);
	}

	public void setAddress(String address, LatLng point) {
		this.point = point;
		addForm.setVisibility(View.VISIBLE);
		this.etAddress.setText(address);
	}

	@Override
	public void deleteItem(int position) {
		// delete data
		DBLocations.deleteById(getActivity(), position);
		updateListview();
	}

	class MyGestureListener extends SimpleOnGestureListener {

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			// TODO Auto-generated method stub
			if (e1.getX() - e2.getX() > 100) {
				mCallback.onFragmentClose();
			}
			return super.onFling(e1, e2, velocityX, velocityY);
		}
	}

}
