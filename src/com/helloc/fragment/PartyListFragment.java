package com.helloc.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.helloc.R;
import com.helloc.activity.ReceivePartyActivity;
import com.helloc.activity.SendPartyActivity;
import com.helloc.activity.ShowPartyDetailsActivity;
import com.helloc.adapter.PartyListViewAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBParty;
import com.helloc.database.DBPartying;
import com.helloc.model.Party;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.SharePrefsUtils;

public class PartyListFragment extends SherlockFragment {
	private int partyStatus;
	private ListView listView;

	private ArrayList<Party> partyList;
	private PartyListViewAdapter listViewAdapter;
	private int itemPosition = -1;
	private Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		partyStatus = getArguments()
				.getInt(ActivityConstants.ARGS_PARTY_STATUS);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getSherlockActivity();
		View v = inflater.inflate(R.layout.fragment_message_list, container,
				false);
		listView = (ListView) v.findViewById(R.id.listview);
		registerForContextMenu(listView);
		setUpListView();
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		com.actionbarsherlock.view.MenuItem populateItem = menu.add(Menu.NONE,
				0, 0, getString(R.string.action_new));
		populateItem
				.setShowAsAction(com.actionbarsherlock.view.MenuItem.SHOW_AS_ACTION_ALWAYS);

		if (partyStatus == StatusConstants.PARTY_STATUS_OVER) {
			populateItem = menu.add(Menu.NONE, 1, 0,
					getString(R.string.action_clear));
			populateItem
					.setShowAsAction(com.actionbarsherlock.view.MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			Intent intent = new Intent(getSherlockActivity(),
					SendPartyActivity.class);
			startActivity(intent);
			getSherlockActivity().finish();
			return true;
			// clear the listview
		case 1:
			new AlertDialog.Builder(getSherlockActivity())
					.setTitle(getString(R.string.action_clear))
					.setMessage(getString(R.string.text_clear_hint))
					.setPositiveButton(getString(R.string.btn_confirm),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									if (partyList.size() > 0) {
										DBParty.deleteByStatus(
												getSherlockActivity(),
												partyStatus);
										DBPartying.deleteByIds(
												getSherlockActivity(),
												partyList);

										// update listview
										partyList.clear();
										listViewAdapter.notifyDataSetChanged();
										listView.postInvalidate();
									}
								}
							})
					.setNegativeButton(getString(R.string.btn_cancel), null)
					.create().show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, 0, 0, getString(R.string.action_delete));
		menu.add(0, 1, 0, getString(R.string.action_details));
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (itemPosition != -1) {
			switch (item.getItemId()) {
			case 0:
				// delete from database
				DBParty.deleteById(getSherlockActivity(),
						partyList.get(itemPosition).getPartyId());
				DBPartying.deleteById(getSherlockActivity(),
						partyList.get(itemPosition).getPartyId());

				quitParty(partyList.get(itemPosition).getPartyId());

				// update the listview
				partyList.remove(itemPosition);
				listViewAdapter.notifyDataSetChanged();
				listView.postInvalidate();

				itemPosition = -1;
				break;
			case 1:
				Intent intent = new Intent();
				intent.setClass(getSherlockActivity(),
						ShowPartyDetailsActivity.class);
				intent.putExtra(ActivityConstants.ARGS_PARTY_THEME, partyList
						.get(itemPosition).getTheme());
				intent.putExtra(ActivityConstants.ARGS_PARTY_ORIGINATOR,
						partyList.get(itemPosition).getOriginator());
				intent.putExtra(ActivityConstants.ARGS_PARTY_DESCRIPTION,
						partyList.get(itemPosition).getDescription());
				intent.putExtra(ActivityConstants.ARGS_PARTY_ADDRESS, partyList
						.get(itemPosition).getAddress());
				intent.putExtra(ActivityConstants.ARGS_PARTY_START_DATE,
						partyList.get(itemPosition).getStartDate());
				intent.putExtra(ActivityConstants.ARGS_PARTY_START_TIME,
						partyList.get(itemPosition).getStartTime());
				intent.putExtra(ActivityConstants.ARGS_PARTY_END_DATE,
						partyList.get(itemPosition).getEndDate());
				intent.putExtra(ActivityConstants.ARGS_PARTY_END_TIME,
						partyList.get(itemPosition).getEndTime());
				startActivity(intent);
				break;
			}
		}

		return super.onContextItemSelected(item);
	}

	private void setUpListView() {
		partyList = DBParty.getAllByStatus(getSherlockActivity(), partyStatus);
		listViewAdapter = new PartyListViewAdapter(getSherlockActivity(),
				partyList);
		listView.setAdapter(listViewAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				itemPosition = position;
				Intent intent = new Intent();
				intent.setClass(getSherlockActivity(),
						ReceivePartyActivity.class);
				intent.putExtra(ActivityConstants.ARGS_PARTY_ID,
						partyList.get(position).getPartyId());
				intent.putExtra(ActivityConstants.ARGS_PARTY_LATITUDE,
						partyList.get(position).getLatitude());
				intent.putExtra(ActivityConstants.ARGS_PARTY_LONGITUDE,
						partyList.get(position).getLongitude());
				intent.putExtra(ActivityConstants.ARGS_PARTY_ADDRESS, partyList
						.get(position).getAddress());
				startActivityForResult(intent,
						ActivityConstants.REQUEST_GO_TO_RECEIVE_PARTY_ACTIVITY);
			}
		});
		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				itemPosition = position;
				return false;
			}

		});
	}

	private void quitParty(final String partyId) {

		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("userId", SharePrefsUtils.getUserId(context));
				map.put("deviceGeneratedPartyId", partyId);
				System.out.println("map = " + map);
				return HttpRequestUtils.post("quitParty", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage.equals(HttpConstants.QUIT_PARTY_SUCCESS)) {

					} else {
						System.out.println("quitParty ---->> " + returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("quitParty ---->> " + e.getMessage());
				}
			}

		}.execute();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ActivityConstants.REQUEST_GO_TO_RECEIVE_PARTY_ACTIVITY) {
			if (resultCode == ActivityConstants.RESULT_UPDATE_UI) {
				// update the listview
				partyList.remove(itemPosition);
				listViewAdapter.notifyDataSetChanged();
				listView.postInvalidate();
				itemPosition = -1;
			}
		}
	}

}
