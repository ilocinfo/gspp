package com.helloc.fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockFragment;
import com.helloc.R;
import com.helloc.activity.SelectFriendsActivity;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.view.AvatarView;
import com.helloc.view.AvatarView.AvatarViewCallback;

public class SendToFragment extends SherlockFragment implements
		AvatarViewCallback {

	private Button addFriends;
	private ViewGroup friendsContainer;
	private RelativeLayout wxItem;
	private Button sendToWX;
	private Button btnRemove;
	private CheckBox feedback;
	private EditText publicMsg;
	private ArrayList<String> friendsIdList;
	private boolean enablePublicMsg;
	private String receiverId; // pass from UserProfileActivity if exist
	private int wxType = Constants.SEND_TO_WECHAT_NULL;
	private boolean isOtherappsSelected;
	private int activitySrc;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activitySrc = getArguments().getInt(
				ActivityConstants.ARGS_ACTIVITY_SOURCE);
		enablePublicMsg = getArguments().getBoolean(
				ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG);
		receiverId = getArguments().getString(
				ActivityConstants.ARGS_ACCOUNT_USER_ID);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_send_to, container, false);
		addFriends = (Button) v.findViewById(R.id.addFriends);
		wxItem = (RelativeLayout) v.findViewById(R.id.wxItem);
		sendToWX = (Button) v.findViewById(R.id.sendToWX);
		btnRemove = (Button) v.findViewById(R.id.btnRemove);
		publicMsg = (EditText) v.findViewById(R.id.publicMsg);
		feedback = (CheckBox) v.findViewById(R.id.feedback);
		friendsContainer = (ViewGroup) v.findViewById(R.id.friendsContainer);

		friendsIdList = new ArrayList<String>();
		if (TextUtils.isEmpty(receiverId) == false) {
			friendsIdList.add(receiverId);
			updateView();
		}

		btnRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				wxType = Constants.SEND_TO_WECHAT_NULL;
				wxItem.setVisibility(View.GONE);
			}
		});

		addFriends.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(getSherlockActivity(),
						SelectFriendsActivity.class);
				intent.putStringArrayListExtra("friendsIdList", friendsIdList);
				intent.putExtra("wxType", wxType);
				intent.putExtra("isOtherappsSelected", isOtherappsSelected);
				startActivityForResult(intent,
						ActivityConstants.REQUEST_GO_TO_SELECT_FRIENDS_ACTIVITY);
			}
		});
		return v;
	}

	private void updateView() {
		int size = friendsContainer.getChildCount();
		for (int i = 0; i < size - 1; i++) {
			friendsContainer.removeViewAt(0);
		}

		// set wechat
		if (enablePublicMsg) {
			switch (wxType) {
			case Constants.SEND_TO_WECHAT_NULL:
				wxItem.setVisibility(View.GONE);
				break;
			case Constants.SEND_TO_WECHAT_FRIEND:
				wxItem.setVisibility(View.VISIBLE);
				sendToWX.setBackgroundResource(R.drawable.icon_wx);
				break;
			case Constants.SEND_TO_WECHAT_MOMENT:
				wxItem.setVisibility(View.VISIBLE);
				sendToWX.setBackgroundResource(R.drawable.icon_moment);
				break;
			}
		} else {
			AvatarView wxView;
			switch (wxType) {
			case Constants.SEND_TO_WECHAT_FRIEND:
				wxView = new AvatarView(getSherlockActivity(),
						friendsContainer, Constants.SEND_TO_WECHAT_FRIEND);
				wxView.setCallback(this);
				friendsContainer.addView(wxView.getInstance(), 0);

				break;
			case Constants.SEND_TO_WECHAT_MOMENT:
				wxView = new AvatarView(getSherlockActivity(),
						friendsContainer, Constants.SEND_TO_WECHAT_MOMENT);
				wxView.setCallback(this);
				friendsContainer.addView(wxView.getInstance(), 0);
				break;
			}
		}

		// set other app
		if (isOtherappsSelected) {
			AvatarView wxView = new AvatarView(getSherlockActivity(),
					friendsContainer);
			wxView.setCallback(this);
			friendsContainer.addView(wxView.getInstance(),
					friendsContainer.getChildCount() - 1);
		}

		// set friends list
		for (int i = 0; i < friendsIdList.size(); i++) {
			AvatarView avatarView = new AvatarView(getSherlockActivity(),
					friendsContainer, friendsIdList.get(i));
			avatarView.setCallback(this);
			friendsContainer.addView(avatarView.getInstance(),
					friendsContainer.getChildCount() - 1);
		}

		updateFeedback();
	}

	private void updateFeedback() {
		feedback.setVisibility(View.INVISIBLE);
		if (activitySrc == ActivityConstants.ARGS_ACTIVITY_SEND_MESSAGE) {
			for (int i = 0; i < friendsIdList.size(); i++) {
				// set feedback
				if (DBFriend.getAutoReplyPermission(getSherlockActivity(),
						friendsIdList.get(i)) == StatusConstants.AUTO_REPLY_STATUS_ALLOWED) {
					feedback.setVisibility(View.VISIBLE);
				} else {
					feedback.setVisibility(View.INVISIBLE);
					break;
				}
			}
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ActivityConstants.REQUEST_GO_TO_SELECT_FRIENDS_ACTIVITY) {
			if (resultCode == ActivityConstants.RESULT_RESULT_OK) {
				friendsIdList = data.getStringArrayListExtra("friendsIdList");
				wxType = data.getIntExtra("wxType",
						Constants.SEND_TO_WECHAT_NULL);
				isOtherappsSelected = data.getBooleanExtra(
						"isOtherappsSelected", false);
				updateView();
			}
		}
	}

	@Override
	public void deleteAvatar(AvatarView instance) {
		friendsIdList.remove(instance.getUserId());
		friendsContainer.removeView(instance.getInstance());
		updateFeedback();
	}

	@Override
	public void resetWxType(AvatarView instance) {
		wxType = Constants.SEND_TO_WECHAT_NULL;
		friendsContainer.removeView(instance.getInstance());
	}

	@Override
	public void deleteOtherApp(AvatarView instance) {
		isOtherappsSelected = false;
		friendsContainer.removeView(instance.getInstance());
	}

	public boolean isFriendsSelected() {
		if (friendsIdList.size() == 0
				&& wxType == Constants.SEND_TO_WECHAT_NULL
				&& isOtherappsSelected == false) {
			return false;
		}
		return true;
	}

	public int getWxType() {
		return wxType;
	}

	public String getPublicMsg() {
		return publicMsg.getText().toString();
	}

	public String getReceivers() {
		StringBuilder receivers = new StringBuilder();
		if (wxType != Constants.SEND_TO_WECHAT_NULL) {
			receivers.append(wxType);
			receivers.append(",");
		}
		if (isOtherappsSelected) {
			receivers.append(Constants.SEND_TO_OTHER_APP);
			receivers.append(",");
		}
		for (int i = 0; i < friendsIdList.size(); i++) {
			receivers.append(friendsIdList.get(i));
			receivers.append(",");
		}
		if (friendsIdList.size() > 0) {
			receivers.deleteCharAt(receivers.length() - 1);
		}
		return receivers.toString();
	}

	public boolean isOtherappsSelected() {
		return isOtherappsSelected;
	}

	public boolean needFeedback() {
		return feedback.isChecked();
	}

}
