package com.helloc.fragment;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBInvitation;
import com.helloc.model.Invitation;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.SharePrefsUtils;

public class SendInvitationDialog extends SherlockDialogFragment {

	private static Context context;
	EditText invitationMsg;

	private SendInvitationDialogListener mListener;

	public interface SendInvitationDialogListener {
		public void updateView();
	}

	public static SendInvitationDialog newInstance(String userId, String uuid,
			String nickname, String phone, Context context) {
		SendInvitationDialog.context = context;
		SendInvitationDialog dialog = new SendInvitationDialog();
		Bundle args = new Bundle();
		args.putString(ActivityConstants.ARGS_ACCOUNT_USER_ID, userId);
		args.putString(ActivityConstants.ARGS_ACCOUNT_UUID, uuid);
		args.putString(ActivityConstants.ARGS_ACCOUNT_NICKNAME, nickname);
		args.putString(ActivityConstants.ARGS_ACCOUNT_PHONE, phone);
		dialog.setArguments(args);
		return dialog;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (SendInvitationDialogListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement SendInvitationDialogListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = LayoutInflater.from(getSherlockActivity());
		View view = inflater.inflate(R.layout.dialog_send_invitation, null);
		invitationMsg = (EditText) view.findViewById(R.id.invitation_message);

		return new AlertDialog.Builder(getSherlockActivity())
				.setTitle(getString(R.string.text_send_invitation))
				.setView(view)
				.setNegativeButton(getString(R.string.btn_cancel), null)
				.setPositiveButton(getString(R.string.btn_confirm),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								sendInvitation();
							}
						}).create();
	}

	private void sendInvitation() {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("senderId", SharePrefsUtils.getUserId(context));
				map.put("invitationMessage", invitationMsg.getText().toString());
				map.put("receiverId",
						getArguments().getString(
								ActivityConstants.ARGS_ACCOUNT_USER_ID));
				return HttpRequestUtils.post("sendInvitation", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.SEND_INVITATION_SUCCESS)) {
						Invitation invitation = new Invitation();
						invitation.setInvitationMsg(invitationMsg.getText()
								.toString());
						invitation.setUserId(getArguments().getString(
								ActivityConstants.ARGS_ACCOUNT_USER_ID));
						invitation.setUuid(getArguments().getString(
								ActivityConstants.ARGS_ACCOUNT_UUID));
						invitation.setNickName(getArguments().getString(
								ActivityConstants.ARGS_ACCOUNT_NICKNAME));
						invitation.setPhone(getArguments().getString(
								ActivityConstants.ARGS_ACCOUNT_PHONE));
						invitation
								.setStatus(StatusConstants.INVITATION_STATUS_SENT);

						DBInvitation.insert(context, invitation);
						Toast.makeText(context,
								context.getString(R.string.toast_send_success),
								Toast.LENGTH_SHORT).show();
						mListener.updateView();
					} else {
						System.out.println("sendInvitation ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("sendInvitation ---->> "
							+ e.getMessage());
				}
			}

		}.execute();
	}
}
