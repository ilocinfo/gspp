package com.helloc.fragment;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.utils.GoogleMapUtils;

public class RadiusFragment extends SherlockFragment {
	private int radius = Constants.RADIUS_BASE;
	private TextView tvRadius;
	private TextView tvRadiusUnit;
	private ImageButton radiusIncrease;
	private ImageButton radiusDecrease;
	private Timer timer;
	private TimerTask timerTask;
	private int counter;
	private int state;
	private final int state_increase = 0;
	private final int state_decrease = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_radius, container, false);
		tvRadius = (TextView) v.findViewById(R.id.radius);
		tvRadiusUnit = (TextView) v.findViewById(R.id.radiusUnit);
		radiusIncrease = (ImageButton) v.findViewById(R.id.radiusIncrease);
		radiusDecrease = (ImageButton) v.findViewById(R.id.radiusDecrease);

		tvRadius.setText(String.valueOf(radius));
		
		radiusIncrease.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					state = state_increase;
					reScheduleTimer();
					break;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
					timer.cancel();
					counter = 0;
					break;
				}
				return false;
			}
		});
		
		radiusDecrease.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					state = state_decrease;
					reScheduleTimer();
					break;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
					timer.cancel();
					counter = 0;
					break;
				}
				return false;
			}
		});
		return v;
	}

	public void reScheduleTimer() {
		timer = new Timer();
		timerTask = new myTimerTask();
		timer.schedule(timerTask, 0, 100);
	}

	private class myTimerTask extends TimerTask {
		@Override
		public void run() {
			counter++;
			if (state == state_increase) {
				radius = GoogleMapUtils.getIncreaseRadius(radius, counter);
			} else if (state == state_decrease) {
				radius = GoogleMapUtils.getDecreaseRadius(radius, counter);
			}
			updateRadius.sendEmptyMessage(0);
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler updateRadius = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			updateRadius();
		}
	};

	private void updateRadius() {
		if(radius < 10000) {
			tvRadius.setText(String.valueOf(radius));
			tvRadiusUnit.setText(R.string.text_meter);
		} else {
			tvRadius.setText(String.valueOf(radius/1000));
			tvRadiusUnit.setText(R.string.text_kilometer);
		}
	}
	
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
		updateRadius();
	}
}
