package com.helloc.fragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.helloc.R;
import com.helloc.activity.SelfGeofenceActivity;
import com.helloc.activity.SendMessageActivity;
import com.helloc.activity.ShowMessageActivity;
import com.helloc.adapter.MessageListviewAdapter;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessage;
import com.helloc.database.DBMessageReply;
import com.helloc.model.Message;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.MyTextUtils;

public class MessageListFragment extends SherlockFragment {
	private int messageStatus = -1;
	private ListView listView;

	private ArrayList<Message> messageList;
	private List<Map<String, String>> listItems;
	private MessageListviewAdapter listViewAdapter;
	private int itemPosition = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		messageStatus = getArguments().getInt(StatusConstants.MESSAGE_STATUS);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_message_list, container,
				false);
		listView = (ListView) v.findViewById(R.id.listview);
		registerForContextMenu(listView);
		messageList = DBMessage.getAllByStatus(getSherlockActivity(),
				messageStatus);
		setUpListView();
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		com.actionbarsherlock.view.MenuItem populateItem = menu.add(Menu.NONE,
				0, 0, getString(R.string.action_new));
		populateItem
				.setShowAsAction(com.actionbarsherlock.view.MenuItem.SHOW_AS_ACTION_ALWAYS);

		populateItem = menu.add(Menu.NONE, 1, 0,
				getString(R.string.action_clear));
		populateItem
				.setShowAsAction(com.actionbarsherlock.view.MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			if (messageStatus == StatusConstants.MESSAGE_STATUS_SELF
					|| messageStatus == StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
				Intent intent = new Intent(getSherlockActivity(),
						SelfGeofenceActivity.class);
				startActivity(intent);
				getSherlockActivity().finish();
			} else if (messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED
					|| messageStatus == StatusConstants.MESSAGE_STATUS_SENT) {
				Intent intent = new Intent(getSherlockActivity(),
						SendMessageActivity.class);
				startActivity(intent);
				getSherlockActivity().finish();
			}
			return true;
			// clear the listview
		case 1:
			new AlertDialog.Builder(getSherlockActivity())
					.setTitle(getString(R.string.action_clear))
					.setMessage(getString(R.string.text_clear_hint))
					.setPositiveButton(getString(R.string.btn_confirm),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									if (messageList.size() > 0) {
										// delete all message from database according to the message status
										DBMessage.deleteByStatus(
												getSherlockActivity(),
												messageStatus);
										// delete message reply
										if (messageStatus != StatusConstants.MESSAGE_STATUS_SELF
												&& messageStatus != StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
											for (int i = 0; i < messageList .size(); i++) {
												DBMessageReply.deleteByMsgId(getSherlockActivity(),
														messageList.get(i).getId());
											}
										}
										// remove all geofence if the status is MESSAGE_STATUS_SELF
										if (messageStatus == StatusConstants.MESSAGE_STATUS_SELF) {
											GeofenceUtils.removeGeofences(
													getSherlockActivity(),
													messageList);
										}
										// update the listview
										messageList.clear();
										listItems.clear();
										listViewAdapter.notifyDataSetChanged();
										listView.postInvalidate();
									}
								}
							})
					.setNegativeButton(getString(R.string.btn_cancel), null)
					.create().show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, 0, 0, getString(R.string.action_delete));
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			if (itemPosition != -1) {
				// delete from database
				DBMessage.deleteByIdAndStatus(getSherlockActivity(),
						messageList.get(itemPosition).getId(), messageStatus);
				// delete message reply
				if (messageStatus != StatusConstants.MESSAGE_STATUS_SELF
						&& messageStatus != StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
					DBMessageReply.deleteByMsgId(getSherlockActivity(),
							messageList.get(itemPosition).getId());
				}
				// remove the geofence if the status is MESSAGE_STATUS_SELF
				if (messageStatus == StatusConstants.MESSAGE_STATUS_SELF) {
					GeofenceUtils.removeGeofence(getSherlockActivity(),
							messageList.get(itemPosition).getId());
				}
				// update the listview
				messageList.remove(itemPosition);
				listItems.remove(itemPosition);
				listViewAdapter.notifyDataSetChanged();
				listView.postInvalidate();
				itemPosition = -1;
			}
			break;
		}
		return super.onContextItemSelected(item);
	}

	private void setUpListView() {
		listItems = new ArrayList<Map<String, String>>();
		listViewAdapter = new MessageListviewAdapter(getActivity(), listItems);
		for (int i = 0; i < messageList.size(); i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put(ActivityConstants.ARGS_MESSAGE_DISPLAY_NAME,
					MyTextUtils.getReceivers(getSherlockActivity(),
							messageList.get(i)));
			map.put(ActivityConstants.ARGS_MESSAGE_ID, messageList.get(i)
					.getId());
			map.put(ActivityConstants.ARGS_MESSAGE_ADDRESS, messageList.get(i)
					.getAddress());
			map.put(ActivityConstants.ARGS_MESSAGE_TYPE,
					String.valueOf(messageList.get(i).getType()));
			map.put(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG, messageList
					.get(i).getPrivateMsg());
			map.put(ActivityConstants.ARGS_MESSAGE_STATUS,
					String.valueOf(messageStatus));
			map.put(ActivityConstants.ARGS_MESSAGE_EXPIRATION_TIME,
					getLatestDate(messageList.get(i).getId()));
			listItems.add(map);
		}
		listView.setAdapter(listViewAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Intent intent = new Intent();
				intent.setClass(getSherlockActivity(),
						ShowMessageActivity.class);
				intent.putExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID,
						messageList.get(position).getSenderId());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_ID, messageList
						.get(position).getId());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_STATUS,
						messageStatus);
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_LATITUDE,
						messageList.get(position).getLatitude());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_LONGITUDE,
						messageList.get(position).getLongitude());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_RADIUS,
						messageList.get(position).getRadius());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_ADDRESS,
						messageList.get(position).getAddress());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_TYPE,
						messageList.get(position).getType());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG,
						messageList.get(position).getPrivateMsg());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG,
						messageList.get(position).getPublicMsg());
				intent.putExtra(ActivityConstants.ARGS_MESSAGE_IMAGE_NAME,
						messageList.get(position).getImageName());
				startActivity(intent);
			}
		});
		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				itemPosition = position;
				getSherlockActivity().openContextMenu(listView);
				return true;
			}
		});
	}

	private String getLatestDate(String messageId) {
		String latestDate;
		String timeMills = messageId.substring(0, messageId.indexOf("_"));
		if (messageStatus == StatusConstants.MESSAGE_STATUS_SENT
				|| messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED
				|| messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED) {
			String latestReplyDate = DBMessageReply.getLatestDateByMsgId(
					getSherlockActivity(), messageId);
			if (latestReplyDate != null) {
				timeMills = latestReplyDate;
			}
		}
		Date date = new Date(Long.parseLong(timeMills));
		latestDate = DateFormat.getTimeFormat(getSherlockActivity()).format(
				date)
				+ ", "
				+ DateFormat.getMediumDateFormat(getSherlockActivity()).format(
						date);
		return latestDate;
	}

}
