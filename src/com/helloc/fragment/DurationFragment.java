package com.helloc.fragment;

import net.simonvt.numberpicker.NumberPicker;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.helloc.R;

public class DurationFragment extends SherlockFragment {

	private TextView tvDuration;
	private int month;
	private int day = 1;
	private int hour;
	private long duration = 1; // in hour

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_duration, container, false);

		tvDuration = (TextView) v.findViewById(R.id.duration);
		tvDuration.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getSherlockActivity());
				LayoutInflater inflater = LayoutInflater
						.from(getSherlockActivity());
				View view = inflater.inflate(R.layout.dialog_duration_picker,
						null);
				final NumberPicker monthPicker = (NumberPicker) view
						.findViewById(R.id.month_picker);
				monthPicker.setMaxValue(12);
				monthPicker.setMinValue(0);
				monthPicker.setValue(month);
				final NumberPicker dayPicker = (NumberPicker) view
						.findViewById(R.id.day_picker);
				dayPicker.setMaxValue(30);
				dayPicker.setMinValue(0);
				dayPicker.setValue(day);
				final NumberPicker hourPicker = (NumberPicker) view
						.findViewById(R.id.hour_picker);
				hourPicker.setMaxValue(23);
				hourPicker.setMinValue(0);
				hourPicker.setValue(hour);
				builder.setView(view);
				builder.setPositiveButton(getString(R.string.btn_confirm),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								month = monthPicker.getValue();
								day = dayPicker.getValue();
								hour = hourPicker.getValue();
								StringBuilder temp = new StringBuilder();
								temp.append(month == 0 ? "" : month + " "
										+ getString(R.string.text_month) + "  ");
								temp.append(day == 0 ? "" : day + " "
										+ getString(R.string.text_day) + "  ");
								temp.append(hour == 0 ? "" : hour + " "
										+ getString(R.string.text_hour));
								if (temp.length() == 0) {
									tvDuration
											.setText(getString(R.string.text_one_day));
								} else {
									tvDuration.setText(temp.toString());
								}
								duration = month * 30 * 24 + day * 24 + hour;
							}
						});
				builder.create().show();

			}
		});

		return v;
	}

	public long getDuration() {
		duration = month * 30 * 24 + day * 24 + hour;
		System.out.println("duration = " + duration);
		return duration * DateUtils.HOUR_IN_MILLIS;
	}

}
