package com.helloc.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import com.actionbarsherlock.app.SherlockFragment;
import com.helloc.R;
import com.helloc.model.Expressions;

public class EmoticonFragment extends SherlockFragment{

	private ViewPager viewPager;
	private ArrayList<GridView> grids;
	private GridView gView0;
	private GridView gView1;
	private GridView gView2;
	private SimpleAdapter mAdapter0;
	private SimpleAdapter mAdapter1;
	private SimpleAdapter mAdapter2;
	private ImageView page0;
	private ImageView page1;
	private ImageView page2;
	private int[] expressionImages0;
	private String[] expressionImageNames0;
	private int[] expressionImages1;
	private String[] expressionImageNames1;
	private int[] expressionImages2;
	private String[] expressionImageNames2;
	private EmoticonFragmentListener mListener;
	
	public interface EmoticonFragmentListener {
		public void onEmoticonSelected(SpannableString emoticon);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (EmoticonFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(getSherlockActivity().toString()
                    + " must implement EmoticonFragmentListener");
        }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater
				.inflate(R.layout.fragment_emoticon, container, false);
		viewPager = (ViewPager) v.findViewById(R.id.viewpager);
		page0 = (ImageView) v.findViewById(R.id.page0_select);
		page1 = (ImageView) v.findViewById(R.id.page1_select);
		page2 = (ImageView) v.findViewById(R.id.page2_select);
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		initEmoticon();
		initViewPager();
	}
	
	private void initEmoticon() {
		expressionImages0 = Expressions.expressionImgs0;
		expressionImages1 = Expressions.expressionImgs1;
		expressionImages2 = Expressions.expressionImgs2;
		expressionImageNames0 = Expressions.expressionImgNames0;
		expressionImageNames1 = Expressions.expressionImgNames1;
		expressionImageNames2 = Expressions.expressionImgNames2;
		
		List<Map<String, Object>> listItems0 = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> listItems1 = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> listItems2 = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < 21; i++) {
			Map<String, Object> listItem0 = new HashMap<String, Object>();
			Map<String, Object> listItem1 = new HashMap<String, Object>();
			Map<String, Object> listItem2 = new HashMap<String, Object>();
			listItem0.put("image", expressionImages0[i]);
			listItem1.put("image", expressionImages1[i]);
			listItem2.put("image", expressionImages2[i]);
			listItems0.add(listItem0);
			listItems1.add(listItem1);
			listItems2.add(listItem2);
		}
		
		mAdapter0 = new SimpleAdapter(getActivity(), listItems0,
				R.layout.item_emoticon, new String[] { "image" },
				new int[] { R.id.image });
		mAdapter1 = new SimpleAdapter(getActivity(), listItems1,
				R.layout.item_emoticon, new String[] { "image" },
				new int[] { R.id.image });
		mAdapter2 = new SimpleAdapter(getActivity(), listItems2,
				R.layout.item_emoticon, new String[] { "image" },
				new int[] { R.id.image });
	}
	
	private void initViewPager() {
		LayoutInflater inflater = LayoutInflater.from(getActivity());
		gView0 = (GridView) inflater.inflate(R.layout.gridview_emoticon, null);
		gView1 = (GridView) inflater.inflate(R.layout.gridview_emoticon, null);
		gView2 = (GridView) inflater.inflate(R.layout.gridview_emoticon, null);
		
		gView0.setAdapter(mAdapter0);
		gView1.setAdapter(mAdapter1);
		gView2.setAdapter(mAdapter2);
		
		grids = new ArrayList<GridView>();
		grids.add(gView0);
		grids.add(gView1);
		grids.add(gView2);
		
		gView0.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				onEmoticonClicked(0, arg2);
			}
		});
		
		gView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				onEmoticonClicked(1, arg2);
			}
		});
		
		gView2.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				onEmoticonClicked(2, arg2);
			}
		});
		
		PagerAdapter mPagerAdapter = new PagerAdapter() {
			
			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}
			
			@Override
			public int getCount() {
				return grids.size();
			}
			
			@Override
			public void destroyItem(View container, int position, Object object) {
				((ViewPager) container).removeView(grids.get(position));
			}
			
			@Override
			public Object instantiateItem(View container, int position) {
				((ViewPager) container).addView(grids.get(position));
				return grids.get(position);
			}
		};
		
		viewPager.setAdapter(mPagerAdapter);
		viewPager.setOnPageChangeListener(new GuidePageChangeListener());
	}
	
	private void onEmoticonClicked(int pageNum, int position) {
		int[] emoticon = null;
		String[]  emoticonName = null;
		switch (pageNum) {
		case 0:
			emoticon = expressionImages0;
			emoticonName = expressionImageNames0;
			break;
		case 1:
			emoticon = expressionImages1;
			emoticonName = expressionImageNames1;
			break;
		case 2:
			emoticon = expressionImages2;
			emoticonName = expressionImageNames2;
			break;
		}
		Bitmap bitmap = null;
		bitmap = BitmapFactory.decodeResource(getResources(),
				emoticon[position % emoticon.length]);
		Matrix matrix = new Matrix(); 
		matrix.postScale(0.7f,0.7f); //长和宽放大缩小的比例
		Bitmap resizeBmp = Bitmap
				.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
		ImageSpan imageSpan = new ImageSpan(getActivity(), resizeBmp);
		SpannableString spannableString = new SpannableString(
				emoticonName[position].substring(1,
						emoticonName[position].length() - 1));
		spannableString.setSpan(imageSpan, 0,
				emoticonName[position].length() - 2,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		mListener.onEmoticonSelected(spannableString);
		
		System.out.println("emotion string = " + spannableString);
	}
	
	class GuidePageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {
			
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			
		}

		@Override
		public void onPageSelected(int arg0) {
			switch(arg0) {
			case 0:
				page0.setImageDrawable(getResources().getDrawable(
						R.drawable.page_focused));
				page1.setImageDrawable(getResources().getDrawable(
						R.drawable.page_unfocused));
				break;
			case 1:
				page1.setImageDrawable(getResources().getDrawable(
						R.drawable.page_focused));
				page0.setImageDrawable(getResources().getDrawable(
						R.drawable.page_unfocused));
				page2.setImageDrawable(getResources().getDrawable(
						R.drawable.page_unfocused));
				break;
			case 2:
				page2.setImageDrawable(getResources().getDrawable(
						R.drawable.page_focused));
				page1.setImageDrawable(getResources().getDrawable(
						R.drawable.page_unfocused));
				page0.setImageDrawable(getResources().getDrawable(
						R.drawable.page_unfocused));
				break;
			}
			
		}
		
	}
}
