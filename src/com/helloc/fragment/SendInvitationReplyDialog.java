package com.helloc.fragment;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.database.DBInvitation;
import com.helloc.model.Friends;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.SharePrefsUtils;

public class SendInvitationReplyDialog extends SherlockDialogFragment {

	private static Context context;

	private SendInvitationReplyDialogListener mListener;

	public interface SendInvitationReplyDialogListener {
		public void updateView();
	}

	public static SendInvitationReplyDialog newInstance(String userId,
			String uuid, String invitationMsg, String nickname, String phone,
			Context context) {
		SendInvitationReplyDialog.context = context;
		SendInvitationReplyDialog dialog = new SendInvitationReplyDialog();
		Bundle args = new Bundle();
		args.putString(ActivityConstants.ARGS_ACCOUNT_USER_ID, userId);
		args.putString(ActivityConstants.ARGS_ACCOUNT_UUID, uuid);
		args.putString(ActivityConstants.ARGS_INVITATION_MSG, invitationMsg);
		args.putString(ActivityConstants.ARGS_ACCOUNT_NICKNAME, nickname);
		args.putString(ActivityConstants.ARGS_ACCOUNT_PHONE, phone);
		dialog.setArguments(args);
		return dialog;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (SendInvitationReplyDialogListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement SendInvitationReplyDialogListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = LayoutInflater.from(getSherlockActivity());
		View view = inflater.inflate(R.layout.dialog_receive_invitation, null);
		TextView invitationMsg = (TextView) view
				.findViewById(R.id.invitation_message);
		if (TextUtils.isEmpty(getArguments().getString(
				ActivityConstants.ARGS_INVITATION_MSG)) == false) {
			invitationMsg.setText(getArguments().getString(
					ActivityConstants.ARGS_INVITATION_MSG));
		}

		return new AlertDialog.Builder(getSherlockActivity())
				.setTitle(getString(R.string.text_receive_invitation))
				.setView(view)
				.setNegativeButton(getString(R.string.btn_reject),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								DBInvitation
										.deleteInvitation(
												getSherlockActivity(),
												getArguments()
														.getString(
																ActivityConstants.ARGS_ACCOUNT_USER_ID),
												StatusConstants.INVITATION_STATUS_RECEIVED_NOT_HANDLE);
								mListener.updateView();
							}
						})
				.setPositiveButton(getString(R.string.btn_accept),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								sendInvitationReply();
							}
						}).create();
	}

	private void sendInvitationReply() {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("senderId", SharePrefsUtils.getUserId(context));
				map.put("receiverId",
						getArguments().getString(
								ActivityConstants.ARGS_ACCOUNT_USER_ID));
				map.put("decision", "accept");
				return HttpRequestUtils.post("sendReplyInvitation", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.SEND_REPLY_INVITATION_SUCCESS)) {
						// save to contact list
						Friends firend = new Friends(
								getArguments().getString(
										ActivityConstants.ARGS_ACCOUNT_USER_ID),
								getArguments().getString(
										ActivityConstants.ARGS_ACCOUNT_UUID),
								getArguments()
										.getString(
												ActivityConstants.ARGS_ACCOUNT_NICKNAME),
								getArguments().getString(
										ActivityConstants.ARGS_ACCOUNT_PHONE));
						DBFriend.insert(context, firend);
						// update invitation
						DBInvitation
								.updateToAccepted(
										context,
										getArguments()
												.getString(
														ActivityConstants.ARGS_ACCOUNT_USER_ID));
						mListener.updateView();
						// upload user relationship
						new UplaodUserRelationship().execute();
					} else {
						System.out.println("sendReplyInvitation ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("sendReplyInvitation ---->> "
							+ e.getMessage());
				}
			}

		}.execute();
	}

	private class UplaodUserRelationship extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("relatingUserId", SharePrefsUtils.getUserId(context));
			map.put("relatedUserId",
					getArguments().getString(
							ActivityConstants.ARGS_ACCOUNT_USER_ID));

			return HttpRequestUtils.post("uploadUserRelationship", map);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage
						.equals(HttpConstants.UPLOAD_USERS_RELATIONSHIP_SUCCESS)) {

				} else {
					System.out.println("uploadUserRelationship ---->> "
							+ returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("uploadUserRelationship ---->> "
						+ e.getMessage());
			}
		}

	}
}
