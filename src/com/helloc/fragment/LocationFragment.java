package com.helloc.fragment;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helloc.R;
import com.helloc.activity.GoogleMapActivity;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.database.DBMapSnapshot;
import com.helloc.model.MapSnapshot;
import com.helloc.utils.GoogleMapUtils;
import com.helloc.utils.ImageUtils;

public class LocationFragment extends SherlockFragment implements
		ConnectionCallbacks, LocationListener, OnConnectionFailedListener {

	private String messageId;
	private TextView tvLocation;
	private LinearLayout radiusItem;
	private ProgressBar addrProgressBar;
	private LocationClient mLocationClient;
	private Bitmap bitmap;
	private LocationRequest mLocationRequest;// A request to connect to Location
												// Services
	private double[] latlng = new double[2];
	private boolean isLocationSet; // is the location set by user or use
									// myLocation as default
	private double[] myLocation;

	private int radius = Constants.RADIUS_BASE;
	private TextView tvRadius;
	private TextView tvRadiusUnit;
	private ImageButton radiusIncrease;
	private ImageButton radiusDecrease;
	private Timer timer;
	private TimerTask timerTask;
	private int counter;
	private int state;
	private final int state_increase = 0;
	private final int state_decrease = 1;

	private GoogleMap mMap;
	private Marker addressMarker;
	private Button mapButton;

	private boolean enableRadiusCircle;
	private int activitySrc;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Create a new global location parameters object
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setInterval(5000); // 5 seconds
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setFastestInterval(16); // 16ms = 60fps

		messageId = getArguments().getString(ActivityConstants.ARGS_MESSAGE_ID);
		activitySrc = getArguments().getInt(
				ActivityConstants.ARGS_ACTIVITY_SOURCE);
		enableRadiusCircle = getArguments().getBoolean(Constants.RADIUS_CIRCLE);

		mLocationClient = new LocationClient(getActivity(), this, this);
		if (activitySrc != ActivityConstants.ARGS_ACTIVITY_RECEIVE_MESSAGE) {
			mLocationClient.connect();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_location, container, false);
		tvLocation = (TextView) v.findViewById(R.id.location);
		addrProgressBar = (ProgressBar) v.findViewById(R.id.addrProgressBar);
		mapButton = (Button) v.findViewById(R.id.mapButton);

		radiusItem = (LinearLayout) v.findViewById(R.id.radiusItem);
		tvRadius = (TextView) v.findViewById(R.id.radius);
		tvRadiusUnit = (TextView) v.findViewById(R.id.radiusUnit);
		radiusIncrease = (ImageButton) v.findViewById(R.id.radiusIncrease);
		radiusDecrease = (ImageButton) v.findViewById(R.id.radiusDecrease);

		if (enableRadiusCircle) {
			tvRadius.setText(String.valueOf(radius));
		} else {
			radiusItem.setVisibility(View.INVISIBLE);
		}

		mapButton.setOnClickListener(new onGoogleMapClicked());
		radiusIncrease.setOnTouchListener(new onRadiusIncreaseTouch());
		radiusDecrease.setOnTouchListener(new onRadiusDecreaseTouch());

		setUpView();
		setUpMapIfNeeded();
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}

	@Override
	public void onStop() {
		mLocationClient.disconnect();
		super.onStop();
		if (bitmap != null) {
			bitmap.recycle();
		}
	}

	private void setUpView() {
		// if come from ReceiveMessageActivity
		if (activitySrc == ActivityConstants.ARGS_ACTIVITY_RECEIVE_MESSAGE) {
			radius = (int) getArguments().getFloat(
					ActivityConstants.ARGS_MESSAGE_RADIUS);
			updateRadius();

			tvLocation.setText(getArguments().getString(
					ActivityConstants.ARGS_MESSAGE_ADDRESS));

			isLocationSet = true;
		}
	}

	private void setUpMapIfNeeded() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSherlockActivity()
					.getSupportFragmentManager().findFragmentById(R.id.map))
					.getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		UiSettings mUiSettings = mMap.getUiSettings();
		mUiSettings.setAllGesturesEnabled(false);
		mUiSettings.setCompassEnabled(false);
		mUiSettings.setMyLocationButtonEnabled(false);
		mUiSettings.setZoomControlsEnabled(false);

		addressMarker = mMap.addMarker(new MarkerOptions().position(
				new LatLng(latlng[0], latlng[1])).icon(
				BitmapDescriptorFactory.fromResource(R.drawable.icon_address)));
		addressMarker.setVisible(false);
		
		if (activitySrc == ActivityConstants.ARGS_ACTIVITY_RECEIVE_MESSAGE) {
			latlng[0] = getArguments().getDouble(
					ActivityConstants.ARGS_MESSAGE_LATITUDE);
			latlng[1] = getArguments().getDouble(
					ActivityConstants.ARGS_MESSAGE_LONGITUDE);

			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
					latlng[0], latlng[1]), 14));
			addressMarker.setPosition(new LatLng(latlng[0], latlng[1]));
			addressMarker.setVisible(true);
		}

		saveSnapshot();
	}

	private class onGoogleMapClicked implements OnClickListener {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setClass(getActivity(), GoogleMapActivity.class);
			// intent.putExtra("geofenceMsgId", geofenceMsgId);
			intent.putExtra("isLocationSet", isLocationSet);
			intent.putExtra("latitude", latlng[0]);
			intent.putExtra("longitude", latlng[1]);
			intent.putExtra("address", tvLocation.getText().toString());
			intent.putExtra("radius", radius);
			intent.putExtra(Constants.CURRENT_ADDRESS, false);
			intent.putExtra(Constants.MAP_SEARCH, true);
			intent.putExtra(Constants.RADIUS_CIRCLE, enableRadiusCircle);
			startActivityForResult(intent,
					ActivityConstants.REQUSET_GO_TO_GOOLEMAP_ACTIVITY);
		}

	}

	private class onRadiusIncreaseTouch implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				state = state_increase;
				reScheduleTimer();
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				timer.cancel();
				counter = 0;
				break;
			}
			return false;
		}

	}

	private class onRadiusDecreaseTouch implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				state = state_decrease;
				reScheduleTimer();
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				timer.cancel();
				counter = 0;
				break;
			}
			return false;
		}

	}

	private void reScheduleTimer() {
		timer = new Timer();
		timerTask = new myTimerTask();
		timer.schedule(timerTask, 0, 100);
	}

	private class myTimerTask extends TimerTask {
		@Override
		public void run() {
			counter++;
			if (state == state_increase) {
				radius = GoogleMapUtils.getIncreaseRadius(radius, counter);
			} else if (state == state_decrease) {
				radius = GoogleMapUtils.getDecreaseRadius(radius, counter);
			}
			updateRadius.sendEmptyMessage(0);
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler updateRadius = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			updateRadius();
		}
	};

	private void updateRadius() {
		if (radius < 10000) {
			tvRadius.setText(String.valueOf(radius));
			tvRadiusUnit.setText(R.string.text_meter);
		} else {
			tvRadius.setText(String.valueOf(radius / 1000));
			tvRadiusUnit.setText(R.string.text_kilometer);
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged(Location location) {
		myLocation[0] = location.getLatitude();
		myLocation[1] = location.getLongitude();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if (servicesConnected()) {
			if (activitySrc != ActivityConstants.ARGS_ACTIVITY_RECEIVE_MESSAGE) {
				isLocationSet = true;
				Location currentLocation = mLocationClient.getLastLocation();
				if (currentLocation != null) {
					myLocation = new double[2];
					myLocation[0] = currentLocation.getLatitude();
					myLocation[1] = currentLocation.getLongitude();

					latlng[0] = currentLocation.getLatitude();
					latlng[1] = currentLocation.getLongitude();

					mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
							new LatLng(latlng[0], latlng[1]), 14));
					addressMarker.setPosition(new LatLng(latlng[0], latlng[1]));
					addressMarker.setVisible(true);
					new AsyncTask<Void, Void, String>() {

						@Override
						protected String doInBackground(Void... params) {
							return GoogleMapUtils.getAddress(latlng,
									getActivity());
						}

						@Override
						protected void onPostExecute(String result) {
							addrProgressBar.setVisibility(View.GONE);
							if (TextUtils.isEmpty(result)) {
								tvLocation.setText("");
							} else {
								// isAddressSet = true;
								tvLocation.setText(result);
							}
						}
					}.execute();
				}

			}
		}
	}

	@Override
	public void onDisconnected() {
		// do nothing

	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d(Constants.DEBUG_TAG,
					getString(R.string.play_services_available));

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					getActivity(), 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getFragmentManager(), Constants.DEBUG_TAG);
			}
			return false;
		}
	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	private void saveSnapshot() {
		mMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {

			@Override
			public void onMapLoaded() {
				SnapshotReadyCallback callback = new SnapshotReadyCallback() {
					@Override
					public void onSnapshotReady(Bitmap snapshot) {
						String imagePath = Constants.SNAPSHOT_FLODER
								+ messageId + ".jpg";
						ImageUtils.saveImage(snapshot, imagePath);
						MapSnapshot mapSnapshot = new MapSnapshot();
						mapSnapshot.setId(messageId);
						mapSnapshot.setPhoto(ImageUtils.readImage(imagePath));
						DBMapSnapshot
								.insert(getSherlockActivity(), mapSnapshot);

					}
				};
				mMap.snapshot(callback);
			}
		});

	}

	public double getLatitude() {
		return latlng[0];
	}

	public double getLongitude() {
		return latlng[1];
	}

	public String getAddress() {
		return tvLocation.getText().toString();
	}

	public int getRadius() {
		return radius;
	}

	public boolean isLocationSet() {
		return isLocationSet;
	}

	/*
	 * public boolean isAddressSet() { return isAddressSet; }
	 */

	public double[] getMyLocation() {
		return myLocation;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ActivityConstants.REQUSET_GO_TO_GOOLEMAP_ACTIVITY) {
			if (resultCode == ActivityConstants.RESULT_RESULT_OK) {
				if (data != null) {
					radius = data.getIntExtra("radius", Constants.RADIUS_BASE);
					updateRadius();
					tvLocation.setText(data.getStringExtra("address"));
					latlng[0] = data.getDoubleExtra("latitude", 0);
					latlng[1] = data.getDoubleExtra("longitude", 0);

					mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
							new LatLng(latlng[0], latlng[1]), 14));
					addressMarker.setPosition(new LatLng(latlng[0], latlng[1]));
					addressMarker.setVisible(true);
					saveSnapshot();
				}
			}
		}
	}

}
