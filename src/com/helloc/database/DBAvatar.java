package com.helloc.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.model.Avatar;

public class DBAvatar {
	
	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}
	
	public static void insert(Context context, Avatar avatar) {
		SQLiteDatabase db = getInstance(context);
		if(updateIfExist(context, avatar)) {
			return;
		}
		ContentValues values = new ContentValues();
		values.put("user_id", avatar.getUserId());
		values.put("photo", avatar.getPhoto());
		db.insertOrThrow("avatar", null, values);
		db.close();
	}
	
	public static boolean updateIfExist(Context context, Avatar avatar) {
		SQLiteDatabase db = getInstance(context);
		// if exist the friend, update his information
		ContentValues cv = new ContentValues();
		cv.put("photo", avatar.getPhoto());
		int result = db.update("avatar", cv, "user_id=?", new String[]{avatar.getUserId()});
		
		db.close();
		if(result == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public static byte[] getAvatar(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		
		byte[] photo = null;
		Cursor cursor = db.rawQuery("select * from avatar where " 
				+ "user_id = '" + userId + "'", null);
		if(cursor.moveToFirst()) {
			photo = cursor.getBlob(cursor.getColumnIndex("photo"));
		}
		
		cursor.close();
		db.close();
		return photo;
	}
	
}
