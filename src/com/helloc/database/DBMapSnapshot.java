package com.helloc.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.model.MapSnapshot;

public class DBMapSnapshot {
	
	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}
	
	public static void insert(Context context, MapSnapshot mapSnapshot) {
		if (updateIfExist(context, mapSnapshot)) {
			return;
		}
		SQLiteDatabase db = getInstance(context);
		ContentValues values = new ContentValues();
		values.put("message_id", mapSnapshot.getId());
		values.put("photo", mapSnapshot.getPhoto());
		db.insertOrThrow("map_snapshot", null, values);
		db.close();
	}
	
	public static boolean updateIfExist(Context context, MapSnapshot mapSnapshot) {
		SQLiteDatabase db = getInstance(context);
		ContentValues values = new ContentValues();
		values.put("message_id", mapSnapshot.getId());
		values.put("photo", mapSnapshot.getPhoto());
		int result = db.update("map_snapshot", values, "message_id=?",
				new String[] { mapSnapshot.getId() });

		db.close();
		if (result == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public static byte[] getMapSnapshot(Context context, String message_id) {
		SQLiteDatabase db = getInstance(context);
		byte[] photo = null;
		Cursor cursor = db.rawQuery("select * from map_snapshot where " 
				+ "message_id = '" + message_id + "'", null);
		if(cursor.moveToFirst()) {
			photo = cursor.getBlob(cursor.getColumnIndex("photo"));
		}
		db.close();
		return photo;
	}
	
	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from map_snapshot");
		db.close();
	}
}
