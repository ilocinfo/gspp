package com.helloc.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.constant.StatusConstants;
import com.helloc.model.Message;

public class DBMessage {

	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}

	public static void insert(Context context, Message message) {
		SQLiteDatabase db = getInstance(context);

		ContentValues values = new ContentValues();
		values.put("message_id", message.getId());
		values.put("latitude", message.getLatitude());
		values.put("longitude", message.getLongitude());
		values.put("radius", message.getRadius());
		values.put("expiration_time", message.getExpirationTime());
		values.put("transition_type", message.getTransitionType());
		values.put("status", message.getStatus());
		values.put("type", message.getType());
		values.put("address", message.getAddress());
		values.put("private_message", message.getPrivateMsg());
		values.put("public_message", message.getPublicMsg());
		values.put("sender_id", message.getSenderId());
		values.put("receiver_id", message.getReceiverId());
		values.put("image_name", message.getImageName());
		values.put("need_feedback", message.isNeedFeedback());
		db.insertOrThrow("message", null, values);

		db.close();
	}

	public static Message getById(Context context, String messageId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery(
				"select * from message where message_id = '" + messageId + "'",
				null);
		Message message = new Message();
		if (cursor.moveToFirst()) {
			message = getMessage(message, cursor);
		} else {
			message = null;
		}
		cursor.close();
		db.close();
		return message;
	}

	public static Message getByIdAndStatus(Context context, String messageId,
			int status) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery(
				"select * from message where message_id = '" + messageId
						+ "' and status = " + status, null);
		Message message = new Message();
		if (cursor.moveToFirst()) {
			message = getMessage(message, cursor);
		} else {
			message = null;
		}
		cursor.close();
		db.close();
		return message;
	}

	public static Message getNotActivatedById(Context context, String messageId) {
		String sql = "select * from message where (message_id = '" + messageId
				+ "' and status = " + StatusConstants.MESSAGE_STATUS_SELF
				+ ") or (message_id = '" + messageId + "' and status = "
				+ StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED + ")";
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery(sql, null);
		Message message = new Message();
		if (cursor.moveToFirst()) {
			message = getMessage(message, cursor);
		} else {
			message = null;
		}
		cursor.close();
		db.close();
		return message;
	}

	public static ArrayList<Message> getAllByStatus(Context context, int status) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from message where status = "
				+ status, null);
		ArrayList<Message> list = new ArrayList<Message>();
		while (cursor.moveToNext()) {
			Message message = new Message();
			message = getMessage(message, cursor);
			list.add(message);
		}
		cursor.close();
		db.close();
		return list;
	}

	public static ArrayList<Message> getAllNotActivated(Context context) {
		SQLiteDatabase db = getInstance(context);

		ArrayList<Message> messageList = new ArrayList<Message>();
		Cursor cursor = db.rawQuery("select * from message where "
				+ "status = "
				+ StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED
				+ " or status = " + StatusConstants.MESSAGE_STATUS_SELF, null);
		while (cursor.moveToNext()) {
			Message message = new Message();
			message = getMessage(message, cursor);
			messageList.add(message);
		}
		cursor.close();
		db.close();
		return messageList;
	}

	public static Message getSelfById(Context context, String messageId) {
		SQLiteDatabase db = getInstance(context);
		String sql = "select * from message where (message_id = '" + messageId
				+ "' and status = " + StatusConstants.MESSAGE_STATUS_SELF
				+ ") or (message_id = '" + messageId + "' and status = "
				+ StatusConstants.MESSAGE_STATUS_SELF_ACTIVED + ")";
		// System.out.println("sql = " + sql);
		Cursor cursor = db.rawQuery(sql, null);
		Message message = new Message();
		if (cursor.moveToFirst()) {
			message = getMessage(message, cursor);
		} else {
			message = null;
		}
		cursor.close();
		db.close();
		return message;
	}

	public static Message getLatestByStatus(Context context, int status) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from message where status = "
				+ status + " order by id desc limit 1", null);
		Message message = new Message();
		if (cursor.moveToFirst()) {
			message = getMessage(message, cursor);
		} else {
			message = null;
		}
		cursor.close();
		db.close();
		return message;
	}

	public static void updateStatusById(Context context, String messageId) {
		SQLiteDatabase db = getInstance(context);

		ContentValues cv = new ContentValues();
		cv.put("status", StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED);
		db.update(
				"message",
				cv,
				"message_id=? and status=?",
				new String[] {
						messageId,
						String.valueOf(StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED) });

		cv.clear();
		cv.put("status", StatusConstants.MESSAGE_STATUS_SELF_ACTIVED);
		db.update(
				"message",
				cv,
				"message_id=? and status=?",
				new String[] { messageId,
						String.valueOf(StatusConstants.MESSAGE_STATUS_SELF) });

		db.close();
	}

	public static int getNumByStatus(Context context, int status) {
		SQLiteDatabase db = getInstance(context);

		int number = 0;
		Cursor cursor = db.rawQuery("select * from message where status = "
				+ status, null);
		number = cursor.getCount();
		cursor.close();

		db.close();
		return number;
	}

	public static void deleteById(Context context, String messageId) {
		SQLiteDatabase db = getInstance(context);
		db.delete("message", "message_id=?", new String[] { messageId });
		db.close();
	}

	public static void deleteByStatus(Context context, int status) {
		SQLiteDatabase db = getInstance(context);
		db.delete("message", "status=?",
				new String[] { String.valueOf(status) });
		db.close();
	}

	public static int deleteByIdAndStatus(Context context, String messageId,
			int status) {
		SQLiteDatabase db = getInstance(context);
		int result = db.delete("message", "message_id=? and status=?",
				new String[] { messageId, String.valueOf(status) });
		db.close();
		return result;
	}

	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from message");
		db.close();
	}

	private static Message getMessage(Message message, Cursor cursor) {
		message.setId(cursor.getString(cursor.getColumnIndex("message_id")));
		message.setLatitude(Double.parseDouble(cursor.getString(cursor
				.getColumnIndex("latitude"))));
		message.setLongitude(Double.parseDouble(cursor.getString(cursor
				.getColumnIndex("longitude"))));
		message.setRadius(Float.parseFloat(cursor.getString(cursor
				.getColumnIndex("radius"))));
		message.setExpirationTime(Long.parseLong(cursor.getString(cursor
				.getColumnIndex("expiration_time"))));
		message.setTransitionType(cursor.getInt(cursor
				.getColumnIndex("transition_type")));
		message.setStatus(cursor.getInt(cursor.getColumnIndex("status")));
		message.setType(Integer.parseInt(cursor.getString(cursor
				.getColumnIndex("type"))));
		message.setAddress(cursor.getString(cursor.getColumnIndex("address")));
		message.setPrivateMsg(cursor.getString(cursor
				.getColumnIndex("private_message")));
		message.setPublicMsg(cursor.getString(cursor
				.getColumnIndex("public_message")));
		message.setSenderId(cursor.getString(cursor.getColumnIndex("sender_id")));
		message.setReceiverId(cursor.getString(cursor
				.getColumnIndex("receiver_id")));
		message.setImageName(cursor.getString(cursor
				.getColumnIndex("image_name")));
		message.setNeedFeedback(cursor.getInt(cursor
				.getColumnIndex("need_feedback")) == 1);
		return message;
	}
}
