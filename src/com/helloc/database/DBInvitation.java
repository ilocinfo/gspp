package com.helloc.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.model.Invitation;

public class DBInvitation {
	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}

	public static void insert(Context context, Invitation invitation) {
		SQLiteDatabase db = getInstance(context);
		if (updateIfExist(context, invitation)) {
			System.out.println("invitation exist, userId = "
					+ invitation.getUserId());
			return;
		}
		ContentValues values = new ContentValues();
		values.put("user_id", invitation.getUserId());
		values.put("uuid", invitation.getUuid());
		values.put("status", invitation.getStatus());
		values.put("nick_name", invitation.getNickName());
		values.put("phone", invitation.getPhone());
		values.put("invitation_msg", invitation.getInvitationMsg());
		db.insertOrThrow("invitation", null, values);
		db.close();
	}

	public static boolean updateIfExist(Context context, Invitation invitation) {
		SQLiteDatabase db = getInstance(context);
		// same name and same device id, then update the invitation message
		ContentValues cv = new ContentValues();
		cv.put("uuid", invitation.getUuid());
		cv.put("nick_name", invitation.getNickName());
		cv.put("phone", invitation.getPhone());
		cv.put("invitation_msg", invitation.getInvitationMsg());
		cv.put("status", invitation.getStatus());
		int result = db.update("invitation", cv, "user_id=?",
				new String[] { invitation.getUserId() });
		db.close();
		if (result > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static ArrayList<Map<String, String>> getAll(Context context) {
		SQLiteDatabase db = getInstance(context);
		ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Cursor cursor = db.rawQuery("select * from invitation", null);
		while (cursor.moveToNext()) {
			Map<String, String> map = new HashMap<String, String>();
			map.put(ActivityConstants.ARGS_ACCOUNT_USER_ID,
					cursor.getString(cursor.getColumnIndex("user_id")));
			map.put(ActivityConstants.ARGS_ACCOUNT_UUID,
					cursor.getString(cursor.getColumnIndex("uuid")));
			map.put(ActivityConstants.ARGS_ACCOUNT_NICKNAME,
					cursor.getString(cursor.getColumnIndex("nick_name")));
			map.put(ActivityConstants.ARGS_ACCOUNT_PHONE,
					cursor.getString(cursor.getColumnIndex("phone")));
			map.put(ActivityConstants.ARGS_INVITATION_MSG,
					cursor.getString(cursor.getColumnIndex("invitation_msg")));
			int status = cursor.getInt(cursor.getColumnIndex("status"));
			switch (status) {
			case StatusConstants.INVITATION_STATUS_RECOMMEND:
				map.put(ActivityConstants.ARGS_INVITATION_STATUS, String
						.valueOf(StatusConstants.INVITATION_STATUS_RECOMMEND));
				map.put(ActivityConstants.ARGS_INVITATION_STATUS_MSG,
						context.getString(R.string.text_invitation_to_add));
				break;
			case StatusConstants.INVITATION_STATUS_SENT:
				map.put(ActivityConstants.ARGS_INVITATION_STATUS,
						String.valueOf(StatusConstants.INVITATION_STATUS_SENT));
				map.put(ActivityConstants.ARGS_INVITATION_STATUS_MSG, context
						.getString(R.string.text_invitation_wait_response));
				break;
			case StatusConstants.INVITATION_STATUS_RECEIVED_NOT_HANDLE:
				map.put(ActivityConstants.ARGS_INVITATION_STATUS,
						String.valueOf(StatusConstants.INVITATION_STATUS_RECEIVED_NOT_HANDLE));
				map.put(ActivityConstants.ARGS_INVITATION_STATUS_MSG,
						context.getString(R.string.text_invitation_to_response));
				break;
			case StatusConstants.INVITATION_STATUS_RECEIVED_ACCEPTED:
				map.put(ActivityConstants.ARGS_INVITATION_STATUS,
						String.valueOf(StatusConstants.INVITATION_STATUS_RECEIVED_ACCEPTED));
				map.put(ActivityConstants.ARGS_INVITATION_STATUS_MSG,
						context.getString(R.string.text_invitation_added));
				break;
			}
			list.add(map);
		}
		cursor.close();
		db.close();
		return list;
	}

	public static Invitation getSentByUserId(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		Invitation invitation = new Invitation();
		String query = "select * from invitation where user_id = '" + userId
				+ "'";
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			invitation.setUserId(userId);
			invitation.setUuid(cursor.getString(cursor.getColumnIndex("uuid")));
			invitation.setNickName(cursor.getString(cursor
					.getColumnIndex("nick_name")));
			invitation
					.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
		}
		cursor.close();
		db.close();
		return invitation;
	}

	public static int getNotHandleNum(Context context) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from invitation where status = "
				+ StatusConstants.INVITATION_STATUS_RECEIVED_NOT_HANDLE, null);
		int num = cursor.getCount();
		cursor.close();
		db.close();
		return num;
	}

	public static void updateToAccepted(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		ContentValues cv = new ContentValues();
		cv.put("status", StatusConstants.INVITATION_STATUS_RECEIVED_ACCEPTED);
		db.update("invitation", cv, "user_id=?", new String[] { userId });
		db.close();
	}

	public static void deleteInvitation(Context context, String userId,
			int status) {
		SQLiteDatabase db = getInstance(context);
		db.delete("invitation", "user_id=? and status=?", new String[] {
				userId, String.valueOf(status) });
		db.close();
	}

	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from invitation");
		db.close();
	}

}
