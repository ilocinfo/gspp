package com.helloc.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.constant.StatusConstants;
import com.helloc.model.Friends;

public class DBFriend {

	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}

	public static void insert(Context context, Friends friend) {
		if (updateIfExist(context, friend)) {
			return;
		}
		SQLiteDatabase db = getInstance(context);
		ContentValues values = new ContentValues();
		values.put("user_id", friend.getUserId());
		values.put("uuid", friend.getUuid());
		values.put("name", friend.getName());
		values.put("phone", friend.getPhone());
		values.put("allowAutoReply", friend.getAllowAutoReply());
		values.put("autoReplyPermission", friend.getAutoReplyPermission());
		db.insertOrThrow("friends", null, values);
		db.close();
	}

	public static Friends getFriendByUserId(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from friends where user_id = '"
				+ userId + "'", null);
		Friends friend = null;
		if (cursor.moveToFirst()) {
			friend = new Friends(userId, cursor.getString(cursor
					.getColumnIndex("uuid")), cursor.getString(cursor
					.getColumnIndex("name")), cursor.getString(cursor
					.getColumnIndex("phone")), cursor.getInt(cursor
					.getColumnIndex("allowAutoReply")), cursor.getInt(cursor
					.getColumnIndex("autoReplyPermission")));
		}
		cursor.close();
		db.close();
		return friend;
	}

	public static ArrayList<Friends> getAll(Context context) {
		SQLiteDatabase db = getInstance(context);

		ArrayList<Friends> list = new ArrayList<Friends>();
		Cursor cursor = db.rawQuery("select * from friends", null);
		while (cursor.moveToNext()) {
			Friends friend = new Friends();
			friend.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
			friend.setUuid(cursor.getString(cursor.getColumnIndex("uuid")));
			friend.setName(cursor.getString(cursor.getColumnIndex("name")));
			friend.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
			friend.setAllowAutoReply(cursor.getInt(cursor
					.getColumnIndex("allowAutoReply")));
			friend.setAutoReplyPermission(cursor.getInt(cursor
					.getColumnIndex("autoReplyPermission")));
			list.add(friend);
		}
		cursor.close();
		db.close();
		return list;
	}

	public static boolean updateIfExist(Context context, Friends friend) {
		SQLiteDatabase db = getInstance(context);
		// if exist the friend, update his information
		ContentValues cv = new ContentValues();
		cv.put("uuid", friend.getUuid());
		cv.put("name", friend.getName());
		cv.put("phone", friend.getPhone());
		cv.put("allowAutoReply", friend.getAllowAutoReply());
		cv.put("autoReplyPermission", friend.getAutoReplyPermission());
		int result = db.update("friends", cv, "user_id=?",
				new String[] { friend.getUserId() });

		db.close();
		if (result == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static String getNameByUserId(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);

		String name = "";
		String sql = "select * from friends where " + "user_id = '" + userId
				+ "'";
		Cursor cursor = db.rawQuery(sql, null);
		if (cursor.moveToFirst()) {
			name = cursor.getString(cursor.getColumnIndex("name"));
		}
		cursor.close();
		db.close();
		return name;
	}

	public static ArrayList<String> getFriendWithPhone(Context context) {
		SQLiteDatabase db = getInstance(context);
		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = db.rawQuery("select * from friends where phone != '' ",
				null);
		while (cursor.moveToNext()) {
			list.add(cursor.getString(cursor.getColumnIndex("phone")));
		}
		cursor.close();
		db.close();
		return list;
	}

	public static int getAllowAutoReply(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from friends where user_id = '"
				+ userId + "'", null);
		int result = StatusConstants.AUTO_REPLY_STATUS_NOT_ALLOWED;
		if (cursor.moveToFirst()) {
			result = cursor.getInt(cursor.getColumnIndex("allowAutoReply"));
		}
		cursor.close();
		db.close();
		return result;
	}

	public static int getAutoReplyPermission(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from friends where user_id = '"
				+ userId + "'", null);
		int result = StatusConstants.AUTO_REPLY_STATUS_NOT_ALLOWED;
		if (cursor.moveToFirst()) {
			result = cursor
					.getInt(cursor.getColumnIndex("autoReplyPermission"));
		}
		cursor.close();
		db.close();
		return result;
	}

	public static boolean isMyFriendById(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from friends where user_id = '"
				+ userId + "'", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		if (count == 0) {
			return false;
		} else {
			return true;
		}

	}

	public static boolean isMyFriendByPhone(Context context, String phone) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from friends where phone = '"
				+ phone + "'", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static void uploadAllowAutoReply(Context context, String userId,
			int newValue) {
		SQLiteDatabase db = getInstance(context);
		ContentValues cv = new ContentValues();
		cv.put("allowAutoReply", newValue);
		db.update("friends", cv, "user_id=?", new String[] { userId });
		db.close();
	}

	public static void uploadAutoReplyPermission(Context context,
			String userId, int newValue) {
		SQLiteDatabase db = getInstance(context);
		ContentValues cv = new ContentValues();
		cv.put("autoReplyPermission", newValue);
		db.update("friends", cv, "user_id=?", new String[] { userId });
		db.close();
	}

	public static void deleteFriend(Context context, String userId) {
		SQLiteDatabase db = getInstance(context);
		db.delete("friends", "user_id=?", new String[] { userId });
		db.close();
	}

	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from friends");
		db.close();
	}

}
