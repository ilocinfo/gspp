package com.helloc.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.model.Party;
import com.helloc.model.Partying;
import com.helloc.utils.SharePrefsUtils;

public class DBPartying {
	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}

	public static void insert(Context context, Partying partying) {
		SQLiteDatabase db = getInstance(context);
		if (updateIfExist(context, partying)) {
			return;
		}
		ContentValues values = new ContentValues();
		values.put("party_id", partying.getPartyId());
		values.put("user_id", partying.getUserId());
		values.put("nick_name", partying.getNickName());
		values.put("intervals", partying.getInterval());
		db.insertOrThrow("partying", null, values);
		db.close();
	}

	public static ArrayList<Partying> getByPartyId(Context context,
			String partyId) {
		SQLiteDatabase db = getInstance(context);
		ArrayList<Partying> list = new ArrayList<Partying>();
		Cursor cursor = db.rawQuery("select * from partying "
				+ "where party_id = '" + partyId + "'", null);
		while (cursor.moveToNext()) {
			Partying partying = new Partying();
			partying.setPartyId(partyId);
			partying.setUserId(cursor.getString(cursor
					.getColumnIndex("user_id")));
			partying.setNickName(cursor.getString(cursor
					.getColumnIndex("nick_name")));
			partying.setInterval(cursor.getInt(cursor
					.getColumnIndex("intervals")));
			list.add(partying);
		}
		cursor.close();
		db.close();
		return list;
	}

	public static int getNumByPartyId(Context context, String partyId) {
		SQLiteDatabase db = getInstance(context);
		int count = 0;
		Cursor cursor = db.rawQuery("select * from partying "
				+ "where party_id = '" + partyId + "'", null);
		count = cursor.getCount();
		cursor.close();
		db.close();
		return count;
	}

	public static int getIterval(Context context, String partyId, String userId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from partying "
				+ "where party_id = '" + partyId + "' and user_id = '" + userId
				+ "'", null);
		int interval = 5;
		if (cursor.moveToFirst()) {
			interval = cursor.getInt(cursor.getColumnIndex("intervals"));
		}
		cursor.close();
		db.close();
		return interval;
	}

	private static boolean updateIfExist(Context context, Partying partying) {
		SQLiteDatabase db = getInstance(context);
		// if exist the friend, update his information
		ContentValues cv = new ContentValues();
		cv.put("nick_name", partying.getNickName());
		cv.put("intervals", partying.getInterval());
		int result = db.update("partying", cv, "party_id=? and user_id=?",
				new String[] { partying.getPartyId(), partying.getUserId() });

		db.close();
		if (result == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static void deleteById(Context context, String partyId) {
		SQLiteDatabase db = getInstance(context);
		db.delete("partying", "party_id=?", new String[] { partyId });
		db.close();
	}

	public static void deleteByPartyIdExceptSelf(Context context, String partyId) {
		SQLiteDatabase db = getInstance(context);
		db.delete("partying", "party_id=? and user_id !=?", new String[] {
				partyId, SharePrefsUtils.getUserId(context) });
		db.close();
	}

	public static void deleteByIds(Context context, ArrayList<Party> partyList) {
		SQLiteDatabase db = getInstance(context);
		StringBuffer ids = new StringBuffer();
		for (int i = 0; i < partyList.size() - 1; i++) {
			ids.append(partyList.get(i).getPartyId());
			ids.append(" or ");
		}
		ids.append(partyList.get(partyList.size() - 1));
		db.delete("partying", "party_id=?", new String[] { ids.toString() });
		db.close();
	}

	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from partying");
		db.close();
	}
}
