package com.helloc.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.constant.StatusConstants;
import com.helloc.model.Party;

public class DBParty {

	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}

	public static void insert(Context context, Party party) {
		SQLiteDatabase db = getInstance(context);
		ContentValues values = new ContentValues();
		values.put("party_id", party.getPartyId());
		values.put("theme", party.getTheme());
		values.put("description", party.getDescription());
		values.put("start_date", party.getStartDate());
		values.put("start_time", party.getStartTime());
		values.put("end_date", party.getEndDate());
		values.put("end_time", party.getEndTime());
		values.put("location", party.getAddress());
		values.put("latitude", party.getLatitude());
		values.put("longitude", party.getLongitude());
		values.put("originator", party.getOriginator());
		values.put("status", party.getStatus());
		db.insertOrThrow("party", null, values);
		db.close();
	}

	public static Party getPartyById(Context context, String partyId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from party "
				+ "where party_id = '" + partyId + "'", null);
		Party party = new Party();
		if (cursor.moveToFirst()) {
			party = getParty(party, cursor);
		} else {
			party = null;
		}
		cursor.close();
		db.close();
		return party;
	}

	public static ArrayList<Party> getAllByStatus(Context context, int status) {
		SQLiteDatabase db = getInstance(context);
		ArrayList<Party> list = new ArrayList<Party>();
		Cursor cursor = db.rawQuery("select * from party where status ="
				+ status, null);
		while (cursor.moveToNext()) {
			Party party = new Party();
			party = getParty(party, cursor);
			list.add(party);
		}
		cursor.close();
		db.close();
		return list;
	}

	public static Party getLatest(Context context) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from party where status = "
				+ StatusConstants.PARTY_STATUS_ONGOING
				+ " order by party_id desc limit 1", null);
		Party party = new Party();
		if (cursor.moveToFirst()) {
			party = getParty(party, cursor);
		} else {
			party = null;
		}
		cursor.close();
		db.close();
		return party;
	}

	public static int getNumByStatus(Context context, int status) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from party where status = "
				+ status, null);
		int num = cursor.getCount();
		cursor.close();
		db.close();
		return num;
	}

	public static int getStatus(Context context, String partyId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery("select * from party "
				+ "where party_id = '" + partyId + "'", null);
		int status = -1;
		if (cursor.moveToFirst()) {
			status = cursor.getInt(cursor.getColumnIndex("status"));
		} else {
			status = StatusConstants.PARTY_STATUS_OVER;
		}
		cursor.close();
		db.close();
		return status;
	}

	private static Party getParty(Party party, Cursor cursor) {
		party.setPartyId(cursor.getString(cursor.getColumnIndex("party_id")));
		party.setTheme(cursor.getString(cursor.getColumnIndex("theme")));
		party.setDescription(cursor.getString(cursor
				.getColumnIndex("description")));
		party.setStartDate(cursor.getString(cursor.getColumnIndex("start_date")));
		party.setStartTime(cursor.getString(cursor.getColumnIndex("start_time")));
		party.setEndDate(cursor.getString(cursor.getColumnIndex("end_date")));
		party.setEndTime(cursor.getString(cursor.getColumnIndex("end_time")));
		party.setAddress(cursor.getString(cursor.getColumnIndex("location")));
		party.setLatitude(cursor.getString(cursor.getColumnIndex("latitude")));
		party.setLongitude(cursor.getString(cursor.getColumnIndex("longitude")));
		party.setOriginator(cursor.getString(cursor
				.getColumnIndex("originator")));
		party.setStatus(cursor.getInt(cursor.getColumnIndex("status")));
		return party;
	}

	public static void updateStatus(Context context, String partyId) {
		SQLiteDatabase db = getInstance(context);
		ContentValues cv = new ContentValues();
		cv.put("status", StatusConstants.PARTY_STATUS_OVER);
		db.update("party", cv, "party_id=?", new String[] { partyId });
		db.close();
	}

	public static void deleteById(Context context, String partyId) {
		SQLiteDatabase db = getInstance(context);
		db.delete("party", "party_id=?", new String[] { partyId });
		db.close();
	}

	public static void deleteByStatus(Context context, int status) {
		SQLiteDatabase db = getInstance(context);
		db.delete("party", "status=?", new String[] { String.valueOf(status) });
		db.close();
	}

	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from party");
		db.close();
	}

}
