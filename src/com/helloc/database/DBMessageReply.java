package com.helloc.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.constant.Constants;
import com.helloc.model.MessageReply;

public class DBMessageReply {
	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}

	public static void insert(Context context, MessageReply msgReply) {
		SQLiteDatabase db = getInstance(context);
		ContentValues values = new ContentValues();
		values.put("orders", msgReply.getOrder());
		values.put("message_id", msgReply.getMessageId());
		values.put("user_id", msgReply.getUserId());
		values.put("user_name", msgReply.getUserName());
		values.put("reply_content", msgReply.getReplyContent());
		values.put("is_my_reply", msgReply.isMyReply());
		values.put("date", msgReply.getDate());
		db.insertOrThrow("message_reply", null, values);
		db.close();
	}

	public static ArrayList<MessageReply> getAllByMessageId(Context context,
			String messageId) {
		SQLiteDatabase db = getInstance(context);
		ArrayList<MessageReply> list = new ArrayList<MessageReply>();
		Cursor cursor = db.rawQuery(
				"select * from message_reply where message_id = '" + messageId
						+ "'", null);
		while (cursor.moveToNext()) {
			MessageReply msgReply = new MessageReply();
			msgReply.setId(cursor.getInt(cursor.getColumnIndex("id")));
			msgReply.setOrder(cursor.getInt(cursor.getColumnIndex("orders")));
			msgReply.setMessageId(messageId);
			msgReply.setUserId(cursor.getString(cursor
					.getColumnIndex("user_id")));
			msgReply.setUserName(cursor.getString(cursor
					.getColumnIndex("user_name")));
			msgReply.setReplyContent(cursor.getString(cursor
					.getColumnIndex("reply_content")));
			msgReply.setDate(cursor.getString(cursor.getColumnIndex("date")));
			msgReply.setMyReply(cursor.getInt(cursor
					.getColumnIndex("is_my_reply")) == 1);
			msgReply.setMessageType(Constants.MESSAGE_TYPE_TEXT);
			list.add(msgReply);
		}
		cursor.close();
		db.close();
		return list;
	}

	public static String getLatestDateByMsgId(Context context, String messageId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery(
				"select MAX(date) AS date from message_reply where message_id = '"
						+ messageId + "'", null);
		String latestDate = null;
		if (cursor.moveToFirst()) {
			latestDate = cursor.getString(cursor.getColumnIndex("date"));
		}
		cursor.close();
		db.close();
		return latestDate;
	}

	public static int getLatestOrderByMsgId(Context context, String messageId) {
		SQLiteDatabase db = getInstance(context);
		Cursor cursor = db.rawQuery(
				"select MAX(orders) AS orders from message_reply where message_id = '"
						+ messageId + "'", null);
		int latestOrder = 0;
		if (cursor.moveToFirst()) {
			latestOrder = cursor.getInt(cursor.getColumnIndex("orders"));
		}
		cursor.close();
		db.close();
		return latestOrder;
	}

	public static void deleteByMsgId(Context context, String messageId) {
		SQLiteDatabase db = getInstance(context);
		db.delete("message_reply", "message_id=?", new String[] { messageId });
		db.close();
	}

	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from message_reply");
		db.close();
	}
}
