package com.helloc.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.helloc.constant.Constants;

public class DBHelper extends SQLiteOpenHelper{

	private SQLiteDatabase db; 
	
	public DBHelper(Context context) {		
		super(context, Constants.DB_NAME, null, 
				Constants.DB_VERSION);
		//SQLiteDatabase.loadLibs(context);
	}

	public SQLiteDatabase openDatabase(){
		
		if(db == null){
			/*得到一个数据库的实例
			 * 调用这个方法时，查找系统中的资源，
			 * 如果不存在相关资源，调用onCreate(SQLiteDatabase db)方法,
			 * 如果存在，直接返回相关数据库
			 * */
			//db = this.getWritableDatabase(Constants.DB_PASSWORD);
			db = this.getWritableDatabase();
		}
		return db;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		System.out.println("--->>database create");
		StringBuffer tableCreate = new StringBuffer();
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists message")
						.append("(id integer primary key autoincrement,")
						.append("message_id text,")
						.append("latitude text,")
						.append("longitude text,")
						.append("radius text,")
						.append("expiration_time text,")
						.append("transition_type Integer,")
						.append("status Integer,")
						.append("type Integer,")
						.append("address text,")
						.append("private_message text,")
						.append("public_message text,")
						.append("sender_id text,")
						.append("receiver_id text,")
						.append("image_name text,")
						.append("need_feedback boolean)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists friends")
						.append("(id integer primary key autoincrement,")
						.append("user_id text,")
						.append("uuid text,")
						.append("name text,")
						.append("phone text,")
						.append("allowAutoReply integer,")
						.append("autoReplyPermission integer)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists invitation")
						.append("(id integer primary key autoincrement,")
						.append("user_id text,")
						.append("uuid text,")
						.append("status integer,") // 0 for send invitation, 1 for received invitation
						.append("nick_name text,")
						.append("phone text,")
						.append("invitation_msg text)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists party")
						.append("(party_id text primary key,")
						.append("theme text,")
						.append("description text,")
						.append("start_date text,")
						.append("start_time text,")
						.append("end_date text,")
						.append("end_time text,")
						.append("location text,")
						.append("latitude text,")
						.append("longitude text,")
						.append("status integer,")
						.append("originator text)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists partying")
						.append("(id integer primary key autoincrement,")
						.append("party_id text,")
						.append("user_id text,")
						.append("nick_name text,")
						.append("intervals integer)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists avatar")
						.append("(user_id text primary key,")
						.append("photo binary)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists map_snapshot")
						.append("(message_id text primary key,")
						.append("photo binary)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists locations")
						.append("(id integer primary key autoincrement,")				
						.append("name text,")
						.append("address text,")
						.append("latitude text,")
						.append("longitude text)");
		db.execSQL(tableCreate.toString());
		
		tableCreate = new StringBuffer();
		tableCreate.append("create table if not exists message_reply")
			.append("(id integer primary key autoincrement,")				
			.append("orders integer,")
			.append("message_id text,")
			.append("user_id text,")
			.append("user_name text,")
			.append("reply_content text,")
			.append("is_my_reply boolean,")
			.append("date text)");
		db.execSQL(tableCreate.toString());
		
		System.out.println("create table success");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//String sql = "drop table if exists simple_geofence";
		System.out.println("--->>database upgrade");
		String sql = "";
		if(newVersion == 13) {
			sql = "ALTER TABLE friends ADD COLUMN uuid text";
			db.execSQL(sql);
			sql = "ALTER TABLE friends ADD COLUMN allowAutoReply integer";
			db.execSQL(sql);
			sql = "ALTER TABLE friends ADD COLUMN autoReplyPermission integer";
			db.execSQL(sql);
			
			sql = "ALTER TABLE invitation ADD COLUMN uuid text";
			db.execSQL(sql);
			
			StringBuffer tableCreate = new StringBuffer();
			tableCreate.append("create table if not exists message_reply")
				.append("(id integer primary key autoincrement,")				
				.append("orders integer,")
				.append("message_id text,")
				.append("user_id text,")
				.append("user_name text,")
				.append("reply_content text,")
				.append("is_my_reply boolean,")
				.append("date text)");
			db.execSQL(tableCreate.toString());
		}
		onCreate(db);
	}

}
