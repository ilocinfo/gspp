package com.helloc.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.helloc.model.Locations;

public class DBLocations {
	
	private static SQLiteDatabase getInstance(Context context) {
		return new DBHelper(context).openDatabase();
	}

	public static void insert(Context context, Locations location) {
		SQLiteDatabase db = getInstance(context);
		ContentValues values = new ContentValues();
		values.put("name", location.getName());
		values.put("address", location.getAddress());
		values.put("latitude", String.valueOf(location.getLatitude()));
		values.put("longitude", String.valueOf(location.getLongitude()));
		db.insertOrThrow("locations", null, values);
		db.close();
	}
	
	public static ArrayList<Map<String,String>> getAll(Context context) {
		SQLiteDatabase db = getInstance(context);
		ArrayList<Map<String,String>> list = new ArrayList<Map<String,String>>();
		Cursor cursor = db.rawQuery("select * from locations", null);
		while(cursor.moveToNext()) {
			Map<String,String> map = new HashMap<String, String>();
			map.put("id", cursor.getString(cursor.getColumnIndex("id")));
			map.put("name", cursor.getString(cursor.getColumnIndex("name")));
			map.put("address", cursor.getString(cursor.getColumnIndex("address")));
			map.put("latitude", cursor.getString(cursor.getColumnIndex("latitude")));
			map.put("longitude", cursor.getString(cursor.getColumnIndex("longitude")));
			list.add(map);
		}
		cursor.close();
		db.close();
		return list;
	}
	
	public static void deleteById(Context context, int id) {
		SQLiteDatabase db = getInstance(context);
		db.delete("locations", "id=?", 
				new String[]{String.valueOf(id)});
		db.close();
	}
	
	public static void clearDatabase(Context context) {
		SQLiteDatabase db = getInstance(context);
		db.execSQL("delete from locations");
		db.close();
	}
}
