package com.helloc.utils;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.helloc.constant.Constants;

public class SystemUtils {
	/**
	 * Uses static final constants to detect if the device's platform version is
	 * Honeycomb or later.
	 */
	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	public static int[] getResolution(Context context) {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		int size[] = new int[] { metrics.widthPixels, metrics.heightPixels };
		System.out.println("height = " + metrics.heightPixels);
		System.out.println("width = " + metrics.widthPixels);
		return size;
	}

	public static float getTextJusifyWidth(Context context) {
		int widthPix = getResolution(context)[0];
		if (widthPix <= 480) {
			return 240f;
		} else if (widthPix <= 720) {
			return 285f;
		} else if (widthPix <= 768) {
			return 270f;
		} else {
			return 350f;
		}
	}

	public static float getDensity(Context context) {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		System.out.println("--->> dpi = " + metrics.density);
		return metrics.density;
	}

	public static void switchSoftKeyboardstate(Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public static void hideSoftKeyboard(View view, Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	public static void showSoftKeyboard(View view, Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
	}

	public static boolean checkPlayServices(SherlockActivity activity) {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(activity);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
						Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {

			}
			return false;
		}
		return true;
	}

	public static boolean isZh(Context context) {
		Locale locale = context.getResources().getConfiguration().locale;
		String language = locale.getLanguage();
		if (language.endsWith("zh"))
			return true;
		else
			return false;
	}

	public static void setLanguage(Context context) {
		Resources resources = context.getResources();
		Configuration config = resources.getConfiguration();
		DisplayMetrics dm = resources.getDisplayMetrics();
		int language = SharePrefsUtils.getLanguage(context);
		if (language < 0) {
			language = Constants.LANGUAGE_AUTO;
		}
		switch (language) {
		case Constants.LANGUAGE_AUTO:
			config.locale = Locale.getDefault();
			break;
		case Constants.LANGUAGE_SIMPLIFIED:
			config.locale = Locale.SIMPLIFIED_CHINESE;
			break;
		case Constants.LANGUAGE_TRADITIONAL:
			config.locale = Locale.TRADITIONAL_CHINESE;
			break;
		case Constants.LANGUAGE_ENGLISH:
			config.locale = Locale.ENGLISH;
			break;
		}
		resources.updateConfiguration(config, dm);
	}
}
