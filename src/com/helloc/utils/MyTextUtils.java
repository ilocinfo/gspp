package com.helloc.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.model.Message;

public class MyTextUtils {

	public static String getReceivers(Context context, Message message) {
		// for status self
		if (message.getStatus() == StatusConstants.MESSAGE_STATUS_SELF
				|| message.getStatus() == StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
			return context.getString(R.string.text_myself);
		}
		// for status sent
		else if (message.getStatus() == StatusConstants.MESSAGE_STATUS_SENT) {
			String[] receiverIds = message.getReceiverId().split(",");
			StringBuffer receiver = new StringBuffer();
			for (int i = 0; i < receiverIds.length; i++) {
				if (receiverIds[i].equals(String
						.valueOf(Constants.SEND_TO_WECHAT_FRIEND))) {
					receiver.append(context
							.getString(R.string.text_wechat_friend));
					receiver.append(", ");
				} else if (receiverIds[i].equals(String
						.valueOf(Constants.SEND_TO_WECHAT_FRIEND))) {
					receiver.append(context
							.getString(R.string.text_wechat_moment));
					receiver.append(", ");
				} else if (receiverIds[i].equals(String
						.valueOf(Constants.SEND_TO_OTHER_APP))) {
					receiver.append(context.getString(R.string.text_other_app));
					receiver.append(", ");
				} else {
					// System.out.println(messageList.get(i).getReceiverId());
					receiver.append(DBFriend.getNameByUserId(context,
							receiverIds[i]));
					receiver.append(", ");
				}
			}
			receiver.delete(receiver.length() - 2, receiver.length() - 1);
			return receiver.toString();
		}
		// for status received
		else if (message.getStatus() == StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED
				|| message.getStatus() == StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED) {

			String name = DBFriend.getNameByUserId(context,
					message.getSenderId());
			if (TextUtils.isEmpty(name)) {
				return context.getString(R.string.text_wechat);
			} else {
				return DBFriend.getNameByUserId(context, message.getSenderId());
			}
			// System.out.println(messageList.get(i).getSenderId());

		}
		return "";
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static void copyToClipboard(String content, Context context) {
		int sdk = android.os.Build.VERSION.SDK_INT;
		if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
			android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
					.getSystemService(Context.CLIPBOARD_SERVICE);
			clipboard.setText(content);
		} else {
			android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context
					.getSystemService(Context.CLIPBOARD_SERVICE);
			android.content.ClipData clip = android.content.ClipData
					.newPlainText("text label", content);
			clipboard.setPrimaryClip(clip);
		}
	}

	public static String[] getDisplayedValues(int minValue, int maxValue,
			int gap) {
		int size = (maxValue - minValue) / gap;
		String[] displayedValues = new String[size];
		for (int i = 0; i < size; i++) {
			displayedValues[i] = String.valueOf(minValue + gap * i);
		}
		return displayedValues;
	}

	public static SpannableString getSpannableString(Context context,
			String input) {
		try {
			SpannableString spannableString = ExpressionUtil
					.getExpressionString(context, input, Constants.REGULAR);
			return spannableString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
