package com.helloc.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.Geofence;
import com.helloc.R;
import com.helloc.geofence.GeofenceConstants;
import com.helloc.geofence.GeofenceRemover;
import com.helloc.geofence.GeofenceRequester;
import com.helloc.geofence.GeofenceReceiver;
import com.helloc.model.Message;

public class GeofenceUtils {

	public static void setUpGeofence(Context context,
			List<Geofence> mCurrentGeofences) {
		if (!servicesConnected(context)) {
			return;
		}
		IntentFilter mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(GeofenceConstants.ACTION_GEOFENCES_ADDED);
		mIntentFilter.addAction(GeofenceConstants.ACTION_GEOFENCE_ERROR);
		mIntentFilter.addCategory(GeofenceConstants.CATEGORY_LOCATION_SERVICES);
		GeofenceReceiver mBroadcastReceiver = new GeofenceReceiver();
		LocalBroadcastManager.getInstance(context).registerReceiver(
				mBroadcastReceiver, mIntentFilter);
		// Try to add geofences
		try {
			// Instantiate a Geofence requester
			GeofenceRequester mGeofenceRequester = new GeofenceRequester(
					context);
			mGeofenceRequester.addGeofences(mCurrentGeofences);
		} catch (UnsupportedOperationException e) {
			// Notify user that previous request hasn't finished.
			Toast.makeText(context,
					R.string.add_geofences_already_requested_error,
					Toast.LENGTH_LONG).show();
		}
	}

	public static void setUpGeofences(Context context,
			ArrayList<Message> messageList) {
		if (messageList.size() > 0) {
			List<Geofence> mCurrentGeofences = new ArrayList<Geofence>();
			for (int i = 0; i < messageList.size(); i++) {
				Geofence geofence = messageList.get(i).getGeofence();
				if (geofence != null) {
					mCurrentGeofences.add(geofence);
				}
			}
			setUpGeofence(context, mCurrentGeofences);
		}
	}

	public static void setUpGeofence(Context context, Message message) {
		Geofence geofence = message.getGeofence();
		if (geofence == null) {
			return;
		}
		List<Geofence> mCurrentGeofences = new ArrayList<Geofence>();
		mCurrentGeofences.add(geofence);
		setUpGeofence(context, mCurrentGeofences);
	}

	public static  void setUpIntervalGeofence(Context context, String partyId,
			double latitude, double longitude) {
		List<Geofence> list = new ArrayList<Geofence>();
		float radius = 0;
		for (int i = 1; i < 5; i++) {
			switch (i) {
			case 1:
				radius = 100;
				break;
			case 2:
				radius = 500;
				break;
			case 3:
				radius = 1000;
				break;
			case 4:
				radius = 5000;
				break;
			}
			Geofence geofence = new Geofence.Builder()
					.setRequestId(partyId + "$" + i)
					.setTransitionTypes(
							Geofence.GEOFENCE_TRANSITION_ENTER
									| Geofence.GEOFENCE_TRANSITION_EXIT)
					.setExpirationDuration(Integer.MAX_VALUE)
					.setCircularRegion(latitude, longitude, radius).build();
			list.add(geofence);
		}
		GeofenceUtils.setUpGeofence(context, list);
	}

	public static void clearGeofences(Context context) {
		if (!servicesConnected(context)) {
			return;
		}

		IntentFilter mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(GeofenceConstants.ACTION_GEOFENCES_REMOVED);
		mIntentFilter.addAction(GeofenceConstants.ACTION_GEOFENCE_ERROR);
		mIntentFilter.addCategory(GeofenceConstants.CATEGORY_LOCATION_SERVICES);
		GeofenceReceiver mBroadcastReceiver = new GeofenceReceiver();
		LocalBroadcastManager.getInstance(context).registerReceiver(
				mBroadcastReceiver, mIntentFilter);
		GeofenceRequester mGeofenceRequester = new GeofenceRequester(context);
		GeofenceRemover mGeofenceRemover = new GeofenceRemover(context);
		try {
			/*
			 * Remove the geofences represented by the currently-active
			 * PendingIntent. If the PendingIntent was removed for some reason,
			 * re-create it; since it's always created with FLAG_UPDATE_CURRENT,
			 * an identical PendingIntent is always created.
			 */
			mGeofenceRemover.removeGeofencesByIntent(mGeofenceRequester
					.getRequestPendingIntent());

		} catch (UnsupportedOperationException e) {
			// Notify user that previous request hasn't finished.
			Toast.makeText(context,
					R.string.remove_geofences_already_requested_error,
					Toast.LENGTH_LONG).show();
		}
	}

	public static void removeGeofence(Context context, String messageId) {
		List<String> mGeofenceIdsToRemove;
		mGeofenceIdsToRemove = Collections.singletonList(messageId);
		removeGeofences(context, mGeofenceIdsToRemove);
	}

	public static void removeGeofences(Context context,
			ArrayList<Message> messageList) {
		List<String> mGeofenceIdsToRemove = new ArrayList<String>();
		for (int i = 0; i < messageList.size(); i++) {
			mGeofenceIdsToRemove.add(messageList.get(i).getId());
		}
		removeGeofences(context, mGeofenceIdsToRemove);

	}

	public static void removeGeofences(Context context, String[] ids) {
		List<String> mGeofenceIdsToRemove = new ArrayList<String>();
		for (int i = 0; i < ids.length; i++) {
			mGeofenceIdsToRemove.add(ids[i]);
		}
		removeGeofences(context, mGeofenceIdsToRemove);
	}

	private static void removeGeofences(Context context,
			List<String> mGeofenceIdsToRemove) {
		if (!servicesConnected(context)) {
			return;
		}
		IntentFilter mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(GeofenceConstants.ACTION_GEOFENCES_REMOVED);
		mIntentFilter.addAction(GeofenceConstants.ACTION_GEOFENCE_ERROR);
		mIntentFilter.addCategory(GeofenceConstants.CATEGORY_LOCATION_SERVICES);
		GeofenceReceiver mBroadcastReceiver = new GeofenceReceiver();

		LocalBroadcastManager.getInstance(context).registerReceiver(
				mBroadcastReceiver, mIntentFilter);
		// Try to remove the geofence
		try {
			// Instantiate a Geofence remover
			GeofenceRemover mGeofenceRemover = new GeofenceRemover(context);
			mGeofenceRemover.removeGeofencesById(mGeofenceIdsToRemove);

			// Catch errors with the provided geofence IDs
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// Notify user that previous request hasn't finished.
			Toast.makeText(context,
					R.string.remove_geofences_already_requested_error,
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private static boolean servicesConnected(Context context) {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(context);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {

			// In debug mode, log the status
			Log.d(GeofenceConstants.APPTAG,
					context.getString(R.string.play_services_available));

			// Continue
			return true;

			// Google Play services was not available for some reason
		} else {

			/*
			 * // Display an error dialog Dialog dialog =
			 * GooglePlayServicesUtil.getErrorDialog(resultCode,
			 * (Acitivty)context, 0); if (dialog != null) { ErrorDialogFragment
			 * errorFragment = new ErrorDialogFragment();
			 * errorFragment.setDialog(dialog);
			 * errorFragment.show(getSupportFragmentManager(),
			 * GeofenceUtils.APPTAG); }
			 */
			return false;
		}
	}
}
