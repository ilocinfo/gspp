package com.helloc.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.helloc.constant.ActivityConstants;

public class SharePrefsUtils {

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private static SharedPreferences getPrefs(Context context) {
		return context.getSharedPreferences(
				ActivityConstants.SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE);
	}

	public static String getUserId(Context context) {
		return getString(context, ActivityConstants.SHARE_PREFS_ITEM_USER_ID);
	}

	public static void setUserId(Context context, String value) {
		if (value.equals("null") == false) {
			setString(context, ActivityConstants.SHARE_PREFS_ITEM_USER_ID,
					value);
		}
	}

	public static String getNickname(Context context) {
		return getString(context, ActivityConstants.SHARE_PREFS_ITEM_NAME);
	}

	public static void setNickName(Context context, String value) {
		if (value.equals("null") == false) {
			setString(context, ActivityConstants.SHARE_PREFS_ITEM_NAME, value);
		}
	}

	public static boolean isLoginAsGuest(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_LOGIN_AS_GUEST);
	}

	public static void setLoginAsGuest(Context context, boolean value) {
		setBoolean(context, ActivityConstants.SHARE_PREFS_ITEM_LOGIN_AS_GUEST,
				value);
	}

	public static boolean isLogin(Context context) {
		return getBoolean(context, ActivityConstants.SHARE_PREFS_ITEM_IS_LOGIN);
	}

	public static void setLogin(Context context, boolean value) {
		setBoolean(context, ActivityConstants.SHARE_PREFS_ITEM_IS_LOGIN, value);
	}

	public static String getLoginAccount(Context context) {
		return getString(context,
				ActivityConstants.SHARE_PREFS_ITEM_LOGIN_ACCOUNT);
	}

	public static void setLoginAccount(Context context, String value) {
		if (value.equals("null") == false) {
			setString(context,
					ActivityConstants.SHARE_PREFS_ITEM_LOGIN_ACCOUNT, value);
		}

	}

	public static String getUuid(Context context) {
		return getString(context, ActivityConstants.SHARE_PREFS_ITEM_UUID);
	}

	public static void setUuid(Context context, String value) {
		if (value.equals("null") == false) {
			setString(context, ActivityConstants.SHARE_PREFS_ITEM_UUID, value);
		}
	}

	public static String getPhone(Context context) {
		return getString(context, ActivityConstants.SHARE_PREFS_ITEM_PHONE);
	}

	public static void setPhone(Context context, String value) {
		if (value.equals("null") == false) {
			setString(context, ActivityConstants.SHARE_PREFS_ITEM_PHONE, value);
		}

	}

	public static String getEmail(Context context) {
		return getString(context, ActivityConstants.SHARE_PREFS_ITEM_EMAIL);
	}

	public static void setEmail(Context context, String value) {
		if (value.equals("null") == false) {
			setString(context, ActivityConstants.SHARE_PREFS_ITEM_EMAIL, value);
		}
	}

	public static boolean isNewFriendsNotice(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NEW_FRIEND);
	}

	public static void setNewFriendsNotice(Context context, boolean value) {
		setBoolean(context, ActivityConstants.SHARE_PREFS_ITEM_NEW_FRIEND,
				value);
	}

	public static int getAppVersion(Context context) {
		return getInt(context, ActivityConstants.SHARE_PREFS_ITEM_APP_VERSION);
	}

	public static void setAppVersion(Context context, int value) {
		setInt(context, ActivityConstants.SHARE_PREFS_ITEM_APP_VERSION, value);
	}

	public static String getDeviceId(Context context) {
		String deviceId = getString(context,
				ActivityConstants.SHARE_PREFS_ITEM_DEVICE_ID);
		if (TextUtils.isEmpty(deviceId)) {
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = getAppVersion(context);
		int currentVersion = SystemUtils.getAppVersion(context);
		if (registeredVersion != currentVersion) {
			return "";
		}

		return deviceId;
	}

	public static void setDevice(Context context, String value) {
		setString(context, ActivityConstants.SHARE_PREFS_ITEM_DEVICE_ID, value);
	}

	public static boolean isRefreshContacts(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_REFRESH_CONTACTS);
	}

	public static void setRefreshContacts(Context context, boolean value) {
		setBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_REFRESH_CONTACTS, value);
	}

	public static boolean isNewInvitation(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NEW_INVITATION);
	}

	public static void setNewInvitation(Context context, boolean value) {
		setBoolean(context, ActivityConstants.SHARE_PREFS_ITEM_NEW_INVITATION,
				value);
	}

	public static boolean isNotFirstToSendMessage(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_FIRST_TO_SEND_MESSAGE);
	}

	public static void setNotFirstToSendMessage(Context context, boolean value) {
		setBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_FIRST_TO_SEND_MESSAGE,
				value);
	}

	public static boolean isRegsiterToWechat(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_IS_REGISTER_TO_WECHAT);
	}

	public static void setRegisterToWechat(Context context, boolean value) {
		setBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_IS_REGISTER_TO_WECHAT, value);
	}

	public static boolean isNotShowRegisterToWechatHint(Context context) {
		return getBoolean(
				context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_REGISTER_TO_WECHAT_HINT);
	}

	public static void SetNotShowRegisterToWechatHint(Context context,
			boolean value) {
		setBoolean(
				context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_REGISTER_TO_WECHAT_HINT,
				value);
	}

	public static boolean isNotShowShareToOtherAppHint(Context context) {
		return getBoolean(
				context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_SHARE_TO_OTHER_APP_HINT);
	}

	public static void setNotShowShareToOtherAppHint(Context context,
			boolean value) {
		setBoolean(
				context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_SHARE_TO_OTHER_APP_HINT,
				value);
	}

	public static boolean isNotShowUserGuide1(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_USER_GUIDE_1);
	}

	public static void setNotShowUserGuide1(Context context, boolean value) {
		setBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_USER_GUIDE_1, value);
	}

	public static boolean isNotShowUserGuide2(Context context) {
		return getBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_USER_GUIDE_2);
	}

	public static void setNotShowUserGuide2(Context context, boolean value) {
		setBoolean(context,
				ActivityConstants.SHARE_PREFS_ITEM_NOT_SHOW_USER_GUIDE_2, value);
	}

	public static boolean isNewFeatureOdd(Context context) {
		return getPrefs(context).getBoolean(
				ActivityConstants.SHARE_PREFS_ITEM_NEW_FEATURE_ODD, true);
	}

	public static void setNewFeatureOdd(Context context, boolean value) {
		setBoolean(context, ActivityConstants.SHARE_PREFS_ITEM_NEW_FEATURE_ODD,
				value);
	}
	
	public static int getLanguage(Context context) {
		return getInt(context, ActivityConstants.SHARE_PREFS_ITEM_LANGUAGE);
	}

	public static void setLanguage(Context context, int value) {
		setInt(context, ActivityConstants.SHARE_PREFS_ITEM_LANGUAGE, value);
	}

	public static void clear(Context context) {
		getPrefs(context).edit().clear().commit();
	}

	private static String getString(Context context, String name) {
		return getPrefs(context).getString(name, "");
	}

	private static boolean getBoolean(Context context, String name) {
		return getPrefs(context).getBoolean(name, false);
	}

	private static int getInt(Context context, String name) {
		return getPrefs(context).getInt(name, Integer.MIN_VALUE);
	}

	private static void setString(Context context, String name, String value) {
		getPrefs(context).edit().putString(name, value).commit();
	}

	private static void setBoolean(Context context, String name, boolean value) {
		getPrefs(context).edit().putBoolean(name, value).commit();
	}

	private static void setInt(Context context, String name, int value) {
		getPrefs(context).edit().putInt(name, value).commit();
	}
}
