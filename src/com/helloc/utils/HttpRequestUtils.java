package com.helloc.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.http.protocol.HTTP;

import android.util.Log;

import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;

public class HttpRequestUtils {
	private static String TAG = "HttpRequestUtils";

	public static String post(String action, Map<String, String> map) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			sb.append(entry.getKey()).append("=").append(entry.getValue());
			sb.append("&");
		}
		sb.deleteCharAt(sb.length() - 1);
		String str = sb.toString();

		String path = HttpConstants.SERVER_URL + action + ".action";
		URL url = null;
		try {
			url = new URL(path);
			System.out.println("url = " + url);
			// use HttpURLConnection open connection
			HttpURLConnection urlConn = (HttpURLConnection) url
					.openConnection();
			urlConn.setRequestMethod("POST");
			urlConn.setConnectTimeout(HttpConstants.CONNECT_TIMEOUT);
			urlConn.setDoInput(true);
			urlConn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			urlConn.setRequestProperty("charset", HTTP.UTF_8);
			urlConn.setRequestProperty("Content-Length",
					String.valueOf(str.getBytes().length));

			OutputStream os = urlConn.getOutputStream();
			os.write(str.getBytes());

			// receive data
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConn.getInputStream(), HTTP.UTF_8));
			return reader.readLine();
		} catch (MalformedURLException e1) {
			Log.e(Constants.DEBUG_TAG, "MalformedURLException");
			return HttpConstants.HTTP_POST_RESPONSE_URL_NULL;
		} catch (IOException e) {
			System.out.println("error = " + e.getMessage());
			Log.e(TAG, e.getMessage());
			return HttpConstants.HTTP_POST_RESPONSE_EXCEPTION;
		}
	}
}
