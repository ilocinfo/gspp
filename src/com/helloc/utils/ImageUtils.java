package com.helloc.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.Base64;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.database.DBAvatar;
import com.helloc.model.Avatar;

public class ImageUtils {

	public static void saveImage(Bitmap image, String path) {
		File file = new File(path);
		try {
			FileOutputStream out = new FileOutputStream(file);
			image.compress(Bitmap.CompressFormat.JPEG, 90, out);
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static byte[] convertBitmapToByte(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
		return baos.toByteArray();
	}

	public static Bitmap convertByteToBitmap(Context context, byte[] image) {
		if (image != null) {
			return BitmapFactory.decodeByteArray(image, 0, image.length);
		} else {
			return BitmapFactory.decodeResource(context.getResources(),
					R.drawable.ic_launcher);
		}
	}

	public static String encodeBitmapToString(Bitmap bitmap) {
		return Base64
				.encodeToString(convertBitmapToByte(bitmap), Base64.DEFAULT)
				.replace("\n", "").replace("+", "%2B");
	}

	public static String encodeBitmapToString(String path) {
		return Base64.encodeToString(readImage(path), Base64.DEFAULT)
				.replace("\n", "").replace("+", "%2B");
	}

	public static byte[] readImage(String path) {
		File file = new File(path);
		FileInputStream fin = null;
		byte fileContent[] = null;
		try {
			// create FileInputStream object
			fin = new FileInputStream(file);

			fileContent = new byte[(int) file.length()];

			// Reads up to certain bytes of data from this input stream into an
			// array of bytes.
			fin.read(fileContent);
			// create string from byte array
			// String s = new String(fileContent);
			// System.out.println("File content: " + s);
		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading file " + ioe);
		} finally {
			// close the streams using close method
			try {
				if (fin != null) {
					fin.close();
				}
			} catch (IOException ioe) {
				System.out.println("Error while closing stream: " + ioe);
			}
		}
		return fileContent;
	}

	public static Bitmap CropImage(Context context, Bitmap bitmap) {
		// Bitmap bitmap = BitmapFactory.decodeFile(path);
		if (bitmap != null) {
			int height = bitmap.getHeight();
			int width = bitmap.getWidth();
			// System.out.println("height = " + height + "  width = " + width);
			int sizePx = (int) (100 * SystemUtils.getDensity(context) * 1.3);
			// System.out.println("sizePx = " + sizePx);
			int[] pixels = new int[width * height];

			int x = (int) (width - sizePx) / 2;
			int y = (int) (height - sizePx) / 2;
			// System.out.println("x = " + x + "  y = " + y);

			bitmap.getPixels(pixels, 0, sizePx, x, y, sizePx, sizePx);

			Bitmap tempBmp = Bitmap.createBitmap(sizePx, sizePx,
					bitmap.getConfig());
			tempBmp.setPixels(pixels, 0, sizePx, 0, 0, sizePx, sizePx);

			return tempBmp;
		}
		return null;
	}

	// 计算图片的缩放值
	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio > widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}

	// 根据路径获得图片并压缩，返回bitmap用于显示
	public static Bitmap getDisplayBitmap(String filePath, int reqWidth,
			int reqHeght) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		options.inSampleSize = ImageUtils.calculateInSampleSize(options,
				reqWidth, reqHeght);
		// System.out.println("inSampleSize = " + options.inSampleSize);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		return BitmapFactory.decodeFile(filePath, options);
	}

	public static boolean uploadSnapshot(String snapshotId) {
		String imagePath = Constants.SNAPSHOT_FLODER + snapshotId + ".jpg";

		Map<String, String> map = new HashMap<String, String>();
		map.put("snapshotId", snapshotId);
		map.put("image", ImageUtils.encodeBitmapToString(imagePath));

		String response = HttpRequestUtils.post("uploadSnapshot", map);
		try {
			JSONObject json = new JSONObject(response);
			String returnMessage = json.getString("returnMessage");
			if (returnMessage.equals(HttpConstants.UPLOAD_SNAPSHOT_SUCCESS)) {
				return true;
			} else {
				System.out.println("uploadSnapshot ---->> " + returnMessage);
				return false;
			}
		} catch (JSONException e) {
			System.out.println("uploadSnapshot ---->> " + e.getMessage());
			return false;
		}
	}

	public static byte[] downloadAvatar(Context context, String userId) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("userId", userId);

		String response = HttpRequestUtils.post("getAvatar", map);

		try {
			JSONObject json = new JSONObject(response);
			String returnMessage = json.getString("returnMessage");
			if (returnMessage.equals(HttpConstants.GET_AVATAR_SUCCESS)) {
				String encodedAvatar = json.getString("encodedAvatar");
				// save the avatar
				byte[] img = Base64.decode(encodedAvatar, Base64.DEFAULT);
				Avatar avatar = new Avatar();
				avatar.setUserId(userId);
				avatar.setPhoto(img);
				DBAvatar.insert(context, avatar);
				return img;

			} else {
				System.out.println("getAvatar ---->> " + returnMessage);
				return null;
			}
		} catch (JSONException e) {
			System.out.println("getAvatar ---->> " + e.getMessage());
			return null;
		}
	}

	private static String getRealPathFromURI(Context context, Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null,
				null, null);
		Cursor cursor = loader.loadInBackground();
		int columnIndex = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(columnIndex);
	}

	/*
	 * 压缩图片，处理某些手机拍照角度旋转的问题
	 */
	public static Bitmap compressImage(Context context, Uri contentUri,
			int reqWidth, int reqHeght) throws FileNotFoundException {
		String imagePath = getRealPathFromURI(context, contentUri);
		Bitmap bm = getSmallBitmap(imagePath, reqWidth, reqHeght);

		int degree = readPictureDegree(imagePath);

		System.out.println("degree = " + degree);
		if (degree != 0) {// 旋转照片角度
			bm = rotateBitmap(bm, degree);
		}
		return bm;
	}

	public static Bitmap compressImage(Context context, String imagePath,
			int reqWidth, int reqHeght) throws FileNotFoundException {
		Bitmap bm = getSmallBitmap(imagePath, reqWidth, reqHeght);

		int degree = readPictureDegree(imagePath);

		System.out.println("degree = " + degree);
		if (degree != 0) {// 旋转照片角度
			bm = rotateBitmap(bm, degree);
		}
		return bm;
	}

	// 根据路径获得图片并压缩，返回bitmap用于显示
	public static Bitmap getSmallBitmap(String filePath, int reqWidth,
			int reqHeght) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeght);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		return BitmapFactory.decodeFile(filePath, options);
	}

	public static int readPictureDegree(String path) {
		int degree = 0;
		try {
			ExifInterface exifInterface = new ExifInterface(path);
			int orientation = exifInterface.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return degree;
	}

	public static Bitmap rotateBitmap(Bitmap bitmap, int degress) {
		if (bitmap != null) {
			Matrix m = new Matrix();
			m.postRotate(degress);
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
					bitmap.getHeight(), m, true);
			return bitmap;
		}
		return bitmap;
	}
}
