package com.helloc.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.helloc.constant.Constants;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;
import android.util.Log;

public class GoogleMapUtils {
	private static double EARTH_RADIUS = 6378.137;// 地球半径
	private static double latitude;
	private static double longitude;

	/* 查询经纬度 */
	public static double[] getLatLng(String AddressName) {
		// List<Map<String, Object>> mData=new ArrayList<Map<String, Object>>();
		String url = "http://maps.googleapis.com/maps/api/geocode/json?address="
				+ AddressName.replace(" ", "+") + "&sensor=false";
		
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		try {
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			InputStream input = entity.getContent();
			int t;
			StringBuffer buffer = new StringBuffer();
			while ((t = input.read()) != -1) {
				buffer.append((char) t);
			}
			// tv.setText(buffer);

			// json解析
			JSONObject object = new JSONObject(buffer.toString());
			JSONObject location = object.getJSONArray("results")
					.getJSONObject(0)// 获得中括号的内容
					.getJSONObject("geometry")// 获得大括号中的内容
					.getJSONObject("location");
			longitude = location.getDouble("lng");
			latitude = location.getDouble("lat");
			Log.i("HHJ", "经纬度是 : " + latitude + "   " + longitude);
			double[] data = { latitude, longitude };
			return data;

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String getAddress(double[] latlng, Context context) {
		if(context != null) {
			Geocoder geocoder = new Geocoder(context);
			// Create a list to contain the result address
			List<Address> addresses = null;
			try {
				/*
				 * Call the synchronous getFromLocation() method with the latitude
				 * and longitude of the current location. Return at most 1 address.
				 */
				addresses = geocoder.getFromLocation(latlng[0], latlng[1], 1);
				// Catch network or other I/O problems.
			} catch (IOException exception1) {
				exception1.printStackTrace();
				return "";

				// Catch incorrect latitude or longitude values
			} catch (IllegalArgumentException exception2) {
				exception2.printStackTrace();
				return "";
			}
			// If the reverse geocode returned an address
			if (addresses != null && addresses.size() > 0) {
				StringBuilder addressText = new StringBuilder();
				addressText.append(TextUtils.isEmpty(addresses.get(0)
						.getAdminArea()) ? "" : addresses.get(0).getAdminArea());
				addressText.append(TextUtils
						.isEmpty(addresses.get(0).getLocality()) ? "" : addresses
						.get(0).getLocality());
				addressText
						.append(TextUtils
								.isEmpty(addresses.get(0).getSubLocality()) ? ""
								: addresses.get(0).getSubLocality());
				addressText.append(TextUtils.isEmpty(addresses.get(0)
						.getThoroughfare()) ? "" : addresses.get(0)
						.getThoroughfare());
				return addressText.toString();
			} else {
				return "";
			}
		}
		return "";
	}

	public static String getAddress(String latitude, String longitude) {
		String addr = "";
		// 也可以是http://maps.google.cn/maps/geo?output=csv&key=abcdef&q=%s,%s，不过解析出来的是英文地址
		// 密钥可以随便写一个key=abc
		// output=csv,也可以是xml或json，不过使用csv返回的数据最简洁方便解析
		String url = String.format(
				"http://ditu.google.cn/maps/geo?output=csv&key=abcdef&q=%s,%s",
				latitude, longitude);
		URL myURL = null;
		URLConnection httpsConn = null;
		try {
			myURL = new URL(url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		try {
			httpsConn = (URLConnection) myURL.openConnection();
			if (httpsConn != null) {
				InputStreamReader insr = new InputStreamReader(
						httpsConn.getInputStream(), "UTF-8");
				BufferedReader br = new BufferedReader(insr);
				String data = null;
				if ((data = br.readLine()) != null) {
					System.out.println(data);
					String[] retList = data.split(",");
					if (retList.length > 2 && ("200".equals(retList[0]))) {
						addr = retList[2];
						addr = addr.replace("\"", "");
					} else {
						addr = "";
					}
					Log.i("HHJ", "123  : " + addr);
				}
				insr.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return addr;
	}

	public static float getZoom(int range) {
		if (range <= 1200) {
			return Constants.MAP_ZOOM;
		} else if (range <= 2600) {
			return Constants.MAP_ZOOM - 1;
		} else if (range <= 5500) {
			return Constants.MAP_ZOOM - 2;
		} else if (range <= 10000) {
			return Constants.MAP_ZOOM - 3;
		} else if (range <= 22000) {
			return Constants.MAP_ZOOM - 4;
		} else if (range <= 45000) {
			return Constants.MAP_ZOOM - 5;
		} else if (range <= 90000) {
			return Constants.MAP_ZOOM - 6;
		} else if (range <= 180000) {
			return Constants.MAP_ZOOM - 7;
		} else if (range <= 380000) {
			return Constants.MAP_ZOOM - 8;
		} else if (range <= 700000) {
			return Constants.MAP_ZOOM - 9;
		} else if (range <= 1500000) {
			return Constants.MAP_ZOOM - 10;
		} else {
			return 2;
		}
	}

	public static double getDistance(String lat1, String lng1, String lat2,
			String lng2) {
		double radLat1 = rad(Double.parseDouble(lat1));
		double radLat2 = rad(Double.parseDouble(lat2));
		double a = radLat1 - radLat2;
		double b = rad(Double.parseDouble(lng1))
				- rad(Double.parseDouble(lng2));

		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * EARTH_RADIUS * 1000;
		s = Math.round(s * 10000) / 10000;
		System.out.println("distance = " + s);
		return s;
	}

	/*public static double getDistatce(double lat1, double lat2, double lon1,
			double lon2) {
		double R = 6371;
		double distance = 0.0;
		double dLat = (lat2 - lat1) * Math.PI / 180;
		double dLon = (lon2 - lon1) * Math.PI / 180;
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(lat1 * Math.PI / 180)
				* Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2)
				* Math.sin(dLon / 2);
		distance = (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))) * R;
		System.out.println("distance = " + distance);
		return distance;
	}*/

	public static double getDistance(double lat1, double lng1, double lat2,
			double lng2) {
		float[] results=new float[1];
	    Location.distanceBetween(lat1, lng1, lat2, lng2, results);
	    System.out.println("distance = " + results[0]);
	    return results[0];
	}

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	public static int getInterval(double lat1, double lng1, double lat2,
			double lng2) {
		double distance = getDistance(lat1, lng1, lat2, lng2);
		if (distance <= 100) {
			return 1;
		} else if (distance <= 200) {
			return 2;
		} else if (distance <= 1000) {
			return 3;
		} else if (distance <= 5000) {
			return 4;
		} else {
			return 5;
		}
	}
	
	public static int getIncreaseRadius(int radius, int counter) {
		int exponent = 0;
		int base = 0;
		if (radius < 10000) {
			counter = counter % 20;
			base = 50;
			exponent = (int) counter / 5 + 1;
		} else if (radius < 100000) {
			counter = counter % 15;
			base = 1000;
			exponent = (int) counter / 10 + 1;
		} else if (radius < 500000) {
			base = 1000;
			exponent = (int) counter / 10 + 1;
		} else if (radius < 1000000) {
			base = 1000;
			exponent = (int) counter / 5 + 1;
		} else {
			base = 1000;
			exponent = (int) counter / 2 + 1;
		}
		radius += base * exponent;
		return radius;
	}

	public static int getDecreaseRadius(int radius, int counter) {
		int exponent = 0;
		int base = 0;
		if (radius < 10000) {
			counter = counter % 20;
			base = 50;
			exponent = (int) counter / 5 + 1;
		} else if (radius < 100000) {
			counter = counter % 20;
			base = 1000;
			exponent = (int) counter / 10 + 1;
		} else if (radius < 500000) {
			base = 1000;
			exponent = (int) counter / 10 + 1;
		} else if (radius < 1000000) {
			base = 1000;
			exponent = (int) counter / 10 + 1;
		} else {
			base = 1000;
			exponent = (int) counter / 2 + 1;
		}
		radius -= base * exponent;
		if (radius < Constants.RADIUS_BASE) {
			radius = Constants.RADIUS_BASE;
		}
		return radius;
	}
}
