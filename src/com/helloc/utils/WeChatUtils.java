package com.helloc.utils;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.tencent.mm.sdk.openapi.GetMessageFromWX;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;

public class WeChatUtils {

	public static void registerToWeChat(Context context) {
		IWXAPI api;
		// 通过WXAPIFactory工厂，获取IWXAPI的实例
		api = WXAPIFactory.createWXAPI(context, Constants.WX_APP_ID, true);
		// register to wechate
		if (api.registerApp(Constants.WX_APP_ID)) {
			SharePrefsUtils.setRegisterToWechat(context, true);
		} else {
			SharePrefsUtils.setRegisterToWechat(context, false);
			if (SharePrefsUtils.isNotShowRegisterToWechatHint(context) == false) {
				SharePrefsUtils.SetNotShowRegisterToWechatHint(context, true);
				Toast.makeText(
						context,
						context.getString(R.string.toast_register_to_wx_failed),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public static void respWXFriends(Context context, IWXAPI api,
			String params, String title, String description, Bundle bundle) {
		WXMediaMessage msg = getMessage(context, params, title, description);
		// 构造一个Resp
		GetMessageFromWX.Resp resp = new GetMessageFromWX.Resp();
		// 将req的transaction设置到resp对象中，其中bundle为微信传递过来的intent所带的内容，通过getExtras方法获取
		resp.transaction = getTransaction(bundle);
		resp.message = msg;
		// 调用api接口响应数据到微信
		api.sendResp(resp);
	}

	public static void reqWXFriends(Context context, IWXAPI api, String params,
			String title, String description) {
		WXMediaMessage msg = getMessage(context, params, title, description);
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("webpage");
		req.message = msg;
		req.scene = SendMessageToWX.Req.WXSceneSession;
		api.sendReq(req);
	}

	public static void reqWXMoments(Context context, IWXAPI api, String params,
			String title, String description) {
		WXMediaMessage msg = getMessage(context, params, title, description);
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("webpage");
		req.message = msg;
		req.scene = SendMessageToWX.Req.WXSceneTimeline;
		api.sendReq(req);
	}

	private static WXMediaMessage getMessage(Context context, String params,
			String title, String description) {
		WXWebpageObject webpage = new WXWebpageObject();
		webpage.webpageUrl = HttpConstants.SERVER_URL + params;

		WXMediaMessage msg = new WXMediaMessage(webpage);
		msg.setThumbImage(BitmapFactory.decodeResource(context.getResources(),
				R.drawable.wx_icon));
		msg.title = title;
		msg.description = description;
		return msg;
	}

	/**
	 * get the wechat transaction
	 */
	private static String getTransaction(Bundle bundle) {
		final GetMessageFromWX.Req req = new GetMessageFromWX.Req(bundle);
		return req.transaction;
	}

	private static String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis())
				: type + System.currentTimeMillis();
	}

}
