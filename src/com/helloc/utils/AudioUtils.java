package com.helloc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Base64;

import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;

public class AudioUtils {

	public static void playAudio(String audioPath) {
		MediaPlayer mMediaPlayer = new MediaPlayer();
		try {
			if (mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
			}
			mMediaPlayer.reset();
			mMediaPlayer.setDataSource(audioPath);
			mMediaPlayer.prepare();
			mMediaPlayer.start();
			mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
				public void onCompletion(MediaPlayer mp) {

				}
			});

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private static String encodeAudioToString(byte[] bFile) {
		return Base64.encodeToString(bFile, Base64.DEFAULT).replace("\n", "")
				.replace("+", "%2B");
	}

	public static boolean downloadAudio(String audioName) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("voiceName", audioName);
		
		String response = HttpRequestUtils.post("getVoiceFile", map);
		try {
			JSONObject json = new JSONObject(response);
			String returnMessage = json.getString("returnMessage");
			if(returnMessage.equals(HttpConstants.GET_AUDIO_FILE_SUCCESS)) {
				byte[] src = Base64.decode(json.getString("voiceContent"), Base64.DEFAULT);
				try {
					String audioPath = Constants.AUDIO_FLODER + audioName + ".amr";
					File audio = new File(audioPath);
					FileOutputStream fos = new FileOutputStream(audio);
					fos.write(src);
					fos.flush();
					fos.close();
					return true;
				} catch (FileNotFoundException e) {
					System.out.println("getVoiceFile ---->> FileNotFoundException");
					return false;
				} catch (IOException e) {
					System.out.println("getVoiceFile ---->> IOException");
					return false;
				}
			} else {
				System.out.println("getVoiceFile ---->> " + returnMessage);
				return false;
			}
		} catch (JSONException e) {
			System.out.println("getVoiceFile ---->> " + e.getMessage());
			return false;
		}
	}
	
	public static boolean uploadAudio(Context context, String audioName) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("voiceName", audioName);

		File file = new File(Constants.AUDIO_FLODER + audioName + ".amr");
		if (file.exists()) {
			byte[] bFile = new byte[(int) file.length()];
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				fileInputStream.read(bFile);
				fileInputStream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String encodeVoice = encodeAudioToString(bFile);
			map.put("voiceContent", encodeVoice);
		} else {
			/*Toast.makeText(context,
					context.getString(R.string.toast_no_audio_file),
					Toast.LENGTH_SHORT).show();*/
			return false;
		}
		String response = HttpRequestUtils.post("uploadVoiceFile", map);
		try {
			JSONObject json = new JSONObject(response);
			String returnMessage = json.getString("returnMessage");
			if (returnMessage.equals(HttpConstants.UPLOAD_AUDIO_FILE_SUCCESS)) {
				return true;
			} else {
				System.out.println("uploadVoiceFile ---->> " + returnMessage);
				return false;
			}
		} catch (JSONException e) {
			System.out.println("uploadVoiceFile ---->> " + e.getMessage());
			return false;
		}
	}
}
