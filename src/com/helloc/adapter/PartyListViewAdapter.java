package com.helloc.adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.SpannableString;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.database.DBMapSnapshot;
import com.helloc.model.MapSnapshot;
import com.helloc.model.Party;
import com.helloc.utils.ExpressionUtil;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;

public class PartyListViewAdapter extends BaseAdapter {
	private ArrayList<Party> listItems;
	private LayoutInflater listContainer; // 视图容器
	private Context context;

	public final class ViewHolder { // 自定义控件集合
		private ImageView image;
		private TextView theme;
		private TextView startTime;
		private TextView endTime;
		private TextView address;
	}

	public PartyListViewAdapter(Context context, ArrayList<Party> listItems) {
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.context = context;
		this.listItems = listItems;
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			// 获取list_item布局文件的视图
			convertView = listContainer.inflate(R.layout.list_item_party, null);
			viewHolder.theme = (TextView) convertView.findViewById(R.id.theme);
			viewHolder.startTime = (TextView) convertView
					.findViewById(R.id.startTime);
			viewHolder.endTime = (TextView) convertView
					.findViewById(R.id.endTime);
			viewHolder.address = (TextView) convertView
					.findViewById(R.id.address);
			viewHolder.image = (ImageView) convertView.findViewById(R.id.image);

			setUpUI(viewHolder, position, listItems);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

	private void setUpUI(ViewHolder viewHolder, final int position,
			final ArrayList<Party> listItems) {
		Party party = listItems.get(position);
		// set theme
		String themeStr = party.getTheme();
		try {
			SpannableString spannableString = ExpressionUtil
					.getExpressionString(context, themeStr, Constants.REGULAR);
			viewHolder.theme.setText(spannableString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		DateFormat formatter = new SimpleDateFormat("yyy-MM-dd hh:mm a", 
				context.getResources().getConfiguration().locale);
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTimeInMillis(Long.parseLong(party.getStartTime()));
		viewHolder.startTime.setText(formatter.format(calendar.getTime()));
		
		calendar.setTimeInMillis(Long.parseLong(party.getEndTime()));
		viewHolder.endTime.setText(formatter.format(calendar.getTime()));
		
		viewHolder.address.setText(party.getAddress());

		// set snapshot
		String partyId = listItems.get(position).getPartyId();
		byte[] snapshot = DBMapSnapshot.getMapSnapshot(context, partyId);
		if(snapshot != null) {
			viewHolder.image.setImageBitmap(ImageUtils.convertByteToBitmap(context,
					snapshot));
		} else {
			downloadSnapshop(partyId, viewHolder.image);
		}
		
	}
	
	private void downloadSnapshop(final String snapshotId,
			final ImageView imageView) {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("snapshotId", snapshotId);
				return HttpRequestUtils.post("downloadSnapshot", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.DOWNLOAD_SNAPSHOT_SUCCESS)) {
						// save the snapshot
						System.out.println("image = " + json.getString("image"));
						byte[] img = Base64.decode(json.getString("image"),
								Base64.DEFAULT);
						Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0,
								img.length);
						if(bitmap != null) {
							String imagePath = Constants.SNAPSHOT_FLODER
									+ snapshotId + ".jpg";
							ImageUtils.saveImage(bitmap, imagePath);

							MapSnapshot mapSnapshot = new MapSnapshot();
							mapSnapshot.setId(snapshotId);
							mapSnapshot.setPhoto(img);

							DBMapSnapshot.insert(context, mapSnapshot);

							imageView.setImageBitmap(ImageUtils
									.convertByteToBitmap(context, img));
						}
					} else {
						System.out.println("downloadSnapshot ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("downloadSnapshot ---->> "
							+ e.getMessage());
				}
			}
		}.execute();
	}
}
