package com.helloc.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.database.DBAvatar;
import com.helloc.utils.ImageUtils;

public class InvitationListviewAdapter extends BaseAdapter {
	private List<Map<String, String>> listItems;
	private LayoutInflater listContainer; // 视图容器
	private Context context;

	private final class ViewHolder { // 自定义控件集合
		public ImageView avatar;
		public TextView name;
		public TextView status;
	}

	public InvitationListviewAdapter(Context context,
			List<Map<String, String>> listItems) {
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.listItems = listItems;
		this.context = context;
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			// 获取list_item布局文件的视图
			convertView = listContainer.inflate(R.layout.list_item_invitation,
					null);
			viewHolder.avatar = (ImageView) convertView
					.findViewById(R.id.avatar);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);
			viewHolder.status = (TextView) convertView
					.findViewById(R.id.status);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		setUpView(viewHolder, position);
		return convertView;
	}

	private void setUpView(final ViewHolder viewHolder, final int position) {
		// set the name
		viewHolder.name.setText(listItems.get(position).get(
				ActivityConstants.ARGS_ACCOUNT_NICKNAME));
		viewHolder.status.setText(listItems.get(position).get(
				ActivityConstants.ARGS_INVITATION_STATUS_MSG));
		// set the avatar
		byte[] photo = DBAvatar.getAvatar(
				context,
				listItems.get(position).get(
						ActivityConstants.ARGS_ACCOUNT_USER_ID));
		if (photo != null) {
			Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
					photo.length);
			viewHolder.avatar.setImageBitmap(avatar);
		} else {
			new AsyncTask<Void, Void, byte[]>() {

				@Override
				protected byte[] doInBackground(Void... params) {
					return ImageUtils.downloadAvatar(
							context,
							listItems.get(position).get(
									ActivityConstants.ARGS_ACCOUNT_USER_ID));
				}

				@Override
				protected void onPostExecute(byte[] result) {
					if (result != null) {
						viewHolder.avatar.setImageBitmap(BitmapFactory
								.decodeByteArray(result, 0, result.length));
					}
				}
			}.execute();
		}
	}
}
