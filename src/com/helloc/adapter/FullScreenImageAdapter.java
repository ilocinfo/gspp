package com.helloc.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.view.TouchImageView;

public class FullScreenImageAdapter extends PagerAdapter {
	private Context context;
	// private ArrayList<String> _imagePaths;
	private ArrayList<String> imageIndex;
	private LayoutInflater inflater;

	// constructor
	public FullScreenImageAdapter(Context context, ArrayList<String> imageIndex) {
		this.context = context;
		this.imageIndex = imageIndex;
	}

	@Override
	public int getCount() {
		return this.imageIndex.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		TouchImageView imgDisplay;
		ProgressBar progressBar;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = inflater.inflate(
				R.layout.item_image_display_fullscreen, container, false);

		imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.image);
		progressBar = (ProgressBar) viewLayout.findViewById(R.id.progressBar);

		String imagePath = Constants.IMAGE_FLODER + imageIndex.get(position)
				+ ".jpg";
		Bitmap bitmap;
		File file = new File(imagePath);
		if (file.exists()) {
			bitmap = ImageUtils.getDisplayBitmap(imagePath, 480, 720);
			// imgDisplay.setScaleType(ImageView.ScaleType.FIT_CENTER);
			imgDisplay.setImageBitmap(bitmap);
		} else {
			imagePath = Constants.THUMBNAIL_FLODER + imageIndex.get(position)
					+ ".jpg";
			bitmap = BitmapFactory.decodeFile(imagePath);
			imgDisplay.setImageBitmap(bitmap);
			new DownloadImageTask(imageIndex.get(position), imgDisplay,
					progressBar, bitmap).execute();
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);
	}

	private class DownloadImageTask extends AsyncTask<Void, Void, String> {

		private String imageName;
		private TouchImageView imgDisplay;
		private ProgressBar progressBar;
		private Bitmap bitmap;

		DownloadImageTask(String imageName, TouchImageView imgDisplay,
				ProgressBar progressBar, Bitmap bitmap) {
			this.imageName = imageName;
			this.imgDisplay = imgDisplay;
			this.progressBar = progressBar;
			this.bitmap = bitmap;
		}

		@Override
		protected void onPreExecute() {
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(Void... params) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("imageName", imageName);

			return HttpRequestUtils.post("downloadImage", map);
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject json = new JSONObject(result);
				String returnMessage = json.getString("returnMessage");
				if (returnMessage.equals(HttpConstants.DOWNLOAD_IMAGE_SUCCESS)) {
					if (bitmap.isRecycled() == false) {
						byte[] img = Base64.decode(json.getString("image"),
								Base64.DEFAULT);

						bitmap = BitmapFactory.decodeByteArray(img, 0,
								img.length);
						String imagePath = Constants.IMAGE_FLODER + imageName
								+ ".jpg";
						ImageUtils.saveImage(bitmap, imagePath);

						imagePath = Constants.IMAGE_FLODER + imageName + ".jpg";
						progressBar.setVisibility(View.GONE);
						bitmap = ImageUtils.getDisplayBitmap(imagePath, 480,
								720);
						// imgDisplay.setScaleType(ImageView.ScaleType.FIT_CENTER);
						imgDisplay.setImageBitmap(bitmap);

					}
				} else {
					System.out.println("downloadImage ---->> " + returnMessage);
				}
			} catch (JSONException e) {
				System.out.println("downloadImage ---->> " + e.getMessage());
			}
		}
	}

}
