package com.helloc.adapter;

import java.util.ArrayList;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.helloc.R;

public class LocationsListviewAdapter extends BaseAdapter {
	private LocationsListAdapterCallback callback;
	private LayoutInflater listContainer; // 视图容器
	private ArrayList<Map<String, String>> listItems;
	private Context context;

	private final class ViewHolder {
		private TextView name;
		private TextView address;
		private Button remove;
	}

	public LocationsListviewAdapter(Context context,
			ArrayList<Map<String, String>> listItems) {
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.listItems = listItems;
		this.context = context;
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = listContainer.inflate(R.layout.list_item_locations,
					null);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);
			viewHolder.address = (TextView) convertView
					.findViewById(R.id.address);
			viewHolder.remove = (Button) convertView.findViewById(R.id.remove);

			viewHolder.name.setText(listItems.get(position).get("name"));
			viewHolder.address.setText(listItems.get(position).get("address"));
			viewHolder.remove.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					new AlertDialog.Builder(context)
							.setTitle(context.getString(R.string.action_delete))
							.setMessage(
									context.getString(R.string.text_delete_hint))
							.setPositiveButton(
									context.getString(R.string.btn_confirm),
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// notifyDataSetChanged();
											callback.deleteItem(Integer
													.parseInt(listItems.get(
															position).get("id")));
										}
									})
							.setNegativeButton(
									context.getString(R.string.btn_cancel),
									null).create().show();

				}
			});
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

	public void setCallback(LocationsListAdapterCallback callback) {
		this.callback = callback;
	}

	public interface LocationsListAdapterCallback {
		public void deleteItem(int position);
	}

}
