package com.helloc.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBAvatar;
import com.helloc.model.Friends;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.PinyinUtils;

public class FriendsListviewAdapter extends BaseAdapter {
	private List<Friends> listItems;
	private LayoutInflater listContainer; // 视图容器
	private Context context;

	private final class ViewHolder { // 自定义控件集合
		public ImageView avatar;
		public TextView name;
		public Button notice;
		public LinearLayout subTitle;
		public TextView letterTag;
	}

	public FriendsListviewAdapter(Context context, List<Friends> listItems) {
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.listItems = listItems;
		this.context = context;
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			// 获取list_item布局文件的视图
			convertView = listContainer.inflate(R.layout.list_item_friends,
					null);
			viewHolder.avatar = (ImageView) convertView
					.findViewById(R.id.avatar);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);
			viewHolder.notice = (Button) convertView.findViewById(R.id.notice);
			viewHolder.subTitle = (LinearLayout) convertView
					.findViewById(R.id.subTitle);
			viewHolder.letterTag = (TextView) convertView
					.findViewById(R.id.letterTag);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		setUpView(viewHolder, position);
		return convertView;
	}

	@SuppressLint("DefaultLocale")
	private void setUpView(final ViewHolder viewHolder, final int position) {
		// set the name
		viewHolder.name.setText(listItems.get(position).getName());
		// show the notice icon if need
		int allowAutoReply = listItems.get(position).getAllowAutoReply();
		if (allowAutoReply == StatusConstants.AUTO_REPLY_STATUS_PENDING) {
			viewHolder.notice.setVisibility(View.VISIBLE);
		}
		// set letter tag
		String pyName = PinyinUtils.getPingYin(listItems.get(position)
				.getName());
		String firstLetter = pyName.substring(0, 1).toUpperCase();
		if (position == 0) {
			viewHolder.subTitle.setVisibility(View.VISIBLE);
			viewHolder.letterTag.setText(firstLetter);
		} else {
			// 获得上一个姓名的拼音首字母
			String firstLetterPre = PinyinUtils
					.getPingYin(listItems.get(position - 1).getName())
					.substring(0, 1).toUpperCase();
			// 比较一下两者是否相同
			if (firstLetter.equals(firstLetterPre)) {
				viewHolder.subTitle.setVisibility(View.GONE);
			} else {
				viewHolder.subTitle.setVisibility(View.VISIBLE);
				viewHolder.letterTag.setText(firstLetter);
			}
		}

		// set the avatar
		byte[] photo = DBAvatar.getAvatar(context, listItems.get(position)
				.getUserId());
		if (photo != null) {
			Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
					photo.length);
			viewHolder.avatar.setImageBitmap(avatar);
		} else {
			new AsyncTask<Void, Void, byte[]>() {

				@Override
				protected byte[] doInBackground(Void... params) {
					return ImageUtils.downloadAvatar(context,
							listItems.get(position).getUserId());
				}

				@Override
				protected void onPostExecute(byte[] result) {
					if (result != null) {
						viewHolder.avatar.setImageBitmap(BitmapFactory
								.decodeByteArray(result, 0, result.length));
					}
				}
			}.execute();
		}
	}
}
