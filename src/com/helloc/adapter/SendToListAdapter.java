package com.helloc.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.constant.Constants;

public class SendToListAdapter extends BaseAdapter {
	private ArrayList<String> nameList;
	private ArrayList<String> userIdList;
	private LayoutInflater listContainer; // 视图容器

	public final class ViewHolder {
		public ImageView avatar;
		public TextView name;
	}

	public SendToListAdapter(Context context, ArrayList<String> nameList,
			ArrayList<String> userIdList) {
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.nameList = nameList;
		this.userIdList = userIdList;
	}

	@Override
	public int getCount() {
		return nameList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = listContainer
					.inflate(R.layout.list_item_sendto, null);
			viewHolder.avatar = (ImageView) convertView
					.findViewById(R.id.avatar);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);

			// set the name
			viewHolder.name.setText(nameList.get(position));
			// set the avatar
			Bitmap avatar = BitmapFactory.decodeFile(Constants.HELLOC_FOLDER
					+ userIdList.get(position) + ".jpg");
			if (avatar != null) {
				viewHolder.avatar.setImageBitmap(avatar);
			}

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

}
