package com.helloc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.helloc.R;

public class CustomInfoWindowAdapter implements InfoWindowAdapter {
	private View mContents;
	private boolean hasTitle;
	
	public CustomInfoWindowAdapter(Context context, boolean hasTitle) {
		mContents = LayoutInflater.from(context).inflate(R.layout.marker_window, null);
		this.hasTitle = hasTitle;
	}
	
	@Override
	public View getInfoContents(Marker marker) {
		render(marker, mContents);
		return mContents;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		// This means that getInfoContents will be called.
		return null;
	}

	private void render(Marker marker, View view) {
		if(hasTitle) {
			String title = marker.getTitle();
			TextView titleUi = ((TextView) view.findViewById(R.id.title));
			titleUi.setText(title);
		} else {
			TextView titleUi = ((TextView) view.findViewById(R.id.title));
			titleUi.setVisibility(View.GONE);
			
			TextView hintUi = (TextView) view.findViewById(R.id.hint);
			hintUi.setVisibility(View.GONE);
		}
		
		
		String snippet = marker.getSnippet();
		TextView snippetUi = (TextView) view.findViewById(R.id.snippet);
		snippetUi.setText(snippet);
	}
	
}

