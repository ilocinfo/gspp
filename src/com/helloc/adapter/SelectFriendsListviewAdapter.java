package com.helloc.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.database.DBAvatar;
import com.helloc.model.Friends;
import com.helloc.utils.ImageUtils;

public class SelectFriendsListviewAdapter extends BaseAdapter {
	private List<Boolean> mChecked;
	private List<Friends> listItems;
	private LayoutInflater listContainer; // 视图容器
	private Context context;

	private final class ViewHolder { // 自定义控件集合
		public RelativeLayout line;
		public ImageView avatar;
		public TextView name;
		public CheckBox checkBox;

	}

	public SelectFriendsListviewAdapter(Context context, List<Friends> listItems) {
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.listItems = listItems;
		mChecked = new ArrayList<Boolean>();
		for (int i = 0; i < listItems.size(); i++) {
			mChecked.add(false);
		}
		this.context = context;
	}

	public SelectFriendsListviewAdapter(Context context,
			List<Friends> listItems, List<Boolean> mChecked) {
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.listItems = listItems;
		this.mChecked = mChecked;
		this.context = context;
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			// 获取list_item布局文件的视图
			convertView = listContainer.inflate(
					R.layout.list_item_select_friends, null);
			viewHolder.line = (RelativeLayout) convertView
					.findViewById(R.id.line);
			viewHolder.avatar = (ImageView) convertView
					.findViewById(R.id.avatar);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);
			viewHolder.checkBox = (CheckBox) convertView
					.findViewById(R.id.checkBox);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		setUpView(viewHolder, position);
		return convertView;
	}

	private void setUpView(final ViewHolder viewHolder, final int position) {
		// set the name
		viewHolder.name.setText(listItems.get(position).getName());
		// set the avatar
		byte[] photo = DBAvatar.getAvatar(context, listItems.get(position)
				.getUserId());
		if (photo != null) {
			Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
					photo.length);
			viewHolder.avatar.setImageBitmap(avatar);
		} else {
			new AsyncTask<Void, Void, byte[]>() {

				@Override
				protected byte[] doInBackground(Void... params) {
					return ImageUtils.downloadAvatar(context,
							listItems.get(position).getUserId());
				}

				@Override
				protected void onPostExecute(byte[] result) {
					if (result != null) {
						viewHolder.avatar.setImageBitmap(BitmapFactory
								.decodeByteArray(result, 0, result.length));
					}
				}
			}.execute();
		}

		viewHolder.line.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				RelativeLayout rl = (RelativeLayout) v;
				CheckBox cb = (CheckBox) rl.getChildAt(2);
				if (mChecked.get(position)) {
					mChecked.set(position, false);
					cb.setChecked(false);
				} else {
					mChecked.set(position, true);
					cb.setChecked(true);
				}
			}
		});

		if (mChecked.get(position)) {
			viewHolder.checkBox.setChecked(true);
		}
	}

	public List<Boolean> getCheckItem() {
		return mChecked;
	}
}
