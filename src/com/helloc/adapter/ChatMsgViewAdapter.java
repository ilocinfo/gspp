package com.helloc.adapter;

import java.io.File;
import java.util.Date;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.constant.Constants;
import com.helloc.database.DBAvatar;
import com.helloc.database.DBFriend;
import com.helloc.model.MessageReply;
import com.helloc.utils.AudioUtils;
import com.helloc.utils.MyTextUtils;
import com.helloc.utils.SharePrefsUtils;
import com.helloc.view.LoadingDialog;

public class ChatMsgViewAdapter extends BaseAdapter {

	private Context context;
	private List<MessageReply> listData;
	private LayoutInflater mInflater;

	public final class ViewHolder {
		private ImageView avatar;
		public TextView tvDate;
		public TextView tvUserName;
		public TextView tvContent;
		public RelativeLayout voiceBg;
		public Button voice;
	}

	public ChatMsgViewAdapter(Context context, List<MessageReply> listData) {
		this.context = context;
		this.listData = listData;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MessageReply msgReply = listData.get(position);
		ViewHolder viewHolder = null;
		//if (convertView == null) {
		if (msgReply.isMyReply()) {
			convertView = mInflater.inflate(
					R.layout.item_chatting_msg_text_right, null);
		} else {
			convertView = mInflater.inflate(
					R.layout.item_chatting_msg_text_left, null);
		}
		viewHolder = new ViewHolder();
		viewHolder.tvDate = (TextView) convertView
				.findViewById(R.id.tv_sendtime);
		viewHolder.avatar = (ImageView) convertView
				.findViewById(R.id.avatar);
		viewHolder.tvUserName = (TextView) convertView
				.findViewById(R.id.tv_username);
		viewHolder.tvContent = (TextView) convertView
				.findViewById(R.id.tv_chatcontent);
		viewHolder.voiceBg = (RelativeLayout) convertView
				.findViewById(R.id.voiceBg);
		viewHolder.voice = (Button) convertView.findViewById(R.id.voice);
		//convertView.setTag(viewHolder);
		//} else {
			//viewHolder = (ViewHolder) convertView.getTag();
		//}
		setUpView(viewHolder, msgReply);
		return convertView;
	}

	private void setUpView(ViewHolder viewHolder, final MessageReply msgReply) {
		Date date = new Date(Long.parseLong(msgReply.getDate()));
		String time = DateFormat.getTimeFormat(context).format(date) + ", "
				+ DateFormat.getMediumDateFormat(context).format(date);
		viewHolder.tvDate.setText(time);

		// set name
		String userName = DBFriend.getNameByUserId(context,
				msgReply.getUserId());
		if (msgReply.getUserId().equals(SharePrefsUtils.getUserId(context))) {
			userName = SharePrefsUtils.getNickname(context);
		}
		userName = userName.equals("") ? msgReply.getUserName() : userName;
		viewHolder.tvUserName.setText(userName);

		// set content
		if (msgReply.getMessageType() == Constants.MESSAGE_TYPE_TEXT) {
			viewHolder.tvContent.setText(MyTextUtils.getSpannableString(
					context, msgReply.getReplyContent()));
		} else if (msgReply.getMessageType() == Constants.MESSAGE_TYPE_AUDIO) {
			viewHolder.tvContent.setVisibility(View.GONE);
			viewHolder.voiceBg.setVisibility(View.VISIBLE);
			viewHolder.voice.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// check whether the audio file download
					String audioPath = Constants.AUDIO_FLODER
							+ msgReply.getMessageId() + ".amr";
					File audio = new File(audioPath);
					// if exist, play it
					if (audio.exists()) {
						AudioUtils.playAudio(audioPath);
					} else {
						// down the voice from server
						downloadAudio(msgReply.getMessageId());
					}
				}
			});
		}

		// set the avatar
		byte[] photo = DBAvatar.getAvatar(context, msgReply.getUserId());
		if (photo != null) {
			Bitmap avatar = BitmapFactory.decodeByteArray(photo, 0,
					photo.length);
			viewHolder.avatar.setImageBitmap(avatar);
		}
	}

	private void downloadAudio(final String audioName) {
		new AsyncTask<Void, Void, Boolean>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = LoadingDialog.createLoadingDialog(context,
						context.getString(R.string.toast_download_audio));
				dialog.show();
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				return AudioUtils.downloadAudio(audioName);
			}

			@Override
			protected void onPostExecute(Boolean result) {
				dialog.dismiss();
				if (result) {
					AudioUtils.playAudio(Constants.AUDIO_FLODER + audioName
							+ ".amr");
				} else {

				}
			}
		}.execute();
	}

}
