package com.helloc.adapter;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.helloc.R;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMapSnapshot;
import com.helloc.model.MapSnapshot;
import com.helloc.utils.AudioUtils;
import com.helloc.utils.HttpRequestUtils;
import com.helloc.utils.ImageUtils;
import com.helloc.utils.MyTextUtils;
import com.helloc.view.LoadingDialog;

public class MessageListviewAdapter extends BaseAdapter {
	private List<Map<String, String>> listItems;
	private LayoutInflater listContainer; // 视图容器
	private Context context;

	public final class ViewHolder { // 自定义控件集合
		public TextView icon;
		public TextView name;
		public TextView privateMsg;
		public RelativeLayout voiceBg;
		public TextView voice;
		public TextView latestDate;
		public TextView address;
		private ImageView image;
	}

	public MessageListviewAdapter(Context context,
			List<Map<String, String>> listItems) {
		this.context = context;
		this.listItems = listItems;
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			// 获取list_item布局文件的视图
			convertView = listContainer.inflate(R.layout.list_item_message,
					null);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);
			viewHolder.icon = (TextView) convertView.findViewById(R.id.icon);
			viewHolder.address = (TextView) convertView
					.findViewById(R.id.address);
			viewHolder.latestDate = (TextView) convertView
					.findViewById(R.id.latestDate);
			viewHolder.privateMsg = (TextView) convertView
					.findViewById(R.id.event);
			viewHolder.voiceBg = (RelativeLayout) convertView
					.findViewById(R.id.voiceBg);
			viewHolder.voice = (TextView) convertView.findViewById(R.id.voice);
			viewHolder.image = (ImageView) convertView.findViewById(R.id.image);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		setUpView(viewHolder, position);
		return convertView;
	}

	private void setUpView(final ViewHolder viewHolder, final int position) {
		Map<String, String> item = listItems.get(position);

		viewHolder.name.setText(item
				.get(ActivityConstants.ARGS_MESSAGE_DISPLAY_NAME));
		viewHolder.address.setText(item
				.get(ActivityConstants.ARGS_MESSAGE_ADDRESS));
		viewHolder.latestDate.setText(item
				.get(ActivityConstants.ARGS_MESSAGE_EXPIRATION_TIME));

		final String id = item.get(ActivityConstants.ARGS_MESSAGE_ID);
		int messageType = Integer.parseInt(item
				.get(ActivityConstants.ARGS_MESSAGE_TYPE));

		// set the event text or event audio
		if (messageType == Constants.MESSAGE_TYPE_TEXT) {
			String privateMsg = item
					.get(ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG);
			viewHolder.privateMsg.setText(MyTextUtils.getSpannableString(
					context, privateMsg));
		} else if (messageType == Constants.MESSAGE_TYPE_AUDIO) {
			viewHolder.privateMsg.setVisibility(View.GONE);
			viewHolder.voiceBg.setVisibility(View.VISIBLE);
			viewHolder.voice.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// check whether the audio file download
					String audioPath = Constants.AUDIO_FLODER + id + ".amr";
					File audio = new File(audioPath);
					// if exist, play it
					if (audio.exists()) {
						AudioUtils.playAudio(audioPath);
					} else {
						downloadAudio(id);
					}
				}
			});
		}

		// set icon
		int messageStatus = Integer.parseInt(item
				.get(ActivityConstants.ARGS_MESSAGE_STATUS));
		if (messageStatus == StatusConstants.MESSAGE_STATUS_SELF
				|| messageStatus == StatusConstants.MESSAGE_STATUS_SELF_ACTIVED) {
			viewHolder.icon.setBackgroundResource(R.drawable.icon_set_to_me);
		} else if (messageStatus == StatusConstants.MESSAGE_STATUS_SENT) {
			viewHolder.icon.setBackgroundResource(R.drawable.icon_to);
		} else if (messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED
				|| messageStatus == StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED) {
			viewHolder.icon.setBackgroundResource(R.drawable.icon_from);
		}

		// set snapshot
		byte[] snapshot = DBMapSnapshot.getMapSnapshot(context, id);
		if (snapshot != null) {
			viewHolder.image.setImageBitmap(ImageUtils.convertByteToBitmap(
					context, snapshot));
		} else {
			downloadSnapshop(id, viewHolder.image);
		}
	}

	private void downloadAudio(final String audioName) {
		new AsyncTask<Void, Void, Boolean>() {
			Dialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = LoadingDialog.createLoadingDialog(context,
						context.getString(R.string.toast_download_audio));
				dialog.show();
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				return AudioUtils.downloadAudio(audioName);
			}

			@Override
			protected void onPostExecute(Boolean result) {
				dialog.dismiss();
				if (result) {
					AudioUtils.playAudio(Constants.AUDIO_FLODER + audioName
							+ ".amr");
				} else {

				}
			}
		}.execute();
	}

	private void downloadSnapshop(final String snapshotId,
			final ImageView imageView) {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("snapshotId", snapshotId);
				return HttpRequestUtils.post("downloadSnapshot", map);
			}

			@Override
			protected void onPostExecute(String result) {
				try {
					JSONObject json = new JSONObject(result);
					String returnMessage = json.getString("returnMessage");
					if (returnMessage
							.equals(HttpConstants.DOWNLOAD_SNAPSHOT_SUCCESS)) {
						byte[] img = Base64.decode(json.getString("image"),
								Base64.DEFAULT);
						Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0,
								img.length);
						if (bitmap != null) {
							String imagePath = Constants.SNAPSHOT_FLODER
									+ snapshotId + ".jpg";
							ImageUtils.saveImage(bitmap, imagePath);

							MapSnapshot mapSnapshot = new MapSnapshot();
							mapSnapshot.setId(snapshotId);
							mapSnapshot.setPhoto(img);

							DBMapSnapshot.insert(context, mapSnapshot);

							imageView.setImageBitmap(ImageUtils
									.convertByteToBitmap(context, img));
						}
					} else {
						System.out.println("downloadSnapshot ---->> "
								+ returnMessage);
					}
				} catch (JSONException e) {
					System.out.println("downloadSnapshot ---->> "
							+ e.getMessage());
				}
			}
		}.execute();
	}
}
