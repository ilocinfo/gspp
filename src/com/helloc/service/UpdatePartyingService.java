package com.helloc.service;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.HttpConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBParty;
import com.helloc.database.DBPartying;
import com.helloc.model.Partying;
import com.helloc.utils.GoogleMapUtils;
import com.helloc.utils.HttpRequestUtils;

public class UpdatePartyingService extends Service {
	private boolean isServiceRunning;
	private double latitude;
	private double longitude;
	private String partyId;
	private String userId;
	private String nickname;
	private String startTime;
	private UpdateLocation updateLocation;
	private int updateFreq = 30000;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (isServiceRunning == false) {
			isServiceRunning = true;
			latitude = intent.getDoubleExtra(
					ActivityConstants.ARGS_PARTY_LATITUDE, 0);
			longitude = intent.getDoubleExtra(
					ActivityConstants.ARGS_PARTY_LONGITUDE, 0);
			partyId = intent.getStringExtra(ActivityConstants.ARGS_PARTY_ID);
			userId = intent
					.getStringExtra(ActivityConstants.ARGS_ACCOUNT_USER_ID);
			nickname = intent
					.getStringExtra(ActivityConstants.ARGS_ACCOUNT_NICKNAME);
			startTime = intent
					.getStringExtra(ActivityConstants.ARGS_PARTY_START_TIME);
			new Thread(new Runnable() {
				@Override
				public void run() {
					boolean flag = true;
					int uploadInterval = -1;
					while (flag) {
						// if party over, finish the thread
						if (DBParty.getStatus(getApplicationContext(), partyId) == StatusConstants.PARTY_STATUS_OVER) {
							flag = false;
							if (updateLocation != null) {
								updateLocation.stopUpdateLocation();
							}
							stopSelf();
							isServiceRunning = false;
							return;
						}
						// check whether start to share location
						if (isStartToShareLocation()) {
							updateFreq = 10000;
							if (updateLocation == null) {
								updateLocation = new UpdateLocation();
							}
							if (uploadInterval != updateLocation.getInterval()) {
								uploadInterval = updateLocation.getInterval();
								Map<String, String> map = new HashMap<String, String>();
								map.put("deviceGeneratedPartyId", partyId);
								map.put("userId", userId);
								map.put("interval",
										String.valueOf(uploadInterval));
								System.out.println("map = " + map);
								String response = HttpRequestUtils.post(
										"updateSelfPartying", map);
								try {
									JSONObject json = new JSONObject(response);
									String returnMessage = json
											.getString("returnMessage");
									if (returnMessage
											.equals(HttpConstants.UPDATE_PARTYING_SUCCESS)) {
									} else {
										System.out
												.println("updatePartying ---->> "
														+ returnMessage);
									}
								} catch (JSONException e) {
									System.out.println("updatePartying ---->> "
											+ e.getMessage());
								}
							}
						}
						try {
							Thread.sleep(updateFreq);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();
		}
		return START_REDELIVER_INTENT;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (updateLocation != null) {
			updateLocation.stopUpdateLocation();
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	private boolean isStartToShareLocation() {
		if (System.currentTimeMillis() >= Long.valueOf(startTime)) {
			return true;
		} else {
			return false;
		}
	}

	private class UpdateLocation implements
			GooglePlayServicesClient.ConnectionCallbacks,
			GooglePlayServicesClient.OnConnectionFailedListener,
			LocationListener {

		LocationRequest mLocationRequest;
		LocationClient mLocationClient;
		int interval;

		public UpdateLocation() {
			mLocationRequest = LocationRequest.create();
			// Set the update interval
			mLocationRequest.setInterval(5000);
			// Use high accuracy
			mLocationRequest
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			// Set the interval ceiling to one minute
			mLocationRequest.setFastestInterval(1000);

			mLocationClient = new LocationClient(getApplicationContext(), this,
					this);
			mLocationClient.connect();
			interval = DBPartying.getIterval(getApplicationContext(), partyId,
					userId);
		}

		@Override
		public void onConnectionFailed(ConnectionResult result) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onConnected(Bundle connectionHint) {
			System.out.println("----->>onConnected");
			mLocationClient.requestLocationUpdates(mLocationRequest, this);
			Location location = mLocationClient.getLastLocation();
			updateInterval(GoogleMapUtils.getInterval(latitude, longitude,
					location.getLatitude(), location.getLongitude()));

		}

		@Override
		public void onDisconnected() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLocationChanged(Location location) {
			updateInterval(GoogleMapUtils.getInterval(latitude, longitude,
					location.getLatitude(), location.getLongitude()));
		}

		public int getInterval() {
			return interval;
		}

		public void stopUpdateLocation() {
			System.out.println("----->>stopUpdateLocation");
			if (mLocationClient.isConnected()) {
				mLocationClient.removeLocationUpdates(this);
			}
			// After disconnect() is called, the client is considered "dead".
			mLocationClient.disconnect();
		}

		public void updateInterval(int temp) {
			System.out.println("interval = " + temp);
			if (interval != temp) {
				interval = temp;
				Partying partying = new Partying();
				partying.setPartyId(partyId);
				partying.setUserId(userId);
				partying.setNickName(nickname);
				partying.setInterval(temp);
				DBPartying.insert(getApplicationContext(), partying);
			}
		}
	}
}
