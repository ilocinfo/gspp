package com.helloc.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.helloc.R;
import com.helloc.activity.ReceivePartyActivity;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.GCMConstants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBFriend;
import com.helloc.database.DBInvitation;
import com.helloc.database.DBMessage;
import com.helloc.database.DBMessageReply;
import com.helloc.model.Friends;
import com.helloc.model.Invitation;
import com.helloc.model.Message;
import com.helloc.model.MessageReply;
import com.helloc.utils.GeofenceUtils;
import com.helloc.utils.NotificationUtils;
import com.helloc.utils.SharePrefsUtils;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {

	private static final String TAG = "GcmIntentService";

	public GcmIntentService() {
		super("GcmIntentService");
		Log.i(TAG, "Start GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle bundle = intent.getExtras();
		String request = bundle.getString(GCMConstants.GCM_COMMAND);

		// if receive nothing, return
		if (TextUtils.isEmpty(request)) {
			return;
		}
		Log.i(TAG, "Received message: " + request);

		// handle the GCM request
		if (request.equals(GCMConstants.GCM_COMMAND_RECEIVE_MESSAGE)) {
			handleReceiveMessage(bundle);
		} else if (request.equals(GCMConstants.GCM_COMMAND_DELETE_MESSAGE)) {
			handleDeleteMessage(bundle);
		} else if (request.equals(GCMConstants.GCM_COMMAND_RECEIVE_INVITATION)) {
			handleReceiveInvitation(bundle);
		} else if (request
				.equals(GCMConstants.GCM_COMMAND_RECEIVE_REPLY_INVITATION)) {
			handleReceiveReplyInvitation(bundle);
		} else if (request.equals(GCMConstants.GCM_COMMAND_RECEIVE_PARTY)) {
			handleReceiveParty(bundle);
		} else if (request
				.equals(GCMConstants.GCM_COMMAND_RECEIVE_AUTOREPLY_REQUEST)) {
			handleReceiveAutoreplyRequest(bundle);
		} else if (request
				.equals(GCMConstants.GCM_COMMAND_RECEIVE_AUTOREPLY_PERMISSION)) {
			handleReceiveAutoreplyPermission(bundle);
		} else if (request
				.equals(GCMConstants.GCM_COMMAND_REMOVE_AUTOREPLY_PERMISSION)) {
			handleRemoveAutoreplyPermission(bundle);
		} else if (request.equals(GCMConstants.GCM_COMMAND_DELETE_FRIEND)) {
			handleDeleteFriend(bundle);
		} else if (request
				.equals(GCMConstants.GCM_COMMAND_RECEIVE_AUTOREPLY_MESSAGE)) {
			handleReceiveAutoreplyMessage(bundle);
		}

		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void handleReceiveMessage(Bundle bundle) {
		String messageId = bundle.getString("deviceGeneratedMessageId");
		String senderUserId = bundle.getString("senderId");
		System.out.println("geofenceMsgId = " + messageId);
		System.out.println("senderUserId = " + senderUserId);

		Message message = new Message();
		message.setId(messageId);
		message.setLatitude(Double.parseDouble(bundle.getString("latitude")));
		message.setLongitude(Double.parseDouble(bundle.getString("longitude")));
		message.setRadius(Float.parseFloat(bundle.getString("radius")));
		message.setExpirationTime(Long.parseLong(bundle
				.getString("expirationTime")));
		message.setTransitionType(Geofence.GEOFENCE_TRANSITION_ENTER
				| Geofence.GEOFENCE_TRANSITION_EXIT);
		message.setStatus(StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED);
		message.setType(Integer.parseInt(bundle.getString("type")));
		message.setAddress(bundle.getString("address"));
		message.setPrivateMsg(bundle.getString("privateMessage"));
		message.setPublicMsg(bundle.getString(("publicMessage")));
		message.setSenderId(bundle.getString("senderId"));
		message.setReceiverId("");
		message.setImageName(bundle.getString("imageName"));
		message.setNeedFeedback(Boolean.parseBoolean(bundle
				.getString("needFeedBack")));

		if (message.getType() == Constants.MESSAGE_TYPE_TEXT
				&& TextUtils.isEmpty(message.getPrivateMsg())) {
			message.setPrivateMsg(getString(R.string.text_image));
		}
		DBMessage.insert(getApplicationContext(), message);
		GeofenceUtils.setUpGeofence(getApplicationContext(), message);
	}

	private void handleDeleteMessage(Bundle bundle) {
		String geofenceMsgId = bundle
				.getString(ActivityConstants.ARGS_MESSAGE_ID);

		// delete message from database if the geofence is not activated
		int result = DBMessage.deleteByIdAndStatus(getApplicationContext(),
				geofenceMsgId,
				StatusConstants.MESSAGE_STATUS_RECEIVED_NOT_ACTIVED);
		if (result > 0) {
			DBMessage.deleteById(getApplicationContext(), geofenceMsgId);
			// unregister the geofence
			GeofenceUtils
					.removeGeofence(getApplicationContext(), geofenceMsgId);
		}
	}

	private void handleReceiveInvitation(Bundle bundle) {
		// save the received invitation to database
		Invitation invitation = new Invitation();

		invitation.setUserId(bundle.getString("senderId"));
		invitation.setUuid(bundle.getString("universalUniqueId"));
		invitation
				.setStatus(StatusConstants.INVITATION_STATUS_RECEIVED_NOT_HANDLE);
		invitation.setNickName(bundle.getString("nickname"));
		invitation.setPhone(bundle.getString("phone"));
		invitation.setInvitationMsg(bundle.getString("invitationMessage"));
		DBInvitation.insert(getApplicationContext(), invitation);

		// save a new invitation notice to sharepreference
		SharePrefsUtils.setNewFriendsNotice(this, true);
		SharePrefsUtils.setNewInvitation(this, true);

		// send a broadcast to update notice
		Intent broadcast = new Intent();
		broadcast.setAction("updateNotice");
		getApplication().sendBroadcast(broadcast);
	}

	private void handleReceiveReplyInvitation(Bundle bundle) {
		if (bundle.getString("decision").equals("accept")) {
			// get the sent invitation
			Invitation invitation = DBInvitation.getSentByUserId(
					getApplicationContext(), bundle.getString("senderId"));

			// update the invitation
			DBInvitation.updateToAccepted(getApplicationContext(),
					bundle.getString("senderId"));

			// save the invitation to friend
			Friends friend = new Friends(invitation.getUserId(),
					invitation.getUuid(), invitation.getNickName(),
					invitation.getPhone());

			DBFriend.insert(getApplicationContext(), friend);

			// save a new invitation notice to sharepreference
			SharePrefsUtils.setNewFriendsNotice(getApplicationContext(), true);

			// send a broadcast to update notice
			Intent broadcast = new Intent();
			broadcast.setAction("updateNotice");
			getApplication().sendBroadcast(broadcast);
		} else {
			// delete the sent invitation
			DBInvitation.deleteInvitation(getApplicationContext(),
					bundle.getString("userId"),
					StatusConstants.INVITATION_STATUS_RECEIVED_NOT_HANDLE);
		}

	}

	private void handleReceiveParty(Bundle bundle) {
		String senderId = bundle.getString("senderId");
		String partyId = bundle.getString("deviceGeneratedPartyId");
		String theme = bundle.getString("theme");
		// send a notification
		Intent notificationIntent = new Intent(getApplicationContext(),
				ReceivePartyActivity.class);
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_ID, partyId);
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_THEME, theme);
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_DESCRIPTION,
				bundle.getString("description"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_START_DATE,
				bundle.getString("startDate"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_START_TIME,
				bundle.getString("startTime"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_END_DATE,
				bundle.getString("endDate"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_END_TIME,
				bundle.getString("endTime"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_ADDRESS,
				bundle.getString("location"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_LATITUDE,
				bundle.getString("latitude"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_LONGITUDE,
				bundle.getString("longitude"));
		notificationIntent.putExtra(ActivityConstants.ARGS_PARTY_ORIGINATOR,
				bundle.getString("originator"));
		String title = DBFriend.getNameByUserId(getApplicationContext(),
				senderId) + getString(R.string.text_originate_party);
		String text = getString(R.string.text_theme) + ": " + theme;
		NotificationUtils.sendNotification(notificationIntent,
				getApplicationContext(), ReceivePartyActivity.class, title,
				text, false);
	}

	private void handleReceiveAutoreplyRequest(Bundle bundle) {
		String senderId = bundle.getString("senderId");
		DBFriend.uploadAllowAutoReply(this, senderId,
				StatusConstants.AUTO_REPLY_STATUS_PENDING);
		// send a broadcast to update notice
		Intent broadcast = new Intent();
		broadcast.setAction("updateNotice");
		broadcast.setAction("updateView");
		getApplication().sendBroadcast(broadcast);
	}

	private void handleReceiveAutoreplyPermission(Bundle bundle) {
		String senderId = bundle.getString("senderId");
		DBFriend.uploadAutoReplyPermission(this, senderId,
				StatusConstants.AUTO_REPLY_STATUS_ALLOWED);
		Intent broadcast = new Intent();
		broadcast.setAction("updateView");
		getApplication().sendBroadcast(broadcast);
	}

	private void handleRemoveAutoreplyPermission(Bundle bundle) {
		String senderId = bundle.getString("senderId");
		DBFriend.uploadAutoReplyPermission(this, senderId,
				StatusConstants.AUTO_REPLY_STATUS_NOT_ALLOWED);
		Intent broadcast = new Intent();
		broadcast.setAction("updateView");
		getApplication().sendBroadcast(broadcast);
	}

	private void handleDeleteFriend(Bundle bundle) {
		String senderId = bundle.getString("senderId");
		DBFriend.deleteFriend(this, senderId);
		// send a broadcast to update notice
		Intent broadcast = new Intent();
		broadcast.setAction("updateNotice");
		getApplication().sendBroadcast(broadcast);
	}

	private void handleReceiveAutoreplyMessage(Bundle bundle) {
		String senderId = bundle.getString("senderId");
		String messageId = bundle.getString("deviceGeneratedMessageId");
		MessageReply messageReply = new MessageReply();
		messageReply.setOrder(0);
		messageReply.setMessageId(messageId);
		messageReply.setUserId(senderId);
		messageReply.setUserName("");
		messageReply
				.setReplyContent(getString(R.string.text_received_auto_reply));
		messageReply.setDate(String.valueOf(System.currentTimeMillis()));
		messageReply.setMyReply(false);
		DBMessageReply.insert(getApplicationContext(), messageReply);
	}

}
