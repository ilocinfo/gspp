package com.helloc.service;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessage;
import com.helloc.database.DBParty;
import com.helloc.model.Message;
import com.helloc.model.Party;
import com.helloc.utils.GeofenceUtils;

public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		System.out.println("BootReceiver-->>");
		// start a service to listen a unique port
		Intent mServiceIntent = new Intent(context, ListenPortService.class);
		context.startService(mServiceIntent);

		// recovery the message geofence
		ArrayList<Message> messageList = DBMessage.getAllNotActivated(context);
		if (messageList.size() > 0) {
			GeofenceUtils.setUpGeofences(context, messageList);
		}

		// recovery the party geofence
		ArrayList<Party> list = DBParty.getAllByStatus(context,
				StatusConstants.PARTY_STATUS_ONGOING);
		long currentTime = System.currentTimeMillis();
		for (int i = 0; i < list.size(); i++) {
			Party party = list.get(i);
			long endTime = Long.parseLong(party.getEndTime());
			if (endTime < currentTime) {
				DBParty.updateStatus(context, party.getPartyId());
			} else {
				GeofenceUtils.setUpIntervalGeofence(context,
						party.getPartyId(),
						Double.parseDouble(party.getLatitude()),
						Double.parseDouble(party.getLongitude()));
			}
		}
	}

}
