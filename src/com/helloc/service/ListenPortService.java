package com.helloc.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import com.helloc.activity.ReceiveMessageActivity;
import com.helloc.activity.ReceivePartyActivity;
import com.helloc.activity.ShowMessageActivity;
import com.helloc.constant.ActivityConstants;
import com.helloc.constant.Constants;
import com.helloc.constant.StatusConstants;
import com.helloc.database.DBMessage;
import com.helloc.utils.SharePrefsUtils;

public class ListenPortService extends Service {
	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;

	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			try {
				ServerSocket mServer = new ServerSocket(2013);
				while (true) {
					Socket socket = mServer.accept();
					InputStream socketIn = socket.getInputStream();
					BufferedReader br = new BufferedReader(
							new InputStreamReader(socketIn, "UTF-8"));
					String firstLine = br.readLine();
					br.close();
					if (firstLine == null) {
						return;
					}
					firstLine = URLDecoder.decode(firstLine, "UTF-8");
					if (firstLine != null) {
						firstLine = firstLine.substring(4);
						System.out.println(firstLine);
						int index = firstLine.indexOf("?");
						if (index == -1) {
							continue;
						}
						// if the link is invoke our app
						if (firstLine.substring(1, index).equals(
								Constants.INVOKE_APP)) {
							String[] params = firstLine.substring(index + 1)
									.split("&");
							// for (int i = 0; i < params.length; i++) {
							// System.out.println(params[i]);
							// }

							Map<String, String> map = getParams(params);
							System.out.println("map = " + map);
							String senderId = map.get("senderId");
							String messageId = map.get("id");
							if (map.get("action").equals("receiveMessage")) {
								Intent intent = new Intent();
								if (senderId.equals(SharePrefsUtils
										.getUserId(getApplicationContext()))) {
									intent.setClass(getApplicationContext(),
											ShowMessageActivity.class);
									intent.putExtra(
											ActivityConstants.ARGS_MESSAGE_STATUS,
											StatusConstants.MESSAGE_STATUS_SENT);
								} else if (DBMessage
										.getByIdAndStatus(
												getApplicationContext(),
												messageId,
												StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED) != null) {
									intent.setClass(getApplicationContext(),
											ShowMessageActivity.class);
									intent.putExtra(
											ActivityConstants.ARGS_MESSAGE_STATUS,
											StatusConstants.MESSAGE_STATUS_RECEIVED_ACTIVIED);
								} else {
									intent.setClass(getApplicationContext(),
											ReceiveMessageActivity.class);
								}
								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_ID,
										messageId);
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_LATITUDE,
										Double.parseDouble(map.get("latitude")));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_LONGITUDE,
										Double.parseDouble(map.get("longitude")));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_RADIUS,
										Float.parseFloat(map.get("radius")));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_EXPIRATION_TIME,
										Long.parseLong(map
												.get("expirationTime")));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_TYPE,
										Integer.parseInt(map.get("type")));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_ADDRESS,
										map.get("address"));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_PRIVATE_MSG,
										map.get("privateMsg"));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_PUBLIC_MSG,
										map.get("publicMsg"));
								intent.putExtra(
										ActivityConstants.ARGS_ACCOUNT_USER_ID,
										senderId);
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_IMAGE_NAME,
										map.get("imageName"));
								intent.putExtra(
										ActivityConstants.ARGS_MESSAGE_NEED_FEEDBACK,
										Boolean.parseBoolean(map
												.get("needFeedback")));
								startActivity(intent);
							} else if (map.get("action").equals("receiveParty")) {
								Intent intent = new Intent();
								intent.setClass(getApplicationContext(),
										ReceivePartyActivity.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_ID,
										map.get("id"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_THEME,
										map.get("theme"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_DESCRIPTION,
										map.get("description"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_START_DATE,
										map.get("startDate"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_START_TIME,
										map.get("startTime"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_END_DATE,
										map.get("endDate"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_END_TIME,
										map.get("endTime"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_ADDRESS,
										map.get("address"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_LATITUDE,
										map.get("latitude"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_LONGITUDE,
										map.get("longitude"));
								intent.putExtra(
										ActivityConstants.ARGS_PARTY_ORIGINATOR,
										map.get("originator"));
								startActivity(intent);
							}

						}
					}

				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}

			// Stop the service using the startId, so that we don't stop
			// the service in the middle of handling another job
			// stopSelf(msg.arg1);
		}
	}

	@Override
	public void onCreate() {
		// Start up the thread running the service. Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block. We also make it
		// background priority so CPU-intensive work will not disrupt our UI.
		HandlerThread thread = new HandlerThread("ServiceStartArguments",
				android.os.Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		// Get the HandlerThread's Looper and use it for our Handler
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// .makeText(this, "service starting", Toast.LENGTH_SHORT).show();

		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the
		// job
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);

		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// We don't provide binding, so return null
		return null;
	}

	@Override
	public void onDestroy() {
		// Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
	}

	private Map<String, String> getParams(String[] params) {
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < params.length; i++) {
			int spliterIndex = params[i].indexOf('=');
			if (spliterIndex >= params[i].length() - 1) {
				map.put(params[i].substring(0, spliterIndex), "");
			} else {
				map.put(params[i].substring(0, spliterIndex),
						params[i].substring(spliterIndex + 1,
								params[i].length()));
			}
		}
		return map;
	}
}
